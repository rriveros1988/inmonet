<?php
	 // ini_set('display_errors', 'On');
	require('conexion.php');
	//select
	function eliminaGarantia($idPromesa){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM GARANTIA
WHERE IDPROMESA = '" . $idPromesa . "'";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function ingresaGarantia($idPromesa,$montoUf,$montoPesos,$formaPago,$banco,$serie,$nro,$fechaPago,$accion,$actor){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				if($fechaPago == ''){
					$sql = "INSERT INTO GARANTIA
					(IDPROMESA, MONTOUF, MONTOPESOS, FORMAPAGO, BANCO, SERIE, NRO, FECHAPAGO, ACCION, ACTOR)
					VALUES(" . $idPromesa . "," . $montoUf . ", " . $montoPesos . ", " . $formaPago . ", '" . $banco . "', '" . $serie . "', '" . $nro . "', NULL,'". $accion ."','". $actor ."')";
				}
				else{
					$sql = "INSERT INTO GARANTIA
					(IDPROMESA, MONTOUF, MONTOPESOS, FORMAPAGO, BANCO, SERIE, NRO, FECHAPAGO, ACCION, ACTOR)
					VALUES(" . $idPromesa . "," . $montoUf . ", " . $montoPesos . ", " . $formaPago . ", '" . $banco . "', '" . $serie . "', '" . $nro . "', '" . $fechaPago ."','". $accion ."','". $actor ."')";
				}
				if ($con->query($sql)) {
					$con->query("COMMIT");
				    return "Ok";
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}

		function datosGarantiasMC($codigoProyecto, $numeroOperacion){
			$con = conectar();
			if($con != 'No conectado'){
			$sql = "SELECT
A.MONTOUF 'ARRUF', A.MONTOPESOS 'ARRPESOS', R.NOMBRE 'ARRTIPO', A.BANCO 'ARRBANCO', A.SERIE 'ARRSERIE', A.NRO 'ARRNRO', A.FECHAPAGO 'ARRFECHA',
VD.MONTOUF 'VDUF', VD.MONTOPESOS 'VDPESOS', RVD.NOMBRE 'VDTIPO', VD.BANCO 'VDBANCO', VD.SERIE 'VDSERIE', VD.NRO 'VDNRO', VD.FECHAPAGO 'VDFECHA',
VC.MONTOUF 'VCUF', VC.MONTOPESOS 'VCPESOS', RVC.NOMBRE 'VCTIPO', VC.BANCO 'VCBANCO', VC.SERIE 'VCSERIE', VC.NRO 'VCNRO', VC.FECHAPAGO 'VCFECHA'
FROM PROMESA P
LEFT JOIN GARANTIA A
ON P.IDPROMESA = A.IDPROMESA
AND A.ACCION = 'Arriendo'
AND A.ACTOR = 'Cliente'
LEFT JOIN RESERVAFORMA R
ON A.FORMAPAGO = R.IDRESERVAFORMA
LEFT JOIN GARANTIA VD
ON P.IDPROMESA = VD.IDPROMESA
AND VD.ACCION = 'Venta'
AND VD.ACTOR = 'Dueno'
LEFT JOIN RESERVAFORMA RVD
ON VD.FORMAPAGO = RVD.IDRESERVAFORMA
LEFT JOIN GARANTIA VC
ON P.IDPROMESA = VC.IDPROMESA
AND VC.ACCION = 'Venta'
AND VC.ACTOR = 'Cliente'
LEFT JOIN RESERVAFORMA RVC
ON VC.FORMAPAGO = RVC.IDRESERVAFORMA
WHERE IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
AND NUMERO = '" . $numeroOperacion . "'";
				if ($row = $con->query($sql)) {
					$return = array();
					while($array = $row->fetch_array(MYSQLI_BOTH)){
						$return[] = $array;
					}

					return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

	function datosAccionPromesa($codigoProyecto, $numeroOperacion){
		$con = conectar();
		if($con != 'No conectado'){
		$sql = "SELECT A.NOMBRE
FROM RESERVA R LEFT JOIN UNIDAD U
ON R.IDUNIDAD = U.IDUNIDAD
LEFT JOIN ACCION A
ON U.IDACCION = A.IDACCION
WHERE R.IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
AND R.NUMERO = '" . $numeroOperacion . "'";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosUnidadPromesaParaEscritura($idPromesa){
		$con = conectar();
		if($con != 'No conectado'){
		$sql = "SELECT *
		FROM UNIDAD
		WHERE IDUNIDAD =
		(
			SELECT IDUNIDAD
			FROM PROMESA
			WHERE IDPROMESA = '" . $idPromesa . "'
		)";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosUnidadEscrituraId($idEscritura){
		$con = conectar();
		if($con != 'No conectado'){
		$sql = "SELECT *
		FROM UNIDAD
		WHERE IDUNIDAD =
		(
			SELECT IDUNIDAD
			FROM ESCRITURA
			WHERE IDESCRITURA = '" . $idEscritura . "'
		)";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosTipoAccionProyecto($codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT DISTINCT A.NOMBRE 'ACCION', A.IDACCION
			FROM UNIDAD U LEFT JOIN ACCION A
			ON U.IDACCION = A.IDACCION
			WHERE IDPROYECTO =
			(
			SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosInforme1Escrituras($accion, $mes, $ano, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL INF6(" . $accion . "," . $mes . "," . $ano . ",'" . $codigoProyecto . "')";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosInforme1EscriturasDetalle($accion, $mes, $ano, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT R.NUMERO, R.FECHAESCRITURA, U.CODIGO 'UNIDAD',
GROUP_CONCAT(DISTINCT URE.CODIGO) 'ESTACIONAMIENTO',
GROUP_CONCAT(DISTINCT URB.CODIGO) 'BODEGA',
R.VALORTOTALUF,
(R.VALORRESERVAUF + R.VALORPIEPROMESAUF + R.VALORPIESALDOUF) 'RESPIE',
R.VALORSALDOTOTALUF,
propio(SUBSTRING_INDEX(C.APELLIDOS,' ',1)) 'APELLIDOS', propio(SUBSTRING_INDEX(C.NOMBRES,' ',1)) 'APELLIDOS', C.CELULAR, C.EMAIL
FROM ESCRITURA R LEFT JOIN UNIDAD U
ON R.IDUNIDAD = U.IDUNIDAD
LEFT JOIN TIPOUNIDAD T
ON U.IDTIPOUNIDAD = T.IDTIPOUNIDAD
LEFT JOIN ESCRITURAESTACIONAMIENTO RE
ON R.IDESCRITURA = RE.IDESCRITURA
LEFT JOIN UNIDAD URE
ON RE.IDUNIDAD = URE.IDUNIDAD
LEFT JOIN ESCRITURABODEGA RB
ON R.IDESCRITURA = RB.IDESCRITURA
LEFT JOIN UNIDAD URB
ON RB.IDUNIDAD = URB.IDUNIDAD
LEFT JOIN CLIENTE C
ON R.IDCLIENTE1 = C.IDCLIENTE
WHERE FECHAESCRITURA > DATE_SUB(STR_TO_DATE(CONCAT('01-','" . $mes . "','-','" . $ano . "'),'%d-%m-%Y'), INTERVAL 11 MONTH)
AND FECHAESCRITURA < DATE_ADD(STR_TO_DATE(CONCAT('01-','" . $mes . "','-','" . $ano . "'),'%d-%m-%Y'), INTERVAL 1 MONTH)
AND R.IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
AND R.IDUNIDAD IN (SELECT IDUNIDAD FROM UNIDAD WHERE IDACCION = '" . $accion . "')
AND T.NOMBRE NOT IN
(
'Bodega',
'Estacionamiento'
)
GROUP BY R.NUMERO, R.FECHAESCRITURA, U.CODIGO, R.VALORTOTALUF,
R.VALORRESERVAUF,R.VALORPIEPROMESAUF,R.VALORPIESALDOUF,
R.VALORSALDOTOTALUF,C.APELLIDOS, C.NOMBRES, C.CELULAR, C.EMAIL
ORDER BY R.FECHAESCRITURA ASC";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosInforme1Promesas($accion, $mes, $ano, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL INF5(" . $accion . "," . $mes . "," . $ano . ",'" . $codigoProyecto . "')";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosInforme1PromesasDetalle($accion, $mes, $ano, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT R.NUMERO, R.FECHAPROMESA, U.CODIGO 'UNIDAD',
GROUP_CONCAT(DISTINCT URE.CODIGO) 'ESTACIONAMIENTO',
GROUP_CONCAT(DISTINCT URB.CODIGO) 'BODEGA',
R.VALORTOTALUF,
(R.VALORRESERVAUF + R.VALORPIEPROMESAUF + R.VALORPIESALDOUF) 'RESPIE',
R.VALORSALDOTOTALUF,
propio(SUBSTRING_INDEX(C.APELLIDOS,' ',1)) 'APELLIDOS', propio(SUBSTRING_INDEX(C.NOMBRES,' ',1)) 'APELLIDOS', C.CELULAR, C.EMAIL
FROM PROMESA R LEFT JOIN UNIDAD U
ON R.IDUNIDAD = U.IDUNIDAD
LEFT JOIN TIPOUNIDAD T
ON U.IDTIPOUNIDAD = T.IDTIPOUNIDAD
LEFT JOIN PROMESAESTACIONAMIENTO RE
ON R.IDPROMESA = RE.IDPROMESA
LEFT JOIN UNIDAD URE
ON RE.IDUNIDAD = URE.IDUNIDAD
LEFT JOIN PROMESABODEGA RB
ON R.IDPROMESA = RB.IDPROMESA
LEFT JOIN UNIDAD URB
ON RB.IDUNIDAD = URB.IDUNIDAD
LEFT JOIN CLIENTE C
ON R.IDCLIENTE1 = C.IDCLIENTE
WHERE FECHAPROMESA > DATE_SUB(STR_TO_DATE(CONCAT('01-','" . $mes . "','-','" . $ano . "'),'%d-%m-%Y'), INTERVAL 11 MONTH)
AND FECHAPROMESA < DATE_ADD(STR_TO_DATE(CONCAT('01-','" . $mes . "','-','" . $ano . "'),'%d-%m-%Y'), INTERVAL 1 MONTH)
AND R.IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
AND R.IDUNIDAD IN (SELECT IDUNIDAD FROM UNIDAD WHERE IDACCION = '" . $accion . "')
AND T.NOMBRE NOT IN
(
'Bodega',
'Estacionamiento'
)
GROUP BY R.NUMERO, R.FECHAPROMESA, U.CODIGO, R.VALORTOTALUF, R.VALORRESERVAUF, R.VALORPIEPROMESAUF, R.VALORPIESALDOUF,
R.VALORSALDOTOTALUF, C.APELLIDOS, C.NOMBRES, C.CELULAR, C.EMAIL
ORDER BY R.FECHAPROMESA ASC
";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosInforme1Reservas($accion, $mes, $ano, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL INF4(" . $accion . "," . $mes . "," . $ano . ",'" . $codigoProyecto . "')";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosInforme1ReservasDetalle($accion, $mes, $ano, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT R.NUMERO, R.FECHARESERVA, U.CODIGO 'UNIDAD',
GROUP_CONCAT(DISTINCT URE.CODIGO) 'ESTACIONAMIENTO',
GROUP_CONCAT(DISTINCT URB.CODIGO) 'BODEGA',
R.VALORTOTALUF,
propio(SUBSTRING_INDEX(C.APELLIDOS,' ',1)) 'APELLIDOS', propio(SUBSTRING_INDEX(C.NOMBRES,' ',1)) 'APELLIDOS', C.CELULAR, C.EMAIL
FROM RESERVA R LEFT JOIN UNIDAD U
ON R.IDUNIDAD = U.IDUNIDAD
LEFT JOIN TIPOUNIDAD T
ON U.IDTIPOUNIDAD = T.IDTIPOUNIDAD
LEFT JOIN RESERVAESTACIONAMIENTO RE
ON R.IDRESERVA = RE.IDRESERVA
LEFT JOIN UNIDAD URE
ON RE.IDUNIDAD = URE.IDUNIDAD
LEFT JOIN RESERVABODEGA RB
ON R.IDRESERVA = RB.IDRESERVA
LEFT JOIN UNIDAD URB
ON RB.IDUNIDAD = URB.IDUNIDAD
LEFT JOIN CLIENTE C
ON R.IDCLIENTE1 = C.IDCLIENTE
WHERE FECHARESERVA > DATE_SUB(STR_TO_DATE(CONCAT('01-','" . $mes . "','-','" . $ano . "'),'%d-%m-%Y'), INTERVAL 11 MONTH)
AND FECHARESERVA < DATE_ADD(STR_TO_DATE(CONCAT('01-','" . $mes . "','-','" . $ano . "'),'%d-%m-%Y'), INTERVAL 1 MONTH)
AND R.IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
AND R.IDUNIDAD IN (SELECT IDUNIDAD FROM UNIDAD WHERE IDACCION = '" . $accion . "')
AND T.NOMBRE NOT IN
(
'Bodega',
'Estacionamiento'
)
GROUP BY R.NUMERO, R.FECHARESERVA, U.CODIGO, R.VALORTOTALUF,C.APELLIDOS, C.NOMBRES, C.CELULAR, C.EMAIL
ORDER BY R.FECHARESERVA ASC";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosCantidadPorVendedor($accion, $mes, $ano, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT propio(CONCAT(U.NOMBRES, ' ', U.APELLIDOS)) 'NOMBRE', COUNT(*) 'CANTIDAD'
FROM COTIZACION C LEFT JOIN USUARIO U
ON C.IDUSUARIO = U.IDUSUARIO
WHERE C.FECHA > DATE_SUB(STR_TO_DATE(CONCAT('01-','" . $mes . "','-','" . $ano . "'),'%d-%m-%Y'), INTERVAL 11 MONTH)
AND C.FECHA < DATE_ADD(STR_TO_DATE(CONCAT('01-','" . $mes . "','-','" . $ano . "'),'%d-%m-%Y'), INTERVAL 1 MONTH)
AND C.IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
AND C.IDUNIDAD IN (SELECT IDUNIDAD FROM UNIDAD WHERE IDACCION = '" . $accion . "')
GROUP BY U.NOMBRES, U.APELLIDOS";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosInforme1NivelInteres($accion, $ano, $mes, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT CASE WHEN ESTADO = 3 THEN 1 ELSE ESTADONIVELINTERES END 'ESTADONIVELINTERES', COUNT(*) 'CANTIDAD'
FROM COTIZACION
WHERE MONTH(FECHA) = " . $mes . "
AND YEAR(FECHA) = " . $ano . "
AND IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
AND IDUNIDAD IN (SELECT IDUNIDAD FROM UNIDAD WHERE IDACCION = '" . $accion . "')
GROUP BY CASE WHEN ESTADO = 3 THEN 1 ELSE ESTADONIVELINTERES END
ORDER BY ESTADONIVELINTERES DESC	";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosInforme3($accion, $mes, $ano, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL INF3(" . $accion . "," . $mes . "," . $ano . ",'" . $codigoProyecto . "')";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosInforme2($accion, $mes, $ano, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL INF2(" . $accion . "," . $mes . "," . $ano . ",'" . $codigoProyecto . "')";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosInforme1($accion, $mes, $ano, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL INF1(" . $accion . "," . $mes . "," . $ano . ",'" . $codigoProyecto . "')";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function valorUnidad($idUnidad){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT VALORUF
FROM UNIDAD
WHERE  IDUNIDAD = " . $idUnidad;
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function valorUnidadReservaEstacionamiento($idUnidad,$idReserva){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT VALORUF
FROM RESERVAESTACIONAMIENTO
WHERE  IDUNIDAD = " . $idUnidad . "
AND IDRESERVA = " . $idReserva;
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosReservaEspecifica($codigoProyecto,$numeroOperacion){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT R.*, C.FECHA 'FECHACOTIZACION', C.CODIGO 'NUMEROCOTIZACION', CL.NOMBRES,
CL.APELLIDOS, CL.RUT, CL.CELULAR, CL.EMAIL, P.DIASRES,
U.CODIGO 'DEPTO', T.NOMBRE 'TIPOLOGIA', TI.NOMBRE 'MODELO', U.ORIENTACION,
(
	SELECT COUNT(IDUNIDAD)
	FROM RESERVABODEGA
	WHERE IDRESERVA = R.IDRESERVA
) BOD,
(
	SELECT COUNT(IDUNIDAD)
	FROM RESERVAESTACIONAMIENTO
	WHERE IDRESERVA = R.IDRESERVA
) EST,
U.M2UTIL, U.M2TERRAZA, U.M2TOTAL,
CASE WHEN C.VALORUNIDADUF IS NOT NULL THEN C.VALORUNIDADUF ELSE U.VALORUF END 'DEPTOUF',
(
	SELECT SUM(VALORUF)
	FROM RESERVABODEGA
	WHERE IDRESERVA = R.IDRESERVA
) BODUF,
(
	SELECT SUM(VALORUF)
	FROM RESERVAESTACIONAMIENTO
	WHERE IDRESERVA = R.IDRESERVA
) ESTUF,
R.BONOVENTAUF, R.DESCUENTO1, R.DESCUENTO2, R.VALORTOTALUF, P.NOMBRE 'NOMBREPROYECTO'
FROM RESERVA R
LEFT JOIN COTIZACION C
ON R.IDCOTIZACION = C.IDCOTIZACION
LEFT JOIN CLIENTE CL
ON R.IDCLIENTE1 = CL.IDCLIENTE
LEFT JOIN PROYECTO P
ON R.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON R.IDUNIDAD = U.IDUNIDAD
LEFT JOIN TIPOLOGIA T
ON U.TIPOLOGIA = T.IDTIPOLOGIA
LEFT JOIN TIPOUNIDAD TI
ON U.IDTIPOUNIDAD = TI.IDTIPOUNIDAD
WHERE R.IDPROYECTO =
(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND R.NUMERO = '" . $numeroOperacion . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function valorUnidadReservaBodega($idUnidad,$idReserva){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT VALORUF
FROM RESERVABODEGA
WHERE  IDUNIDAD = " . $idUnidad . "
AND IDRESERVA = " . $idReserva;
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function numeroCotizacion($idCotizacion){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT CODIGO
FROM COTIZACION
WHERE IDCOTIZACION = '" . $idCotizacion . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function numeroOperacion($idReserva){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NUMERO
FROM RESERVA
WHERE IDRESERVA = '" . $idReserva . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function chequeaCotizaUni($codigoProyecto,$tipoUnidad,$numeroUnidad,$accionUnidad){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT COUNT(*) CANTIDAD
FROM COTIZACION
WHERE IDUNIDAD =
(
SELECT IDUNIDAD
FROM UNIDAD
WHERE IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND IDTIPOUNIDAD =
(
SELECT IDTIPOUNIDAD
FROM TIPOUNIDAD
WHERE NOMBRE  = '" . $tipoUnidad . "'
)
AND IDACCION =
(
SELECT IDACCION
FROM ACCION
WHERE NOMBRE = '" . $accionUnidad . "'
)
AND CODIGO = '" . $numeroUnidad . "'
)
";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function chequeaCotizaCli($rutCliente){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT COUNT(*) CANTIDAD
FROM COTIZACION
WHERE IDCLIENTE =
(
SELECT IDCLIENTE
FROM CLIENTE
WHERE RUT = '" . $rutCliente . "'
)";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function chequeaCotizaUsu($rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT COUNT(*) CANTIDAD
FROM COTIZACION
WHERE IDUSUARIO =
(
SELECT IDUSUARIO
FROM USUARIO
WHERE RUT = '" . $rutUsuario . "'
)";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}


	function valorUnidadVenta($codigoProyecto, $codigoUnidad){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT VALORUF
FROM UNIDAD
WHERE CODIGO = '" . $codigoUnidad . "'
AND IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND IDACCION = 1";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosUnidadCorretaje($codigoProyecto, $codigoUnidad, $accionUnidad){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT DIRECCION, COMUNA, VALORUF, DESCRIPCION1, DESCRIPCION2, COMISIONLVCLIENTE, COMISIONVENCLIENTE, IDUNIDAD
FROM UNIDAD
WHERE CODIGO = '" . $codigoUnidad . "'
AND IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND IDACCION =
(
SELECT IDACCION
FROM ACCION
WHERE NOMBRE = '" . $accionUnidad . "'
)";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosUnidad($codigoProyecto, $codigoUnidad, $accionUnidad){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT DIRECCION, COMUNA, VALORUF, DESCRIPCION1, DESCRIPCION2, COMISIONLVCLIENTE, COMISIONVENCLIENTE, IDUNIDAD, IDPROYECTO
FROM UNIDAD
WHERE CODIGO = '" . $codigoUnidad . "'
AND IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND IDACCION =
(
SELECT IDACCION
FROM ACCION
WHERE NOMBRE = '" . $accionUnidad . "'
)";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function domicilioCliente($rutCliente){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT DOMICILIO, COMUNA
FROM CLIENTE
WHERE RUT = '" . $rutCliente . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function chequeaProyecto($codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function chequeaCliente($rutCliente){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDCLIENTE
FROM CLIENTE
WHERE RUT = '" . $rutCliente . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function chequeaUsuario($rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDUSUARIO
FROM USUARIO
WHERE RUT = '" . $rutUsuario . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function chequeaUsuarioID($idUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM USUARIO
WHERE IDUSUARIO = '" . $idUsuario . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function chequeaUnidad($codigoProyecto,$tipoUnidad,$numeroUnidad,$accionUnidad){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDUNIDAD
FROM UNIDAD
WHERE IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND IDTIPOUNIDAD = '" . $tipoUnidad . "'
AND IDACCION = '" . $accionUnidad . "'
AND CODIGO = '". $numeroUnidad . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function chequeaUnidadEli($codigoProyecto,$tipoUnidad,$numeroUnidad,$accionUnidad){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDUNIDAD
FROM UNIDAD
WHERE IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND IDTIPOUNIDAD =
(
SELECT IDTIPOUNIDAD
FROM TIPOUNIDAD
WHERE NOMBRE  = '" . $tipoUnidad . "'
)
AND IDACCION =
(
SELECT IDACCION
FROM ACCION
WHERE NOMBRE = '" . $accionUnidad . "'
)
AND CODIGO = '" . $numeroUnidad . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaDatosUnidadCoti($codigoProyecto,$tipoUnidad,$numeroUnidad,$accionUnidad){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT U.IDTIPOUNIDAD, U.IDACCION, U.CODIGO,
(
SELECT NOMBRE
FROM TIPOLOGIA
WHERE IDTIPOLOGIA = U.TIPOLOGIA
) 'TIPOLOGIA'
			, U.ESTADO, U.PISO, U.ORIENTACION, U.CANTDORMITORIOS, U.CANTBANOS, U.LOGIA, U.M2UTIL, U.M2TERRAZA, U.M2OTRO, U.M2TOTAL, U.VALORUF, U.IMGPLANO, U.IMGOTRA, U.COMISIONLVDUENO, U.COMISIONLVCLIENTE, U.COMISIONVENDUENO, U.COMISIONVENCLIENTE, U.DESCRIPCION1, U.DESCRIPCION2, U.DIRECCION, U.COMUNA, U.CODIGOTIPO, U.CANTBODEGAS, U.CANTESTACIONAMIENTOS
FROM UNIDAD U
WHERE IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND IDTIPOUNIDAD =
(
SELECT IDTIPOUNIDAD
FROM TIPOUNIDAD
WHERE NOMBRE = '" . $tipoUnidad . "'
)
AND IDACCION =
(
SELECT IDACCION
FROM ACCION
WHERE NOMBRE = '" . $accionUnidad . "'
)
AND CODIGO = '". $numeroUnidad . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_ASSOC);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaDatosUnidad($codigoProyecto,$tipoUnidad,$numeroUnidad,$accionUnidad){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDTIPOUNIDAD, IDACCION, CODIGO, TIPOLOGIA, ESTADO, PISO, ORIENTACION, CANTDORMITORIOS, CANTBANOS, LOGIA, M2UTIL, M2TERRAZA, M2OTRO, M2TOTAL, VALORUF, IMGPLANO, IMGOTRA, COMISIONLVDUENO, COMISIONLVCLIENTE, COMISIONVENDUENO, COMISIONVENCLIENTE, DESCRIPCION1, DESCRIPCION2, DIRECCION, COMUNA, CODIGOTIPO, CANTBODEGAS, CANTESTACIONAMIENTOS, RUTVENDEDOR, NOMBRESVENDEDOR, APELLIDOSVENDEDOR, CELULARVENDEDOR, EMAILVENDEDOR
FROM UNIDAD
WHERE IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND IDTIPOUNIDAD =
(
SELECT IDTIPOUNIDAD
FROM TIPOUNIDAD
WHERE NOMBRE = '" . $tipoUnidad . "'
)
AND IDACCION =
(
SELECT IDACCION
FROM ACCION
WHERE NOMBRE = '" . $accionUnidad . "'
)
AND CODIGO = '". $numeroUnidad . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_ASSOC);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaProyectosNoDisponiles($rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, P.NOMBRE, P.INMOBILIARIA,
(
SELECT COLOR
FROM ESTADOPROYECTO
WHERE IDESTADOPROYECTO = P.ESTADO
) 'ESTADO'
FROM PROYECTO P
WHERE P.ESTADO <> 5
AND P.IDPROYECTO IN
(
SELECT IDPROYECTO
FROM USUARIOPROYECTO
WHERE IDUSUARIO =
(
SELECT IDUSUARIO
FROM USUARIO
WHERE RUT = '" . $rutUsuario . "'
)
)
";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaProyectosNoDisponilesCli($rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, P.NOMBRE, P.INMOBILIARIA,
(
SELECT COLOR
FROM ESTADOPROYECTO
WHERE IDESTADOPROYECTO = P.ESTADO
) 'ESTADO'
FROM PROYECTO P
WHERE P.ESTADO <> 5
AND P.IDPROYECTO IN
(
SELECT IDPROYECTO
FROM COTIZACION
WHERE IDCLIENTE =
(
SELECT IDCLIENTE
FROM CLIENTE
WHERE RUT = '" .$rutUsuario . "'
)
)
";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaProyectosNoDisponilesAdmin(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, P.NOMBRE, P.INMOBILIARIA,
(
SELECT COLOR
FROM ESTADOPROYECTO
WHERE IDESTADOPROYECTO = P.ESTADO
) 'ESTADO'
FROM PROYECTO P
WHERE P.ESTADO <> 5";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaProyectosDisponiles($rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, P.NOMBRE, P.INMOBILIARIA,
(
SELECT COLOR
FROM ESTADOPROYECTO
WHERE IDESTADOPROYECTO = P.ESTADO
) 'ESTADO'
FROM PROYECTO P
WHERE P.ESTADO <> 5
AND P.IDPROYECTO NOT IN
(
SELECT IDPROYECTO
FROM USUARIOPROYECTO
WHERE IDUSUARIO =
(
SELECT IDUSUARIO
FROM USUARIO
WHERE RUT = '" . $rutUsuario . "'
)
)
";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaDatosCliente($rutCliente){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT RUT, NOMBRES, APELLIDOS, EMAIL, CELULAR, DOMICILIO, NUMERODOMICILIO, TIPODOMICILIO, COMUNA, CIUDAD, PAIS, FECHANAC, ESTADOCIVIL, NIVELEDUCACIONAL, CASAHABITA, MOTIVOCOMPRA, PROFESION, INSTITUCION, ACTIVIDAD, NACIONALIDAD, RESIDENCIA, SEXO, EM_RUT, EM_NOMBRE, EM_DOMICILIO, EM_COMUNA, EM_CIUDAD, EM_PAIS, EM_FONO, EM_GIRO, EM_FECHAINGRESO, EM_ANTIGUEDAD, EM_CARGO, EM_RENTALIQUIDA, OBSERVACION, ESTADO, FECHAALTA, REGION
FROM CLIENTE
WHERE RUT = '". $rutCliente . "'";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}		else{
			return "Error";
		}
	}

	function consultaDatosUsuario($rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT RUT, APELlIDOS, NOMBRES, EMAIL, FONO, IDPERFIL, ESTADO
FROM USUARIO
WHERE RUT = '". $rutUsuario . "'";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}		else{
			return "Error";
		}
	}

	function chequeaUnidadesProyecto($codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT COUNT(*) 'CANTIDAD'
FROM UNIDAD
WHERE IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaValorReserva($codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT VALORRESERVA
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaAccionCotiza($codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT DISTINCT A.NOMBRE
FROM UNIDAD U, ACCION A
WHERE IDPROYECTO =
(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND U.IDACCION = A.IDACCION
AND U.ESTADO = 1";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaDesBono($codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT DESCUENTOSALA1,DESCUENTOSALA2,BONOVENTA
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaNombreProyecto($codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE
FROM  PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function checkUsuario($rut, $pass){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRES, APELLIDOS, RUT, IDPERFIL, ESTADO
			FROM USUARIO
			WHERE RUT = '" . $rut . "' AND CONTRASENA = '" . $pass . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function checkUsuarioSinPass($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRES, APELLIDOS, RUT, IDPERFIL, ESTADO
			FROM USUARIO
			WHERE RUT = '" . $rut . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function checkCotizacionUsu($codigoProyecto, $codigo, $rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDCOTIZACION
FROM COTIZACION
WHERE IDUSUARIO =
(
SELECT IDUSUARIO
FROM USUARIO
WHERE RUT = '" . $rutUsuario . "'
)
AND IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND CODIGO = '" . $codigo . "'
";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaProyectos(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, P.NOMBRE, P.INMOBILIARIA, P.FECHAINICIOVENTA, P.FECHAFINOBRA, P.FECHAENTREGA,
(
SELECT COLOR
FROM ESTADOPROYECTO
WHERE IDESTADOPROYECTO = P.ESTADO
) 'ESTADO'
FROM PROYECTO P";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaProyectosUser($rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, P.NOMBRE, P.INMOBILIARIA, P.FECHAINICIOVENTA, P.FECHAFINOBRA, P.FECHAENTREGA,
(
SELECT COLOR
FROM ESTADOPROYECTO
WHERE IDESTADOPROYECTO = P.ESTADO
) 'ESTADO'
FROM PROYECTO P, USUARIOPROYECTO U
WHERE P.IDPROYECTO = U.IDPROYECTO
AND U.IDUSUARIO =
(
SELECT IDUSUARIO
FROM USUARIO
WHERE RUT = '" . $rutUsuario . "'
)
";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCotizacionesVigentesParaReserva(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, C.CODIGO, DATE_FORMAT(C.FECHA, '%d-%m-%Y') FECHA, U.CODIGO DEPTO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL,
C.CANTEST EST,
C.CANTBOD BOD,
A.NOMBRE FORMA,
C.VALORTOTALUF, CONCAT(S.NOMBRES,' ',S.APELLIDOS) VENDEDOR, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4,
EC.COLOR 'ESTADO',
C.RUTA_PDF
FROM COTIZACION C LEFT JOIN PROYECTO P
ON C.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON C.IDUNIDAD = U.IDUNIDAD
LEFT JOIN CLIENTE E
ON C.IDCLIENTE = E.IDCLIENTE
LEFT JOIN USUARIO S
ON C.IDUSUARIO = S.IDUSUARIO
LEFT JOIN ACCION A
ON U.IDACCION = A.IDACCION
LEFT JOIN ESTADOCOTIZACION EC
ON C.ESTADO = EC.IDESTADOCOTIZACION
WHERE C.ESTADO IN
(
	1,2
)
AND C.IDUNIDAD IN
(
	SELECT IDUNIDAD
	FROM UNIDAD
	WHERE ESTADO NOT IN
	(
		2,3,4,5,6,7
	)
)
GROUP BY C.IDCOTIZACION, P.CODIGOPROYECTO, C.CODIGO, C.FECHA, U.CODIGO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL, C.CANTEST, C.CANTBOD, A.NOMBRE, C.VALORTOTALUF, S.NOMBRES, S.APELLIDOS, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4, C.ESTADO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCotizacionesVigentes(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, C.CODIGO, DATE_FORMAT(C.FECHA, '%d-%m-%Y') FECHA, U.CODIGO DEPTO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL,
C.CANTEST EST,
C.CANTBOD BOD,
A.NOMBRE FORMA,
C.VALORTOTALUF, CONCAT(S.NOMBRES,' ',S.APELLIDOS) VENDEDOR, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4,
EC.COLOR 'ESTADO',
C.RUTA_PDF, EC.NOMBRE 'ESTADOTEXTO'
FROM COTIZACION C LEFT JOIN PROYECTO P
ON C.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON C.IDUNIDAD = U.IDUNIDAD
LEFT JOIN CLIENTE E
ON C.IDCLIENTE = E.IDCLIENTE
LEFT JOIN USUARIO S
ON C.IDUSUARIO = S.IDUSUARIO
LEFT JOIN ACCION A
ON U.IDACCION = A.IDACCION
LEFT JOIN ESTADOCOTIZACION EC
ON C.ESTADO = EC.IDESTADOCOTIZACION
WHERE C.ESTADO IN
(
	1,2
)
GROUP BY C.IDCOTIZACION, P.CODIGOPROYECTO, C.CODIGO, C.FECHA, U.CODIGO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL, C.CANTEST, C.CANTBOD, A.NOMBRE, C.VALORTOTALUF, S.NOMBRES, S.APELLIDOS, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4, C.ESTADO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCotizacionesVigentesProyecto($codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, C.CODIGO, DATE_FORMAT(C.FECHA, '%d-%m-%Y') FECHA, U.CODIGO DEPTO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL,
C.CANTEST EST,
C.CANTBOD BOD,
A.NOMBRE FORMA,
C.VALORTOTALUF, CONCAT(S.NOMBRES,' ',S.APELLIDOS) VENDEDOR, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4,
EC.COLOR 'ESTADO',
C.RUTA_PDF, EC.NOMBRE 'ESTADOTEXTO'
FROM COTIZACION C LEFT JOIN PROYECTO P
ON C.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON C.IDUNIDAD = U.IDUNIDAD
LEFT JOIN CLIENTE E
ON C.IDCLIENTE = E.IDCLIENTE
LEFT JOIN USUARIO S
ON C.IDUSUARIO = S.IDUSUARIO
LEFT JOIN ACCION A
ON U.IDACCION = A.IDACCION
LEFT JOIN ESTADOCOTIZACION EC
ON C.ESTADO = EC.IDESTADOCOTIZACION
WHERE C.ESTADO IN
(
	1,2
)
AND P.CODIGOPROYECTO = '" . $codigoProyecto . "'
GROUP BY C.IDCOTIZACION, P.CODIGOPROYECTO, C.CODIGO, C.FECHA, U.CODIGO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL, C.CANTEST, C.CANTBOD, A.NOMBRE, C.VALORTOTALUF, S.NOMBRES, S.APELLIDOS, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4, C.ESTADO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCotizaciones(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, C.CODIGO, DATE_FORMAT(C.FECHA, '%d-%m-%Y') FECHA, U.CODIGO DEPTO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL,
C.CANTEST EST,
C.CANTBOD BOD,
A.NOMBRE FORMA,
C.VALORTOTALUF, CONCAT(S.NOMBRES,' ',S.APELLIDOS) VENDEDOR, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4,
EC.COLOR 'ESTADO',
C.RUTA_PDF, EC.NOMBRE 'ESTADOTEXTO'
FROM COTIZACION C LEFT JOIN PROYECTO P
ON C.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON C.IDUNIDAD = U.IDUNIDAD
LEFT JOIN CLIENTE E
ON C.IDCLIENTE = E.IDCLIENTE
LEFT JOIN USUARIO S
ON C.IDUSUARIO = S.IDUSUARIO
LEFT JOIN ACCION A
ON U.IDACCION = A.IDACCION
LEFT JOIN ESTADOCOTIZACION EC
ON C.ESTADO = EC.IDESTADOCOTIZACION
GROUP BY C.IDCOTIZACION, P.CODIGOPROYECTO, C.CODIGO, C.FECHA, U.CODIGO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL, C.CANTEST, C.CANTBOD, A.NOMBRE, C.VALORTOTALUF, S.NOMBRES, S.APELLIDOS, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4, C.ESTADO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCotizacionesTodasProyecto($codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, C.CODIGO, DATE_FORMAT(C.FECHA, '%d-%m-%Y') FECHA, U.CODIGO DEPTO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL,
C.CANTEST EST,
C.CANTBOD BOD,
A.NOMBRE FORMA,
C.VALORTOTALUF, CONCAT(S.NOMBRES,' ',S.APELLIDOS) VENDEDOR, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4,
EC.COLOR 'ESTADO',
C.RUTA_PDF, EC.NOMBRE 'ESTADOTEXTO'
FROM COTIZACION C LEFT JOIN PROYECTO P
ON C.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON C.IDUNIDAD = U.IDUNIDAD
LEFT JOIN CLIENTE E
ON C.IDCLIENTE = E.IDCLIENTE
LEFT JOIN USUARIO S
ON C.IDUSUARIO = S.IDUSUARIO
LEFT JOIN ACCION A
ON U.IDACCION = A.IDACCION
LEFT JOIN ESTADOCOTIZACION EC
ON C.ESTADO = EC.IDESTADOCOTIZACION
WHERE P.CODIGOPROYECTO = '" . $codigoProyecto . "'
GROUP BY C.IDCOTIZACION, P.CODIGOPROYECTO, C.CODIGO, C.FECHA, U.CODIGO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL, C.CANTEST, C.CANTBOD, A.NOMBRE, C.VALORTOTALUF, S.NOMBRES, S.APELLIDOS, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4, C.ESTADO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCotizacionesUsuVigentes($rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, C.CODIGO, DATE_FORMAT(C.FECHA, '%d-%m-%Y') FECHA, U.CODIGO DEPTO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL,
C.CANTEST EST,
C.CANTBOD BOD,
A.NOMBRE FORMA,
C.VALORTOTALUF, CONCAT(S.NOMBRES,' ',S.APELLIDOS) VENDEDOR, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4,
EC.COLOR 'ESTADO',
C.RUTA_PDF, EC.NOMBRE 'ESTADOTEXTO'
FROM COTIZACION C LEFT JOIN PROYECTO P
ON C.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON C.IDUNIDAD = U.IDUNIDAD
LEFT JOIN CLIENTE E
ON C.IDCLIENTE = E.IDCLIENTE
LEFT JOIN USUARIO S
ON C.IDUSUARIO = S.IDUSUARIO
LEFT JOIN ACCION A
ON U.IDACCION = A.IDACCION
LEFT JOIN ESTADOCOTIZACION EC
ON C.ESTADO = EC.IDESTADOCOTIZACION
LEFT JOIN USUARIOPROYECTO US
ON P.IDPROYECTO = US.IDPROYECTO
WHERE US.IDUSUARIO =
(
SELECT IDUSUARIO
FROM USUARIO
WHERE RUT = '" . $rutUsuario	 . "'
)
AND C.ESTADO IN
(
	1,2
)
GROUP BY C.IDCOTIZACION, P.CODIGOPROYECTO, C.CODIGO, C.FECHA, U.CODIGO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL, C.CANTEST, C.CANTBOD, A.NOMBRE, C.VALORTOTALUF, S.NOMBRES, S.APELLIDOS, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4, C.ESTADO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCotizacionesUsuVigentesParaReserva($rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, C.CODIGO, DATE_FORMAT(C.FECHA, '%d-%m-%Y') FECHA, U.CODIGO DEPTO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL,
C.CANTEST EST,
C.CANTBOD BOD,
A.NOMBRE FORMA,
C.VALORTOTALUF, CONCAT(S.NOMBRES,' ',S.APELLIDOS) VENDEDOR, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4,
EC.COLOR 'ESTADO',
C.RUTA_PDF
FROM COTIZACION C LEFT JOIN PROYECTO P
ON C.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON C.IDUNIDAD = U.IDUNIDAD
LEFT JOIN CLIENTE E
ON C.IDCLIENTE = E.IDCLIENTE
LEFT JOIN USUARIO S
ON C.IDUSUARIO = S.IDUSUARIO
LEFT JOIN ACCION A
ON U.IDACCION = A.IDACCION
LEFT JOIN ESTADOCOTIZACION EC
ON C.ESTADO = EC.IDESTADOCOTIZACION
LEFT JOIN USUARIOPROYECTO US
ON P.IDPROYECTO = US.IDPROYECTO
WHERE US.IDUSUARIO =
(
SELECT IDUSUARIO
FROM USUARIO
WHERE RUT = '" . $rutUsuario	 . "'
)
AND C.ESTADO IN
(
	1,2
)
AND C.IDUNIDAD IN
(
	SELECT IDUNIDAD
	FROM UNIDAD
	WHERE ESTADO NOT IN
	(
		2,3,4,5,6,7
	)
)
GROUP BY C.IDCOTIZACION, P.CODIGOPROYECTO, C.CODIGO, C.FECHA, U.CODIGO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL, C.CANTEST, C.CANTBOD, A.NOMBRE, C.VALORTOTALUF, S.NOMBRES, S.APELLIDOS, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4, C.ESTADO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCotizacionesUsuVigentesProyecto($rutUsuario, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, C.CODIGO, DATE_FORMAT(C.FECHA, '%d-%m-%Y') FECHA, U.CODIGO DEPTO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL,
C.CANTEST EST,
C.CANTBOD BOD,
A.NOMBRE FORMA,
C.VALORTOTALUF, CONCAT(S.NOMBRES,' ',S.APELLIDOS) VENDEDOR, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4,
EC.COLOR 'ESTADO',
C.RUTA_PDF, EC.NOMBRE 'ESTADOTEXTO'
FROM COTIZACION C LEFT JOIN PROYECTO P
ON C.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON C.IDUNIDAD = U.IDUNIDAD
LEFT JOIN CLIENTE E
ON C.IDCLIENTE = E.IDCLIENTE
LEFT JOIN USUARIO S
ON C.IDUSUARIO = S.IDUSUARIO
LEFT JOIN ACCION A
ON U.IDACCION = A.IDACCION
LEFT JOIN ESTADOCOTIZACION EC
ON C.ESTADO = EC.IDESTADOCOTIZACION
LEFT JOIN USUARIOPROYECTO US
ON P.IDPROYECTO = US.IDPROYECTO
WHERE US.IDUSUARIO =
(
SELECT IDUSUARIO
FROM USUARIO
WHERE RUT = '" . $rutUsuario	 . "'
)
AND C.ESTADO IN
(
	1,2
)
AND P.CODIGOPROYECTO = '" . $codigoProyecto . "'
GROUP BY C.IDCOTIZACION, P.CODIGOPROYECTO, C.CODIGO, C.FECHA, U.CODIGO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL, C.CANTEST, C.CANTBOD, A.NOMBRE, C.VALORTOTALUF, S.NOMBRES, S.APELLIDOS, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4, C.ESTADO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCotizacionesUsu($rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, C.CODIGO, DATE_FORMAT(C.FECHA, '%d-%m-%Y') FECHA, U.CODIGO DEPTO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL,
C.CANTEST EST,
C.CANTBOD BOD,
A.NOMBRE FORMA,
C.VALORTOTALUF, CONCAT(S.NOMBRES,' ',S.APELLIDOS) VENDEDOR, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4,
EC.COLOR 'ESTADO',
C.RUTA_PDF, EC.NOMBRE 'ESTADOTEXTO'
FROM COTIZACION C LEFT JOIN PROYECTO P
ON C.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON C.IDUNIDAD = U.IDUNIDAD
LEFT JOIN CLIENTE E
ON C.IDCLIENTE = E.IDCLIENTE
LEFT JOIN USUARIO S
ON C.IDUSUARIO = S.IDUSUARIO
LEFT JOIN ACCION A
ON U.IDACCION = A.IDACCION
LEFT JOIN ESTADOCOTIZACION EC
ON C.ESTADO = EC.IDESTADOCOTIZACION
LEFT JOIN USUARIOPROYECTO US
ON P.IDPROYECTO = US.IDPROYECTO
WHERE US.IDUSUARIO =
(
SELECT IDUSUARIO
FROM USUARIO
WHERE RUT = '" . $rutUsuario	 . "'
)
GROUP BY C.IDCOTIZACION, P.CODIGOPROYECTO, C.CODIGO, C.FECHA, U.CODIGO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL, C.CANTEST, C.CANTBOD, A.NOMBRE, C.VALORTOTALUF, S.NOMBRES, S.APELLIDOS, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4, C.ESTADO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCotizacionesUsuTodasProyecto($rutUsuario,$codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, C.CODIGO, DATE_FORMAT(C.FECHA, '%d-%m-%Y') FECHA, U.CODIGO DEPTO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL,
C.CANTEST EST,
C.CANTBOD BOD,
A.NOMBRE FORMA,
C.VALORTOTALUF, CONCAT(S.NOMBRES,' ',S.APELLIDOS) VENDEDOR, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4,
EC.COLOR 'ESTADO',
C.RUTA_PDF, EC.NOMBRE 'ESTADOTEXTO'
FROM COTIZACION C LEFT JOIN PROYECTO P
ON C.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON C.IDUNIDAD = U.IDUNIDAD
LEFT JOIN CLIENTE E
ON C.IDCLIENTE = E.IDCLIENTE
LEFT JOIN USUARIO S
ON C.IDUSUARIO = S.IDUSUARIO
LEFT JOIN ACCION A
ON U.IDACCION = A.IDACCION
LEFT JOIN ESTADOCOTIZACION EC
ON C.ESTADO = EC.IDESTADOCOTIZACION
LEFT JOIN USUARIOPROYECTO US
ON P.IDPROYECTO = US.IDPROYECTO
WHERE US.IDUSUARIO =
(
SELECT IDUSUARIO
FROM USUARIO
WHERE RUT = '" . $rutUsuario	 . "'
)
AND P.CODIGOPROYECTO = '" . $codigoProyecto . "'
GROUP BY C.IDCOTIZACION, P.CODIGOPROYECTO, C.CODIGO, C.FECHA, U.CODIGO, E.NOMBRES, E.APELLIDOS,
E.RUT, E.CELULAR, E.EMAIL, C.CANTEST, C.CANTBOD, A.NOMBRE, C.VALORTOTALUF, S.NOMBRES, S.APELLIDOS, C.MEDIO, C.COTIZAEN,
C.ESTADONIVELINTERES, C.ESTADOSEGUIMIENTO1, C.ESTADOSEGUIMIENTO2, C.ESTADOSEGUIMIENTO3, C.ESTADOSEGUIMIENTO4, C.ESTADO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaBodegasDisponiblesProyecto($codigoProyecto,$codigoCotiza){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDUNIDAD, CODIGO, VALORUF
FROM UNIDAD
WHERE IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND ESTADO = '1'
AND IDTIPOUNIDAD = '3'
AND IDUNIDAD NOT IN
(
	SELECT IDUNIDAD
	FROM COTIZACIONBODEGA
	WHERE IDCOTIZACION =
	(
		SELECT IDCOTIZACION
		FROM COTIZACION
		WHERE IDPROYECTO =
		(
		SELECT IDPROYECTO
		FROM PROYECTO
		WHERE CODIGOPROYECTO = '$codigoProyecto'
		)
		AND CODIGO = '" . $codigoCotiza . "'
	)
)
ORDER BY CODIGO ASC
";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaEstacionamientosDisponiblesProyecto($codigoProyecto, $codigoCotiza){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDUNIDAD, CODIGO, VALORUF
FROM UNIDAD
WHERE IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND ESTADO = '1'
AND IDTIPOUNIDAD = '4'
AND IDUNIDAD NOT IN
(
	SELECT IDUNIDAD
	FROM COTIZACIONESTACIONAMIENTO
	WHERE IDCOTIZACION =
	(
		SELECT IDCOTIZACION
		FROM COTIZACION
		WHERE IDPROYECTO =
		(
		SELECT IDPROYECTO
		FROM PROYECTO
		WHERE CODIGOPROYECTO = '$codigoProyecto'
		)
		AND CODIGO = '" . $codigoCotiza . "'
	)
)
ORDER BY CODIGO ASC
";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaEstadosProyecto(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDESTADOPROYECTO, NOMBRE, COLOR
FROM ESTADOPROYECTO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaEstadosReservas(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM ESTADORESERVA";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaEstadosPromesas(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM ESTADOPROMESA";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaPerfil(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDPERFIL, NOMBREPERFIL
FROM PERFIL";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaEstadoCivil(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDESTADOCIVIL, NOMBRE
FROM ESTADOCIVIL";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaFormaPagoReserva(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDRESERVAFORMA, NOMBRE
FROM RESERVAFORMA";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaFormaPagoReservaId($idReserva){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE
FROM RESERVAFORMA
WHERE IDRESERVAFORMA = " . $idReserva;
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaNivelEducacional(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDNIVELEDUCACIONAL, NOMBRE
FROM NIVELEDUCACIONAL";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCasaHabita(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDCASAHABITA, NOMBRE
FROM CASAHABITA";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaMotivoCompra(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDMOTIVOCOMPRA, NOMBRE
FROM MOTIVOCOMPRA";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaEstadosCliente(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDESTADOCLIENTE, NOMBRE
FROM ESTADOCLIENTE";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaTipoUnidad(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDTIPOUNIDAD, NOMBRE
FROM TIPOUNIDAD";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaAccion(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDACCION, NOMBRE
FROM ACCION";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaEstadosUnidad(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDESTADOUNIDAD, NOMBRE, COLOR
FROM ESTADOUNIDAD";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaTipologiaUnidad(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDTIPOLOGIA, NOMBRE
FROM TIPOLOGIA";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaUnidadesProyectos($codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT U.CODIGO, U.PISO, U.CODIGOTIPO, T.NOMBRE 'TIPO',
(
SELECT NOMBRE
FROM ACCION
WHERE IDACCION = U.IDACCION
) 'ACCION',
(
SELECT NOMBRE
FROM TIPOLOGIA
WHERE IDTIPOLOGIA = U.TIPOLOGIA
) 'TIPOLOGIA'
, U.ORIENTACION, U.M2UTIL, U.M2TOTAL, U.VALORUF,
(SELECT E.COLOR FROM ESTADOUNIDAD E WHERE E.IDESTADOUNIDAD = U.ESTADO) 'ESTADO',
(SELECT E.NOMBRE FROM ESTADOUNIDAD E WHERE E.IDESTADOUNIDAD = U.ESTADO) 'ESTADO2'
FROM PROYECTO P, UNIDAD U, TIPOUNIDAD T, ACCION A
WHERE P.IDPROYECTO = U.IDPROYECTO
AND U.IDTIPOUNIDAD = T.IDTIPOUNIDAD
AND U.IDACCION = A.IDACCION
AND P.CODIGOPROYECTO = '" . $codigoProyecto . "'";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaUnidadesProyectosCotizacion($codigoProyecto,$accionUnidad){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT DISTINCT U.CODIGO,T.NOMBRE 'TIPO', U.DIRECCION, A.NOMBRE 'ACCION'
FROM PROYECTO P, UNIDAD U, TIPOUNIDAD T, ACCION A
WHERE P.IDPROYECTO = U.IDPROYECTO
AND U.IDTIPOUNIDAD = T.IDTIPOUNIDAD
AND U.IDACCION = A.IDACCION
AND P.CODIGOPROYECTO = '" . $codigoProyecto . "'
AND U.ESTADO = 1
AND U.IDTIPOUNIDAD NOT IN (3,4)
AND U.IDACCION = (SELECT IDACCION FROM ACCION WHERE NOMBRE = '" . $accionUnidad . "')";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaMedioContacto(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDMEDIOCONTACTO, NOMBRE
FROM MEDIOCONTACTO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCotizaEn(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDCOTIZAEN, NOMBRE
FROM COTIZAEN";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaDatosProyecto($codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, P.NOMBRE, P.INMOBILIARIA,
P.EMAILINMOBILIARIA, P.DIRECCION, P.FONO1, P.FONO2,
P.FECHAINICIOVENTA, P.FECHAFINOBRA, P.FECHAENTREGA,
P.EMAILPROYECTO, P.WEBPROYECTO, P.LOGO, P.ESTADO,
P.COMISIONLVPROMESA, P.COMISIONLVESCRITURA, P.COMISIONVENPROMESA,
P.COMISIONVENESCRITURA, P.DESCUENTOSALA1, P.DESCRIPCION1,
P.DESCUENTOSALA2, P.DESCRIPCION2, P.VALORRESERVA,
P.BONOVENTA, P.COTIZA1, P.COTIZA2, P.COTIZA3, P.INFOBONOVENTA, P.RESERVA1, P.RESERVA2, P.RESERVA3, P.DIASCOT, P.DIASRES,
P.DEFAULTPIEPROMESA, P.DEFAULTPIECUOTAS, P.DEFAULTTASA, P.IVA, P.COMISIONLVPROMESA2, P.COMISIONLVESCRITURA2, P.COMISIONVENPROMESA2,
P.COMISIONVENESCRITURA2, P.SALTO
FROM PROYECTO P
WHERE P.CODIGOPROYECTO = '" . $codigoProyecto . "'";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaClientes(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT C.RUT, C.APELLIDOS, C.NOMBRES, C.EMAIL, C.CELULAR, C.FECHAALTA, E.COLOR 'ESTADO'
FROM CLIENTE C, ESTADOCLIENTE E
WHERE C.ESTADO = E.IDESTADOCLIENTE";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaUsuarios(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT U.RUT, U.APELLIDOS, U.NOMBRES, U.EMAIL, U.FONO, P.NOMBREPERFIL 'PERFIL'
FROM USUARIO U, PERFIL P
WHERE U.IDPERFIL = P.IDPERFIL
AND U.ESTADO = 'Activo'";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaUsuarioId($id){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT U.RUT, CONCAT(UCASE(LEFT(U.NOMBRES, 1)), LCASE(SUBSTRING(U.NOMBRES, 2)), ' ',
		UCASE(LEFT(U.APELLIDOS, 1)), LCASE(SUBSTRING(U.APELLIDOS, 2))) 'NOMBRE',
		U.EMAIL, U.FONO, P.NOMBREPERFIL 'PERFIL'
FROM USUARIO U, PERFIL P
WHERE U.IDPERFIL = P.IDPERFIL
AND U.ESTADO = 'Activo'
AND U.IDUSUARIO = '" . $id . "'";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaUsuariosBloqueados(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT U.RUT, U.APELLIDOS, U.NOMBRES, U.EMAIL, U.FONO, P.NOMBREPERFIL 'PERFIL'
FROM USUARIO U, PERFIL P
WHERE U.IDPERFIL = P.IDPERFIL
AND U.ESTADO = 'Bloqueado'";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Inserciones
	function ingresaProyecto($codigoProyectoCrear,	$nombreProyectoCrear, $inmobiliariaProyectoCrear,	$emailInmobiliariaProyectoCrear,	$direccionProyectoCrear,	$fono1InmobiliariaProyectoCrear,	$fono2InmobiliariaProyectoCrear,	$fInicioVentaProyectoCrear,	$fFinObraProyectoCrear,	$fEntregaProyectoCrear,	$emailProyectoCrear,	$webProyectoCrear,	$comisionLVPromesaProyectoCrear,	$comisionLVEscrituraProyectoCrear,	$comisionVenPromesaProyectoCrear,	$comisionVenEscrituraProyectoCrear,	$descuentoSala1ProyectoCrear,	$infoDescuento1ProyectoCrear,	$descuentoSala2ProyectoCrear,	$infoDescuento2ProyectoCrear,	$descuentoSala3ProyectoCrear,	$bonoVentaProyectoCrear,	$cotiza1ProyectoCrear, $cotiza2ProyectoCrear, $cotiza3ProyectoCrear, $estadoProyectoCrear, $logo, $infoBonoVentaProyectoCrear, $reserva1ProyectoCrear, $reserva2ProyectoCrear, $reserva3ProyectoCrear, $diasCotProyecto, $diasResProyecto, $defaultPiePromesa, $defaultPieCuotas, $defaultTasa, $comisionIVAProyectoCrear, $unidadesSaltoProyectoCrear, $comisionLVPromesaProyectoCrear2, $comisionLVEscrituraProyectoCrear2, $comisionVenPromesaProyectoCrear2, $comisionVenEscrituraProyectoCrear2){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO PROYECTO (CODIGOPROYECTO, NOMBRE,INMOBILIARIA,EMAILINMOBILIARIA,DIRECCION,FONO1,FONO2,FECHAINICIOVENTA,FECHAFINOBRA,FECHAENTREGA,EMAILPROYECTO,WEBPROYECTO,COMISIONLVPROMESA,COMISIONLVESCRITURA,COMISIONVENPROMESA,COMISIONVENESCRITURA,BONOVENTA,DESCUENTOSALA1,DESCUENTOSALA2,VALORRESERVA,DESCRIPCION1,DESCRIPCION2,COTIZA1,COTIZA2,COTIZA3, ESTADO, LOGO, INFOBONOVENTA, RESERVA1, RESERVA2, RESERVA3, DIASCOT, DIASRES, DEFAULTPIEPROMESA, DEFAULTPIECUOTAS, DEFAULTTASA, IVA, SALTO, COMISIONLVPROMESA2, COMISIONLVESCRITURA2, COMISIONVENPROMESA2, COMISIONVENESCRITURA2)
			VALUES('" . $codigoProyectoCrear . "','" .	$nombreProyectoCrear . "','" .	$inmobiliariaProyectoCrear . "','" .	$emailInmobiliariaProyectoCrear . "','" .	$direccionProyectoCrear . "','" .	$fono1InmobiliariaProyectoCrear . "','" .	$fono2InmobiliariaProyectoCrear . "','" .	$fInicioVentaProyectoCrear . "','" .	$fFinObraProyectoCrear . "','" .	$fEntregaProyectoCrear . "','" .	$emailProyectoCrear . "','" .	$webProyectoCrear . "','" .	$comisionLVPromesaProyectoCrear . "','" .	$comisionLVEscrituraProyectoCrear . "','" .	$comisionVenPromesaProyectoCrear . "','" .	$comisionVenEscrituraProyectoCrear . "','" . $bonoVentaProyectoCrear . "','" .	$descuentoSala1ProyectoCrear . "','" .	$descuentoSala2ProyectoCrear . "','" .	$descuentoSala3ProyectoCrear . "','" .	$infoDescuento1ProyectoCrear . "','" .	$infoDescuento2ProyectoCrear  . "','" .	$cotiza1ProyectoCrear . "','" .	$cotiza2ProyectoCrear . "','" .	$cotiza3ProyectoCrear . "','" .	$estadoProyectoCrear . "','" .	$logo . "','" .	$infoBonoVentaProyectoCrear . "','" .	$reserva1ProyectoCrear . "','" .	$reserva2ProyectoCrear . "','" .	$reserva3ProyectoCrear . "','" .	$diasCotProyecto . "','" .	$diasResProyecto . "','" .	$defaultPiePromesa . "','" .	$defaultPieCuotas . "','" .	$defaultTasa . "',' " . $comisionIVAProyectoCrear . " ','" . $unidadesSaltoProyectoCrear . "','" . $comisionLVPromesaProyectoCrear2 . "','" . $comisionLVEscrituraProyectoCrear2 . "','" . $comisionVenPromesaProyectoCrear2 . "','" . $comisionVenEscrituraProyectoCrear2 . "')";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				//return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function ingresaCliente($rutCrearCliente,$nombresCrearCliente,$apellidosCrearCliente,$emailCrearCliente,$celularCrearCliente,$domicilioCrearCliente,$numeroDomicilioCrearCliente,$tipoDomicilioCrearCliente,$comunaCrearCliente,$ciudadCrearCliente,$paisCrearCliente,$fNacCrearCliente,$estadoCivilCrearCliente,$nivelEducacionalCrearCliente,$casaHabitacionalCrearCliente,$motivoCrearCliente,$profesionCrearCliente,$institucionCrearCliente,$actividadCrearCliente,$nacionalidadCrearCliente,$residenciaCrearCliente,$sexoCrearCliente,$EMRutCrearCliente,$EMNombreCrearCliente,$EMDomicilioCrearCliente,$EMComunaCrearCliente,$EMCiudadCrearCliente,$EMPaisCrearCliente,$EMFonoCrearCliente,$EMGiroCrearCliente,$EMFIngresoCrearCliente,$EMAntiguedadCrearCliente,$EMCargoCrearCliente,$EMRentaLiquidaCrearCliente,$observacionCrearCliente,$estadoCrearCliente,$fAltaCrearCliente,$regionCrearCliente){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO CLIENTE
(RUT, NOMBRES, APELLIDOS, EMAIL, CELULAR, DOMICILIO, NUMERODOMICILIO, TIPODOMICILIO, COMUNA, CIUDAD, PAIS, FECHANAC, ESTADOCIVIL, NIVELEDUCACIONAL, CASAHABITA, MOTIVOCOMPRA, PROFESION, INSTITUCION, ACTIVIDAD, NACIONALIDAD, RESIDENCIA, SEXO, EM_RUT, EM_NOMBRE, EM_DOMICILIO, EM_COMUNA, EM_CIUDAD, EM_PAIS, EM_FONO, EM_GIRO, EM_FECHAINGRESO, EM_ANTIGUEDAD, EM_CARGO, EM_RENTALIQUIDA, OBSERVACION, ESTADO, FECHAALTA, REGION)
VALUES('" . $rutCrearCliente . "', '" . $nombresCrearCliente . "', '" . $apellidosCrearCliente . "', '" . $emailCrearCliente . "', '" . $celularCrearCliente . "', '" . $domicilioCrearCliente . "', '" . $numeroDomicilioCrearCliente . "', '" . $tipoDomicilioCrearCliente . "', '" . $comunaCrearCliente . "', '" . $ciudadCrearCliente . "', '" . $paisCrearCliente . "', '" . $fNacCrearCliente . "', '" . $estadoCivilCrearCliente . "', '" . $nivelEducacionalCrearCliente . "', '" . $casaHabitacionalCrearCliente . "', '" . $motivoCrearCliente . "', '" . $profesionCrearCliente . "', '" . $institucionCrearCliente ."', '" . $actividadCrearCliente . "', '" . $nacionalidadCrearCliente . "', '" . $residenciaCrearCliente . "', '" . $sexoCrearCliente . "', '" . $EMRutCrearCliente . "', '" . $EMNombreCrearCliente . "', '" . $EMDomicilioCrearCliente . "', '" . $EMComunaCrearCliente . "', '" . $EMCiudadCrearCliente . "', '" . $EMPaisCrearCliente . "', '" . $EMFonoCrearCliente . "', '" . $EMGiroCrearCliente . "', '" . $EMFIngresoCrearCliente . "', '" . $EMAntiguedadCrearCliente . "', '" . $EMCargoCrearCliente . "', '" . $EMRentaLiquidaCrearCliente . "', '" . $observacionCrearCliente . "', '" . $estadoCrearCliente . "', '" . $fAltaCrearCliente . "', '" . $regionCrearCliente . "')";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function ingresaClienteCotiza($rutCrearCliente,$nombresCrearCliente,$apellidosCrearCliente,$emailCrearCliente,$celularCrearCliente,$estado,$fechaAlta){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO CLIENTE(RUT,NOMBRES,APELLIDOS, EMAIL, CELULAR, ESTADO, FECHAALTA)
VALUES('" . $rutCrearCliente . "','" . $nombresCrearCliente . "','" . $apellidosCrearCliente . "','" . $emailCrearCliente . "'," . $celularCrearCliente . "," . $estado . ",'" . $fechaAlta . "')";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function ingresaUsuario($rutCrearUsuario,$apellidosCrearUsuario,$nombresCrearUsuario,$emailCrearUsuario,$fonoCrearUsuario,$perfilCrearUsuario,$passCrearUsuario){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO USUARIO
(IDPERFIL, RUT, NOMBRES, APELLIDOS, EMAIL, FONO, CONTRASENA, ESTADO)
VALUES('" . $perfilCrearUsuario . "', '" . $rutCrearUsuario . "', '" . $nombresCrearUsuario . "', '" . $apellidosCrearUsuario . "', '" . $emailCrearUsuario . "', '" . $fonoCrearUsuario . "', '" . $passCrearUsuario . "','Activo')";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function ingresaCotizacion($idProyecto, $idUnidad, $idCliente, $idUsuario,$codigo,$fecha,$valorTotalUF,$descuento1UF,$descuento2UF,$valorReservaUF,$valorPiePromesaUF,$valorPieSaldoUF,$cantidadCuotasPie,$valorSaldoTotalUF,$medio,$cotizaEn, $bonoVenta, $cantBod, $cantEst, $valorUnidadUF){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO COTIZACION
(IDPROYECTO, IDUNIDAD, IDCLIENTE, IDUSUARIO, CODIGO, FECHA, VALORTOTALUF, DESCUENTO1UF, DESCUENTO2UF, VALORRESERVAUF, VALORPIEPROMESAUF, VALORPIESALDOUF, CANTIDADCUOTASPIE, VALORSALDOTOTALUF, MEDIO, COTIZAEN, BONOVENTA, CANTEST, CANTBOD, VALORUNIDADUF)
VALUES(" . $idProyecto . "," .$idUnidad . "," . $idCliente . "," . $idUsuario . "," . $codigo . ", '" . $fecha . "'," . $valorTotalUF . "," . $descuento1UF . "," . $descuento2UF . "," . $valorReservaUF . "," . $valorPiePromesaUF . "," . $valorPieSaldoUF . "," . $cantidadCuotasPie . "," . $valorSaldoTotalUF . ", '" . $medio . "', '" . $cotizaEn . "', '" . $bonoVenta . "', '" . $cantEst . "', '" . $cantBod. "', '" . $valorUnidadUF . "');
";
			if ($con->query($sql)) {
					return $con;
			}
			else{
				//return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function ingresaReserva($idCotizacion, $idProyecto, $idUnidad, $idCliente1, $idCliente2, $idUsuario, $fechaReserva, $valorBrutoUF, $descuento1, $descuento2, $bonoVentaUF, $valorTotalUF, $valorReservaUF, $valorPiePromesaUF, $valorPieSaldoUF, $cantidadCuotasPie, $valorSaldoTotalUF, $fechaPagoReserva, $valorPagoReserva, $formaPagoReserva, $bancoReserva, $serieChequeReserva, $nroChequeReserva, $valorPiePromesa, $valorPieSaldo, $cotizaPDF){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO RESERVA
(IDCOTIZACION, IDPROYECTO, IDUNIDAD, IDCLIENTE1, IDCLIENTE2, IDUSUARIO, ESTADO, FECHARESERVA, VALORBRUTOUF, DESCUENTO1, DESCUENTO2, BONOVENTAUF, VALORTOTALUF, VALORRESERVAUF, VALORPIEPROMESAUF, VALORPIESALDOUF, CANTCUOTASPIE, VALORSALDOTOTALUF, FECHAPAGORESERVA, VALORPAGORESERVA, FORMAPAGORESERVA, BANCORESERVA, SERIECHEQUERESERVA, NROCHEQUERESERVA, ESTADOPAGORESERVA, OBSERVACIONRESERVA, VALORPIEPROMESA, VALORPIESALDO, COTIZA_PDF)
VALUES('" . $idCotizacion . "', '" . $idProyecto . "', '" . $idUnidad . "', '" . $idCliente1 . "', '" . $idCliente2 . "', '" . $idUsuario . "','" .  2 . "', '" . $fechaReserva . "', '" . $valorBrutoUF . "', '" . $descuento1 . "', '" . $descuento2 . "', '" . $bonoVentaUF . "', '" . $valorTotalUF . "', '" . $valorReservaUF . "', '" . $valorPiePromesaUF . "', '" . $valorPieSaldoUF . "', '" . $cantidadCuotasPie . "', '" . $valorSaldoTotalUF . "', '" . $fechaPagoReserva . "', '" . $valorPagoReserva . "', '" . $formaPagoReserva . "', '" . $bancoReserva . "', '" . $serieChequeReserva . "', '" . $nroChequeReserva . "', '', '','" . $valorPiePromesa . "','" . $valorPieSaldo . "','" . $cotizaPDF . "')";
			if ($con->query($sql)) {
					return $con;
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function actualizarReserva($codigoProyecto, $numeroOperacion, $valorReservaUF, $valorPiePromesaUF, $valorPieSaldoUF, $cantidadCuotasPie, $valorSaldoTotalUF, $fechaPagoReserva, $valorPagoReserva, $formaPagoReserva, $bancoReserva, $serieChequeReserva, $nroChequeReserva, $valorPiePromesa, $valorPieSaldo, $idUsuario){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE RESERVA
SET VALORRESERVAUF = '" . $valorReservaUF . "',
VALORPIEPROMESAUF  = '" . $valorPiePromesaUF . "',
VALORPIESALDOUF = '" . $valorPieSaldoUF . "',
CANTCUOTASPIE = '" . $cantidadCuotasPie . "',
VALORSALDOTOTALUF = '" . $valorSaldoTotalUF . "',
FECHAPAGORESERVA = '" . $fechaPagoReserva . "',
VALORPAGORESERVA = '" . $valorPagoReserva . "',
FORMAPAGORESERVA = '" . $formaPagoReserva . "',
BANCORESERVA = '" . $bancoReserva . "',
SERIECHEQUERESERVA = '" . $serieChequeReserva . "',
NROCHEQUERESERVA = '" . $nroChequeReserva . "',
VALORPIEPROMESA = '" . $valorPiePromesa . "',
VALORPIESALDO = '" . $valorPieSaldo . "',
IDUSUARIO = '" . $idUsuario . "'
WHERE IDPROYECTO =
(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND NUMERO = '" . $numeroOperacion . "'";
			if ($con->query($sql)) {
					return $con;
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function ingresaCotizacionBodega($idCotizacion, $idUnidad, $con){
		if($con != 'No conectado'){
			$sql = "INSERT INTO COTIZACIONBODEGA
(IDCOTIZACION, IDUNIDAD,VALORUF)
VALUES('" . $idCotizacion . "','" . $idUnidad . "',
(
	SELECT VALORUF
	FROM UNIDAD
	WHERE IDUNIDAD = '" . $idUnidad . "'
)
)";
			if ($con->query($sql)) {
			    return "Ok";
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function ingresaCotizacionEstacionamiento($idCotizacion, $idUnidad, $con){
		if($con != 'No conectado'){
			$sql = "INSERT INTO COTIZACIONESTACIONAMIENTO
(IDCOTIZACION, IDUNIDAD, VALORUF)
VALUES('" . $idCotizacion . "','" . $idUnidad . "',
(
	SELECT VALORUF
	FROM UNIDAD
	WHERE IDUNIDAD = '" . $idUnidad . "'
)
)";
			if ($con->query($sql)) {
			    return "Ok";
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function ingresaReservaEstacionamiento($idReserva, $idUnidad, $con){
		if($con != 'No conectado'){
			$sql = "INSERT INTO RESERVAESTACIONAMIENTO
(IDRESERVA, IDUNIDAD, VALORUF)
VALUES('" .$idReserva . "','" . $idUnidad . "',
(
	SELECT VALORUF
	FROM UNIDAD
	WHERE IDUNIDAD = '" . $idUnidad . "'
)
)";
			if ($con->query($sql)) {
			    return "Ok";
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function ingresaReservaBodega($idReserva, $idUnidad, $con){
		if($con != 'No conectado'){
			$sql = "INSERT INTO RESERVABODEGA
(IDRESERVA, IDUNIDAD, VALORUF)
VALUES('" .$idReserva . "','" . $idUnidad . "',
(
	SELECT VALORUF
	FROM UNIDAD
	WHERE IDUNIDAD = '" . $idUnidad . "'
)
)";
			if ($con->query($sql)) {
			    return "Ok";
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function ingresaUnidad($codigoProyecto,	$tipoCrearUnidad,	$accionCrearUniad,	$numeroCrearUnidad,	$estadoCrearUnidad,	$pisoCrearUnidad,	$orientacionCrearUnidad,	$cantDormitoriosCrearUnidad,	$cantBanosCrearUnidad,	$logiaCrearUnidad,	$m2UtilCrearUnidad,	$m2TerrazaCrearUnidad,	$m2OtroCrearUnidad,	$valorCrearUnidad,	$comisionLVDuenoCrearUnidad,	$comisionLVClienteCrearUnidad,	$comisionVenDuenoCrearUnidad,	$comisionVenClienteCrearUnidad,	$direccionCrearUnidad,	$comunaCrearUnidad,	$codigoTipoCrearUnidad,	$cantidadBodegasCrearUnidad,	$cantidadEstacionamientosCrearUnidad,	$descripcion1CrearUnidad,	$descripcion2CrearUnidad,
$imgPlano,$imgOtra,$m2TotalCrearUnidad,$tipologiaCrearUnidad, $rutVendedorCrearUnidad, $nombresVendedorCrearUnidad, $apellidosVendedorCrearUnidad, $celularVendedorCrearUnidad, $emailVendedorCrearUnidad){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO UNIDAD
(IDPROYECTO, IDTIPOUNIDAD, IDACCION, CODIGO, TIPOLOGIA, ESTADO, PISO, ORIENTACION, CANTDORMITORIOS, CANTBANOS, LOGIA, M2UTIL, M2TERRAZA, M2OTRO, M2TOTAL, VALORUF, IMGPLANO, IMGOTRA, COMISIONLVDUENO, COMISIONLVCLIENTE, COMISIONVENDUENO, COMISIONVENCLIENTE, DESCRIPCION1, DESCRIPCION2, DIRECCION, COMUNA, CODIGOTIPO, CANTBODEGAS, CANTESTACIONAMIENTOS, RUTVENDEDOR, NOMBRESVENDEDOR, APELLIDOSVENDEDOR, CELULARVENDEDOR, EMAILVENDEDOR)
VALUES((SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
, '" . $tipoCrearUnidad . "', '" . $accionCrearUniad . "', '" . $numeroCrearUnidad . "', '" . $tipologiaCrearUnidad . "', '" . $estadoCrearUnidad . "', '" . $pisoCrearUnidad . "', '" . $orientacionCrearUnidad . "', '" . $cantDormitoriosCrearUnidad . "', '" . $cantBanosCrearUnidad . "', '" . $logiaCrearUnidad . "', '" . $m2UtilCrearUnidad . "', '" . $m2TerrazaCrearUnidad . "', '" . $m2OtroCrearUnidad . "', '" . $m2TotalCrearUnidad . "', '" . $valorCrearUnidad . "', '" . $imgPlano . "', '" . $imgOtra . "', '" .  $comisionLVDuenoCrearUnidad . "', '" . $comisionLVClienteCrearUnidad . "', '" . $comisionVenDuenoCrearUnidad . "', '" . $comisionVenClienteCrearUnidad . "', '" . $descripcion1CrearUnidad . "', '" . $descripcion2CrearUnidad . "', '" . $direccionCrearUnidad . "', '" . $comunaCrearUnidad . "', '" . $codigoTipoCrearUnidad . "', '" . $cantidadBodegasCrearUnidad . "', '" . $cantidadEstacionamientosCrearUnidad . "', '" . $rutVendedorCrearUnidad . "', '" . $nombresVendedorCrearUnidad . "', '" . $apellidosVendedorCrearUnidad . "', '" . $celularVendedorCrearUnidad . "', '" . $emailVendedorCrearUnidad . "')
";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function consultaPass($rut, $passNuevo){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT CONTRASENA
FROM USUARIO
WHERE RUT = '" . $rut . "'
AND CONTRASENA = '" . $passNuevo . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

		function actualizaProyecto($codigoDetalleProyecto,	$nombreDetalleProyecto, $inmobiliariaDetalleProyecto,	$emailInmobiliariaDetalleProyecto,	$direccionDetalleProyecto,	$fono1InmobiliariaDetalleProyecto,	$fono2InmobiliariaDetalleProyecto,	$fInicioVentaDetalleProyecto,	$fFinObraDetalleProyecto,	$fEntregaDetalleProyecto,	$emailDetalleProyecto,	$webDetalleProyecto,	$comisionLVPromesaDetalleProyecto,	$comisionLVEscrituraDetalleProyecto,	$comisionVenPromesaDetalleProyecto,	$comisionVenEscrituraDetalleProyecto,	$descuentoSala1DetalleProyecto,	$infoDescuento1DetalleProyecto,	$descuentoSala2DetalleProyecto,	$infoDescuento2DetalleProyecto,	$descuentoSala3DetalleProyecto,	$bonoVentaDetalleProyecto,	$cotiza1DetalleProyecto, $cotiza2DetalleProyecto, $cotiza3DetalleProyecto, $estadoDetalleProyecto, $logo, $infoBonoVentaDetalleProyecto, $reserva1DetalleProyecto, $reserva2DetalleProyecto, $reserva3DetalleProyecto, $diasCotProyecto, $diasResProyecto, $defaultPiePromesa, $defaultPieCuotas, $defaultTasa, $comisionIVADetalleProyecto, $unidadesSaltoDetalleProyecto, $comisionLVPromesaDetalleProyecto2, $comisionLVEscrituraDetalleProyecto2, $comisionVenPromesaDetalleProyecto2, $comisionVenEscrituraDetalleProyecto2){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			if($logo == ''){
				$sql = "UPDATE PROYECTO
				SET
				NOMBRE = '" . $nombreDetalleProyecto . "',
				INMOBILIARIA = '" . $inmobiliariaDetalleProyecto . "',
				EMAILINMOBILIARIA = '" . $emailInmobiliariaDetalleProyecto . "',
				DIRECCION = '" . $direccionDetalleProyecto . "',
				FONO1 = '" . $fono1InmobiliariaDetalleProyecto . "',
				FONO2 = '" . $fono2InmobiliariaDetalleProyecto . "',
				FECHAINICIOVENTA = '" . $fInicioVentaDetalleProyecto . "',
				FECHAFINOBRA = '" . $fFinObraDetalleProyecto . "',
				FECHAENTREGA = '" . $fEntregaDetalleProyecto . "',
				EMAILPROYECTO = '" . $emailDetalleProyecto . "',
				WEBPROYECTO = '". $webDetalleProyecto . "',
				COMISIONLVPROMESA = '" . $comisionLVPromesaDetalleProyecto . "',
				COMISIONLVESCRITURA = '" . $comisionLVEscrituraDetalleProyecto . "',
				COMISIONVENPROMESA = '" . $comisionVenPromesaDetalleProyecto . "',
				COMISIONVENESCRITURA = '" . $comisionVenEscrituraDetalleProyecto . "',
				BONOVENTA = '" . $bonoVentaDetalleProyecto . "',
				DESCUENTOSALA1 = '" . $descuentoSala1DetalleProyecto . "',
				DESCUENTOSALA2 = '" . $descuentoSala2DetalleProyecto . "',
			 	VALORRESERVA = '" . $descuentoSala3DetalleProyecto . "',
				DESCRIPCION1 = '" . $infoDescuento1DetalleProyecto . "',
				DESCRIPCION2 = '" . $infoDescuento2DetalleProyecto . "',
				COTIZA1 = '" . $cotiza1DetalleProyecto . "',
				COTIZA2 = '" . $cotiza2DetalleProyecto . "',
				COTIZA3 = '" . $cotiza3DetalleProyecto . "',
				ESTADO = '" . $estadoDetalleProyecto . "',
				INFOBONOVENTA = '" . $infoBonoVentaDetalleProyecto . "',
				RESERVA1 = '" . $reserva1DetalleProyecto . "',
				RESERVA2 = '" . $reserva2DetalleProyecto . "',
				RESERVA3 = '" . $reserva3DetalleProyecto . "',
				DIASCOT = '" . $diasCotProyecto . "',
				DIASRES = '" . $diasResProyecto . "',
				DEFAULTPIEPROMESA = '" . $defaultPiePromesa . "',
				DEFAULTPIECUOTAS = '" . $defaultPieCuotas . "',
				DEFAULTTASA = '" . $defaultTasa . "',
				IVA = '" . $comisionIVADetalleProyecto . "',
				SALTO = '" . $unidadesSaltoDetalleProyecto . "',
				COMISIONLVPROMESA2 = '" . $comisionLVPromesaDetalleProyecto2 . "',
				COMISIONLVESCRITURA2 = '" . $comisionLVEscrituraDetalleProyecto2 . "',
				COMISIONVENPROMESA2 = '" . $comisionVenPromesaDetalleProyecto2 . "',
				COMISIONVENESCRITURA2 = '" . $comisionVenEscrituraDetalleProyecto2 . "'
				WHERE CODIGOPROYECTO = '" . $codigoDetalleProyecto . "'";
			}
			else{
				$sql = "UPDATE PROYECTO
				SET
				NOMBRE = '" . $nombreDetalleProyecto . "',
				INMOBILIARIA = '" . $inmobiliariaDetalleProyecto . "',
				EMAILINMOBILIARIA = '" . $emailInmobiliariaDetalleProyecto . "',
				DIRECCION = '" . $direccionDetalleProyecto . "',
				FONO1 = '" . $fono1InmobiliariaDetalleProyecto . "',
				FONO2 = '" . $fono2InmobiliariaDetalleProyecto . "',
				FECHAINICIOVENTA = '" . $fInicioVentaDetalleProyecto . "',
				FECHAFINOBRA = '" . $fFinObraDetalleProyecto . "',
				FECHAENTREGA = '" . $fEntregaDetalleProyecto . "',
				EMAILPROYECTO = '" . $emailDetalleProyecto . "',
				WEBPROYECTO = '". $webDetalleProyecto . "',
				COMISIONLVPROMESA = '" . $comisionLVPromesaDetalleProyecto . "',
				COMISIONLVESCRITURA = '" . $comisionLVEscrituraDetalleProyecto . "',
				COMISIONVENPROMESA = '" . $comisionVenPromesaDetalleProyecto . "',
				COMISIONVENESCRITURA = '" . $comisionVenEscrituraDetalleProyecto . "',
				BONOVENTA = '" . $bonoVentaDetalleProyecto . "',
				DESCUENTOSALA1 = '" . $descuentoSala1DetalleProyecto . "',
				DESCUENTOSALA2 = '" . $descuentoSala2DetalleProyecto . "',
				VALORRESERVA = '" . $descuentoSala3DetalleProyecto . "',
				DESCRIPCION1 = '" . $infoDescuento1DetalleProyecto . "',
				DESCRIPCION2 = '" . $infoDescuento2DetalleProyecto . "',
				COTIZA1 = '" . $cotiza1DetalleProyecto . "',
				COTIZA2 = '" . $cotiza2DetalleProyecto . "',
				COTIZA3 = '" . $cotiza3DetalleProyecto . "',
				ESTADO = '" . $estadoDetalleProyecto . "',
				LOGO = '" . $logo . "',
				INFOBONOVENTA = '" . $infoBonoVentaDetalleProyecto . "',
				RESERVA1 = '" . $reserva1DetalleProyecto . "',
				RESERVA2 = '" . $reserva2DetalleProyecto . "',
				RESERVA3 = '" . $reserva3DetalleProyecto . "',
				DIASCOT = '" . $diasCotProyecto . "',
				DIASRES = '" . $diasResProyecto . "',
				DEFAULTPIEPROMESA = '" . $defaultPiePromesa . "',
				DEFAULTPIECUOTAS = '" . $defaultPieCuotas . "',
				DEFAULTTASA = '" . $defaultTasa . "',
				IVA = '" . $comisionIVADetalleProyecto . "',
				SALTO = '" . $unidadesSaltoDetalleProyecto . "',
				COMISIONLVPROMESA2 = '" . $comisionLVPromesaDetalleProyecto2 . "',
				COMISIONLVESCRITURA2 = '" . $comisionLVEscrituraDetalleProyecto2 . "',
				COMISIONVENPROMESA2 = '" . $comisionVenPromesaDetalleProyecto2 . "',
				COMISIONVENESCRITURA2 = '" . $comisionVenEscrituraDetalleProyecto2 . "'
				WHERE CODIGOPROYECTO = '" . $codigoDetalleProyecto . "'";
			}
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				//return $con->error;
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function actualizaEstadoCotizaPorFecha(){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "CALL ACTUALIZASEM()";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function actualizaClienteEnPromesa($rut, $codigoProyecto, $numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE PROMESA
SET IDCLIENTE1 =
(
	SELECT IDCLIENTE
	FROM CLIENTE
	WHERE RUT = '" . $rut . "'
)
WHERE NUMERO = '" . $numeroOperacion . "'
AND IDPROYECTO =
(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function actualizaPass($rut, $pass){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE USUARIO
SET CONTRASENA = '" . $pass . "'
WHERE RUT  = '" . $rut . "'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function actualizaEstadoUnidad($idUnidad, $idEstado, $con){
		if($con != 'No conectado'){
			$sql = "UPDATE UNIDAD
SET ESTADO = '" . $idEstado . "'
WHERE IDUNIDAD = '" . $idUnidad . "'
AND CODIGO <> '9999'";
			if ($con->query($sql)) {
			    return "Ok";
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function actualizaInteresCotizacion($idCotizacion, $con){
		if($con != 'No conectado'){
			$sql = "UPDATE COTIZACION
SET ESTADOSEGUIMIENTO4 = 5,
FECHASEGUIMIENTO4 = NOW()
WHERE IDCOTIZACION = '" . $idCotizacion . "'";
			if ($con->query($sql)) {
			    return "Ok";
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function actualizaRutasDocumentosReserva($rutaFicha, $rutaRCompra, $rutaCI, $rutaPB, $rutaCC, $rutaCR, $rutaCO, $idReserva){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE RESERVA
SET FICHA_PDF = '" . $rutaFicha . "',
RCOMPRA_PDF = '" . $rutaRCompra . "',
CI_PDF = '" . $rutaCI . "',
PB_PDF = '" . $rutaPB . "',
CC_PDF = '" . $rutaCC . "',
CR_PDF = '" . $rutaCR . "',
COTIZA_PDF = '" . $rutaCO . "'
WHERE IDRESERVA = '" . $idReserva . "'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
			  $con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function actualizaRutaDocumentosCotizacion($rutaCO, $idReserva){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE COTIZACION
SET RUTA_PDF = '" . $rutaCO . "'
WHERE IDCOTIZACION =
(
	SELECT IDCOTIZACION
	FROM RESERVA
	WHERE IDRESERVA = '" . $idReserva . "'
)";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
			  $con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function actualizaInteres($codigoProyecto, $codigo, $sem, $act){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = '';
			if($sem == 1){
				$sql = "UPDATE COTIZACION
				SET ESTADOSEGUIMIENTO1 = '" . $act . "'
				WHERE IDPROYECTO =
				(
				SELECT IDPROYECTO
				FROM PROYECTO
				WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
				)
				AND CODIGO = '" . $codigo . "'";
			}
			if($sem == 2){
				$sql = "UPDATE COTIZACION
				SET ESTADOSEGUIMIENTO2 = '" . $act . "'
				WHERE IDPROYECTO =
				(
				SELECT IDPROYECTO
				FROM PROYECTO
				WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
				)
				AND CODIGO = '" . $codigo . "'";
			}
			if($sem == 3){
				$sql = "UPDATE COTIZACION
				SET ESTADOSEGUIMIENTO3 = '" . $act . "'
				WHERE IDPROYECTO =
				(
				SELECT IDPROYECTO
				FROM PROYECTO
				WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
				)
				AND CODIGO = '" . $codigo . "'";
			}
			if($sem == 4){
				$sql = "UPDATE COTIZACION
				SET ESTADOSEGUIMIENTO4 = '" . $act . "'
				WHERE IDPROYECTO =
				(
				SELECT IDPROYECTO
				FROM PROYECTO
				WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
				)
				AND CODIGO = '" . $codigo . "'";
			}
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function actualizaClienteCotiza($rutCrearCliente,$nombresCrearCliente,$apellidosCrearCliente,$emailCrearCliente,$celularCrearCliente){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE CLIENTE
SET NOMBRES = '" . $nombresCrearCliente . "',
APELLIDOS = '" . $apellidosCrearCliente . "',
EMAIL = '" . $emailCrearCliente . "',
CELULAR = '" . $celularCrearCliente . "'
WHERE RUT = '" . $rutCrearCliente . "'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function actualizaIMG($codigoProyecto, $codigoTipoUnidad, $imgPlano, $imgOtra){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE UNIDAD
    SET IMGPLANO = '" . $imgPlano . "',
		IMGOTRA = '" . $imgOtra . "'
    WHERE IDPROYECTO =
		(
				SELECT IDPROYECTO
				FROM PROYECTO
				WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
		)
    AND CODIGOTIPO = '" . $codigoTipoUnidad . "'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function actualizaRCOMPRA_PDF($ruta_rcompra,$idReserva){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE RESERVA
SET RCOMPRA_PDF = '" . $ruta_rcompra . "'
WHERE IDRESERVA = " . $idReserva;
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function actualizaFICHA_PDF($ruta_ficha,$idReserva){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE RESERVA
SET FICHA_PDF = '" . $ruta_ficha . "'
WHERE IDRESERVA = " . $idReserva;
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function liberaUnidadesReservaBodega($codigoProyecto,$numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE UNIDAD
SET ESTADO = '1'
WHERE IDUNIDAD IN
(
	SELECT IDUNIDAD
	FROM RESERVABODEGA
	WHERE IDRESERVA =
	(
		SELECT IDRESERVA
		FROM RESERVA
		WHERE NUMERO = '" . $numeroOperacion . "'
		AND IDPROYECTO =
		(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
		)
	)
)";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function liberaUnidadesReservaEstacionamiento($codigoProyecto,$numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE UNIDAD
SET ESTADO = '1'
WHERE IDUNIDAD IN
(
	SELECT IDUNIDAD
	FROM RESERVAESTACIONAMIENTO
	WHERE IDRESERVA =
	(
		SELECT IDRESERVA
		FROM RESERVA
		WHERE NUMERO = '" . $numeroOperacion . "'
		AND IDPROYECTO =
		(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
		)
	)
)";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function liberaUnidadesReserva($codigoProyecto,$numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE UNIDAD
SET ESTADO = '1'
WHERE IDUNIDAD IN
(
	SELECT IDUNIDAD
	FROM RESERVA
	WHERE NUMERO = '" . $numeroOperacion . "'
	AND IDPROYECTO =
	(
		SELECT IDPROYECTO
		FROM PROYECTO
		WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
	)
)";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function actualizaRUTA_PDF_COTIZA($ruta_pdf_cotiza,$idCotizacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE COTIZACION
SET RUTA_PDF = '" . $ruta_pdf_cotiza . "'
WHERE IDCOTIZACION = " . $idCotizacion;
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function actualizaUnidad($codigoProyecto,	$tipoDetalleUnidad,	$accionDetalleUnidad,	$numeroDetalleUnidad,	$estadoDetalleUnidad,	$pisoDetalleUnidad,	$orientacionDetalleUnidad,	$cantDormitoriosDetalleUnidad,	$cantBanosDetalleUnidad,	$logiaDetalleUnidad,	$m2UtilDetalleUnidad,	$m2TerrazaDetalleUnidad,	$m2OtroDetalleUnidad,	$valorDetalleUnidad,	$comisionLVDuenoDetalleUnidad,	$comisionLVClienteDetalleUnidad,	$comisionVenDuenoDetalleUnidad,	$comisionVenClienteDetalleUnidad,	$direccionDetalleUnidad,	$comunaDetalleUnidad,	$codigoTipoDetalleUnidad,	$cantidadBodegasDetalleUnidad,	$cantidadEstacionamientosDetalleUnidad,	$descripcion1DetalleUnidad,	$descripcion2DetalleUnidad,
$imgPlano,$imgOtra,$m2TotalDetalleUnidad,$tipologiaDetalleUnidad,$rutVendedorDetalleUnidad, $nombresVendedorDetalleUnidad,$apellidosVendedorDetalleUnidad,$celularVendedorDetalleUnidad,$emailVendedorDetalleUnidad){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE UNIDAD
			SET
			IDACCION = '" . $accionDetalleUnidad . "',
			ESTADO = '" . $estadoDetalleUnidad . "',
			PISO = '". $pisoDetalleUnidad ."',
			ORIENTACION = '". $orientacionDetalleUnidad ."',
			CANTDORMITORIOS = '" . $cantDormitoriosDetalleUnidad . "',
			CANTBANOS = '" . $cantBanosDetalleUnidad . "',
			LOGIA = '" . $logiaDetalleUnidad . "',
			M2UTIL = '" . $m2UtilDetalleUnidad . "',
			M2TERRAZA = '" . $m2TerrazaDetalleUnidad . "',
			M2OTRO = '" . $m2OtroDetalleUnidad . "',
			VALORUF = '" . $valorDetalleUnidad . "',
			IMGPLANO = '" . $imgPlano . "',
			IMGOTRA = '" . $imgOtra . "',
			COMISIONLVDUENO = '" . $comisionLVDuenoDetalleUnidad . "',
			COMISIONLVCLIENTE = '" . $comisionLVClienteDetalleUnidad . "',
			COMISIONVENDUENO =  '" . $comisionVenDuenoDetalleUnidad . "',
			COMISIONVENCLIENTE =  '" . $comisionVenClienteDetalleUnidad. "',
			DESCRIPCION1 = '" . $descripcion1DetalleUnidad . "',
			DESCRIPCION2 = '" . $descripcion2DetalleUnidad ."',
			DIRECCION = '" . $direccionDetalleUnidad . "',
			COMUNA = '" . $comunaDetalleUnidad . "',
			CODIGOTIPO = '" . $codigoTipoDetalleUnidad . "',
			CANTBODEGAS = '" . $cantidadBodegasDetalleUnidad . "',
			CANTESTACIONAMIENTOS = '" . $cantidadEstacionamientosDetalleUnidad . "',
			TIPOLOGIA = '" . $tipologiaDetalleUnidad . "',
			M2TOTAL = '" . $m2TotalDetalleUnidad . "',
			RUTVENDEDOR = '" . $rutVendedorDetalleUnidad . "',
			NOMBRESVENDEDOR = '" . $nombresVendedorDetalleUnidad . "',
			APELLIDOSVENDEDOR = '" . $apellidosVendedorDetalleUnidad . "',
			CELULARVENDEDOR = '" . $celularVendedorDetalleUnidad . "',
			EMAILVENDEDOR = '" . $emailVendedorDetalleUnidad . "'
			WHERE IDPROYECTO =
			(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			AND IDTIPOUNIDAD = '" . $tipoDetalleUnidad . "'
			AND IDACCION = '" . $accionDetalleUnidad . "'
			AND CODIGO = '". $numeroDetalleUnidad . "'";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function actualizaCliente($rutDetalleCliente,$nombresDetalleCliente,$apellidosDetalleCliente,$emailDetalleCliente,$celularDetalleCliente,$domicilioDetalleCliente,$numeroDomicilioDetalleCliente,$tipoDomicilioDetalleCliente,$comunaDetalleCliente,$ciudadDetalleCliente,$paisDetalleCliente,$fNacDetalleCliente,$estadoCivilDetalleCliente,$nivelEducacionalDetalleCliente,$casaHabitacionalDetalleCliente,$motivoDetalleCliente,$profesionDetalleCliente,$institucionDetalleCliente,$actividadDetalleCliente,$nacionalidadDetalleCliente,
	$residenciaDetalleCliente,$sexoDetalleCliente,$EMRutDetalleCliente,$EMNombreDetalleCliente,$EMDomicilioDetalleCliente,$EMComunaDetalleCliente,$EMCiudadDetalleCliente,$EMPaisDetalleCliente,$EMFonoDetalleCliente,$EMGiroDetalleCliente,$EMFIngresoDetalleCliente,$EMAntiguedadDetalleCliente,$EMCargoDetalleCliente,$EMRentaLiquidaDetalleCliente,$observacionDetalleCliente,$estadoDetalleCliente,$fAltaDetalleCliente,$regionDetalleCliente){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE CLIENTE
							SET NOMBRES='" . $nombresDetalleCliente . "',
							APELLIDOS='" . $apellidosDetalleCliente . "',
							EMAIL='" . $emailDetalleCliente . "',
							CELULAR='" . $celularDetalleCliente . "',
							DOMICILIO='" . $domicilioDetalleCliente . "',
							NUMERODOMICILIO= '" . $numeroDomicilioDetalleCliente . "',
							TIPODOMICILIO='" . $tipoDomicilioDetalleCliente . "',
							COMUNA='" . $comunaDetalleCliente . "',
							CIUDAD='" . $ciudadDetalleCliente . "',
							PAIS='" . $paisDetalleCliente . "',
							FECHANAC='" . $fNacDetalleCliente . "',
							ESTADOCIVIL='" . $estadoCivilDetalleCliente . "',
							NIVELEDUCACIONAL='" . $nivelEducacionalDetalleCliente . "',
							CASAHABITA='" . $casaHabitacionalDetalleCliente . "',
							MOTIVOCOMPRA='" . $motivoDetalleCliente . "',
							PROFESION='" . $profesionDetalleCliente . "',
							INSTITUCION='" . $institucionDetalleCliente . "',
							ACTIVIDAD='" . $actividadDetalleCliente . "',
							NACIONALIDAD='" . $nacionalidadDetalleCliente . "',
							RESIDENCIA='" . $residenciaDetalleCliente . "',
							SEXO='" . $sexoDetalleCliente . "',
							EM_RUT='" . $EMRutDetalleCliente . "',
							EM_NOMBRE='" . $EMNombreDetalleCliente  . "',
							EM_DOMICILIO='" . $EMDomicilioDetalleCliente . "',
							EM_COMUNA='" . $EMComunaDetalleCliente . "',
							EM_CIUDAD='" . $EMCiudadDetalleCliente . "',
							EM_PAIS='" . $EMPaisDetalleCliente . "',
							EM_FONO='" . $EMFonoDetalleCliente . "',
							EM_GIRO='" . $EMGiroDetalleCliente . "',
							EM_FECHAINGRESO='" . $EMFIngresoDetalleCliente . "',
							EM_ANTIGUEDAD='" . $EMAntiguedadDetalleCliente . "',
							EM_CARGO='" . $EMCargoDetalleCliente . "',
							EM_RENTALIQUIDA='" . $EMRentaLiquidaDetalleCliente . "',
							OBSERVACION='" . $observacionDetalleCliente . "',
							ESTADO='" . $estadoDetalleCliente . "',
							FECHAALTA='" . $fAltaDetalleCliente .  "',
							REGION='" . $regionDetalleCliente .  "'
							WHERE RUT='" . $rutDetalleCliente . "'";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function actualizaUsuario($rutDetalleUsuario,$perfilDetalleUsuario,$nombresDetalleUsuario,$apellidosDetalleUsuario,$emailDetalleUsuario,$fonoDetalleUsuario, $actualizaUsuario){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE USUARIO
SET
IDPERFIL = '". $perfilDetalleUsuario . "',
NOMBRES = '" . $nombresDetalleUsuario . "',
APELLIDOS = '" . $apellidosDetalleUsuario . "',
EMAIL = '" . $emailDetalleUsuario . "',
FONO = '" . $fonoDetalleUsuario . "',
ESTADO = '" . $actualizaUsuario . "'
WHERE RUT = '" . $rutDetalleUsuario . "'";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	//Eliminaciones
	function eliminaProyecto($codigoProyecto){
		eliminaAsignacionPreProyecto($codigoProyecto);
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function eliminaCotizacion($codigoProyecto, $codigo){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM COTIZACION
WHERE IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND CODIGO = '" . $codigo . "'";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function eliminaReserva($codigoProyecto, $numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM RESERVA
WHERE NUMERO = '" .  $numeroOperacion. "'
AND IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function eliminaCotizacionBodega($codigoProyecto, $codigo){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM COTIZACIONBODEGA
WHERE IDCOTIZACION =
(
SELECT IDCOTIZACION
FROM COTIZACION
WHERE CODIGO = '" . $codigo . "'
AND IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
)";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function eliminaReservaBodega($codigoProyecto, $numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM RESERVABODEGA
			WHERE IDRESERVA =
			(
			SELECT IDRESERVA
			FROM RESERVA
			WHERE NUMERO = '" . $numeroOperacion . "'
			AND IDPROYECTO =
			(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			)";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function eliminaReservaEstacionamiento($codigoProyecto, $numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM RESERVAESTACIONAMIENTO
			WHERE IDRESERVA =
			(
			SELECT IDRESERVA
			FROM RESERVA
			WHERE NUMERO = '" . $numeroOperacion . "'
			AND IDPROYECTO =
			(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			)";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function eliminaCotizacionEstacionamiento($codigoProyecto, $codigo){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM COTIZACIONESTACIONAMIENTO
WHERE IDCOTIZACION =
(
SELECT IDCOTIZACION
FROM COTIZACION
WHERE CODIGO = '" . $codigo . "'
AND IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
)";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function eliminaUnidad($codigoProyecto, $codigoUnidad, $tipoUnidad, $accionUnidad){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM UNIDAD
WHERE IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND CODIGO = '" . $codigoUnidad . "'
AND IDTIPOUNIDAD =
(
SELECT IDTIPOUNIDAD
FROM TIPOUNIDAD
WHERE NOMBRE  = '" . $tipoUnidad . "'
)
AND IDACCION =
(
SELECT IDACCION
FROM ACCION
WHERE NOMBRE = '" . $accionUnidad . "'
)";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function eliminaCliente($rutCliente){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM CLIENTE WHERE RUT = '" . $rutCliente . "'";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function eliminaUsuario($rutUsuario){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM USUARIO
WHERE RUT = '" . $rutUsuario . "'";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function eliminaAsignacion($rutUsuario, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "DELETE FROM USUARIOPROYECTO
WHERE IDUSUARIO =
(
	SELECT IDUSUARIO
	FROM USUARIO
	WHERE RUT = '" . $rutUsuario . "'
)
AND IDPROYECTO =
(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)";
			if ($con->query($sql)) {
			  return "Ok";
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function eliminaAsignacionPreProyecto($codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "DELETE FROM USUARIOPROYECTO
WHERE IDPROYECTO =
(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)";
			if ($con->query($sql)) {
			  return "Ok";
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function checkAsignacionPreUser($rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT COUNT(*) CANTIDAD
FROM USUARIOPROYECTO
WHERE IDUSUARIO =
(
SELECT IDUSUARIO
FROM USUARIO
WHERE RUT = '" . $rutUsuario . "'
)";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function agregaAsignacion($rutUsuario, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "INSERT INTO USUARIOPROYECTO (IDUSUARIO, IDPROYECTO)
SELECT
(SELECT IDUSUARIO FROM USUARIO WHERE RUT = '" . $rutUsuario . "'),
(SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
";
			if ($con->query($sql)) {
			  return "Ok";
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function editarValorUnidad($codigoProyecto, $codigoUnidad, $valor){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "UPDATE UNIDAD
SET VALORUF = '" . $valor . "'
WHERE IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND CODIGO = '" . $codigoUnidad . "'";
			if ($con->query($sql)) {
			  return "Ok";
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaReservas(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, R.NUMERO, R.FECHARESERVA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
CN.ESTADONIVELINTERES 'INTERES',
GROUP_CONCAT(DISTINCT URE.CODIGO) 'EST',
GROUP_CONCAT(DISTINCT URB.CODIGO) 'BOD',
ER.COLOR 'ESTADO',
CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_FICHA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
R.FICHA_PDF,
CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_COTIZA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
R.COTIZA_PDF,
CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_RCOMPRA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
R.RCOMPRA_PDF,
CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CI_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
R.CI_PDF,
CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PB_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
R.PB_PDF,
CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CC_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
R.CC_PDF,
CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CR_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
R.CR_PDF,
R.VALORTOTALUF,
R.IDUSUARIO,
C.CELULAR, C.EMAIL, ER.NOMBRE 'ESTADO_TEXTO'
FROM RESERVA R LEFT JOIN PROYECTO P
ON R.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON R.IDUNIDAD = U.IDUNIDAD
LEFT JOIN CLIENTE C
ON R.IDCLIENTE1 = C.IDCLIENTE
LEFT JOIN ESTADORESERVA ER
ON R.ESTADO = ER.IDESTADORESERVA
LEFT JOIN COTIZACION CN
ON R.IDCOTIZACION = CN.IDCOTIZACION
LEFT JOIN RESERVAESTACIONAMIENTO RE
ON R.IDRESERVA = RE.IDRESERVA
LEFT JOIN UNIDAD URE
ON RE.IDUNIDAD = URE.IDUNIDAD
LEFT JOIN RESERVABODEGA RB
ON R.IDRESERVA = RB.IDRESERVA
LEFT JOIN UNIDAD URB
ON RB.IDUNIDAD = URB.IDUNIDAD
GROUP BY P.CODIGOPROYECTO, R.NUMERO, R.FECHARESERVA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, CN.ESTADONIVELINTERES, ER.NOMBRE, R.IDRESERVA, R.IDUSUARIO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaReservasPro($codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, R.NUMERO, R.FECHARESERVA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
CN.ESTADONIVELINTERES 'INTERES',
GROUP_CONCAT(DISTINCT URE.CODIGO) 'EST',
GROUP_CONCAT(DISTINCT URB.CODIGO) 'BOD',
ER.COLOR 'ESTADO',
CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_FICHA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
R.FICHA_PDF,
CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_COTIZA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
R.COTIZA_PDF,
CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_RCOMPRA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
R.RCOMPRA_PDF,
CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CI_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
R.CI_PDF,
CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PB_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
R.PB_PDF,
CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CC_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
R.CC_PDF,
CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CR_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
R.CR_PDF,
R.VALORTOTALUF,
R.IDUSUARIO,
C.CELULAR, C.EMAIL, ER.NOMBRE 'ESTADO_TEXTO'
FROM RESERVA R LEFT JOIN PROYECTO P
ON R.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON R.IDUNIDAD = U.IDUNIDAD
LEFT JOIN CLIENTE C
ON R.IDCLIENTE1 = C.IDCLIENTE
LEFT JOIN ESTADORESERVA ER
ON R.ESTADO = ER.IDESTADORESERVA
LEFT JOIN COTIZACION CN
ON R.IDCOTIZACION = CN.IDCOTIZACION
LEFT JOIN RESERVAESTACIONAMIENTO RE
ON R.IDRESERVA = RE.IDRESERVA
LEFT JOIN UNIDAD URE
ON RE.IDUNIDAD = URE.IDUNIDAD
LEFT JOIN RESERVABODEGA RB
ON R.IDRESERVA = RB.IDRESERVA
LEFT JOIN UNIDAD URB
ON RB.IDUNIDAD = URB.IDUNIDAD
WHERE P.CODIGOPROYECTO = '" . $codigoProyecto  . "'
GROUP BY P.CODIGOPROYECTO, R.NUMERO, R.FECHARESERVA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, CN.ESTADONIVELINTERES, ER.NOMBRE, R.IDRESERVA, R.IDUSUARIO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaReservasUsu($rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, R.NUMERO, R.FECHARESERVA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
CN.ESTADONIVELINTERES 'INTERES',
GROUP_CONCAT(DISTINCT URE.CODIGO) 'EST',
GROUP_CONCAT(DISTINCT URB.CODIGO) 'BOD',
ER.COLOR 'ESTADO',
CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_FICHA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
R.FICHA_PDF,
CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_COTIZA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
R.COTIZA_PDF,
CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_RCOMPRA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
R.RCOMPRA_PDF,
CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CI_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
R.CI_PDF,
CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PB_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
R.PB_PDF,
CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CC_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
R.CC_PDF,
CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CR_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
R.CR_PDF,
R.VALORTOTALUF,
R.IDUSUARIO,
C.CELULAR, C.EMAIL, ER.NOMBRE 'ESTADO_TEXTO'
FROM RESERVA R LEFT JOIN PROYECTO P
ON R.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON R.IDUNIDAD = U.IDUNIDAD
LEFT JOIN CLIENTE C
ON R.IDCLIENTE1 = C.IDCLIENTE
LEFT JOIN ESTADORESERVA ER
ON R.ESTADO = ER.IDESTADORESERVA
LEFT JOIN COTIZACION CN
ON R.IDCOTIZACION = CN.IDCOTIZACION
LEFT JOIN RESERVAESTACIONAMIENTO RE
ON R.IDRESERVA = RE.IDRESERVA
LEFT JOIN UNIDAD URE
ON RE.IDUNIDAD = URE.IDUNIDAD
LEFT JOIN RESERVABODEGA RB
ON R.IDRESERVA = RB.IDRESERVA
LEFT JOIN UNIDAD URB
ON RB.IDUNIDAD = URB.IDUNIDAD
WHERE R.IDPROYECTO IN
(
	SELECT IDPROYECTO
	FROM USUARIOPROYECTO
	WHERE IDUSUARIO =
	(
		SELECT IDUSUARIO
		FROM USUARIO
		WHERE RUT = '" . $rutUsuario . "'
	)
)
GROUP BY P.CODIGOPROYECTO, R.NUMERO, R.FECHARESERVA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, CN.ESTADONIVELINTERES, ER.NOMBRE, R.IDRESERVA, R.IDUSUARIO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaReservasUsuPro($rutUsuario, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, R.NUMERO, R.FECHARESERVA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
CN.ESTADONIVELINTERES 'INTERES',
GROUP_CONCAT(DISTINCT URE.CODIGO) 'EST',
GROUP_CONCAT(DISTINCT URB.CODIGO) 'BOD',
ER.COLOR 'ESTADO',
CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_FICHA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
R.FICHA_PDF,
CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_COTIZA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
R.COTIZA_PDF,
CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_RCOMPRA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
R.RCOMPRA_PDF,
CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CI_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
R.CI_PDF,
CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PB_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
R.PB_PDF,
CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CC_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
R.CC_PDF,
CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CR_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
R.CR_PDF,
R.VALORTOTALUF,
R.IDUSUARIO,
C.CELULAR, C.EMAIL, ER.NOMBRE 'ESTADO_TEXTO'
FROM RESERVA R LEFT JOIN PROYECTO P
ON R.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON R.IDUNIDAD = U.IDUNIDAD
LEFT JOIN CLIENTE C
ON R.IDCLIENTE1 = C.IDCLIENTE
LEFT JOIN ESTADORESERVA ER
ON R.ESTADO = ER.IDESTADORESERVA
LEFT JOIN COTIZACION CN
ON R.IDCOTIZACION = CN.IDCOTIZACION
LEFT JOIN RESERVAESTACIONAMIENTO RE
ON R.IDRESERVA = RE.IDRESERVA
LEFT JOIN UNIDAD URE
ON RE.IDUNIDAD = URE.IDUNIDAD
LEFT JOIN RESERVABODEGA RB
ON R.IDRESERVA = RB.IDRESERVA
LEFT JOIN UNIDAD URB
ON RB.IDUNIDAD = URB.IDUNIDAD
WHERE R.IDPROYECTO IN
(
	SELECT IDPROYECTO
	FROM USUARIOPROYECTO
	WHERE IDUSUARIO =
	(
		SELECT IDUSUARIO
		FROM USUARIO
		WHERE RUT = '" . $rutUsuario . "'
	)
)
AND P.CODIGOPROYECTO = '" . $codigoProyecto . "'
GROUP BY P.CODIGOPROYECTO, R.NUMERO, R.FECHARESERVA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, CN.ESTADONIVELINTERES, ER.NOMBRE, R.IDRESERVA, R.IDUSUARIO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaReservasCli($rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, R.NUMERO, R.FECHARESERVA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
CN.ESTADONIVELINTERES 'INTERES',
GROUP_CONCAT(DISTINCT URE.CODIGO) 'EST',
GROUP_CONCAT(DISTINCT URB.CODIGO) 'BOD',
ER.COLOR 'ESTADO',
CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_FICHA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
R.FICHA_PDF,
CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_COTIZA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
R.COTIZA_PDF,
CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_RCOMPRA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
R.RCOMPRA_PDF,
CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CI_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
R.CI_PDF,
CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PB_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
R.PB_PDF,
CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CC_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
R.CC_PDF,
CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CR_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
R.CR_PDF,
R.VALORTOTALUF,
R.IDUSUARIO,
C.CELULAR, C.EMAIL, ER.NOMBRE 'ESTADO_TEXTO'
FROM RESERVA R LEFT JOIN PROYECTO P
ON R.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON R.IDUNIDAD = U.IDUNIDAD
LEFT JOIN CLIENTE C
ON R.IDCLIENTE1 = C.IDCLIENTE
LEFT JOIN ESTADORESERVA ER
ON R.ESTADO = ER.IDESTADORESERVA
LEFT JOIN COTIZACION CN
ON R.IDCOTIZACION = CN.IDCOTIZACION
LEFT JOIN RESERVAESTACIONAMIENTO RE
ON R.IDRESERVA = RE.IDRESERVA
LEFT JOIN UNIDAD URE
ON RE.IDUNIDAD = URE.IDUNIDAD
LEFT JOIN RESERVABODEGA RB
ON R.IDRESERVA = RB.IDRESERVA
LEFT JOIN UNIDAD URB
ON RB.IDUNIDAD = URB.IDUNIDAD
WHERE R.IDCLIENTE1 IN
(
		SELECT IDCLIENTE
		FROM CLIENTE
		WHERE RUT = '" . $rutUsuario . "'
)
GROUP BY P.CODIGOPROYECTO, R.NUMERO, R.FECHARESERVA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, CN.ESTADONIVELINTERES, ER.NOMBRE, R.IDRESERVA, R.IDUSUARIO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaReservasCliPro($rutUsuario, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, R.NUMERO, R.FECHARESERVA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
CN.ESTADONIVELINTERES 'INTERES',
GROUP_CONCAT(DISTINCT URE.CODIGO) 'EST',
GROUP_CONCAT(DISTINCT URB.CODIGO) 'BOD',
ER.COLOR 'ESTADO',
CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_FICHA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
R.FICHA_PDF,
CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_COTIZA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
R.COTIZA_PDF,
CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_RCOMPRA_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
R.RCOMPRA_PDF,
CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CI_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
R.CI_PDF,
CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PB_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
R.PB_PDF,
CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CC_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
R.CC_PDF,
CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CR_RESERVA()''></span>'
ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
R.CR_PDF,
R.VALORTOTALUF,
R.IDUSUARIO,
C.CELULAR, C.EMAIL, ER.NOMBRE 'ESTADO_TEXTO'
FROM RESERVA R LEFT JOIN PROYECTO P
ON R.IDPROYECTO = P.IDPROYECTO
LEFT JOIN UNIDAD U
ON R.IDUNIDAD = U.IDUNIDAD
LEFT JOIN CLIENTE C
ON R.IDCLIENTE1 = C.IDCLIENTE
LEFT JOIN ESTADORESERVA ER
ON R.ESTADO = ER.IDESTADORESERVA
LEFT JOIN COTIZACION CN
ON R.IDCOTIZACION = CN.IDCOTIZACION
LEFT JOIN RESERVAESTACIONAMIENTO RE
ON R.IDRESERVA = RE.IDRESERVA
LEFT JOIN UNIDAD URE
ON RE.IDUNIDAD = URE.IDUNIDAD
LEFT JOIN RESERVABODEGA RB
ON R.IDRESERVA = RB.IDRESERVA
LEFT JOIN UNIDAD URB
ON RB.IDUNIDAD = URB.IDUNIDAD
WHERE R.IDCLIENTE1 IN
(
		SELECT IDCLIENTE
		FROM CLIENTE
		WHERE RUT = '" . $rutUsuario . "'
)
AND P.CODIGOPROYECTO = '" . $codigoProyecto . "'
GROUP BY P.CODIGOPROYECTO, R.NUMERO, R.FECHARESERVA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, CN.ESTADONIVELINTERES, ER.NOMBRE, R.IDRESERVA, R.IDUSUARIO";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCotizacionEspecifica($codigoProyecto, $numeroCotiza){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM COTIZACION
WHERE IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND CODIGO = '" . $numeroCotiza . "'";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCotizacionEspecificaID($idCotizacion){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM COTIZACION
WHERE IDCOTIZACION = '" . $idCotizacion . "'";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaProyectoEspecifico($idProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM PROYECTO
WHERE IDPROYECTO = '" . $idProyecto . "'";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaProyectoEspecificoCodigo($codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM PROYECTO
WHERE IDPROYECTO =
(
		SELECT IDPROYECTO
		FROM PROYECTO
		WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaClienteEspecifico($idCliente){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM CLIENTE
WHERE IDCLIENTE = '" . $idCliente . "'";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaClienteEspecificoRut($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM CLIENTE
WHERE RUT = '" . $rut . "'";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaUnidadEspecifica($idUnidad){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT U.*,T.NOMBRE 'TIPOLOGIADET', A.NOMBRE 'ACCIONDET', V.NOMBRE 'TIPOUNIDAD'
FROM UNIDAD U, TIPOLOGIA T, ACCION A, TIPOUNIDAD V
WHERE U.TIPOLOGIA = T.IDTIPOLOGIA
AND U.IDACCION = A.IDACCION
AND U.IDTIPOUNIDAD = V.IDTIPOUNIDAD
AND U.IDUNIDAD = '" . $idUnidad . "'";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaUnidadEspecificaReserva($idUnidad){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT U.*
FROM UNIDAD U
WHERE U.IDUNIDAD = '" . $idUnidad . "'";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCotizacionBodega($idCotizacion){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM COTIZACIONBODEGA
WHERE IDCOTIZACION = '" . $idCotizacion . "'";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCotizacionEstacionamiento($idCotizacion){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
FROM COTIZACIONESTACIONAMIENTO
WHERE IDCOTIZACION = '" . $idCotizacion . "'";
			if ($row = $con->query($sql)) {
			   	while($array = $row->fetch_array(MYSQLI_BOTH)){
			   		$return[] = $array;
			   	}

			   	return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function actualizaClienteReserva($rutDetalleCliente,$nombresDetalleCliente,$apellidosDetalleCliente,$emailDetalleCliente,$celularDetalleCliente,$domicilioDetalleCliente,$numeroDomicilioDetalleCliente,$comunaDetalleCliente,$ciudadDetalleCliente,$paisDetalleCliente,$estadoCivilDetalleCliente,$nivelEducacionalDetalleCliente,$casaHabitacionalDetalleCliente,$motivoDetalleCliente,$profesionDetalleCliente,$institucionDetalleCliente,$nacionalidadDetalleCliente,
	$sexoDetalleCliente,$regionDetalleCliente,$fNacDetalleCliente,$tipoDomicilioDetalleCliente,$residenciaDetalleCliente, $actividadDetalleCliente){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE CLIENTE
							SET NOMBRES='" . $nombresDetalleCliente . "',
							APELLIDOS='" . $apellidosDetalleCliente . "',
							EMAIL='" . $emailDetalleCliente . "',
							CELULAR='" . $celularDetalleCliente . "',
							DOMICILIO='" . $domicilioDetalleCliente . "',
							NUMERODOMICILIO= '" . $numeroDomicilioDetalleCliente . "',
							TIPODOMICILIO='" . $tipoDomicilioDetalleCliente . "',
							COMUNA='" . $comunaDetalleCliente . "',
							CIUDAD='" . $ciudadDetalleCliente . "',
							PAIS='" . $paisDetalleCliente . "',
							ESTADOCIVIL='" . $estadoCivilDetalleCliente . "',
							NIVELEDUCACIONAL='" . $nivelEducacionalDetalleCliente . "',
							CASAHABITA='" . $casaHabitacionalDetalleCliente . "',
							MOTIVOCOMPRA='" . $motivoDetalleCliente . "',
							PROFESION='" . $profesionDetalleCliente . "',
							INSTITUCION='" . $institucionDetalleCliente . "',
							NACIONALIDAD='" . $nacionalidadDetalleCliente . "',
							SEXO='" . $sexoDetalleCliente . "',
							REGION='" . $regionDetalleCliente .  "',
							FECHANAC='" . $fNacDetalleCliente . "',
							TIPODOMICILIO='" . $tipoDomicilioDetalleCliente . "',
							RESIDENCIA='" . $residenciaDetalleCliente . "',
							ACTIVIDAD='" . $actividadDetalleCliente . "'
							WHERE RUT='" . $rutDetalleCliente . "'";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function consultaReservaEspecificaCargaDocs($codigoProyecto, $numeroReserva){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT R.IDRESERVA, R.FICHA_PDF, R.COTIZA_PDF, R.RCOMPRA_PDF, R.CI_PDF, R.PB_PDF, R.CR_PDF, R.CC_PDF, U.CODIGO, C.NOMBRES, C.APELLIDOS
FROM RESERVA R, UNIDAD U, CLIENTE C
WHERE R.IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND R.NUMERO = '" . $numeroReserva . "'
AND R.IDUNIDAD = U.IDUNIDAD
AND R.IDCLIENTE1 = C.IDCLIENTE";
				if ($row = $con->query($sql)) {
				   	while($array = $row->fetch_array(MYSQLI_BOTH)){
				   		$return[] = $array;
				   	}

				   	return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function cambiarEstadoReserva($codigoProyecto, $numeroOperacion, $estadoReserva){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "UPDATE RESERVA
SET ESTADO = '" . $estadoReserva . "'
WHERE IDPROYECTO =
(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND NUMERO = '" . $numeroOperacion . "'";
				if ($con->query($sql)) {
				    $con->query("COMMIT");
				    return "Ok";
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}

		function cambiarEstadoPromesa($codigoProyecto, $numeroOperacion, $estadoPromesa){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "UPDATE PROMESA
SET ESTADO = '" . $estadoPromesa . "'
WHERE IDPROYECTO =
(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND NUMERO = '" . $numeroOperacion . "'";
				if ($con->query($sql)) {
				    $con->query("COMMIT");
				    return "Ok";
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}


		function retornaEstadoCotizacion($codigoProyecto, $numeroOperacion){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "UPDATE COTIZACION
SET ESTADOSEGUIMIENTO4 = 2
WHERE IDCOTIZACION =
(
	SELECT IDCOTIZACION
	FROM RESERVA
	WHERE IDPROYECTO =
	(
		SELECT IDPROYECTO
		FROM PROYECTO
		WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
	)
	AND NUMERO = '" . $numeroOperacion . "'
)";
				if ($con->query($sql)) {
				    $con->query("COMMIT");
				    return "Ok";
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}

		function consultaBodegasCotizacionReserva($codigoProyecto, $numeroCotiza){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT R.IDUNIDAD, U.CODIGO, R.VALORUF
FROM UNIDAD U JOIN COTIZACIONBODEGA R ON
U.IDUNIDAD = R.IDUNIDAD
WHERE IDCOTIZACION =
(
	SELECT IDCOTIZACION
	FROM COTIZACION
	WHERE CODIGO = '" . $numeroCotiza . "'
	AND IDPROYECTO =
	(
		SELECT IDPROYECTO
		FROM PROYECTO
		WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
	)
)";
			if ($row = $con->query($sql)) {
					while($array = $row->fetch_array(MYSQLI_BOTH)){
						$return[] = $array;
					}

					return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaEstacionamientpsCotizacionReserva($codigoProyecto, $numeroCotiza){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT R.IDUNIDAD, U.CODIGO, R.VALORUF
FROM UNIDAD U JOIN COTIZACIONESTACIONAMIENTO R ON
U.IDUNIDAD = R.IDUNIDAD
WHERE IDCOTIZACION =
(
SELECT IDCOTIZACION
FROM COTIZACION
WHERE CODIGO = '" . $numeroCotiza . "'
AND IDPROYECTO =
(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
)";
		if ($row = $con->query($sql)) {
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function consultaUsuariosVendedores(){
$con = conectar();
if($con != 'No conectado'){
	$sql = "SELECT U.IDUSUARIO, CONCAT(UCASE(LEFT(U.NOMBRES, 1)), LCASE(SUBSTRING(U.NOMBRES, 2)), ' ',
UCASE(LEFT(U.APELLIDOS, 1)), LCASE(SUBSTRING(U.APELLIDOS, 2))) 'NOMBRE', P.*
FROM USUARIO U
LEFT JOIN PERFIL P
ON U.IDPERFIL = P.IDPERFIL
WHERE ESTADO <> 'Bloqueado'
AND P.NOMBREPERFIL IN
(
'Super Administrador',
'Administrador',
'Usuario',
'Corredor'
)";
	if ($row = $con->query($sql)) {
			while($array = $row->fetch_array(MYSQLI_BOTH)){
				$return[] = $array;
			}

			return $return;
	}
	else{
		return "Error";
	}
}
else{
	return "Error";
}
}

function consultaBancos(){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT *
						FROM BANCO";
			if ($row = $con->query($sql)) {
					while($array = $row->fetch_array(MYSQLI_BOTH)){
						$return[] = $array;
					}

					return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
}

function fechaMinComision(){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT CASE WHEN  MIN(FECHACOMISIONPROMESA) IS NULL THEN
CAST(NOW() AS DATE) ELSE MIN(FECHACOMISIONPROMESA)  END 'FECHAMIN'
FROM COMISIONPROMESA";
		if ($row = $con->query($sql)) {

			$array = $row->fetch_array(MYSQLI_BOTH);

			return $array;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function fechaMaxComision(){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT CASE WHEN  MAX(FECHACOMISIONESCRITURA) IS NULL THEN
CAST(NOW() AS DATE) ELSE MAX(FECHACOMISIONESCRITURA)  END 'FECHAMAX'
FROM COMISIONESCRITURA";
		if ($row = $con->query($sql)) {

			$array = $row->fetch_array(MYSQLI_BOTH);

			return $array;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function consultaComisionesVendedorMesAnoInforme($codigoProyecto, $fechaIni, $fechaFin){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT S.RUT, CONCAT(S.NOMBRES, ' ', S.APELLIDOS) 'NOMBRE', P.CODIGOPROYECTO,
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.NUMERO ELSE ES.NUMERO END 'NUMERO',
U.CODIGO 'DEPTO',
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN GROUP_CONCAT(DISTINCT UPE.CODIGO) ELSE GROUP_CONCAT(DISTINCT UEE.CODIGO) END 'EST',
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN GROUP_CONCAT(DISTINCT UPB.CODIGO) ELSE GROUP_CONCAT(DISTINCT UEB.CODIGO) END 'BOD',
ROUND((CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.VALORTOTALUF ELSE ES.VALORTOTALUF END)
/
(((CASE WHEN P.IVA IS NULL THEN 0  ELSE P.IVA END)/100)+1),2) 'VALORNETOUF',
CASE WHEN (CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "') THEN CP.FECHACOMISIONPROMESA ELSE NULL END 'FECHACOMISIONPROMESA',
CASE WHEN (CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "') THEN ROUND(CP.PORCENTAJECOMISIONVENDEDOR,3) ELSE NULL END 'COMISIONPROMESA',
CASE WHEN (CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "') THEN ROUND(CP.MONTOUFVENDEDOR,2) ELSE NULL END 'UFPROMESA',
CP.MONTOPESOSVENDEDOR 'PESOSPROMESA',
CASE WHEN (CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "') THEN CP.ESTADOPAGO ELSE NULL END 'ESTADOPROMESA',
CASE WHEN (CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "')  THEN CE.FECHACOMISIONESCRITURA ELSE NULL END 'FECHACOMISIONESCRITURA',
CASE WHEN (CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "')  THEN ROUND(CE.PORCENTAJECOMISIONVENDEDOR,3) ELSE NULL END 'COMISIONESCRITURA',
CASE WHEN (CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "') THEN ROUND(CE.MONTOUFVENDEDOR,2) ELSE NULL END 'UFESCRITURA',
CE.MONTOPESOSVENDEDOR 'PESOSESCRITURA',
CASE WHEN (CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "') THEN CE.ESTADOPAGO ELSE NULL END 'ESTADOESCRITURA'
FROM USUARIO S
LEFT JOIN COMISIONESCRITURA CE
ON S.IDUSUARIO = CE.IDUSUARIO
LEFT JOIN COMISIONPROMESA CP
ON S.IDUSUARIO = CP.IDUSUARIO
AND CE.IDPROMESA = CP.IDPROMESA
LEFT JOIN PROYECTO P
ON CP.IDPROYECTO = P.IDPROYECTO
LEFT JOIN PROMESA PR
ON CP.IDPROMESA = PR.IDPROMESA
LEFT JOIN ESCRITURA ES
ON CE.IDESCRITURA = ES.IDESCRITURA
LEFT JOIN UNIDAD U
ON CASE WHEN PR.IDUNIDAD IS NULL THEN ES.IDUNIDAD ELSE PR.IDUNIDAD END = U.IDUNIDAD
LEFT JOIN PROMESAESTACIONAMIENTO PE
ON PR.IDPROMESA = PE.IDPROMESA
LEFT JOIN UNIDAD UPE
ON PE.IDUNIDAD = UPE.IDUNIDAD
LEFT JOIN PROMESABODEGA PB
ON PR.IDPROMESA = PB.IDPROMESA
LEFT JOIN UNIDAD UPB
ON PB.IDUNIDAD = UPB.IDUNIDAD
LEFT JOIN ESCRITURAESTACIONAMIENTO EE
ON ES.IDESCRITURA = EE.IDESCRITURA
LEFT JOIN UNIDAD UEE
ON EE.IDUNIDAD = UEE.IDUNIDAD
LEFT JOIN ESCRITURABODEGA EB
ON ES.IDESCRITURA = EB.IDESCRITURA
LEFT JOIN UNIDAD UEB
ON EB.IDUNIDAD = UEB.IDUNIDAD
WHERE
(
(CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "')
OR
(CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "')
)
AND P.IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.NUMERO ELSE ES.NUMERO END IS NOT NULL
GROUP BY S.RUT, S.NOMBRES, S.APELLIDOS, P.CODIGOPROYECTO, PR.NUMERO, U.CODIGO, PR.VALORTOTALUF, CP.FECHACOMISIONPROMESA, CP.PORCENTAJECOMISIONVENDEDOR, CP.MONTOUFVENDEDOR,
CP.MONTOPESOSVENDEDOR, CP.ESTADOPAGO, CE.FECHACOMISIONESCRITURA, CE.PORCENTAJECOMISIONVENDEDOR, CE.MONTOUFVENDEDOR, CE.MONTOPESOSVENDEDOR,
CE.ESTADOPAGO, PR.IDUNIDAD, ES.NUMERO, ES.VALORTOTALUF, P.IVA";
		if ($row = $con->query($sql)) {
			while($array = $row->fetch_array(MYSQLI_BOTH)){
				$return[] = $array;
			}

			return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function consultaComisionesVendedorMesAnoInformeTodos($fechaIni, $fechaFin){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT S.RUT, CONCAT(S.NOMBRES, ' ', S.APELLIDOS) 'NOMBRE', P.CODIGOPROYECTO,
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.NUMERO ELSE ES.NUMERO END 'NUMERO',
U.CODIGO 'DEPTO',
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN GROUP_CONCAT(DISTINCT UPE.CODIGO) ELSE GROUP_CONCAT(DISTINCT UEE.CODIGO) END 'EST',
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN GROUP_CONCAT(DISTINCT UPB.CODIGO) ELSE GROUP_CONCAT(DISTINCT UEB.CODIGO) END 'BOD',
ROUND((CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.VALORTOTALUF ELSE ES.VALORTOTALUF END)
/
(((CASE WHEN P.IVA IS NULL THEN 0  ELSE P.IVA END)/100)+1),2) 'VALORNETOUF',
CASE WHEN (CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "') THEN CP.FECHACOMISIONPROMESA ELSE NULL END 'FECHACOMISIONPROMESA',
CASE WHEN (CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "') THEN ROUND(CP.PORCENTAJECOMISIONVENDEDOR,3) ELSE NULL END 'COMISIONPROMESA',
CASE WHEN (CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "') THEN ROUND(CP.MONTOUFVENDEDOR,2) ELSE NULL END 'UFPROMESA',
CP.MONTOPESOSVENDEDOR 'PESOSPROMESA',
CASE WHEN (CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "') THEN CP.ESTADOPAGO ELSE NULL END 'ESTADOPROMESA',
CASE WHEN (CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "')  THEN CE.FECHACOMISIONESCRITURA ELSE NULL END 'FECHACOMISIONESCRITURA',
CASE WHEN (CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "')  THEN ROUND(CE.PORCENTAJECOMISIONVENDEDOR,3) ELSE NULL END 'COMISIONESCRITURA',
CASE WHEN (CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "') THEN ROUND(CE.MONTOUFVENDEDOR,2) ELSE NULL END 'UFESCRITURA',
CE.MONTOPESOSVENDEDOR 'PESOSESCRITURA',
CASE WHEN (CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "') THEN CE.ESTADOPAGO ELSE NULL END 'ESTADOESCRITURA'
FROM USUARIO S
LEFT JOIN COMISIONESCRITURA CE
ON S.IDUSUARIO = CE.IDUSUARIO
LEFT JOIN COMISIONPROMESA CP
ON S.IDUSUARIO = CP.IDUSUARIO
AND CE.IDPROMESA = CP.IDPROMESA
LEFT JOIN PROYECTO P
ON CP.IDPROYECTO = P.IDPROYECTO
LEFT JOIN PROMESA PR
ON CP.IDPROMESA = PR.IDPROMESA
LEFT JOIN ESCRITURA ES
ON CE.IDESCRITURA = ES.IDESCRITURA
LEFT JOIN UNIDAD U
ON CASE WHEN PR.IDUNIDAD IS NULL THEN ES.IDUNIDAD ELSE PR.IDUNIDAD END = U.IDUNIDAD
LEFT JOIN PROMESAESTACIONAMIENTO PE
ON PR.IDPROMESA = PE.IDPROMESA
LEFT JOIN UNIDAD UPE
ON PE.IDUNIDAD = UPE.IDUNIDAD
LEFT JOIN PROMESABODEGA PB
ON PR.IDPROMESA = PB.IDPROMESA
LEFT JOIN UNIDAD UPB
ON PB.IDUNIDAD = UPB.IDUNIDAD
LEFT JOIN ESCRITURAESTACIONAMIENTO EE
ON ES.IDESCRITURA = EE.IDESCRITURA
LEFT JOIN UNIDAD UEE
ON EE.IDUNIDAD = UEE.IDUNIDAD
LEFT JOIN ESCRITURABODEGA EB
ON ES.IDESCRITURA = EB.IDESCRITURA
LEFT JOIN UNIDAD UEB
ON EB.IDUNIDAD = UEB.IDUNIDAD
WHERE
(
(CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "')
OR
(CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "')
)
AND CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.NUMERO ELSE ES.NUMERO END IS NOT NULL
GROUP BY S.RUT, S.NOMBRES, S.APELLIDOS, P.CODIGOPROYECTO, PR.NUMERO, U.CODIGO, PR.VALORTOTALUF, CP.FECHACOMISIONPROMESA, CP.PORCENTAJECOMISIONVENDEDOR, CP.MONTOUFVENDEDOR,
CP.MONTOPESOSVENDEDOR, CP.ESTADOPAGO, CE.FECHACOMISIONESCRITURA, CE.PORCENTAJECOMISIONVENDEDOR, CE.MONTOUFVENDEDOR, CE.MONTOPESOSVENDEDOR,
CE.ESTADOPAGO, PR.IDUNIDAD, ES.NUMERO, ES.VALORTOTALUF, P.IVA";
		if ($row = $con->query($sql)) {
			while($array = $row->fetch_array(MYSQLI_BOTH)){
				$return[] = $array;
			}

			return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function consultaComisionesEmpresaMesAnoInforme($codigoProyecto, $fechaIni, $fechaFin){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT P.CODIGOPROYECTO,
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.NUMERO ELSE ES.NUMERO END 'NUMERO',
U.CODIGO 'DEPTO',
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN GROUP_CONCAT(DISTINCT UPE.CODIGO) ELSE GROUP_CONCAT(DISTINCT UEE.CODIGO) END 'EST',
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN GROUP_CONCAT(DISTINCT UPB.CODIGO) ELSE GROUP_CONCAT(DISTINCT UEB.CODIGO) END 'BOD',
ROUND((CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.VALORTOTALUF ELSE ES.VALORTOTALUF END)
/
(((CASE WHEN P.IVA IS NULL THEN 0  ELSE P.IVA END)/100)+1),2) 'VALORNETOUF',
CASE WHEN (CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "') THEN CP.FECHACOMISIONPROMESA ELSE NULL END 'FECHACOMISIONPROMESA',
CASE WHEN (CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "') THEN ROUND(CP.PORCENTAJECOMISIONEMPRESA,3) ELSE NULL END 'COMISIONPROMESA',
CASE WHEN (CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "') THEN ROUND(CP.MONTOUFEMPRESA,2) ELSE NULL END 'UFPROMESA',
CP.MONTOPESOSEMPRESA 'PESOSPROMESA',
CASE WHEN (CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "') THEN CP.ESTADOPAGO ELSE NULL END 'ESTADOPROMESA',
CASE WHEN (CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "')  THEN CE.FECHACOMISIONESCRITURA ELSE NULL END 'FECHACOMISIONESCRITURA',
CASE WHEN (CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "')  THEN ROUND(CE.PORCENTAJECOMISIONEMPRESA,3) ELSE NULL END 'COMISIONESCRITURA',
CASE WHEN (CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "') THEN ROUND(CE.MONTOUFEMPRESA,2) ELSE NULL END 'UFESCRITURA',
CE.MONTOPESOSEMPRESA 'PESOSESCRITURA',
CASE WHEN (CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "') THEN CE.ESTADOPAGO ELSE NULL END 'ESTADOESCRITURA'
FROM USUARIO S
LEFT JOIN COMISIONESCRITURA CE
ON S.IDUSUARIO = CE.IDUSUARIO
LEFT JOIN COMISIONPROMESA CP
ON S.IDUSUARIO = CP.IDUSUARIO
AND CE.IDPROMESA = CP.IDPROMESA
LEFT JOIN PROYECTO P
ON CP.IDPROYECTO = P.IDPROYECTO
LEFT JOIN PROMESA PR
ON CP.IDPROMESA = PR.IDPROMESA
LEFT JOIN ESCRITURA ES
ON CE.IDESCRITURA = ES.IDESCRITURA
LEFT JOIN UNIDAD U
ON CASE WHEN PR.IDUNIDAD IS NULL THEN ES.IDUNIDAD ELSE PR.IDUNIDAD END = U.IDUNIDAD
LEFT JOIN PROMESAESTACIONAMIENTO PE
ON PR.IDPROMESA = PE.IDPROMESA
LEFT JOIN UNIDAD UPE
ON PE.IDUNIDAD = UPE.IDUNIDAD
LEFT JOIN PROMESABODEGA PB
ON PR.IDPROMESA = PB.IDPROMESA
LEFT JOIN UNIDAD UPB
ON PB.IDUNIDAD = UPB.IDUNIDAD
LEFT JOIN ESCRITURAESTACIONAMIENTO EE
ON ES.IDESCRITURA = EE.IDESCRITURA
LEFT JOIN UNIDAD UEE
ON EE.IDUNIDAD = UEE.IDUNIDAD
LEFT JOIN ESCRITURABODEGA EB
ON ES.IDESCRITURA = EB.IDESCRITURA
LEFT JOIN UNIDAD UEB
ON EB.IDUNIDAD = UEB.IDUNIDAD
WHERE
(
(CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "')
OR
(CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "')
)
AND P.IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.NUMERO ELSE ES.NUMERO END IS NOT NULL
GROUP BY S.RUT, S.NOMBRES, S.APELLIDOS, P.CODIGOPROYECTO, PR.NUMERO, U.CODIGO, PR.VALORTOTALUF, CP.FECHACOMISIONPROMESA, CP.PORCENTAJECOMISIONEMPRESA,
CP.MONTOUFEMPRESA,CP.MONTOPESOSEMPRESA, CP.ESTADOPAGO, CE.FECHACOMISIONESCRITURA, CE.PORCENTAJECOMISIONEMPRESA,
CE.MONTOUFEMPRESA, CE.MONTOPESOSEMPRESA,CE.ESTADOPAGO, PR.IDUNIDAD, ES.NUMERO, ES.VALORTOTALUF, P.IVA";
		if ($row = $con->query($sql)) {
			while($array = $row->fetch_array(MYSQLI_BOTH)){
				$return[] = $array;
			}

			return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}

function consultaComisionesEmpresaMesAnoInformeTodos($fechaIni, $fechaFin){
	$con = conectar();
	if($con != 'No conectado'){
		$sql = "SELECT P.CODIGOPROYECTO,
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.NUMERO ELSE ES.NUMERO END 'NUMERO',
U.CODIGO 'DEPTO',
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN GROUP_CONCAT(DISTINCT UPE.CODIGO) ELSE GROUP_CONCAT(DISTINCT UEE.CODIGO) END 'EST',
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN GROUP_CONCAT(DISTINCT UPB.CODIGO) ELSE GROUP_CONCAT(DISTINCT UEB.CODIGO) END 'BOD',
ROUND((CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.VALORTOTALUF ELSE ES.VALORTOTALUF END)
/
(((CASE WHEN P.IVA IS NULL THEN 0  ELSE P.IVA END)/100)+1),2) 'VALORNETOUF',
CASE WHEN (CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "') THEN CP.FECHACOMISIONPROMESA ELSE NULL END 'FECHACOMISIONPROMESA',
CASE WHEN (CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "') THEN ROUND(CP.PORCENTAJECOMISIONEMPRESA,3) ELSE NULL END 'COMISIONPROMESA',
CASE WHEN (CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "') THEN ROUND(CP.MONTOUFEMPRESA,2) ELSE NULL END 'UFPROMESA',
CP.MONTOPESOSEMPRESA 'PESOSPROMESA',
CASE WHEN (CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "') THEN CP.ESTADOPAGO ELSE NULL END 'ESTADOPROMESA',
CASE WHEN (CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "')  THEN CE.FECHACOMISIONESCRITURA ELSE NULL END 'FECHACOMISIONESCRITURA',
CASE WHEN (CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "')  THEN ROUND(CE.PORCENTAJECOMISIONEMPRESA,3) ELSE NULL END 'COMISIONESCRITURA',
CASE WHEN (CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "') THEN ROUND(CE.MONTOUFEMPRESA,2) ELSE NULL END 'UFESCRITURA',
CE.MONTOPESOSEMPRESA 'PESOSESCRITURA',
CASE WHEN (CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "') THEN CE.ESTADOPAGO ELSE NULL END 'ESTADOESCRITURA'
FROM USUARIO S
LEFT JOIN COMISIONESCRITURA CE
ON S.IDUSUARIO = CE.IDUSUARIO
LEFT JOIN COMISIONPROMESA CP
ON S.IDUSUARIO = CP.IDUSUARIO
AND CE.IDPROMESA = CP.IDPROMESA
LEFT JOIN PROYECTO P
ON CP.IDPROYECTO = P.IDPROYECTO
LEFT JOIN PROMESA PR
ON CP.IDPROMESA = PR.IDPROMESA
LEFT JOIN ESCRITURA ES
ON CE.IDESCRITURA = ES.IDESCRITURA
LEFT JOIN UNIDAD U
ON CASE WHEN PR.IDUNIDAD IS NULL THEN ES.IDUNIDAD ELSE PR.IDUNIDAD END = U.IDUNIDAD
LEFT JOIN PROMESAESTACIONAMIENTO PE
ON PR.IDPROMESA = PE.IDPROMESA
LEFT JOIN UNIDAD UPE
ON PE.IDUNIDAD = UPE.IDUNIDAD
LEFT JOIN PROMESABODEGA PB
ON PR.IDPROMESA = PB.IDPROMESA
LEFT JOIN UNIDAD UPB
ON PB.IDUNIDAD = UPB.IDUNIDAD
LEFT JOIN ESCRITURAESTACIONAMIENTO EE
ON ES.IDESCRITURA = EE.IDESCRITURA
LEFT JOIN UNIDAD UEE
ON EE.IDUNIDAD = UEE.IDUNIDAD
LEFT JOIN ESCRITURABODEGA EB
ON ES.IDESCRITURA = EB.IDESCRITURA
LEFT JOIN UNIDAD UEB
ON EB.IDUNIDAD = UEB.IDUNIDAD
WHERE
(
(CE.FECHACOMISIONESCRITURA >= '" . $fechaIni . "'
AND CE.FECHACOMISIONESCRITURA <= '" . $fechaFin . "')
OR
(CP.FECHACOMISIONPROMESA >= '" . $fechaIni . "'
AND CP.FECHACOMISIONPROMESA <= '" . $fechaFin . "')
)
AND CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.NUMERO ELSE ES.NUMERO END IS NOT NULL
GROUP BY S.RUT, S.NOMBRES, S.APELLIDOS, P.CODIGOPROYECTO, PR.NUMERO, U.CODIGO, PR.VALORTOTALUF, CP.FECHACOMISIONPROMESA, CP.PORCENTAJECOMISIONEMPRESA,
CP.MONTOUFEMPRESA,CP.MONTOPESOSEMPRESA, CP.ESTADOPAGO, CE.FECHACOMISIONESCRITURA, CE.PORCENTAJECOMISIONEMPRESA,
CE.MONTOUFEMPRESA, CE.MONTOPESOSEMPRESA,CE.ESTADOPAGO, PR.IDUNIDAD, ES.NUMERO, ES.VALORTOTALUF, P.IVA";
		if ($row = $con->query($sql)) {
			while($array = $row->fetch_array(MYSQLI_BOTH)){
				$return[] = $array;
			}

			return $return;
		}
		else{
			return "Error";
		}
	}
	else{
		return "Error";
	}
}


	/* ========== Nico =========== */

	function consultaPromesas(){
				$con = conectar();
				if($con != 'No conectado'){
						$sql = "SELECT P.CODIGOPROYECTO, PR.NUMERO, PR.FECHAPROMESA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
			GROUP_CONCAT(DISTINCT UPE.CODIGO) 'EST',
			GROUP_CONCAT(DISTINCT UPB.CODIGO) 'BOD',
			PR.VALORTOTALUF, EPR.COLOR 'ESTADO',
			CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_FICHA_RESERVA()''></span>'
		ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
		R.FICHA_PDF,
		CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_COTIZA_RESERVA()''></span>'
		ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
		R.COTIZA_PDF,
		CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_RCOMPRA_RESERVA()''></span>'
		ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
		R.RCOMPRA_PDF,
		CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CI_RESERVA()''></span>'
		ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
		R.CI_PDF,
		CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_PB_RESERVA()''></span>'
		ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
		R.PB_PDF,
		CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CC_RESERVA()''></span>'
		ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
		R.CC_PDF,
		CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CR_RESERVA()''></span>'
		ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
		R.CR_PDF,
			CASE WHEN PR.PR_PDF IS NOT NULL  AND PR.PR_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PR_PROMESA()''></span>'
		ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PR',
		PR.PR_PDF,
		CASE WHEN PR.CP_PDF IS NOT NULL AND PR.CP_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CP_PROMESA()''></span>'
		ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CP',
		PR.CP_PDF,
		CASE WHEN PR.PF_PDF IS NOT NULL AND PR.PF_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PF_PROMESA()''></span>'
		ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PF',
		PR.PF_PDF,
		CASE WHEN PR.PS_PDF IS NOT NULL AND PR.PS_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PS_PROMESA()''></span>'
		ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PS',
		PR.PS_PDF, PR.IDUSUARIO,
		C.CELULAR, C.EMAIL, EPR.NOMBRE 'ESTADO_TEXTO',  A.NOMBRE 'ACCION'
			FROM PROMESA PR
			LEFT JOIN RESERVA R ON
			PR.IDRESERVA = R.IDRESERVA
				LEFT JOIN PROYECTO P ON
			PR.IDPROYECTO = P.IDPROYECTO
			LEFT JOIN CLIENTE C ON
				PR.IDCLIENTE1 = C.IDCLIENTE
			LEFT JOIN ESTADOPROMESA EPR ON
				PR.ESTADO = EPR.IDESTADOPROMESA
				LEFT JOIN UNIDAD U ON
			PR.IDUNIDAD = U.IDUNIDAD
			LEFT JOIN PROMESAESTACIONAMIENTO PE
			ON PR.IDPROMESA = PE.IDPROMESA
			LEFT JOIN UNIDAD UPE
			ON PE.IDUNIDAD = UPE.IDUNIDAD
			LEFT JOIN PROMESABODEGA PB
			ON PR.IDPROMESA = PB.IDPROMESA
			LEFT JOIN UNIDAD UPB
			ON PB.IDUNIDAD = UPB.IDUNIDAD
	        LEFT JOIN ACCION A ON U.IDACCION = A.IDACCION
			GROUP BY P.CODIGOPROYECTO, PR.NUMERO, PR.FECHAPROMESA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, PR.VALORTOTALUF, EPR.COLOR, R.FICHA_PDF, R.COTIZA_PDF, R.RCOMPRA_PDF, R.CI_PDF, R.PB_PDF, R.CC_PDF, R.CR_PDF, PR.PR_PDF, PR.CP_PDF, PR.PF_PDF, PR.PS_PDF, PR.IDUSUARIO, C.CELULAR, C.EMAIL, EPR.NOMBRE, A.NOMBRE";
					if ($row = $con->query($sql)) {
							while($array = $row->fetch_array(MYSQLI_BOTH)){
								$return[] = $array;
							}

							return $return;
					}
					else{
						return "Error";
					}
				}
				else{
					return "Error";
				}
			}

			function consultaPromesasPro($codigoProyecto){
					$con = conectar();
					if($con != 'No conectado'){
							$sql = "SELECT P.CODIGOPROYECTO, PR.NUMERO, PR.FECHAPROMESA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
				GROUP_CONCAT(DISTINCT UPE.CODIGO) 'EST',
				GROUP_CONCAT(DISTINCT UPB.CODIGO) 'BOD',
				PR.VALORTOTALUF, EPR.COLOR 'ESTADO',
				CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_FICHA_RESERVA()''></span>'
			ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
			R.FICHA_PDF,
			CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_COTIZA_RESERVA()''></span>'
			ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
			R.COTIZA_PDF,
			CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_RCOMPRA_RESERVA()''></span>'
			ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
			R.RCOMPRA_PDF,
			CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CI_RESERVA()''></span>'
			ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
			R.CI_PDF,
			CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_PB_RESERVA()''></span>'
			ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
			R.PB_PDF,
			CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CC_RESERVA()''></span>'
			ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
			R.CC_PDF,
			CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CR_RESERVA()''></span>'
			ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
			R.CR_PDF,
				CASE WHEN PR.PR_PDF IS NOT NULL  AND PR.PR_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PR_PROMESA()''></span>'
			ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PR',
			PR.PR_PDF,
			CASE WHEN PR.CP_PDF IS NOT NULL AND PR.CP_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CP_PROMESA()''></span>'
			ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CP',
			PR.CP_PDF,
			CASE WHEN PR.PF_PDF IS NOT NULL AND PR.PF_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PF_PROMESA()''></span>'
			ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PF',
			PR.PF_PDF,
			CASE WHEN PR.PS_PDF IS NOT NULL AND PR.PS_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PS_PROMESA()''></span>'
			ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PS',
			PR.PS_PDF, PR.IDUSUARIO,
			C.CELULAR, C.EMAIL, EPR.NOMBRE 'ESTADO_TEXTO', A.NOMBRE 'ACCION'
				FROM PROMESA PR
				LEFT JOIN RESERVA R ON
				PR.IDRESERVA = R.IDRESERVA
					LEFT JOIN PROYECTO P ON
				PR.IDPROYECTO = P.IDPROYECTO
				LEFT JOIN CLIENTE C ON
					PR.IDCLIENTE1 = C.IDCLIENTE
				LEFT JOIN ESTADOPROMESA EPR ON
					PR.ESTADO = EPR.IDESTADOPROMESA
					LEFT JOIN UNIDAD U ON
				PR.IDUNIDAD = U.IDUNIDAD
				LEFT JOIN PROMESAESTACIONAMIENTO PE
				ON PR.IDPROMESA = PE.IDPROMESA
				LEFT JOIN UNIDAD UPE
				ON PE.IDUNIDAD = UPE.IDUNIDAD
				LEFT JOIN PROMESABODEGA PB
				ON PR.IDPROMESA = PB.IDPROMESA
				LEFT JOIN UNIDAD UPB
				ON PB.IDUNIDAD = UPB.IDUNIDAD
	            LEFT JOIN ACCION A
	            ON U.IDACCION = A.IDACCION
				WHERE P.CODIGOPROYECTO = '" . $codigoProyecto . "'
				GROUP BY P.CODIGOPROYECTO, PR.NUMERO, PR.FECHAPROMESA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, PR.VALORTOTALUF, EPR.COLOR, R.FICHA_PDF, R.COTIZA_PDF, R.RCOMPRA_PDF, R.CI_PDF, R.PB_PDF, R.CC_PDF, R.CR_PDF, PR.PR_PDF, PR.CP_PDF, PR.PF_PDF, PR.PS_PDF, PR.IDUSUARIO, C.CELULAR, C.EMAIL, EPR.NOMBRE, A.NOMBRE";
						if ($row = $con->query($sql)) {
								while($array = $row->fetch_array(MYSQLI_BOTH)){
									$return[] = $array;
								}

								return $return;
						}
						else{
							return "Error";
						}
					}
					else{
						return "Error";
					}
				}

		function consultaPromesasUsu($rutUsuario){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT P.CODIGOPROYECTO, PR.NUMERO, PR.FECHAPROMESA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
	GROUP_CONCAT(DISTINCT UPE.CODIGO) 'EST',
	GROUP_CONCAT(DISTINCT UPB.CODIGO) 'BOD',
	PR.VALORTOTALUF, EPR.COLOR 'ESTADO',
	CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_FICHA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
	R.FICHA_PDF,
	CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_COTIZA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
	R.COTIZA_PDF,
	CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_RCOMPRA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
	R.RCOMPRA_PDF,
	CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CI_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
	R.CI_PDF,
	CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_PB_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
	R.PB_PDF,
	CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CC_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
	R.CC_PDF,
	CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CR_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
	R.CR_PDF,
	CASE WHEN PR.PR_PDF IS NOT NULL  AND PR.PR_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PR_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PR',
	PR.PR_PDF,
	CASE WHEN PR.CP_PDF IS NOT NULL AND PR.CP_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CP_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CP',
	PR.CP_PDF,
	CASE WHEN PR.PF_PDF IS NOT NULL AND PR.PF_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PF_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PF',
	PR.PF_PDF,
	CASE WHEN PR.PS_PDF IS NOT NULL AND PR.PS_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PS_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PS',
	PR.PS_PDF, PR.IDUSUARIO,
	C.CELULAR, C.EMAIL, EPR.NOMBRE 'ESTADO_TEXTO'
	FROM PROMESA PR
	LEFT JOIN RESERVA R ON
	PR.IDRESERVA = R.IDRESERVA
		LEFT JOIN PROYECTO P ON
	PR.IDPROYECTO = P.IDPROYECTO
	LEFT JOIN CLIENTE C ON
		PR.IDCLIENTE1 = C.IDCLIENTE
	LEFT JOIN ESTADOPROMESA EPR ON
		PR.ESTADO = EPR.IDESTADOPROMESA
		LEFT JOIN UNIDAD U ON
	PR.IDUNIDAD = U.IDUNIDAD
	LEFT JOIN PROMESAESTACIONAMIENTO PE
	ON PR.IDPROMESA = PE.IDPROMESA
	LEFT JOIN UNIDAD UPE
	ON PE.IDUNIDAD = UPE.IDUNIDAD
	LEFT JOIN PROMESABODEGA PB
	ON PR.IDPROMESA = PB.IDPROMESA
	LEFT JOIN UNIDAD UPB
	ON PB.IDUNIDAD = UPB.IDUNIDAD
	WHERE PR.IDPROYECTO IN
	(
	SELECT IDPROYECTO
	FROM USUARIOPROYECTO
	WHERE IDUSUARIO =
	(
		SELECT IDUSUARIO
		FROM USUARIO
		WHERE RUT = '"  . $rutUsuario . "'
	)
	)
		GROUP BY P.CODIGOPROYECTO, PR.NUMERO, PR.FECHAPROMESA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, PR.VALORTOTALUF, EPR.COLOR, R.FICHA_PDF, R.COTIZA_PDF, R.RCOMPRA_PDF, R.CI_PDF, R.PB_PDF, R.CC_PDF, R.CR_PDF, PR.PR_PDF, PR.CP_PDF, PR.PF_PDF, PR.PS_PDF, PR.IDUSUARIO, C.CELULAR, C.EMAIL, EPR.NOMBRE";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaPromesasUsuPro($rutUsuario,$codigoProyecto){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT P.CODIGOPROYECTO, PR.NUMERO, PR.FECHAPROMESA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
	GROUP_CONCAT(DISTINCT UPE.CODIGO) 'EST',
	GROUP_CONCAT(DISTINCT UPB.CODIGO) 'BOD',
	PR.VALORTOTALUF, EPR.COLOR 'ESTADO',
	CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_FICHA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
	R.FICHA_PDF,
	CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_COTIZA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
	R.COTIZA_PDF,
	CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_RCOMPRA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
	R.RCOMPRA_PDF,
	CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CI_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
	R.CI_PDF,
	CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_PB_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
	R.PB_PDF,
	CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CC_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
	R.CC_PDF,
	CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CR_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
	R.CR_PDF,
	CASE WHEN PR.PR_PDF IS NOT NULL  AND PR.PR_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PR_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PR',
	PR.PR_PDF,
	CASE WHEN PR.CP_PDF IS NOT NULL AND PR.CP_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CP_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CP',
	PR.CP_PDF,
	CASE WHEN PR.PF_PDF IS NOT NULL AND PR.PF_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PF_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PF',
	PR.PF_PDF,
	CASE WHEN PR.PS_PDF IS NOT NULL AND PR.PS_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PS_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PS',
	PR.PS_PDF, PR.IDUSUARIO,
	C.CELULAR, C.EMAIL, EPR.NOMBRE 'ESTADO_TEXTO', A.NOMBRE 'ACCION'
	FROM PROMESA PR
	LEFT JOIN RESERVA R ON
	PR.IDRESERVA = R.IDRESERVA
		LEFT JOIN PROYECTO P ON
	PR.IDPROYECTO = P.IDPROYECTO
	LEFT JOIN CLIENTE C ON
		PR.IDCLIENTE1 = C.IDCLIENTE
	LEFT JOIN ESTADOPROMESA EPR ON
		PR.ESTADO = EPR.IDESTADOPROMESA
		LEFT JOIN UNIDAD U ON
	PR.IDUNIDAD = U.IDUNIDAD
	LEFT JOIN PROMESAESTACIONAMIENTO PE
	ON PR.IDPROMESA = PE.IDPROMESA
	LEFT JOIN UNIDAD UPE
	ON PE.IDUNIDAD = UPE.IDUNIDAD
	LEFT JOIN PROMESABODEGA PB
	ON PR.IDPROMESA = PB.IDPROMESA
	LEFT JOIN UNIDAD UPB
	ON PB.IDUNIDAD = UPB.IDUNIDAD
    LEFT JOIN ACCION A
    ON U.IDACCION = A.IDACCION
	WHERE PR.IDPROYECTO IN
	(
	SELECT IDPROYECTO
	FROM USUARIOPROYECTO
	WHERE IDUSUARIO =
	(
		SELECT IDUSUARIO
		FROM USUARIO
		WHERE RUT = '"  . $rutUsuario . "'
	)
	)
	AND P.CODIGOPROYECTO = '" . $codigoProyecto . "'
		GROUP BY P.CODIGOPROYECTO, PR.NUMERO, PR.FECHAPROMESA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, PR.VALORTOTALUF, EPR.COLOR, R.FICHA_PDF, R.COTIZA_PDF, R.RCOMPRA_PDF, R.CI_PDF, R.PB_PDF, R.CC_PDF, R.CR_PDF, PR.PR_PDF, PR.CP_PDF, PR.PF_PDF, PR.PS_PDF, PR.IDUSUARIO, C.CELULAR, C.EMAIL, EPR.NOMBRE, A.NOMBRE";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaPromesasCli($rutUsuario){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT P.CODIGOPROYECTO, PR.NUMERO, PR.FECHAPROMESA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
	GROUP_CONCAT(DISTINCT UPE.CODIGO) 'EST',
	GROUP_CONCAT(DISTINCT UPB.CODIGO) 'BOD',
	PR.VALORTOTALUF, EPR.COLOR 'ESTADO',
	CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_FICHA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
	R.FICHA_PDF,
	CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_COTIZA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
	R.COTIZA_PDF,
	CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_RCOMPRA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
	R.RCOMPRA_PDF,
	CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CI_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
	R.CI_PDF,
	CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_PB_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
	R.PB_PDF,
	CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CC_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
	R.CC_PDF,
	CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CR_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
	R.CR_PDF,
	CASE WHEN PR.PR_PDF IS NOT NULL  AND PR.PR_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PR_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PR',
	PR.PR_PDF,
	CASE WHEN PR.CP_PDF IS NOT NULL AND PR.CP_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CP_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CP',
	PR.CP_PDF,
	CASE WHEN PR.PF_PDF IS NOT NULL AND PR.PF_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PF_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PF',
	PR.PF_PDF,
	CASE WHEN PR.PS_PDF IS NOT NULL AND PR.PS_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PS_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PS',
	PR.PS_PDF, PR.IDUSUARIO,
	C.CELULAR, C.EMAIL, EPR.NOMBRE 'ESTADO_TEXTO', A.NOMBRE 'ACCION'
	FROM PROMESA PR
	LEFT JOIN RESERVA R ON
	PR.IDRESERVA = R.IDRESERVA
		LEFT JOIN PROYECTO P ON
	PR.IDPROYECTO = P.IDPROYECTO
	LEFT JOIN CLIENTE C ON
		PR.IDCLIENTE1 = C.IDCLIENTE
	LEFT JOIN ESTADOPROMESA EPR ON
		PR.ESTADO = EPR.IDESTADOPROMESA
		LEFT JOIN UNIDAD U ON
	PR.IDUNIDAD = U.IDUNIDAD
	LEFT JOIN PROMESAESTACIONAMIENTO PE
	ON PR.IDPROMESA = PE.IDPROMESA
	LEFT JOIN UNIDAD UPE
	ON PE.IDUNIDAD = UPE.IDUNIDAD
	LEFT JOIN PROMESABODEGA PB
	ON PR.IDPROMESA = PB.IDPROMESA
	LEFT JOIN UNIDAD UPB
	ON PB.IDUNIDAD = UPB.IDUNIDAD
    LEFT JOIN ACCION A
    ON U.IDACCION = A.IDACCION
	WHERE PR.IDCLIENTE1 IN
	(
		SELECT IDCLIENTE
		FROM CLIENTE
		WHERE RUT = '"  . $rutUsuario . "'
	)
		GROUP BY P.CODIGOPROYECTO, PR.NUMERO, PR.FECHAPROMESA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, PR.VALORTOTALUF, EPR.COLOR, R.FICHA_PDF, R.COTIZA_PDF, R.RCOMPRA_PDF, R.CI_PDF, R.PB_PDF, R.CC_PDF, R.CR_PDF, PR.PR_PDF, PR.CP_PDF, PR.PF_PDF, PR.PS_PDF, PR.IDUSUARIO, C.CELULAR, C.EMAIL, EPR.NOMBRE, A.NOMBRE";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaPromesasCliPro($rutUsuario,$codigoProyecto){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT P.CODIGOPROYECTO, PR.NUMERO, PR.FECHAPROMESA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
	GROUP_CONCAT(DISTINCT UPE.CODIGO) 'EST',
	GROUP_CONCAT(DISTINCT UPB.CODIGO) 'BOD',
	PR.VALORTOTALUF, EPR.COLOR 'ESTADO',
	CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_FICHA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
	R.FICHA_PDF,
	CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_COTIZA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
	R.COTIZA_PDF,
	CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_RCOMPRA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
	R.RCOMPRA_PDF,
	CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CI_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
	R.CI_PDF,
	CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_PB_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
	R.PB_PDF,
	CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CC_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
	R.CC_PDF,
	CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CR_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
	R.CR_PDF,
	CASE WHEN PR.PR_PDF IS NOT NULL  AND PR.PR_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PR_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PR',
	PR.PR_PDF,
	CASE WHEN PR.CP_PDF IS NOT NULL AND PR.CP_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CP_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CP',
	PR.CP_PDF,
	CASE WHEN PR.PF_PDF IS NOT NULL AND PR.PF_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PF_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PF',
	PR.PF_PDF,
	CASE WHEN PR.PS_PDF IS NOT NULL AND PR.PS_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PS_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PS',
	PR.PS_PDF, PR.IDUSUARIO,
	C.CELULAR, C.EMAIL, EPR.NOMBRE 'ESTADO_TEXTO', A.NOMBRE 'ACCION'
	FROM PROMESA PR
	LEFT JOIN RESERVA R ON
	PR.IDRESERVA = R.IDRESERVA
		LEFT JOIN PROYECTO P ON
	PR.IDPROYECTO = P.IDPROYECTO
	LEFT JOIN CLIENTE C ON
		PR.IDCLIENTE1 = C.IDCLIENTE
	LEFT JOIN ESTADOPROMESA EPR ON
		PR.ESTADO = EPR.IDESTADOPROMESA
		LEFT JOIN UNIDAD U ON
	PR.IDUNIDAD = U.IDUNIDAD
	LEFT JOIN PROMESAESTACIONAMIENTO PE
	ON PR.IDPROMESA = PE.IDPROMESA
	LEFT JOIN UNIDAD UPE
	ON PE.IDUNIDAD = UPE.IDUNIDAD
	LEFT JOIN PROMESABODEGA PB
	ON PR.IDPROMESA = PB.IDPROMESA
	LEFT JOIN UNIDAD UPB
	ON PB.IDUNIDAD = UPB.IDUNIDAD
    LEFT JOIN ACCION A
    ON U.IDACCION = A.IDACCION
	WHERE PR.IDCLIENTE1 IN
	(
		SELECT IDCLIENTE
		FROM CLIENTE
		WHERE RUT = '"  . $rutUsuario . "'
	)
	AND P.CODIGOPROYECTO = '" . $codigoProyecto . "'
		GROUP BY P.CODIGOPROYECTO, PR.NUMERO, PR.FECHAPROMESA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, PR.VALORTOTALUF, EPR.COLOR, R.FICHA_PDF, R.COTIZA_PDF, R.RCOMPRA_PDF, R.CI_PDF, R.PB_PDF, R.CC_PDF, R.CR_PDF, PR.PR_PDF, PR.CP_PDF, PR.PF_PDF, PR.PS_PDF, PR.IDUSUARIO, C.CELULAR, C.EMAIL, EPR.NOMBRE, A.NOMBRE";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaReservasAutorizadasParaPromesa(){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT P.CODIGOPROYECTO, R.NUMERO, DATE_FORMAT(R.FECHARESERVA, '%d-%m-%Y') FECHA, U.CODIGO DEPTO, E.NOMBRES,
				E.APELLIDOS,E.RUT,
				GROUP_CONCAT(DISTINCT URE.CODIGO) 'EST',
				GROUP_CONCAT(DISTINCT URB.CODIGO) 'BOD',
				R.VALORTOTALUF, ER.COLOR 'ESTADO',
				CONCAT(S.NOMBRES,' ',S.APELLIDOS) VENDEDOR
				FROM RESERVA R LEFT JOIN PROYECTO P
				ON R.IDPROYECTO = P.IDPROYECTO
				LEFT JOIN UNIDAD U
				ON R.IDUNIDAD = U.IDUNIDAD
				LEFT JOIN CLIENTE E
				ON R.IDCLIENTE1 = E.IDCLIENTE
				LEFT JOIN RESERVAESTACIONAMIENTO RE
				ON R.IDRESERVA = RE.IDRESERVA
				LEFT JOIN UNIDAD URE
				ON RE.IDUNIDAD = URE.IDUNIDAD
				LEFT JOIN RESERVABODEGA RB
				ON R.IDRESERVA = RB.IDRESERVA
				LEFT JOIN UNIDAD URB
				ON RB.IDUNIDAD = URB.IDUNIDAD
				LEFT JOIN USUARIO S
				ON R.IDUSUARIO = S.IDUSUARIO
				LEFT JOIN ESTADORESERVA ER
				ON R.ESTADO = ER.IDESTADORESERVA
				WHERE R.ESTADO IN (3)
				AND R.IDRESERVA NOT IN
				(
					SELECT IDRESERVA
					FROM PROMESA
				)
				 GROUP BY P.CODIGOPROYECTO, R.NUMERO, R.FECHARESERVA, U.CODIGO, E.NOMBRES, E.APELLIDOS,E.RUT, R.VALORTOTALUF, ER.NOMBRE, S.NOMBRES, S.APELLIDOS";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaReservasUsuAutorizadasParaPromesa($rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, R.NUMERO, DATE_FORMAT(R.FECHARESERVA, '%d-%m-%Y') FECHA, U.CODIGO DEPTO, E.NOMBRES,
			E.APELLIDOS,E.RUT,
			GROUP_CONCAT(DISTINCT URE.CODIGO) 'EST',
			GROUP_CONCAT(DISTINCT URB.CODIGO) 'BOD',
			R.VALORTOTALUF, ER.COLOR 'ESTADO',
			CONCAT(S.NOMBRES,' ',S.APELLIDOS) VENDEDOR
			FROM RESERVA R LEFT JOIN PROYECTO P
			ON R.IDPROYECTO = P.IDPROYECTO
			LEFT JOIN UNIDAD U
			ON R.IDUNIDAD = U.IDUNIDAD
			LEFT JOIN CLIENTE E
			ON R.IDCLIENTE1 = E.IDCLIENTE
			LEFT JOIN RESERVAESTACIONAMIENTO RE
			ON R.IDRESERVA = RE.IDRESERVA
			LEFT JOIN UNIDAD URE
			ON RE.IDUNIDAD = URE.IDUNIDAD
			LEFT JOIN RESERVABODEGA RB
			ON R.IDRESERVA = RB.IDRESERVA
			LEFT JOIN UNIDAD URB
			ON RB.IDUNIDAD = URB.IDUNIDAD
			LEFT JOIN USUARIO S
			ON R.IDUSUARIO = S.IDUSUARIO
			LEFT JOIN ESTADORESERVA ER
			ON R.ESTADO = ER.IDESTADORESERVA
			WHERE R.IDPROYECTO IN
			(
				SELECT IDPROYECTO
				FROM USUARIOPROYECTO
				WHERE IDUSUARIO =
				(
					SELECT IDUSUARIO
					FROM USUARIO
					WHERE RUT = '" . $rutUsuario . "'
				)
			)
			AND R.ESTADO IN (
				3
			)
			AND R.IDRESERVA NOT IN
			(
				SELECT IDRESERVA
				FROM PROMESA
			)GROUP BY P.CODIGOPROYECTO, R.NUMERO, R.FECHARESERVA, U.CODIGO, E.NOMBRES, E.APELLIDOS,E.RUT, R.VALORTOTALUF, ER.NOMBRE, S.NOMBRES, S.APELLIDOS";
			if ($row = $con->query($sql)) {
					while($array = $row->fetch_array(MYSQLI_BOTH)){
						$return[] = $array;
					}

					return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function consultaReservaEspecifica($codigoProyecto, $numeroReserva){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT IDRESERVA, IDCOTIZACION, IDPROYECTO, IDUNIDAD, IDCLIENTE1, IDCLIENTE2, IDUSUARIO, NUMERO, ESTADO, FECHARESERVA, VALORBRUTOUF, DESCUENTO1, DESCUENTO2,
				CASE WHEN BONOVENTAUF IS NULL THEN 0 ELSE BONOVENTAUF END 'BONOVENTAUF',
				VALORTOTALUF, VALORRESERVAUF, VALORPIEPROMESAUF, VALORPIESALDOUF, CANTCUOTASPIE, VALORSALDOTOTALUF, FECHAPAGORESERVA, VALORPAGORESERVA, FORMAPAGORESERVA, BANCORESERVA, SERIECHEQUERESERVA, NROCHEQUERESERVA, ESTADOPAGORESERVA, OBSERVACIONRESERVA, VALORPIEPROMESA, VALORPIESALDO, RCOMPRA_PDF, FICHA_PDF, PB_PDF, CI_PDF, COTIZA_PDF, CC_PDF, CR_PDF
	FROM RESERVA
	WHERE IDPROYECTO =
	(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
	)
	AND NUMERO = '" . $numeroReserva . "'";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
		function consultaReservaBodega($idReserva){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT *
				FROM RESERVABODEGA
				WHERE IDRESERVA = '" . $idReserva . "'";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
		function consultaReservaEstacionamiento($idReserva){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT *
				FROM RESERVAESTACIONAMIENTO
				WHERE IDRESERVA = '" . $idReserva . "'";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
		function consultaReservaBodegaCantidad($idReserva){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT
				(
				SELECT COUNT(*) FROM RESERVABODEGA
				WHERE IDRESERVA = '" . $idReserva . "'
				) 'BOD'
				FROM RESERVA";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
		function consultaReservaEstacionamientoCantidad($idReserva){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT
				(
				SELECT COUNT(*) FROM RESERVAESTACIONAMIENTO
				WHERE IDRESERVA = '" . $idReserva . "'
				) 'EST'
				FROM RESERVA";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
			function consultaBodegasReservadas($idReserva){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT R.IDUNIDAD, U.CODIGO, R.VALORUF
	FROM UNIDAD U JOIN RESERVABODEGA R ON
	U.IDUNIDAD = R.IDUNIDAD
	WHERE IDRESERVA =
	(
	SELECT IDRESERVA
	FROM RESERVA
	WHERE IDRESERVA = '" . $idReserva . "'
	)
	";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaEstacionamientosReservados($idReserva){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT R.IDUNIDAD, U.CODIGO, R.VALORUF
	FROM UNIDAD U JOIN RESERVAESTACIONAMIENTO R ON
	U.IDUNIDAD = R.IDUNIDAD
	WHERE IDRESERVA =
	(
	SELECT IDRESERVA
	FROM RESERVA
	WHERE IDRESERVA = '" . $idReserva . "'
	)
	";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaFormaPagoReservaEspecifica($idFormaPagoReserva){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT NOMBRE
	FROM RESERVAFORMA WHERE IDRESERVAFORMA = '".$idFormaPagoReserva."'";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
		function consultaCodigoCotizacionPromesa($idNumeroReserva){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT C.CODIGO
	FROM COTIZACION C JOIN RESERVA R ON C.IDCOTIZACION = R.IDCOTIZACION
	WHERE R.IDRESERVA = '".$idNumeroReserva."'";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
		function ingresaPromesa($idReserva, $idProyecto, $idUnidad, $idCliente1, $idCliente2, $idUsuario,$numeroOperacion, $fechaPromesa, $valorBrutoUF, $descuento1, $descuento2, $bonoVentaUF, $valorTotalUF, $valorReservaUF, $valorPiePromesaUF, $valorPieSaldoUF, $cantidadCuotasPie, $valorSaldoTotalUF, $fechaPagoPromesa, $valorPagoPromesa, $formaPagoPromesa, $bancoPromesa, $serieChequePromesa, $nroChequePromesa, $valorPiePromesa, $valorPieSaldo, $valorReserva){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "INSERT INTO PROMESA
	(IDRESERVA, IDPROYECTO, IDUNIDAD, IDCLIENTE1, IDCLIENTE2, IDUSUARIO, NUMERO, ESTADO, FECHAPROMESA, VALORBRUTOUF, DESCUENTO1, DESCUENTO2, BONOVENTAUF, VALORTOTALUF, VALORRESERVAUF, VALORPIEPROMESAUF, VALORPIESALDOUF, CANTCUOTASPIE, VALORSALDOTOTALUF, FECHAPAGOPROMESA, VALORPAGOPROMESA, FORMAPAGOPROMESA, BANCOPROMESA, SERIECHEQUEPROMESA, NROCHEQUEPROMESA, ESTADOPAGOPROMESA, OBSERVACIONPROMESA, VALORPIEPROMESA, VALORPIESALDO, VALORPAGORESERVA)
	VALUES('" . $idReserva . "', '" . $idProyecto . "', '" . $idUnidad . "', '" . $idCliente1 . "', '" . $idCliente2 . "', '" . $idUsuario . "','" . $numeroOperacion . "','" .  2 . "', '" . $fechaPromesa . "', '" . $valorBrutoUF . "', '" . $descuento1 . "', '" . $descuento2 . "', '" . $bonoVentaUF . "', '" . $valorTotalUF . "', '" . $valorReservaUF . "', '" . $valorPiePromesaUF . "', '" . $valorPieSaldoUF . "', '" . $cantidadCuotasPie . "', '" . $valorSaldoTotalUF . "', '" . $fechaPagoPromesa . "', '" . $valorPagoPromesa . "', '" . $formaPagoPromesa . "', '" . $bancoPromesa . "', '" . $serieChequePromesa . "', '" . $nroChequePromesa . "', '', '','" . $valorPiePromesa . "','" . $valorPieSaldo . "','" . $valorReserva . "')";
				if ($con->query($sql)) {
						return $con;
				}
				else{
					return $con->error;
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}

		function ingresaPromesaEstacionamiento($idPromesa, $idUnidad, $con){
			if($con != 'No conectado'){
				$sql = "INSERT INTO PROMESAESTACIONAMIENTO
	(IDPROMESA, IDUNIDAD, VALORUF)
	VALUES('" .$idPromesa . "','" . $idUnidad . "',
	(
		SELECT VALORUF
		FROM UNIDAD
		WHERE IDUNIDAD = '" . $idUnidad . "'
	)
	)";
				if ($con->query($sql)) {
						return "Ok";
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function ingresaPromesaBodega($idPromesa, $idUnidad, $con){
			if($con != 'No conectado'){
				$sql = "INSERT INTO PROMESABODEGA
	(IDPROMESA, IDUNIDAD, VALORUF)
	VALUES('" .$idPromesa . "','" . $idUnidad . "',
	(
		SELECT VALORUF
		FROM UNIDAD
		WHERE IDUNIDAD = '" . $idUnidad . "'
	)
	)";
				if ($con->query($sql)) {
						return "Ok";
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function ingresaPromesaCuotas($idPromesa, $numero_cuota, $uf_cuota, $monto_cuota, $forma_cuota, $banco_cuota, $serie_cuota, $nro_cuota, $fecha_cuota, $con){
			if($con != 'No conectado'){
				$sql = "INSERT INTO PROMESACUOTAS
	(IDPROMESA, NUMEROCUOTA, VALORUFCUOTA, VALORMONTOCUOTA, FORMAPAGOCUOTA, BANCOCUOTA, SERIECHEQUECUOTA, NROCHEQUECUOTA, FECHAPAGOCUOTA)
	VALUES('" .$idPromesa . "','" . $numero_cuota . "','" . $uf_cuota . "','" . $monto_cuota . "','" . $forma_cuota . "','" . $banco_cuota . "','" . $serie_cuota . "','" . $nro_cuota . "','" . $fecha_cuota ."')";
				if ($con->query($sql)) {
						return "Ok";
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function liberarUnidadesPromesa($idUnidad,$con){
			if($con != 'No conectado'){
				$sql = "UPDATE UNIDAD
				SET ESTADO = 1
				WHERE IDUNIDAD = " . $idUnidad;
				if ($con->query($sql)) {
						return "Ok";
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function borrarPromesaBodega($idUnidad,$con){
			if($con != 'No conectado'){
				$sql = "DELETE FROM RESERVABODEGA
				WHERE IDUNIDAD = " . $idUnidad . " ";
				if ($con->query($sql)) {
						return "Ok";
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function borrarPromesaEstacionamiento($idUnidad,$con){
			if($con != 'No conectado'){
				$sql = "DELETE FROM RESERVAESTACIONAMIENTO
				WHERE IDUNIDAD = " . $idUnidad . " ";
				if ($con->query($sql)) {
						return "Ok";
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
		function promesaDatosVendedor($idUsuario){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT CONCAT (NOMBRES,' ',APELLIDOS) VENDEDOR, RUT
				FROM USUARIO
				WHERE IDUSUARIO = '" . $idUsuario . "'";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
		function actualizaPromesaFICHA_PDF($pr_pdf,$idPromesa){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "UPDATE PROMESA
	SET PR_PDF = '" . $pr_pdf . "'
	WHERE IDPROMESA = " . $idPromesa;
				if ($con->query($sql)) {
						$con->query("COMMIT");
						return "Ok";
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}

		function consultaPromesaEspecificaCargaDocs($codigoProyecto, $numeroOperacion){
				$con = conectar();
				if($con != 'No conectado'){
					$sql = "SELECT PR.IDPROMESA, PR.PR_PDF, PR.CP_PDF, PR.PF_PDF, PR.PS_PDF, U.CODIGO, C.NOMBRES, C.APELLIDOS
	FROM PROMESA PR, UNIDAD U, CLIENTE C
	WHERE PR.IDPROYECTO =
	(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
	)
	AND PR.NUMERO = '" . $numeroOperacion . "'
	AND PR.IDUNIDAD = U.IDUNIDAD
	AND PR.IDCLIENTE1 = C.IDCLIENTE";
					if ($row = $con->query($sql)) {
							while($array = $row->fetch_array(MYSQLI_BOTH)){
								$return[] = $array;
							}

							return $return;
					}
					else{
						return "Error";
					}
				}
				else{
					return "Error";
				}
			}
			function actualizaRutasDocumentosPromesa($rutaPR, $rutaCP, $rutaPF, $rutaPS, $idPromesa){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "UPDATE PROMESA
	SET PR_PDF = '" . $rutaPR . "',
	CP_PDF = '" . $rutaCP . "',
	PF_PDF = '" . $rutaPF . "',
	PS_PDF = '" . $rutaPS . "'
	WHERE IDPROMESA = '" . $idPromesa . "'";
				if ($con->query($sql)) {
						$con->query("COMMIT");
						return "Ok";
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		function consultaPromesaEspecifica($codigoProyecto, $numeroOperacion){
				$con = conectar();
				if($con != 'No conectado'){
					$sql = "SELECT IDPROMESA, IDRESERVA, IDPROYECTO, IDUNIDAD, IDCLIENTE1, IDCLIENTE2, IDUSUARIO, NUMERO, ESTADO, FECHAPROMESA, VALORBRUTOUF, DESCUENTO1, DESCUENTO2,
				CASE WHEN BONOVENTAUF IS NULL THEN 0 ELSE BONOVENTAUF END 'BONOVENTAUF',
				VALORTOTALUF, VALORRESERVAUF, VALORPAGORESERVA, VALORPIEPROMESAUF, VALORPIESALDOUF, CANTCUOTASPIE, VALORSALDOTOTALUF, FECHAPAGOPROMESA, VALORPAGOPROMESA, FORMAPAGOPROMESA, BANCOPROMESA, SERIECHEQUEPROMESA, NROCHEQUEPROMESA, ESTADOPAGOPROMESA, VALORPIEPROMESA, VALORPIESALDO, PR_PDF, CP_PDF, PF_PDF, PS_PDF
					FROM PROMESA
					WHERE IDPROYECTO =
					(
					SELECT IDPROYECTO
					FROM PROYECTO
					WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
					)
					AND NUMERO = '" . $numeroOperacion . "'";
					if ($row = $con->query($sql)) {
							while($array = $row->fetch_array(MYSQLI_BOTH)){
								$return[] = $array;
							}

							return $return;
					}
					else{
						return "Error";
					}
				}
				else{
					return "Error";
				}
			}

		function consultaPromesaBodegasEspecificas($idPromesa){
				$con = conectar();
				if($con != 'No conectado'){
					$sql = "SELECT IDUNIDAD
					FROM PROMESABODEGA
					WHERE IDPROMESA = '" . $idPromesa . "'";
					if ($row = $con->query($sql)) {
							while($array = $row->fetch_array(MYSQLI_BOTH)){
								$return[] = $array;
							}

							return $return;
					}
					else{
						return "Error";
					}
				}
				else{
					return "Error";
				}
			}

		function consultaPromesaEstacionamientosEspecificas($idPromesa){
				$con = conectar();
				if($con != 'No conectado'){
					$sql = "SELECT IDUNIDAD
					FROM PROMESAESTACIONAMIENTO
					WHERE IDPROMESA = '" . $idPromesa . "'";
					if ($row = $con->query($sql)) {
							while($array = $row->fetch_array(MYSQLI_BOTH)){
								$return[] = $array;
							}

							return $return;
					}
					else{
						return "Error";
					}
				}
				else{
					return "Error";
				}
			}

		function eliminaPromesa($codigoProyecto,$numeroOperacion){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "DELETE FROM PROMESA
			WHERE NUMERO = '" .  $numeroOperacion. "'
	AND IDPROYECTO =
	(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
	)";
				if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}

		function eliminaPromesaCuotas($codigoProyecto,$numeroOperacion){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "DELETE FROM PROMESACUOTAS
			WHERE IDPROMESA = (SELECT IDPROMESA FROM PROMESA
			WHERE IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '".$codigoProyecto."')
			AND NUMERO = '".$numeroOperacion."')";
				if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}

		function cambioEstadoUnidadesPromesa($idUnidad){
			$con = conectar();
			$con->query ("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "UPDATE UNIDAD
			SET ESTADO = 3
			WHERE IDUNIDAD = '".$idUnidad."'";
				if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}

		function eliminaPromesaBodegas($idPromesa){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "DELETE FROM PROMESABODEGA
			WHERE IDPROMESA =
			(
			SELECT IDPROMESA
			FROM PROMESA
			WHERE NUMERO = '" . $numeroOperacion . "'
			AND IDPROYECTO =
			(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			)";
				if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		function eliminaPromesaEstacionamientos($idPromesa){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "DELETE FROM PROMESAESTACIONAMIENTO
			WHERE IDPROMESA = '".$idPromesa."'";
				if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		function consultaBodegasPromesadas($idPromesa){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT P.IDUNIDAD, U.CODIGO, P.VALORUF
	FROM UNIDAD U JOIN PROMESABODEGA P ON
	U.IDUNIDAD = P.IDUNIDAD
	WHERE IDPROMESA =
	(
	SELECT IDPROMESA
	FROM PROMESA
	WHERE IDPROMESA = '".$idPromesa."'
	)
	";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaEstacionamientosPromesados($idPromesa){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT P.IDUNIDAD, U.CODIGO, P.VALORUF
	FROM UNIDAD U JOIN PROMESAESTACIONAMIENTO P ON
	U.IDUNIDAD = P.IDUNIDAD
	WHERE IDPROMESA =
	(
	SELECT IDPROMESA
	FROM PROMESA
	WHERE IDPROMESA = '".$idPromesa."'
	)
	";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function datosPromesaEspecifica($codigoProyecto,$numeroOperacion){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT PR.*, R.VALORRESERVAUF, R.FECHARESERVA, R.FORMAPAGORESERVA, R.BANCORESERVA, R.SERIECHEQUERESERVA, R.NROCHEQUERESERVA, R.FECHAPAGORESERVA, C.CODIGO 'NUMEROCOTIZACION', CL.NOMBRES,
	CL.APELLIDOS, CL.RUT, CL.CELULAR, CL.EMAIL, U.CODIGO 'DEPTO', T.NOMBRE 'TIPOLOGIA', TI.NOMBRE 'MODELO', U.ORIENTACION,
	(
	SELECT COUNT(IDUNIDAD)
	FROM PROMESABODEGA
	WHERE IDPROMESA = PR.IDPROMESA
	) BOD,
	(
	SELECT COUNT(IDUNIDAD)
	FROM PROMESAESTACIONAMIENTO
	WHERE IDPROMESA = PR.IDPROMESA
	) EST,
	U.M2UTIL, U.M2TERRAZA, U.M2TOTAL,
	CASE WHEN C.VALORUNIDADUF IS NOT NULL THEN C.VALORUNIDADUF ELSE U.VALORUF END 'DEPTOUF',
	(
	SELECT SUM(VALORUF)
	FROM PROMESABODEGA
	WHERE IDPROMESA = PR.IDPROMESA
	) BODUF,
	(
	SELECT SUM(VALORUF)
	FROM PROMESAESTACIONAMIENTO
	WHERE IDPROMESA = PR.IDPROMESA
	) ESTUF,
	PR.BONOVENTAUF, PR.DESCUENTO1, PR.DESCUENTO2, PR.VALORTOTALUF, P.NOMBRE 'NOMBREPROYECTO',
	G.MONTOUF, G.MONTOPESOS, G.FORMAPAGO, G.BANCO, G.SERIE, G.NRO, G.FECHAPAGO,
    VC.MONTOUF 'MONTOUFVC', VC.MONTOPESOS 'MONTOPESOSVC' , VC.FORMAPAGO 'FORMAPAGOVC', VC.BANCO 'BANCOVC', VC.SERIE 'SERIEVC', VC.NRO 'NROVC', VC.FECHAPAGO 'FECHAPAGOVC',
    VD.MONTOUF 'MONTOUFVD', VD.MONTOPESOS 'MONTOPESOSVD' , VD.FORMAPAGO 'FORMAPAGOVD', VD.BANCO 'BANCOVD', VD.SERIE 'SERIEVD', VD.NRO 'NROVD', VD.FECHAPAGO 'FECHAPAGOVD'
	FROM PROMESA PR
	LEFT JOIN RESERVA R
	ON PR.IDRESERVA = R.IDRESERVA
	LEFT JOIN COTIZACION C
	ON R.IDCOTIZACION = C.IDCOTIZACION
	LEFT JOIN CLIENTE CL
	ON PR.IDCLIENTE1 = CL.IDCLIENTE
	LEFT JOIN PROYECTO P
	ON PR.IDPROYECTO = P.IDPROYECTO
	LEFT JOIN UNIDAD U
	ON PR.IDUNIDAD = U.IDUNIDAD
	LEFT JOIN TIPOLOGIA T
	ON U.TIPOLOGIA = T.IDTIPOLOGIA
	LEFT JOIN TIPOUNIDAD TI
	ON U.IDTIPOUNIDAD = TI.IDTIPOUNIDAD
	LEFT JOIN GARANTIA G
	ON PR.IDPROMESA = G.IDPROMESA
    AND G.ACCION = 'Arriendo' AND G.ACTOR = 'Cliente'
    LEFT JOIN GARANTIA VC
    ON PR.IDPROMESA = VC.IDPROMESA
    AND VC.ACCION = 'Venta' AND VC.ACTOR = 'Cliente'
    LEFT JOIN GARANTIA VD
    ON PR.IDPROMESA = VD.IDPROMESA
    AND VD.ACCION = 'Venta' AND VD.ACTOR = 'Dueno'
	WHERE PR.IDPROYECTO =
	(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
	)
	AND PR.NUMERO = '" . $numeroOperacion . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaCuotasPromesadas($codigoProyecto,$numeroOperacion){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT * FROM PROMESACUOTAS
				WHERE IDPROMESA = (SELECT IDPROMESA FROM PROMESA
				WHERE IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '".$codigoProyecto."')
				 AND NUMERO = '".$numeroOperacion."')
				 ORDER BY NUMEROCUOTA ASC";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaPagoComisionCliente($codigoProyecto,$numeroOperacion){
				$con = conectar();
				if($con != 'No conectado'){
					$sql = "SELECT P.VALORUFCOM 'VALORUF', P.MONTOPAGOCOM,
					(
						SELECT NOMBRE
						FROM RESERVAFORMA
						WHERE IDRESERVAFORMA = P.FORMAPAGO
					) 'FORMAPAGO', P.BANCOPROMESA, P.SERIECHEQUEPROMESA, P.NROCHEQUEPROMESA, P.FECHAPAGOCOMISION
FROM COMISIONPROMESA C LEFT JOIN PAGOCOMISIONPROMESA P
ON C.IDCOMISIONPROMESA = P.IDCOMISIONPROMESA
WHERE C.IDPROMESA = (SELECT IDPROMESA FROM PROMESA
WHERE IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
 AND NUMERO = '" . $numeroOperacion . "')";
					if ($row = $con->query($sql)) {
							while($array = $row->fetch_array(MYSQLI_BOTH)){
								$return[] = $array;
							}

							return $return;
					}
					else{
						return "Error";
					}
				}
				else{
					return "Error";
				}
			}

	function liberaUnidadesPromesaBodega($codigoProyecto,$numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE UNIDAD
	SET ESTADO = '3'
	WHERE IDUNIDAD IN
	(
	SELECT IDUNIDAD
	FROM PROMESABODEGA
	WHERE IDPROMESA =
	(
		SELECT IDPROMESA
		FROM PROMESA
		WHERE NUMERO = '" . $numeroOperacion . "'
		AND IDPROYECTO =
		(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
		)
	)
	)";
			if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function liberaUnidadesPromesaEstacionamiento($codigoProyecto,$numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE UNIDAD
	SET ESTADO = '3'
	WHERE IDUNIDAD IN
	(
	SELECT IDUNIDAD
	FROM PROMESAESTACIONAMIENTO
	WHERE IDPROMESA =
	(
		SELECT IDPROMESA
		FROM PROMESA
		WHERE NUMERO = '" . $numeroOperacion . "'
		AND IDPROYECTO =
		(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
		)
	)
	)";
			if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function liberaUnidadesPromesadas($codigoProyecto,$numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE UNIDAD
	SET ESTADO = '3'
	WHERE IDUNIDAD IN
	(
	SELECT IDUNIDAD
	FROM PROMESA
	WHERE NUMERO = '" . $numeroOperacion . "'
	AND IDPROYECTO =
	(
		SELECT IDPROYECTO
		FROM PROYECTO
		WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
	)
	)";
			if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function eliminaPromesaBodega($codigoProyecto, $numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM PROMESABODEGA
			WHERE IDPROMESA =
			(
			SELECT IDPROMESA
			FROM PROMESA
			WHERE NUMERO = '" . $numeroOperacion . "'
			AND IDPROYECTO =
			(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			)";
			if ($con->query($sql)) {
				$con->query("COMMIT");
				return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function eliminaPromesaEstacionamiento($codigoProyecto, $numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM PROMESAESTACIONAMIENTO
			WHERE IDPROMESA =
			(
			SELECT IDPROMESA
			FROM PROMESA
			WHERE NUMERO = '" . $numeroOperacion . "'
			AND IDPROYECTO =
			(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			)";
			if ($con->query($sql)) {
				$con->query("COMMIT");
				return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function actualizarPromesa($codigoProyecto, $numeroOperacion, $valorTotalUF, $valorPiePromesaUF, $valorPieSaldoUF, $cantidadCuotasPie, $valorSaldoTotalUF, $fechaPagoPromesa, $valorPagoPromesa, $formaPagoPromesa, $bancoPromesa, $serieChequePromesa, $nroChequePromesa, $valorPiePromesa, $valorPieSaldo, $vendedorID, $fechaPromesaI){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE PROMESA
	SET VALORTOTALUF = '" . $valorTotalUF . "',
	VALORPIEPROMESAUF  = '" . $valorPiePromesaUF . "',
	VALORPIESALDOUF = '" . $valorPieSaldoUF . "',
	CANTCUOTASPIE = '" . $cantidadCuotasPie . "',
	VALORSALDOTOTALUF = '" . $valorSaldoTotalUF . "',
	FECHAPAGOPROMESA = '" . $fechaPagoPromesa . "',
	VALORPAGOPROMESA = '" . $valorPagoPromesa . "',
	FORMAPAGOPROMESA = '" . $formaPagoPromesa . "',
	BANCOPROMESA = '" . $bancoPromesa . "',
	SERIECHEQUEPROMESA = '" . $serieChequePromesa . "',
	NROCHEQUEPROMESA = '" . $nroChequePromesa . "',
	VALORPIEPROMESA = '" . $valorPiePromesa . "',
	VALORPIESALDO = '" . $valorPieSaldo . "',
	IDUSUARIO = '" . $vendedorID . "',
	FECHAPROMESA = '" . $fechaPromesaI . "'
	WHERE IDPROYECTO =
	(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
	)
	AND NUMERO = '" . $numeroOperacion . "'";
			if ($con->query($sql)) {
					return $con;
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function consultaPromesasAutorizadasParaEscritura(){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT P.CODIGOPROYECTO, PR.NUMERO, DATE_FORMAT(PR.FECHAPROMESA, '%d-%m-%Y') FECHA, U.CODIGO DEPTO, E.NOMBRES,
				E.APELLIDOS,E.RUT,
				GROUP_CONCAT(DISTINCT UPE.CODIGO) 'EST',
				GROUP_CONCAT(DISTINCT UPB.CODIGO) 'BOD',
				PR.VALORTOTALUF, EP.COLOR 'ESTADO'
				FROM PROMESA PR LEFT JOIN PROYECTO P
				ON PR.IDPROYECTO = P.IDPROYECTO
				LEFT JOIN UNIDAD U
				ON PR.IDUNIDAD = U.IDUNIDAD
				LEFT JOIN CLIENTE E
				ON PR.IDCLIENTE1 = E.IDCLIENTE
				LEFT JOIN PROMESAESTACIONAMIENTO PE
				ON PR.IDPROMESA = PE.IDPROMESA
				LEFT JOIN UNIDAD UPE
				ON PE.IDUNIDAD = UPE.IDUNIDAD
				LEFT JOIN PROMESABODEGA PB
				ON PR.IDPROMESA = PB.IDPROMESA
				LEFT JOIN UNIDAD UPB
				ON PB.IDUNIDAD = UPB.IDUNIDAD
				LEFT JOIN USUARIO S
				ON PR.IDUSUARIO = S.IDUSUARIO
				LEFT JOIN ESTADOPROMESA EP
				ON PR.ESTADO = EP.IDESTADOPROMESA
				WHERE PR.ESTADO IN (3)
				AND PR.IDPROMESA NOT IN
				(
					SELECT IDPROMESA
					FROM ESCRITURA
				)
				 GROUP BY P.CODIGOPROYECTO, PR.NUMERO, PR.FECHAPROMESA, U.CODIGO, E.NOMBRES, E.APELLIDOS,E.RUT, PR.VALORTOTALUF, EP.COLOR";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaPromesasUsuAutorizadasParaEscritura($rutUsuario){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT P.CODIGOPROYECTO, PR.NUMERO, DATE_FORMAT(PR.FECHAPROMESA, '%d-%m-%Y') FECHA, U.CODIGO DEPTO, E.NOMBRES,
				E.APELLIDOS,E.RUT,
				GROUP_CONCAT(DISTINCT UPE.CODIGO) 'EST',
				GROUP_CONCAT(DISTINCT UPB.CODIGO) 'BOD',
				PR.VALORTOTALUF, EP.COLOR 'ESTADO'
				FROM PROMESA PR LEFT JOIN PROYECTO P
				ON PR.IDPROYECTO = P.IDPROYECTO
				LEFT JOIN UNIDAD U
				ON PR.IDUNIDAD = U.IDUNIDAD
				LEFT JOIN CLIENTE E
				ON PR.IDCLIENTE1 = E.IDCLIENTE
				LEFT JOIN PROMESAESTACIONAMIENTO PE
				ON PR.IDPROMESA = PE.IDPROMESA
				LEFT JOIN UNIDAD UPE
				ON PE.IDUNIDAD = UPE.IDUNIDAD
				LEFT JOIN PROMESABODEGA PB
				ON PR.IDPROMESA = PB.IDPROMESA
				LEFT JOIN UNIDAD UPB
				ON PB.IDUNIDAD = UPB.IDUNIDAD
				LEFT JOIN USUARIO S
				ON PR.IDUSUARIO = S.IDUSUARIO
				LEFT JOIN ESTADOPROMESA EP
				ON PR.ESTADO = EP.IDESTADOPROMESA
				WHERE PR.IDPROYECTO IN
				(
					SELECT IDPROYECTO
					FROM USUARIOPROYECTO
					WHERE IDUSUARIO =
					(
						SELECT IDUSUARIO
						FROM USUARIO
						WHERE RUT = '". $rutUsuario ."'
					)
				)
				AND PR.ESTADO IN (3)
				AND PR.IDPROMESA NOT IN
				(
					SELECT IDPROMESA
					FROM ESCRITURA
				)
				 GROUP BY P.CODIGOPROYECTO, PR.NUMERO, PR.FECHAPROMESA, U.CODIGO, E.NOMBRES, E.APELLIDOS,E.RUT, PR.VALORTOTALUF, EP.COLOR";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaPromesaBodega($idPromesa){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT *
				FROM PROMESABODEGA
				WHERE IDPROMESA = '" . $idPromesa . "'";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
		function consultaPromesaEstacionamiento($idPromesa){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT *
				FROM PROMESAESTACIONAMIENTO
				WHERE IDPROMESA = '" . $idPromesa . "'";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
		function consultaPromesaBodegaCantidad($idPromesa){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT
				(
				SELECT COUNT(*) FROM PROMESABODEGA
				WHERE IDPROMESA = '" . $idPromesa . "'
				) 'BOD'
				FROM PROMESA";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
		function consultaPromesaEstacionamientoCantidad($idPromesa){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT
				(
				SELECT COUNT(*) FROM PROMESAESTACIONAMIENTO
				WHERE IDPROMESA = '" . $idPromesa . "'
				) 'EST'
				FROM PROMESA";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
			function liberaUnidadesPromesaBodega_escritura($codigoProyecto,$numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE UNIDAD
	SET ESTADO = '1'
	WHERE IDUNIDAD IN
	(
	SELECT IDUNIDAD
	FROM PROMESABODEGA
	WHERE IDPROMESA =
	(
		SELECT IDPROMESA
		FROM PROMESA
		WHERE NUMERO = '" . $numeroOperacion . "'
		AND IDPROYECTO =
		(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
		)
	)
	)";
			if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function liberaUnidadesPromesaEstacionamiento_escritura($codigoProyecto,$numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE UNIDAD
	SET ESTADO = '1'
	WHERE IDUNIDAD IN
	(
	SELECT IDUNIDAD
	FROM PROMESAESTACIONAMIENTO
	WHERE IDPROMESA =
	(
		SELECT IDPROMESA
		FROM PROMESA
		WHERE NUMERO = '" . $numeroOperacion . "'
		AND IDPROYECTO =
		(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
		)
	)
	)";
			if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function ingresaEscritura($idPromesa, $idProyecto, $idUnidad, $idCliente1, $idCliente2, $idUsuario,$numeroOperacion, $fechaEscritura, $valorBrutoUF, $descuento1, $descuento2, $bonoVentaUF, $valorTotalUF, $valorReservaUF, $valorPiePromesaUF, $valorPieSaldoUF, $cantidadCuotasPie, $valorSaldoTotalUF, $fechaPagoEscritura, $valorPagoEscritura, $formaPagoEscritura, $bancoEscritura, $serieChequeEscritura, $nroChequeEscritura, $valorPiePromesa, $valorPieSaldo, $valorReserva){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "INSERT INTO ESCRITURA
	(IDPROMESA, IDPROYECTO, IDUNIDAD, IDCLIENTE1, IDCLIENTE2, IDUSUARIO, NUMERO, ESTADO, FECHAESCRITURA, VALORBRUTOUF, DESCUENTO1, DESCUENTO2, BONOVENTAUF, VALORTOTALUF, VALORRESERVAUF, VALORPIEPROMESAUF, VALORPIESALDOUF, CANTCUOTASPIE, VALORSALDOTOTALUF, FECHAPAGOESCRITURA, VALORPAGOESCRITURA, FORMAPAGOESCRITURA, BANCOESCRITURA, SERIECHEQUEESCRITURA, NROCHEQUEESCRITURA, ESTADOPAGOESCRITURA, OBSERVACIONESCRITURA, VALORPIEPROMESA, VALORPIESALDO, VALORPAGORESERVA)
	VALUES('" . $idPromesa . "', '" . $idProyecto . "', '" . $idUnidad . "', '" . $idCliente1 . "', '" . $idCliente2 . "', '" . $idUsuario . "','" . $numeroOperacion . "','" .  2 . "', '" . $fechaEscritura . "', '" . $valorBrutoUF . "', '" . $descuento1 . "', '" . $descuento2 . "', '" . $bonoVentaUF . "', '" . $valorTotalUF . "', '" . $valorReservaUF . "', '" . $valorPiePromesaUF . "', '" . $valorPieSaldoUF . "', '" . $cantidadCuotasPie . "', '" . $valorSaldoTotalUF . "', '" . $fechaPagoEscritura . "', '" . $valorPagoEscritura . "', '" . $formaPagoEscritura . "', '" . $bancoEscritura . "', '" . $serieChequeEscritura . "', '" . $nroChequeEscritura . "', '', '','" . $valorPiePromesa . "','" . $valorPieSaldo . "','" . $valorReserva . "')";
				if ($con->query($sql)) {
						return $con;
				}
				else{
					return $con->error;
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}

		function ingresaEscrituraEstacionamiento($idEscritura, $idUnidad, $con){
			if($con != 'No conectado'){
				$sql = "INSERT INTO ESCRITURAESTACIONAMIENTO
	(IDESCRITURA, IDUNIDAD, VALORUF)
	VALUES('" .$idEscritura . "','" . $idUnidad . "',
	(
		SELECT VALORUF
		FROM UNIDAD
		WHERE IDUNIDAD = '" . $idUnidad . "'
	)
	)";
				if ($con->query($sql)) {
						return "Ok";
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function ingresaEscrituraBodega($idEscritura, $idUnidad, $con){
			if($con != 'No conectado'){
				$sql = "INSERT INTO ESCRITURABODEGA
	(IDESCRITURA, IDUNIDAD, VALORUF)
	VALUES('" .$idEscritura . "','" . $idUnidad . "',
	(
		SELECT VALORUF
		FROM UNIDAD
		WHERE IDUNIDAD = '" . $idUnidad . "'
	)
	)";
				if ($con->query($sql)) {
						return "Ok";
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
		function actualizaEscrituraFICHA_PDF($es_pdf,$idEscritura){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "UPDATE ESCRITURA
	SET ES_PDF = '" . $es_pdf . "'
	WHERE IDESCRITURA = " . $idEscritura;
				if ($con->query($sql)) {
						$con->query("COMMIT");
						return "Ok";
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		function consultaEscrituras(){
			$con = conectar();
			if($con != 'No conectado'){
					$sql = "SELECT P.CODIGOPROYECTO, ES.NUMERO, ES.FECHAESCRITURA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
		GROUP_CONCAT(DISTINCT UEE.CODIGO) 'EST',
		GROUP_CONCAT(DISTINCT UEB.CODIGO) 'BOD',
		ES.VALORTOTALUF, EES.COLOR 'ESTADO',
		CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_FICHA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
	R.FICHA_PDF,
	CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_COTIZA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
	R.COTIZA_PDF,
	CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_RCOMPRA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
	R.RCOMPRA_PDF,
	CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_CI_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
	R.CI_PDF,
	CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_PB_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
	R.PB_PDF,
	CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_CC_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
	R.CC_PDF,
	CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_CR_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
	R.CR_PDF,
		CASE WHEN PR.PR_PDF IS NOT NULL  AND PR.PR_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_PR_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PR',
	PR.PR_PDF,
	CASE WHEN PR.CP_PDF IS NOT NULL AND PR.CP_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_CP_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CP',
	PR.CP_PDF,
	CASE WHEN PR.PF_PDF IS NOT NULL AND PR.PF_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_PF_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PF',
	PR.PF_PDF,
	CASE WHEN PR.PS_PDF IS NOT NULL AND PR.PS_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_PS_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PS',
	PR.PS_PDF,
	CASE WHEN ES.ES_PDF IS NOT NULL AND ES.ES_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_ES_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'ES',
	ES.ES_PDF,
	CASE WHEN ES.EN_PDF IS NOT NULL AND ES.EN_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_EN_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'EN',
	ES.EN_PDF,
	CASE WHEN ES.DE_PDF IS NOT NULL AND ES.DE_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_DE_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'DE',
	ES.DE_PDF, ES.IDUSUARIO,
	C.CELULAR, C.EMAIL, EES.NOMBRE 'ESTADO_TEXTO'
		FROM ESCRITURA ES
		LEFT JOIN PROMESA PR ON
		ES.IDPROMESA = PR.IDPROMESA
		LEFT JOIN RESERVA R ON
		PR.IDRESERVA = R.IDRESERVA
			LEFT JOIN PROYECTO P ON
		ES.IDPROYECTO = P.IDPROYECTO
		LEFT JOIN CLIENTE C ON
			ES.IDCLIENTE1 = C.IDCLIENTE
		LEFT JOIN ESTADOESCRITURA EES ON
			ES.ESTADO = EES.IDESTADOESCRITURA
			LEFT JOIN UNIDAD U ON
		ES.IDUNIDAD = U.IDUNIDAD
		LEFT JOIN ESCRITURAESTACIONAMIENTO ESE
		ON ES.IDESCRITURA = ESE.IDESCRITURA
		LEFT JOIN UNIDAD UEE
		ON ESE.IDUNIDAD = UEE.IDUNIDAD
		LEFT JOIN ESCRITURABODEGA ESB
		ON ES.IDESCRITURA = ESB.IDESCRITURA
		LEFT JOIN UNIDAD UEB
		ON ESB.IDUNIDAD = UEB.IDUNIDAD
		GROUP BY P.CODIGOPROYECTO, ES.NUMERO, ES.FECHAESCRITURA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, ES.VALORTOTALUF, EES.COLOR, R.FICHA_PDF, R.COTIZA_PDF, R.RCOMPRA_PDF, R.CI_PDF, R.PB_PDF, R.CC_PDF, R.CR_PDF, PR.PR_PDF, PR.CP_PDF, PR.PF_PDF, PR.PS_PDF, ES.ES_PDF, ES.EN_PDF, ES.DE_PDF, ES.IDUSUARIO,
		C.CELULAR, C.EMAIL, EES.NOMBRE";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaEscriturasPro($codigoProyecto){
			$con = conectar();
			if($con != 'No conectado'){
					$sql = "SELECT P.CODIGOPROYECTO, ES.NUMERO, ES.FECHAESCRITURA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
		GROUP_CONCAT(DISTINCT UEE.CODIGO) 'EST',
		GROUP_CONCAT(DISTINCT UEB.CODIGO) 'BOD',
		ES.VALORTOTALUF, EES.COLOR 'ESTADO',
		CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_FICHA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
	R.FICHA_PDF,
	CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_COTIZA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
	R.COTIZA_PDF,
	CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_RCOMPRA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
	R.RCOMPRA_PDF,
	CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_CI_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
	R.CI_PDF,
	CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_PB_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
	R.PB_PDF,
	CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_CC_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
	R.CC_PDF,
	CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_CR_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
	R.CR_PDF,
		CASE WHEN PR.PR_PDF IS NOT NULL  AND PR.PR_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_PR_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PR',
	PR.PR_PDF,
	CASE WHEN PR.CP_PDF IS NOT NULL AND PR.CP_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_CP_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CP',
	PR.CP_PDF,
	CASE WHEN PR.PF_PDF IS NOT NULL AND PR.PF_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_PF_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PF',
	PR.PF_PDF,
	CASE WHEN PR.PS_PDF IS NOT NULL AND PR.PS_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''escritura_pdf_PS_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PS',
	PR.PS_PDF,
	CASE WHEN ES.ES_PDF IS NOT NULL AND ES.ES_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_ES_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'ES',
	ES.ES_PDF,
	CASE WHEN ES.EN_PDF IS NOT NULL AND ES.EN_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_EN_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'EN',
	ES.EN_PDF,
	CASE WHEN ES.DE_PDF IS NOT NULL AND ES.DE_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_DE_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'DE',
	ES.DE_PDF, ES.IDUSUARIO,
	C.CELULAR, C.EMAIL, EES.NOMBRE 'ESTADO_TEXTO'
		FROM ESCRITURA ES
		LEFT JOIN PROMESA PR ON
		ES.IDPROMESA = PR.IDPROMESA
		LEFT JOIN RESERVA R ON
		PR.IDRESERVA = R.IDRESERVA
			LEFT JOIN PROYECTO P ON
		ES.IDPROYECTO = P.IDPROYECTO
		LEFT JOIN CLIENTE C ON
			ES.IDCLIENTE1 = C.IDCLIENTE
		LEFT JOIN ESTADOESCRITURA EES ON
			ES.ESTADO = EES.IDESTADOESCRITURA
			LEFT JOIN UNIDAD U ON
		ES.IDUNIDAD = U.IDUNIDAD
		LEFT JOIN ESCRITURAESTACIONAMIENTO ESE
		ON ES.IDESCRITURA = ESE.IDESCRITURA
		LEFT JOIN UNIDAD UEE
		ON ESE.IDUNIDAD = UEE.IDUNIDAD
		LEFT JOIN ESCRITURABODEGA ESB
		ON ES.IDESCRITURA = ESB.IDESCRITURA
		LEFT JOIN UNIDAD UEB
		ON ESB.IDUNIDAD = UEB.IDUNIDAD
		WHERE P.CODIGOPROYECTO = '" . $codigoProyecto . "'
		GROUP BY P.CODIGOPROYECTO, ES.NUMERO, ES.FECHAESCRITURA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, ES.VALORTOTALUF, EES.COLOR, R.FICHA_PDF, R.COTIZA_PDF, R.RCOMPRA_PDF, R.CI_PDF, R.PB_PDF, R.CC_PDF, R.CR_PDF, PR.PR_PDF, PR.CP_PDF, PR.PF_PDF, PR.PS_PDF, ES.ES_PDF, ES.EN_PDF, ES.DE_PDF, ES.IDUSUARIO,
		C.CELULAR, C.EMAIL, EES.NOMBRE";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaEscriturasUsu($rutUsuario){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT P.CODIGOPROYECTO, ES.NUMERO, ES.FECHAESCRITURA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
		GROUP_CONCAT(DISTINCT UEE.CODIGO) 'EST',
		GROUP_CONCAT(DISTINCT UEB.CODIGO) 'BOD',
		ES.VALORTOTALUF, EES.COLOR 'ESTADO',
		CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_FICHA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
	R.FICHA_PDF,
	CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_COTIZA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
	R.COTIZA_PDF,
	CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_RCOMPRA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
	R.RCOMPRA_PDF,
	CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CI_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
	R.CI_PDF,
	CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_PB_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
	R.PB_PDF,
	CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CC_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
	R.CC_PDF,
	CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CR_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
	R.CR_PDF,
		CASE WHEN PR.PR_PDF IS NOT NULL  AND PR.PR_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PR_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PR',
	PR.PR_PDF,
	CASE WHEN PR.CP_PDF IS NOT NULL AND PR.CP_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CP_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CP',
	PR.CP_PDF,
	CASE WHEN PR.PF_PDF IS NOT NULL AND PR.PF_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PF_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PF',
	PR.PF_PDF,
	CASE WHEN PR.PS_PDF IS NOT NULL AND PR.PS_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PS_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PS',
	PR.PS_PDF,
	CASE WHEN ES.ES_PDF IS NOT NULL AND ES.ES_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_ES_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'ES',
	ES.ES_PDF,
	CASE WHEN ES.EN_PDF IS NOT NULL AND ES.EN_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_EN_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'EN',
	ES.EN_PDF,
	CASE WHEN ES.DE_PDF IS NOT NULL AND ES.DE_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_DE_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'DE',
	ES.DE_PDF, ES.IDUSUARIO,
	C.CELULAR, C.EMAIL, EES.NOMBRE 'ESTADO_TEXTO'
		FROM ESCRITURA ES
		LEFT JOIN PROMESA PR ON
		ES.IDPROMESA = PR.IDPROMESA
		LEFT JOIN RESERVA R ON
		PR.IDRESERVA = R.IDRESERVA
			LEFT JOIN PROYECTO P ON
		ES.IDPROYECTO = P.IDPROYECTO
		LEFT JOIN CLIENTE C ON
			ES.IDCLIENTE1 = C.IDCLIENTE
		LEFT JOIN ESTADOESCRITURA EES ON
			ES.ESTADO = EES.IDESTADOESCRITURA
			LEFT JOIN UNIDAD U ON
		ES.IDUNIDAD = U.IDUNIDAD
		LEFT JOIN ESCRITURAESTACIONAMIENTO ESE
		ON ES.IDESCRITURA = ESE.IDESCRITURA
		LEFT JOIN UNIDAD UEE
		ON ESE.IDUNIDAD = UEE.IDUNIDAD
		LEFT JOIN ESCRITURABODEGA ESB
		ON ES.IDESCRITURA = ESB.IDESCRITURA
		LEFT JOIN UNIDAD UEB
		ON ESB.IDUNIDAD = UEB.IDUNIDAD
	WHERE ES.IDPROYECTO IN
	(
	SELECT IDPROYECTO
	FROM USUARIOPROYECTO
	WHERE IDUSUARIO =
	(
		SELECT IDUSUARIO
		FROM USUARIO
		WHERE RUT = '"  . $rutUsuario . "'
	)
	)
		GROUP BY P.CODIGOPROYECTO, ES.NUMERO, ES.FECHAESCRITURA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, ES.VALORTOTALUF, EES.COLOR, R.FICHA_PDF, R.COTIZA_PDF, R.RCOMPRA_PDF, R.CI_PDF, R.PB_PDF, R.CC_PDF, R.CR_PDF, PR.PR_PDF, PR.CP_PDF, PR.PF_PDF, PR.PS_PDF, ES.ES_PDF, ES.EN_PDF, ES.DE_PDF, ES.IDUSUARIO,
		C.CELULAR, C.EMAIL, EES.NOMBRE";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaEscriturasUsuPro($rutUsuario,$codigoProyecto){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT P.CODIGOPROYECTO, ES.NUMERO, ES.FECHAESCRITURA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
		GROUP_CONCAT(DISTINCT UEE.CODIGO) 'EST',
		GROUP_CONCAT(DISTINCT UEB.CODIGO) 'BOD',
		ES.VALORTOTALUF, EES.COLOR 'ESTADO',
		CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_FICHA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
	R.FICHA_PDF,
	CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_COTIZA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
	R.COTIZA_PDF,
	CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_RCOMPRA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
	R.RCOMPRA_PDF,
	CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CI_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
	R.CI_PDF,
	CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_PB_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
	R.PB_PDF,
	CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CC_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
	R.CC_PDF,
	CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CR_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
	R.CR_PDF,
		CASE WHEN PR.PR_PDF IS NOT NULL  AND PR.PR_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PR_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PR',
	PR.PR_PDF,
	CASE WHEN PR.CP_PDF IS NOT NULL AND PR.CP_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CP_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CP',
	PR.CP_PDF,
	CASE WHEN PR.PF_PDF IS NOT NULL AND PR.PF_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PF_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PF',
	PR.PF_PDF,
	CASE WHEN PR.PS_PDF IS NOT NULL AND PR.PS_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PS_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PS',
	PR.PS_PDF,
	CASE WHEN ES.ES_PDF IS NOT NULL AND ES.ES_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_ES_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'ES',
	ES.ES_PDF,
	CASE WHEN ES.EN_PDF IS NOT NULL AND ES.EN_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_EN_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'EN',
	ES.EN_PDF,
	CASE WHEN ES.DE_PDF IS NOT NULL AND ES.DE_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_DE_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'DE',
	ES.DE_PDF, ES.IDUSUARIO,
	C.CELULAR, C.EMAIL, EES.NOMBRE 'ESTADO_TEXTO'
		FROM ESCRITURA ES
		LEFT JOIN PROMESA PR ON
		ES.IDPROMESA = PR.IDPROMESA
		LEFT JOIN RESERVA R ON
		PR.IDRESERVA = R.IDRESERVA
			LEFT JOIN PROYECTO P ON
		ES.IDPROYECTO = P.IDPROYECTO
		LEFT JOIN CLIENTE C ON
			ES.IDCLIENTE1 = C.IDCLIENTE
		LEFT JOIN ESTADOESCRITURA EES ON
			ES.ESTADO = EES.IDESTADOESCRITURA
			LEFT JOIN UNIDAD U ON
		ES.IDUNIDAD = U.IDUNIDAD
		LEFT JOIN ESCRITURAESTACIONAMIENTO ESE
		ON ES.IDESCRITURA = ESE.IDESCRITURA
		LEFT JOIN UNIDAD UEE
		ON ESE.IDUNIDAD = UEE.IDUNIDAD
		LEFT JOIN ESCRITURABODEGA ESB
		ON ES.IDESCRITURA = ESB.IDESCRITURA
		LEFT JOIN UNIDAD UEB
		ON ESB.IDUNIDAD = UEB.IDUNIDAD
	WHERE ES.IDPROYECTO IN
	(
	SELECT IDPROYECTO
	FROM USUARIOPROYECTO
	WHERE IDUSUARIO =
	(
		SELECT IDUSUARIO
		FROM USUARIO
		WHERE RUT = '"  . $rutUsuario . "'
	)
	)
	AND P.CODIGOPROYECTO = '" . $codigoProyecto . "'
		GROUP BY P.CODIGOPROYECTO, ES.NUMERO, ES.FECHAESCRITURA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, ES.VALORTOTALUF, EES.COLOR, R.FICHA_PDF, R.COTIZA_PDF, R.RCOMPRA_PDF, R.CI_PDF, R.PB_PDF, R.CC_PDF, R.CR_PDF, PR.PR_PDF, PR.CP_PDF, PR.PF_PDF, PR.PS_PDF, ES.ES_PDF, ES.EN_PDF, ES.DE_PDF, ES.IDUSUARIO,
		C.CELULAR, C.EMAIL, EES.NOMBRE";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaEscriturasCli($rutUsuario){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT P.CODIGOPROYECTO, ES.NUMERO, ES.FECHAESCRITURA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
		GROUP_CONCAT(DISTINCT UEE.CODIGO) 'EST',
		GROUP_CONCAT(DISTINCT UEB.CODIGO) 'BOD',
		ES.VALORTOTALUF, EES.COLOR 'ESTADO',
		CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_FICHA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
	R.FICHA_PDF,
	CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_COTIZA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
	R.COTIZA_PDF,
	CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_RCOMPRA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
	R.RCOMPRA_PDF,
	CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CI_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
	R.CI_PDF,
	CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_PB_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
	R.PB_PDF,
	CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CC_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
	R.CC_PDF,
	CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CR_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
	R.CR_PDF,
		CASE WHEN PR.PR_PDF IS NOT NULL  AND PR.PR_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PR_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PR',
	PR.PR_PDF,
	CASE WHEN PR.CP_PDF IS NOT NULL AND PR.CP_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CP_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CP',
	PR.CP_PDF,
	CASE WHEN PR.PF_PDF IS NOT NULL AND PR.PF_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PF_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PF',
	PR.PF_PDF,
	CASE WHEN PR.PS_PDF IS NOT NULL AND PR.PS_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PS_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PS',
	PR.PS_PDF,
	CASE WHEN ES.ES_PDF IS NOT NULL AND ES.ES_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_ES_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'ES',
	ES.ES_PDF,
	CASE WHEN ES.EN_PDF IS NOT NULL AND ES.EN_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_EN_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'EN',
	ES.EN_PDF,
	CASE WHEN ES.DE_PDF IS NOT NULL AND ES.DE_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_DE_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'DE',
	ES.DE_PDF, ES.IDUSUARIO,
	C.CELULAR, C.EMAIL, EES.NOMBRE 'ESTADO_TEXTO'
		FROM ESCRITURA ES
		LEFT JOIN PROMESA PR ON
		ES.IDPROMESA = PR.IDPROMESA
		LEFT JOIN RESERVA R ON
		PR.IDRESERVA = R.IDRESERVA
			LEFT JOIN PROYECTO P ON
		ES.IDPROYECTO = P.IDPROYECTO
		LEFT JOIN CLIENTE C ON
			ES.IDCLIENTE1 = C.IDCLIENTE
		LEFT JOIN ESTADOESCRITURA EES ON
			ES.ESTADO = EES.IDESTADOESCRITURA
			LEFT JOIN UNIDAD U ON
		ES.IDUNIDAD = U.IDUNIDAD
		LEFT JOIN ESCRITURAESTACIONAMIENTO ESE
		ON ES.IDESCRITURA = ESE.IDESCRITURA
		LEFT JOIN UNIDAD UEE
		ON ESE.IDUNIDAD = UEE.IDUNIDAD
		LEFT JOIN ESCRITURABODEGA ESB
		ON ES.IDESCRITURA = ESB.IDESCRITURA
		LEFT JOIN UNIDAD UEB
		ON ESB.IDUNIDAD = UEB.IDUNIDAD
	WHERE ES.IDCLIENTE1 IN
	(
		SELECT IDCLIENTE
		FROM CLIENTE
		WHERE RUT = '"  . $rutUsuario . "'
	)
		GROUP BY P.CODIGOPROYECTO, ES.NUMERO, ES.FECHAESCRITURA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, ES.VALORTOTALUF, EES.COLOR, R.FICHA_PDF, R.COTIZA_PDF, R.RCOMPRA_PDF, R.CI_PDF, R.PB_PDF, R.CC_PDF, R.CR_PDF, PR.PR_PDF, PR.CP_PDF, PR.PF_PDF, PR.PS_PDF, ES.ES_PDF, ES.EN_PDF, ES.DE_PDF, ES.IDUSUARIO,
		C.CELULAR, C.EMAIL, EES.NOMBRE";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaEscriturasCliPro($rutUsuario,$codigoProyecto){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT P.CODIGOPROYECTO, ES.NUMERO, ES.FECHAESCRITURA 'FECHA', U.CODIGO 'UNIDAD', C.NOMBRES, C.APELLIDOS, C.RUT,
		GROUP_CONCAT(DISTINCT UEE.CODIGO) 'EST',
		GROUP_CONCAT(DISTINCT UEB.CODIGO) 'BOD',
		ES.VALORTOTALUF, EES.COLOR 'ESTADO',
		CASE WHEN R.FICHA_PDF IS NOT NULL  AND R.FICHA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_FICHA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'FICHA',
	R.FICHA_PDF,
	CASE WHEN R.COTIZA_PDF IS NOT NULL AND R.COTIZA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_COTIZA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'COTIZA',
	R.COTIZA_PDF,
	CASE WHEN R.RCOMPRA_PDF IS NOT NULL AND R.RCOMPRA_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_RCOMPRA_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'RCOMPRA',
	R.RCOMPRA_PDF,
	CASE WHEN R.CI_PDF IS NOT NULL AND R.CI_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CI_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CI',
	R.CI_PDF,
	CASE WHEN R.PB_PDF IS NOT NULL  AND R.PB_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_PB_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PB',
	R.PB_PDF,
	CASE WHEN R.CC_PDF IS NOT NULL  AND R.CC_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CC_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CC',
	R.CC_PDF,
	CASE WHEN R.CR_PDF IS NOT NULL  AND R.CR_PDF <> ''  THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''promesa_pdf_CR_RESERVA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CR',
	R.CR_PDF,
		CASE WHEN PR.PR_PDF IS NOT NULL  AND PR.PR_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PR_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PR',
	PR.PR_PDF,
	CASE WHEN PR.CP_PDF IS NOT NULL AND PR.CP_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_CP_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'CP',
	PR.CP_PDF,
	CASE WHEN PR.PF_PDF IS NOT NULL AND PR.PF_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PF_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PF',
	PR.PF_PDF,
	CASE WHEN PR.PS_PDF IS NOT NULL AND PR.PS_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_PS_PROMESA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'PS',
	PR.PS_PDF,
	CASE WHEN ES.ES_PDF IS NOT NULL AND ES.ES_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_ES_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'ES',
	ES.ES_PDF,
	CASE WHEN ES.EN_PDF IS NOT NULL AND ES.EN_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_EN_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'EN',
	ES.EN_PDF,
	CASE WHEN ES.DE_PDF IS NOT NULL AND ES.DE_PDF <> '' THEN '<span class=''fa fa-file-pdf-o pdf'' title=''Documento cargado'' onclick=''pdf_DE_ESCRITURA()''></span>'
	ELSE '<span style=''color: black;'' class=''fa fa-file-pdf-o'' title=''Vacio''></span>' END 'DE',
	ES.DE_PDF, ES.IDUSUARIO,
	C.CELULAR, C.EMAIL, EES.NOMBRE 'ESTADO_TEXTO'
		FROM ESCRITURA ES
		LEFT JOIN PROMESA PR ON
		ES.IDPROMESA = PR.IDPROMESA
		LEFT JOIN RESERVA R ON
		PR.IDRESERVA = R.IDRESERVA
			LEFT JOIN PROYECTO P ON
		ES.IDPROYECTO = P.IDPROYECTO
		LEFT JOIN CLIENTE C ON
			ES.IDCLIENTE1 = C.IDCLIENTE
		LEFT JOIN ESTADOESCRITURA EES ON
			ES.ESTADO = EES.IDESTADOESCRITURA
			LEFT JOIN UNIDAD U ON
		ES.IDUNIDAD = U.IDUNIDAD
		LEFT JOIN ESCRITURAESTACIONAMIENTO ESE
		ON ES.IDESCRITURA = ESE.IDESCRITURA
		LEFT JOIN UNIDAD UEE
		ON ESE.IDUNIDAD = UEE.IDUNIDAD
		LEFT JOIN ESCRITURABODEGA ESB
		ON ES.IDESCRITURA = ESB.IDESCRITURA
		LEFT JOIN UNIDAD UEB
		ON ESB.IDUNIDAD = UEB.IDUNIDAD
	WHERE ES.IDCLIENTE1 IN
	(
		SELECT IDCLIENTE
		FROM CLIENTE
		WHERE RUT = '"  . $rutUsuario . "'
	)
	AND P.CODIGOPROYECTO = '"  . $codigoProyecto . "'
		GROUP BY P.CODIGOPROYECTO, ES.NUMERO, ES.FECHAESCRITURA, U.CODIGO, C.NOMBRES, C.APELLIDOS, C.RUT, ES.VALORTOTALUF, EES.COLOR, R.FICHA_PDF, R.COTIZA_PDF, R.RCOMPRA_PDF, R.CI_PDF, R.PB_PDF, R.CC_PDF, R.CR_PDF, PR.PR_PDF, PR.CP_PDF, PR.PF_PDF, PR.PS_PDF, ES.ES_PDF, ES.EN_PDF, ES.DE_PDF, ES.IDUSUARIO,
		C.CELULAR, C.EMAIL, EES.NOMBRE";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaEscrituraEspecificaCargaDocs($codigoProyecto, $numeroOperacion){
				$con = conectar();
				if($con != 'No conectado'){
					$sql = "SELECT ES.IDESCRITURA, ES.ES_PDF, ES.EN_PDF, ES.DE_PDF, U.CODIGO, C.NOMBRES, C.APELLIDOS
	FROM ESCRITURA ES, UNIDAD U, CLIENTE C
	WHERE ES.IDPROYECTO =
	(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
	)
	AND ES.NUMERO = '" . $numeroOperacion . "'
	AND ES.IDUNIDAD = U.IDUNIDAD
	AND ES.IDCLIENTE1 = C.IDCLIENTE";
					if ($row = $con->query($sql)) {
							while($array = $row->fetch_array(MYSQLI_BOTH)){
								$return[] = $array;
							}

							return $return;
					}
					else{
						return "Error";
					}
				}
				else{
					return "Error";
				}
			}
			function actualizaRutasDocumentosEscritura($rutaES, $rutaEN, $rutaDE, $idEscritura){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "UPDATE ESCRITURA
	SET ES_PDF = '" . $rutaES . "',
	EN_PDF = '" . $rutaEN . "',
	DE_PDF = '" . $rutaDE . "'
	WHERE IDESCRITURA = '" . $idEscritura . "'";
				if ($con->query($sql)) {
						$con->query("COMMIT");
						return "Ok";
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}

		function eliminaEscritura($codigoProyecto,$numeroOperacion){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "DELETE FROM ESCRITURA
			WHERE NUMERO = '" .  $numeroOperacion. "'
	AND IDPROYECTO =
	(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
	)";
				if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		function consultaEscrituraEspecifica($codigoProyecto, $numeroOperacion){
				$con = conectar();
				if($con != 'No conectado'){
					$sql = "SELECT IDESCRITURA, IDUNIDAD, ES_PDF, EN_PDF, DE_PDF, ESTADO
					FROM ESCRITURA
					WHERE IDPROYECTO =
					(
					SELECT IDPROYECTO
					FROM PROYECTO
					WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
					)
					AND NUMERO = '" . $numeroOperacion . "'";
					if ($row = $con->query($sql)) {
							while($array = $row->fetch_array(MYSQLI_BOTH)){
								$return[] = $array;
							}

							return $return;
					}
					else{
						return "Error";
					}
				}
				else{
					return "Error";
				}
			}
		function cambioEstadoUnidadesEscritura($idUnidad){
			$con = conectar();
			$con->query ("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "UPDATE UNIDAD
			SET ESTADO = 4
			WHERE IDUNIDAD = '".$idUnidad."'";
				if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		function liberaUnidadesEscrituraBodega($codigoProyecto,$numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE UNIDAD
	SET ESTADO = '4'
	WHERE IDUNIDAD IN
	(
	SELECT IDUNIDAD
	FROM ESCRITURABODEGA
	WHERE IDESCRITURA =
	(
		SELECT IDESCRITURA
		FROM ESCRITURA
		WHERE NUMERO = '" . $numeroOperacion . "'
		AND IDPROYECTO =
		(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
		)
	)
	)";
			if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function liberaUnidadesEscrituraEstacionamiento($codigoProyecto,$numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE UNIDAD
	SET ESTADO = '4'
	WHERE IDUNIDAD IN
	(
	SELECT IDUNIDAD
	FROM ESCRITURAESTACIONAMIENTO
	WHERE IDESCRITURA =
	(
		SELECT IDESCRITURA
		FROM ESCRITURA
		WHERE NUMERO = '" . $numeroOperacion . "'
		AND IDPROYECTO =
		(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
		)
	)
	)";
			if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function liberaUnidadesEscrituradas($codigoProyecto,$numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE UNIDAD
	SET ESTADO = '4'
	WHERE IDUNIDAD IN
	(
	SELECT IDUNIDAD
	FROM ESCRITURA
	WHERE NUMERO = '" . $numeroOperacion . "'
	AND IDPROYECTO =
	(
		SELECT IDPROYECTO
		FROM PROYECTO
		WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
	)
	)";
			if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function eliminaEscrituraBodega($codigoProyecto, $numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM ESCRITURABODEGA
			WHERE IDESCRITURA =
			(
			SELECT IDESCRITURA
			FROM ESCRITURA
			WHERE NUMERO = '" . $numeroOperacion . "'
			AND IDPROYECTO =
			(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			)";
			if ($con->query($sql)) {
				$con->query("COMMIT");
				return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function eliminaEscrituraEstacionamiento($codigoProyecto, $numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM ESCRITURAESTACIONAMIENTO
			WHERE IDESCRITURA =
			(
			SELECT IDESCRITURA
			FROM ESCRITURA
			WHERE NUMERO = '" . $numeroOperacion . "'
			AND IDPROYECTO =
			(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			)";
			if ($con->query($sql)) {
				$con->query("COMMIT");
				return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function ingresaEscrituraCuota($idEscritura, $monto_cuota, $forma_cuota, $banco_cuota, $serie_cuota, $nro_cuota, $fecha_cuota, $montouf_cuota, $con){
			if($con != 'No conectado'){
				$sql = "INSERT INTO ESCRITURACUOTA
	(IDESCRITURA, VALORMONTOCUOTA, FORMAPAGOCUOTA, BANCOCUOTA, SERIECHEQUECUOTA, NROCHEQUECUOTA, FECHAPAGOCUOTA, MONTO_UF)
	VALUES('" .$idEscritura . "','" . $monto_cuota . "','" . $forma_cuota . "','" . $banco_cuota . "','" . $serie_cuota . "','" . $nro_cuota . "','" . $fecha_cuota ."','" . $montouf_cuota . "')";
				if ($con->query($sql)) {
						return "Ok";
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
	function eliminaEscrituraCuota($codigoProyecto,$numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM ESCRITURACUOTA
			WHERE IDESCRITURA =
			(
			SELECT IDESCRITURA
			FROM ESCRITURA
			WHERE NUMERO = '" . $numeroOperacion . "'
			AND IDPROYECTO =
			(
			SELECT IDPROYECTO
			FROM PROYECTO
			WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			)";
			if ($con->query($sql)) {
				$con->query("COMMIT");
				return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function datosEscrituraEspecifica($codigoProyecto,$numeroOperacion){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT ES.*, R.FORMAPAGORESERVA, R.BANCORESERVA, R.SERIECHEQUERESERVA, R.NROCHEQUERESERVA, R.FECHAPAGORESERVA, PR.FECHAPROMESA, PR.FORMAPAGOPROMESA, PR.BANCOPROMESA, PR.SERIECHEQUEPROMESA, PR.NROCHEQUEPROMESA, PR.FECHAPAGOPROMESA, CL.NOMBRES, CL.APELLIDOS, CL.RUT, CL.CELULAR, CL.EMAIL, U.CODIGO 'DEPTO', T.NOMBRE 'TIPOLOGIA', TI.NOMBRE 'MODELO', U.ORIENTACION,
	(
	SELECT COUNT(IDUNIDAD)
	FROM ESCRITURABODEGA
	WHERE IDESCRITURA = ES.IDESCRITURA
	) BOD,
	(
	SELECT COUNT(IDUNIDAD)
	FROM ESCRITURAESTACIONAMIENTO
	WHERE IDESCRITURA = ES.IDESCRITURA
	) EST,
	U.M2UTIL, U.M2TERRAZA, U.M2TOTAL,
	CASE WHEN C.VALORUNIDADUF IS NOT NULL THEN C.VALORUNIDADUF ELSE U.VALORUF END 'DEPTOUF',
	(
	SELECT SUM(VALORUF)
	FROM ESCRITURABODEGA
	WHERE IDESCRITURA = ES.IDESCRITURA
	) BODUF,
	(
	SELECT SUM(VALORUF)
	FROM ESCRITURAESTACIONAMIENTO
	WHERE IDESCRITURA = ES.IDESCRITURA
	) ESTUF,
	P.NOMBRE 'NOMBREPROYECTO'
	-- ES.BONOVENTAUF, ES.DESCUENTO1, ES.DESCUENTO2, ES.VALORTOTALUF
	FROM ESCRITURA ES
	LEFT JOIN PROMESA PR
	ON ES.IDPROMESA = PR.IDPROMESA
	LEFT JOIN RESERVA R
	ON PR.IDRESERVA = R.IDRESERVA
	LEFT JOIN COTIZACION C
	ON R.IDCOTIZACION = C.IDCOTIZACION
	LEFT JOIN CLIENTE CL
	ON ES.IDCLIENTE1 = CL.IDCLIENTE
	LEFT JOIN PROYECTO P
	ON ES.IDPROYECTO = P.IDPROYECTO
	LEFT JOIN UNIDAD U
	ON ES.IDUNIDAD = U.IDUNIDAD
	LEFT JOIN TIPOLOGIA T
	ON U.TIPOLOGIA = T.IDTIPOLOGIA
	LEFT JOIN TIPOUNIDAD TI
	ON U.IDTIPOUNIDAD = TI.IDTIPOUNIDAD
	WHERE ES.IDPROYECTO =
	(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
	)
	AND ES.NUMERO = '" . $numeroOperacion . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function consultaBodegasEscrituradas($idEscritura){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT E.IDUNIDAD, U.CODIGO, E.VALORUF
	FROM UNIDAD U JOIN ESCRITURABODEGA E ON
	U.IDUNIDAD = E.IDUNIDAD
	WHERE IDESCRITURA =
	(
	SELECT IDESCRITURA
	FROM ESCRITURA
	WHERE IDESCRITURA = '".$idEscritura."'
	)
	";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

		function consultaEstacionamientosEscriturados($idEscritura){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT E.IDUNIDAD, U.CODIGO, E.VALORUF
	FROM UNIDAD U JOIN ESCRITURAESTACIONAMIENTO E ON
	U.IDUNIDAD = E.IDUNIDAD
	WHERE IDESCRITURA =
	(
	SELECT IDESCRITURA
	FROM ESCRITURA
	WHERE IDESCRITURA = '".$idEscritura."'
	)
	";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
	function consultaCuotasEscrituradas($codigoProyecto, $numeroOperacion){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT * FROM ESCRITURACUOTA
			WHERE IDESCRITURA = (SELECT IDESCRITURA FROM ESCRITURA
			WHERE IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '".$codigoProyecto."')
			 AND NUMERO = '".$numeroOperacion."')";
			if ($row = $con->query($sql)) {
					while($array = $row->fetch_array(MYSQLI_BOTH)){
						$return[] = $array;
					}
					return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function actualizarEscritura($codigoProyecto, $numeroOperacion, $valorTotalUF,  $valorSaldoTotalUF, $fechaPagoEscritura, $valorPagoEscritura, $formaPagoEscritura, $bancoEscritura, $serieChequeEscritura, $nroChequeEscritura, $vendedorID, $fechaEscritura){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE ESCRITURA
	SET VALORTOTALUF = '" . $valorTotalUF . "',
	VALORSALDOTOTALUF = '" . $valorSaldoTotalUF . "',
	FECHAPAGOESCRITURA = '" . $fechaPagoEscritura . "',
	VALORPAGOESCRITURA = '" . $valorPagoEscritura . "',
	FORMAPAGOESCRITURA = '" . $formaPagoEscritura . "',
	BANCOESCRITURA = '" . $bancoEscritura . "',
	SERIECHEQUEESCRITURA = '" . $serieChequeEscritura . "',
	NROCHEQUEESCRITURA = '" . $nroChequeEscritura . "',
	IDUSUARIO = '" . $vendedorID . "',
	FECHAESCRITURA = '" . $fechaEscritura . "'
	WHERE IDPROYECTO =
	(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
	)
	AND NUMERO = '" . $numeroOperacion . "'";
			if ($con->query($sql)) {
					return $con;
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function consultaEscrituraEspecificaActualizar($codigoProyecto, $numeroOperacion){
				$con = conectar();
				if($con != 'No conectado'){
					$sql = "SELECT ES.*, PR.FECHAPAGOPROMESA, PR.FORMAPAGOPROMESA, PR.BANCOPROMESA, PR.SERIECHEQUEPROMESA, PR.NROCHEQUEPROMESA, R.IDRESERVA, R.FECHAPAGORESERVA, R.FORMAPAGORESERVA, R.BANCORESERVA, R.SERIECHEQUERESERVA, R.NROCHEQUERESERVA
					FROM ESCRITURA ES
					LEFT JOIN PROMESA PR
					ON ES.IDPROMESA = PR.IDPROMESA
					LEFT JOIN RESERVA R
					ON PR.IDRESERVA = R.IDRESERVA
					WHERE ES.IDPROYECTO =
					(
					SELECT IDPROYECTO
					FROM PROYECTO
					WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
					)
					AND ES.NUMERO = '" . $numeroOperacion . "'";
					if ($row = $con->query($sql)) {
							while($array = $row->fetch_array(MYSQLI_BOTH)){
								$return[] = $array;
							}

							return $return;
					}
					else{
						return "Error";
					}
				}
				else{
					return "Error";
				}
			}
	function consultaEstadosEscrituras(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
	FROM ESTADOESCRITURA";
			if ($row = $con->query($sql)) {
					while($array = $row->fetch_array(MYSQLI_BOTH)){
						$return[] = $array;
					}

					return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function cambiarEstadoEscritura($codigoProyecto, $numeroOperacion, $estadoEscritura){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "UPDATE ESCRITURA
	SET ESTADO = '" . $estadoEscritura . "'
	WHERE IDPROYECTO =
	(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
	)
	AND NUMERO = '" . $numeroOperacion . "'";
				if ($con->query($sql)) {
						$con->query("COMMIT");
						return "Ok";
				}
				else{
					$con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
	function consultaOperacionesContables(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, R.NUMERO, U.CODIGO 'DEPTO',
GROUP_CONCAT(DISTINCT URE.CODIGO) 'EST',
GROUP_CONCAT(DISTINCT URB.CODIGO) 'BOD',
R.VALORTOTALUF,
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Promesado' THEN PA.VALORRESERVAUF + PA.VALORPIEPROMESAUF + PA.VALORPIESALDOUF ELSE
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Reservado' THEN R.VALORRESERVAUF + R.VALORPIEPROMESAUF + R.VALORPIESALDOUF ELSE
EA.VALORRESERVAUF + EA.VALORPIEPROMESAUF + EA.VALORPIESALDOUF END END  'VALORPIE',
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Promesado' THEN PA.VALORSALDOTOTALUF ELSE
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Reservado' THEN R.VALORSALDOTOTALUF ELSE
EA.VALORSALDOTOTALUF END END 'VALORSALDOUF',
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.COLOR END AS 'ESTADO',
C.RUT, C.NOMBRES, C.APELLIDOS, C.CELULAR, C.EMAIL,
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END AS 'ESTADO_TEXTO',
BA.NOMBRE 'BANCO', MO.NOMBRE 'INVERSION'
FROM UNIDAD U
LEFT JOIN RESERVA R ON U.IDUNIDAD = R.IDUNIDAD
AND R.ESTADO NOT IN (7,9)
LEFT JOIN PROMESA PA ON U.IDUNIDAD = PA.IDUNIDAD
AND R.NUMERO = PA.NUMERO
AND PA.ESTADO NOT IN (1,9)
LEFT JOIN ESCRITURA EA ON U.IDUNIDAD = EA.IDUNIDAD
AND R.NUMERO = EA.NUMERO
AND EA.ESTADO NOT IN (1,8)
LEFT JOIN ESTADORESERVA RES ON R.ESTADO = RES.IDESTADORESERVA
LEFT JOIN PROYECTO P ON U.IDPROYECTO = P.IDPROYECTO
LEFT JOIN CLIENTE C ON R.IDCLIENTE1 = C.IDCLIENTE
LEFT JOIN ESTADOUNIDAD UE ON U.ESTADO = UE.IDESTADOUNIDAD
LEFT JOIN RESERVAESTACIONAMIENTO RE ON R.IDRESERVA = RE.IDRESERVA
LEFT JOIN UNIDAD URE ON RE.IDUNIDAD = URE.IDUNIDAD
LEFT JOIN RESERVABODEGA RB ON R.IDRESERVA = RB.IDRESERVA
LEFT JOIN UNIDAD URB ON RB.IDUNIDAD = URB.IDUNIDAD
LEFT JOIN COMENTARIO CO
ON P.IDPROYECTO = CO.IDPROYECTO
AND U.IDUNIDAD = CO.IDUNIDAD
AND R.NUMERO = CO.NUMERO
LEFT JOIN BANCO BA
ON CO.IDBANCO = BA.IDBANCO
LEFT JOIN MOTIVOCOMPRA MO
ON CO.IDTIPO = MO.IDMOTIVOCOMPRA
WHERE U.IDTIPOUNIDAD NOT IN (3, 4)
AND ((U.ESTADO >= 3 AND U.CODIGO <> '9999') OR ( U.CODIGO = '9999'))
GROUP BY P.CODIGOPROYECTO, R.NUMERO, U.CODIGO, R.VALORTOTALUF, RES.COLOR, UE.COLOR, UE.NOMBRE, C.RUT, C.NOMBRES, C.APELLIDOS, C.CELULAR, C.EMAIL, EA.IDUNIDAD, PA.IDUNIDAD, PA.VALORRESERVAUF, PA.VALORPIEPROMESAUF, PA.VALORPIESALDOUF,
R.VALORRESERVAUF, R.VALORPIEPROMESAUF, R.VALORPIESALDOUF,
EA.VALORRESERVAUF, EA.VALORPIEPROMESAUF, EA.VALORPIESALDOUF,
PA.VALORSALDOTOTALUF, R.VALORSALDOTOTALUF, EA.VALORSALDOTOTALUF, BA.NOMBRE, MO.IDMOTIVOCOMPRA";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
		function consultaOperacionesContablesUsu($rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, R.NUMERO, U.CODIGO 'DEPTO',
GROUP_CONCAT(DISTINCT URE.CODIGO) 'EST',
GROUP_CONCAT(DISTINCT URB.CODIGO) 'BOD',
R.VALORTOTALUF,
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Promesado' THEN PA.VALORRESERVAUF + PA.VALORPIEPROMESAUF + PA.VALORPIESALDOUF ELSE
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Reservado' THEN R.VALORRESERVAUF + R.VALORPIEPROMESAUF + R.VALORPIESALDOUF ELSE
EA.VALORRESERVAUF + EA.VALORPIEPROMESAUF + EA.VALORPIESALDOUF END END  'VALORPIE',
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Promesado' THEN PA.VALORSALDOTOTALUF ELSE
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Reservado' THEN R.VALORSALDOTOTALUF ELSE
EA.VALORSALDOTOTALUF END END 'VALORSALDOUF',
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.COLOR END AS 'ESTADO',
C.RUT, C.NOMBRES, C.APELLIDOS, C.CELULAR, C.EMAIL,
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END AS 'ESTADO_TEXTO',
BA.NOMBRE 'BANCO', MO.NOMBRE 'INVERSION'
FROM UNIDAD U
LEFT JOIN RESERVA R ON U.IDUNIDAD = R.IDUNIDAD
AND R.ESTADO NOT IN (7,9)
LEFT JOIN PROMESA PA ON U.IDUNIDAD = PA.IDUNIDAD
AND R.NUMERO = PA.NUMERO
AND PA.ESTADO NOT IN (1,9)
LEFT JOIN ESCRITURA EA ON U.IDUNIDAD = EA.IDUNIDAD
AND R.NUMERO = EA.NUMERO
AND EA.ESTADO NOT IN (1,8)
LEFT JOIN ESTADORESERVA RES ON R.ESTADO = RES.IDESTADORESERVA
LEFT JOIN PROYECTO P ON U.IDPROYECTO = P.IDPROYECTO
LEFT JOIN CLIENTE C ON R.IDCLIENTE1 = C.IDCLIENTE
LEFT JOIN ESTADOUNIDAD UE ON U.ESTADO = UE.IDESTADOUNIDAD
LEFT JOIN RESERVAESTACIONAMIENTO RE ON R.IDRESERVA = RE.IDRESERVA
LEFT JOIN UNIDAD URE ON RE.IDUNIDAD = URE.IDUNIDAD
LEFT JOIN RESERVABODEGA RB ON R.IDRESERVA = RB.IDRESERVA
LEFT JOIN UNIDAD URB ON RB.IDUNIDAD = URB.IDUNIDAD
LEFT JOIN COMENTARIO CO
ON P.IDPROYECTO = CO.IDPROYECTO
AND U.IDUNIDAD = CO.IDUNIDAD
AND R.NUMERO = CO.NUMERO
LEFT JOIN BANCO BA
ON CO.IDBANCO = BA.IDBANCO
LEFT JOIN MOTIVOCOMPRA MO
ON CO.IDTIPO = MO.IDMOTIVOCOMPRA
WHERE U.IDTIPOUNIDAD NOT IN (3, 4)
AND ((U.ESTADO >= 3 AND U.CODIGO <> '9999') OR ( U.CODIGO = '9999'))
AND R.IDPROYECTO IN
(
	SELECT IDPROYECTO
	FROM USUARIOPROYECTO
	WHERE IDUSUARIO =
	(
		SELECT IDUSUARIO
		FROM USUARIO
		WHERE RUT = '" . $rutUsuario . "'
	)
)
GROUP BY P.CODIGOPROYECTO, R.NUMERO, U.CODIGO, R.VALORTOTALUF, RES.COLOR, UE.COLOR, UE.NOMBRE, C.RUT, C.NOMBRES, C.APELLIDOS, C.CELULAR, C.EMAIL, EA.IDUNIDAD, PA.IDUNIDAD, PA.VALORRESERVAUF, PA.VALORPIEPROMESAUF, PA.VALORPIESALDOUF,
R.VALORRESERVAUF, R.VALORPIEPROMESAUF, R.VALORPIESALDOUF,
EA.VALORRESERVAUF, EA.VALORPIEPROMESAUF, EA.VALORPIESALDOUF,
PA.VALORSALDOTOTALUF, R.VALORSALDOTOTALUF, EA.VALORSALDOTOTALUF, BA.NOMBRE, MO.IDMOTIVOCOMPRA";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
		function filtroContable($codigoProyecto, $idEstado){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, R.NUMERO, U.CODIGO 'DEPTO',
GROUP_CONCAT(DISTINCT URE.CODIGO) 'EST',
GROUP_CONCAT(DISTINCT URB.CODIGO) 'BOD',
R.VALORTOTALUF,
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Promesado' THEN PA.VALORRESERVAUF + PA.VALORPIEPROMESAUF + PA.VALORPIESALDOUF ELSE
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Reservado' THEN R.VALORRESERVAUF + R.VALORPIEPROMESAUF + R.VALORPIESALDOUF ELSE
EA.VALORRESERVAUF + EA.VALORPIEPROMESAUF + EA.VALORPIESALDOUF END END  'VALORPIE',
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Promesado' THEN PA.VALORSALDOTOTALUF ELSE
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Reservado' THEN R.VALORSALDOTOTALUF ELSE
EA.VALORSALDOTOTALUF END END 'VALORSALDOUF',
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.COLOR END AS 'ESTADO',
C.RUT, C.NOMBRES, C.APELLIDOS, C.CELULAR, C.EMAIL,
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END AS 'ESTADO_TEXTO',
BA.NOMBRE 'BANCO', MO.NOMBRE 'INVERSION'
FROM UNIDAD U
LEFT JOIN RESERVA R ON U.IDUNIDAD = R.IDUNIDAD
AND R.ESTADO NOT IN (7,9)
LEFT JOIN PROMESA PA ON U.IDUNIDAD = PA.IDUNIDAD
AND R.NUMERO = PA.NUMERO
AND PA.ESTADO NOT IN (1,9)
LEFT JOIN ESCRITURA EA ON U.IDUNIDAD = EA.IDUNIDAD
AND R.NUMERO = EA.NUMERO
AND EA.ESTADO NOT IN (1,8)
LEFT JOIN ESTADORESERVA RES ON R.ESTADO = RES.IDESTADORESERVA
LEFT JOIN PROYECTO P ON U.IDPROYECTO = P.IDPROYECTO
LEFT JOIN CLIENTE C ON R.IDCLIENTE1 = C.IDCLIENTE
LEFT JOIN ESTADOUNIDAD UE ON U.ESTADO = UE.IDESTADOUNIDAD
LEFT JOIN RESERVAESTACIONAMIENTO RE ON R.IDRESERVA = RE.IDRESERVA
LEFT JOIN UNIDAD URE ON RE.IDUNIDAD = URE.IDUNIDAD
LEFT JOIN RESERVABODEGA RB ON R.IDRESERVA = RB.IDRESERVA
LEFT JOIN UNIDAD URB ON RB.IDUNIDAD = URB.IDUNIDAD
LEFT JOIN COMENTARIO CO
ON P.IDPROYECTO = CO.IDPROYECTO
AND U.IDUNIDAD = CO.IDUNIDAD
AND R.NUMERO = CO.NUMERO
LEFT JOIN BANCO BA
ON CO.IDBANCO = BA.IDBANCO
LEFT JOIN MOTIVOCOMPRA MO
ON CO.IDTIPO = MO.IDMOTIVOCOMPRA
WHERE U.IDTIPOUNIDAD NOT IN (3, 4)
AND U.IDPROYECTO = (
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND U.ESTADO = (
SELECT IDESTADOUNIDAD
FROM ESTADOUNIDAD
WHERE IDESTADOUNIDAD = '" . $idEstado . "'
)
AND ((U.ESTADO >= 3 AND U.CODIGO <> '9999') OR ( U.CODIGO = '9999'))
GROUP BY P.CODIGOPROYECTO, R.NUMERO, U.CODIGO, R.VALORTOTALUF, RES.COLOR, UE.COLOR, UE.NOMBRE, C.RUT, C.NOMBRES, C.APELLIDOS, C.CELULAR, C.EMAIL, EA.IDUNIDAD, PA.IDUNIDAD, PA.VALORRESERVAUF, PA.VALORPIEPROMESAUF, PA.VALORPIESALDOUF,
R.VALORRESERVAUF, R.VALORPIEPROMESAUF, R.VALORPIESALDOUF,
EA.VALORRESERVAUF, EA.VALORPIEPROMESAUF, EA.VALORPIESALDOUF,
PA.VALORSALDOTOTALUF, R.VALORSALDOTOTALUF, EA.VALORSALDOTOTALUF, BA.NOMBRE, MO.IDMOTIVOCOMPRA";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
		function filtroContableProyecto($codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, R.NUMERO, U.CODIGO 'DEPTO',
GROUP_CONCAT(DISTINCT URE.CODIGO) 'EST',
GROUP_CONCAT(DISTINCT URB.CODIGO) 'BOD',
R.VALORTOTALUF,
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Promesado' THEN PA.VALORRESERVAUF + PA.VALORPIEPROMESAUF + PA.VALORPIESALDOUF ELSE
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Reservado' THEN R.VALORRESERVAUF + R.VALORPIEPROMESAUF + R.VALORPIESALDOUF ELSE
EA.VALORRESERVAUF + EA.VALORPIEPROMESAUF + EA.VALORPIESALDOUF END END  'VALORPIE',
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Promesado' THEN PA.VALORSALDOTOTALUF ELSE
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Reservado' THEN R.VALORSALDOTOTALUF ELSE
EA.VALORSALDOTOTALUF END END 'VALORSALDOUF',
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.COLOR END AS 'ESTADO',
C.RUT, C.NOMBRES, C.APELLIDOS, C.CELULAR, C.EMAIL,
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END AS 'ESTADO_TEXTO',
BA.NOMBRE 'BANCO', MO.NOMBRE 'INVERSION'
FROM UNIDAD U
LEFT JOIN RESERVA R ON U.IDUNIDAD = R.IDUNIDAD
AND R.ESTADO NOT IN (7,9)
LEFT JOIN PROMESA PA ON U.IDUNIDAD = PA.IDUNIDAD
AND R.NUMERO = PA.NUMERO
AND PA.ESTADO NOT IN (1,9)
LEFT JOIN ESCRITURA EA ON U.IDUNIDAD = EA.IDUNIDAD
AND R.NUMERO = EA.NUMERO
AND EA.ESTADO NOT IN (1,8)
LEFT JOIN ESTADORESERVA RES ON R.ESTADO = RES.IDESTADORESERVA
LEFT JOIN PROYECTO P ON U.IDPROYECTO = P.IDPROYECTO
LEFT JOIN CLIENTE C ON R.IDCLIENTE1 = C.IDCLIENTE
LEFT JOIN ESTADOUNIDAD UE ON U.ESTADO = UE.IDESTADOUNIDAD
LEFT JOIN RESERVAESTACIONAMIENTO RE ON R.IDRESERVA = RE.IDRESERVA
LEFT JOIN UNIDAD URE ON RE.IDUNIDAD = URE.IDUNIDAD
LEFT JOIN RESERVABODEGA RB ON R.IDRESERVA = RB.IDRESERVA
LEFT JOIN UNIDAD URB ON RB.IDUNIDAD = URB.IDUNIDAD
LEFT JOIN COMENTARIO CO
ON P.IDPROYECTO = CO.IDPROYECTO
AND U.IDUNIDAD = CO.IDUNIDAD
AND R.NUMERO = CO.NUMERO
LEFT JOIN BANCO BA
ON CO.IDBANCO = BA.IDBANCO
LEFT JOIN MOTIVOCOMPRA MO
ON CO.IDTIPO = MO.IDMOTIVOCOMPRA
WHERE U.IDPROYECTO = (
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND U.IDTIPOUNIDAD NOT IN (3, 4)
AND ((U.ESTADO >= 3 AND U.CODIGO <> '9999') OR ( U.CODIGO = '9999'))
GROUP BY P.CODIGOPROYECTO, R.NUMERO, U.CODIGO, R.VALORTOTALUF, RES.COLOR, UE.COLOR, UE.NOMBRE, C.RUT, C.NOMBRES, C.APELLIDOS, C.CELULAR, C.EMAIL, EA.IDUNIDAD, PA.IDUNIDAD, PA.VALORRESERVAUF, PA.VALORPIEPROMESAUF, PA.VALORPIESALDOUF,
R.VALORRESERVAUF, R.VALORPIEPROMESAUF, R.VALORPIESALDOUF,
EA.VALORRESERVAUF, EA.VALORPIEPROMESAUF, EA.VALORPIESALDOUF,
PA.VALORSALDOTOTALUF, R.VALORSALDOTOTALUF, EA.VALORSALDOTOTALUF, BA.NOMBRE, MO.IDMOTIVOCOMPRA";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}

	function filtroContableEstado($idEstado){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, R.NUMERO, U.CODIGO 'DEPTO',
GROUP_CONCAT(DISTINCT URE.CODIGO) 'EST',
GROUP_CONCAT(DISTINCT URB.CODIGO) 'BOD',
R.VALORTOTALUF,
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Promesado' THEN PA.VALORRESERVAUF + PA.VALORPIEPROMESAUF + PA.VALORPIESALDOUF ELSE
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Reservado' THEN R.VALORRESERVAUF + R.VALORPIEPROMESAUF + R.VALORPIESALDOUF ELSE
EA.VALORRESERVAUF + EA.VALORPIEPROMESAUF + EA.VALORPIESALDOUF END END  'VALORPIE',
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Promesado' THEN PA.VALORSALDOTOTALUF ELSE
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Reservado' THEN R.VALORSALDOTOTALUF ELSE
EA.VALORSALDOTOTALUF END END 'VALORSALDOUF',
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.COLOR END AS 'ESTADO',
C.RUT, C.NOMBRES, C.APELLIDOS, C.CELULAR, C.EMAIL,
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END AS 'ESTADO_TEXTO',
BA.NOMBRE 'BANCO', MO.NOMBRE 'INVERSION'
FROM UNIDAD U
LEFT JOIN RESERVA R ON U.IDUNIDAD = R.IDUNIDAD
AND R.ESTADO NOT IN (7,9)
LEFT JOIN PROMESA PA ON U.IDUNIDAD = PA.IDUNIDAD
AND R.NUMERO = PA.NUMERO
AND PA.ESTADO NOT IN (1,9)
LEFT JOIN ESCRITURA EA ON U.IDUNIDAD = EA.IDUNIDAD
AND R.NUMERO = EA.NUMERO
AND EA.ESTADO NOT IN (1,8)
LEFT JOIN ESTADORESERVA RES ON R.ESTADO = RES.IDESTADORESERVA
LEFT JOIN PROYECTO P ON U.IDPROYECTO = P.IDPROYECTO
LEFT JOIN CLIENTE C ON R.IDCLIENTE1 = C.IDCLIENTE
LEFT JOIN ESTADOUNIDAD UE ON U.ESTADO = UE.IDESTADOUNIDAD
LEFT JOIN RESERVAESTACIONAMIENTO RE ON R.IDRESERVA = RE.IDRESERVA
LEFT JOIN UNIDAD URE ON RE.IDUNIDAD = URE.IDUNIDAD
LEFT JOIN RESERVABODEGA RB ON R.IDRESERVA = RB.IDRESERVA
LEFT JOIN UNIDAD URB ON RB.IDUNIDAD = URB.IDUNIDAD
LEFT JOIN COMENTARIO CO
ON P.IDPROYECTO = CO.IDPROYECTO
AND U.IDUNIDAD = CO.IDUNIDAD
AND R.NUMERO = CO.NUMERO
LEFT JOIN BANCO BA
ON CO.IDBANCO = BA.IDBANCO
LEFT JOIN MOTIVOCOMPRA MO
ON CO.IDTIPO = MO.IDMOTIVOCOMPRA
WHERE U.IDTIPOUNIDAD NOT IN (3, 4)
AND U.ESTADO = (
	SELECT IDESTADOUNIDAD
	FROM ESTADOUNIDAD
	WHERE IDESTADOUNIDAD = '" . $idEstado . "'
)
AND ((U.ESTADO >= 3 AND U.CODIGO <> '9999') OR ( U.CODIGO = '9999'))
GROUP BY P.CODIGOPROYECTO, R.NUMERO, U.CODIGO, R.VALORTOTALUF, RES.COLOR, UE.COLOR, UE.NOMBRE, C.RUT, C.NOMBRES, C.APELLIDOS, C.CELULAR, C.EMAIL, EA.IDUNIDAD, PA.IDUNIDAD, PA.VALORRESERVAUF, PA.VALORPIEPROMESAUF, PA.VALORPIESALDOUF,
R.VALORRESERVAUF, R.VALORPIEPROMESAUF, R.VALORPIESALDOUF,
EA.VALORRESERVAUF, EA.VALORPIEPROMESAUF, EA.VALORPIESALDOUF,
PA.VALORSALDOTOTALUF, R.VALORSALDOTOTALUF, EA.VALORSALDOTOTALUF, BA.NOMBRE, MO.IDMOTIVOCOMPRA";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
		function filtroContableEstadoUsu($idEstado, $rutUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, R.NUMERO, U.CODIGO 'DEPTO',
GROUP_CONCAT(DISTINCT URE.CODIGO) 'EST',
GROUP_CONCAT(DISTINCT URB.CODIGO) 'BOD',
R.VALORTOTALUF,
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Promesado' THEN PA.VALORRESERVAUF + PA.VALORPIEPROMESAUF + PA.VALORPIESALDOUF ELSE
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Reservado' THEN R.VALORRESERVAUF + R.VALORPIEPROMESAUF + R.VALORPIESALDOUF ELSE
EA.VALORRESERVAUF + EA.VALORPIEPROMESAUF + EA.VALORPIESALDOUF END END  'VALORPIE',
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Promesado' THEN PA.VALORSALDOTOTALUF ELSE
CASE WHEN (
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END
) = 'Reservado' THEN R.VALORSALDOTOTALUF ELSE
EA.VALORSALDOTOTALUF END END 'VALORSALDOUF',
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT COLOR FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.COLOR END AS 'ESTADO',
C.RUT, C.NOMBRES, C.APELLIDOS, C.CELULAR, C.EMAIL,
CASE WHEN U.CODIGO = '9999' THEN
CASE WHEN EA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 5)
ELSE
CASE WHEN PA.IDUNIDAD IS NOT NULL THEN (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 4)
ELSE (SELECT NOMBRE FROM ESTADOUNIDAD WHERE IDESTADOUNIDAD = 3)
END
END
ELSE UE.NOMBRE END AS 'ESTADO_TEXTO',
BA.NOMBRE 'BANCO', MO.NOMBRE 'INVERSION'
FROM UNIDAD U
LEFT JOIN RESERVA R ON U.IDUNIDAD = R.IDUNIDAD
AND R.ESTADO NOT IN (7,9)
LEFT JOIN PROMESA PA ON U.IDUNIDAD = PA.IDUNIDAD
AND R.NUMERO = PA.NUMERO
AND PA.ESTADO NOT IN (1,9)
LEFT JOIN ESCRITURA EA ON U.IDUNIDAD = EA.IDUNIDAD
AND R.NUMERO = EA.NUMERO
AND EA.ESTADO NOT IN (1,8)
LEFT JOIN ESTADORESERVA RES ON R.ESTADO = RES.IDESTADORESERVA
LEFT JOIN PROYECTO P ON U.IDPROYECTO = P.IDPROYECTO
LEFT JOIN CLIENTE C ON R.IDCLIENTE1 = C.IDCLIENTE
LEFT JOIN ESTADOUNIDAD UE ON U.ESTADO = UE.IDESTADOUNIDAD
LEFT JOIN RESERVAESTACIONAMIENTO RE ON R.IDRESERVA = RE.IDRESERVA
LEFT JOIN UNIDAD URE ON RE.IDUNIDAD = URE.IDUNIDAD
LEFT JOIN RESERVABODEGA RB ON R.IDRESERVA = RB.IDRESERVA
LEFT JOIN UNIDAD URB ON RB.IDUNIDAD = URB.IDUNIDAD
LEFT JOIN COMENTARIO CO
ON P.IDPROYECTO = CO.IDPROYECTO
AND U.IDUNIDAD = CO.IDUNIDAD
AND R.NUMERO = CO.NUMERO
LEFT JOIN BANCO BA
ON CO.IDBANCO = BA.IDBANCO
LEFT JOIN MOTIVOCOMPRA MO
ON CO.IDTIPO = MO.IDMOTIVOCOMPRA
WHERE U.IDTIPOUNIDAD NOT IN (3, 4)
AND ((U.ESTADO >= 3 AND U.CODIGO <> '9999') OR ( U.CODIGO = '9999'))
AND U.ESTADO = (
SELECT IDESTADOUNIDAD
FROM ESTADOUNIDAD
WHERE IDESTADOUNIDAD = '" . $idEstado . "'
)
AND R.IDPROYECTO IN
(
	SELECT IDPROYECTO
	FROM USUARIOPROYECTO
	WHERE IDUSUARIO =
	(
		SELECT IDUSUARIO
		FROM USUARIO
		WHERE RUT = '" . $rutUsuario . "'
	)
)
GROUP BY P.CODIGOPROYECTO, R.NUMERO, U.CODIGO, R.VALORTOTALUF, RES.COLOR, UE.COLOR, UE.NOMBRE, C.RUT, C.NOMBRES, C.APELLIDOS, C.CELULAR, C.EMAIL, EA.IDUNIDAD, PA.IDUNIDAD, PA.VALORRESERVAUF, PA.VALORPIEPROMESAUF, PA.VALORPIESALDOUF,
R.VALORRESERVAUF, R.VALORPIEPROMESAUF, R.VALORPIESALDOUF,
EA.VALORRESERVAUF, EA.VALORPIEPROMESAUF, EA.VALORPIESALDOUF,
PA.VALORSALDOTOTALUF, R.VALORSALDOTOTALUF, EA.VALORSALDOTOTALUF, BA.NOMBRE, MO.IDMOTIVOCOMPRA";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
	function consultaPagosReservaContable($codigoProyecto, $numero){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT R.FECHARESERVA, R.VALORRESERVAUF, R.VALORPAGORESERVA, R.FECHAPAGORESERVA, RF.NOMBRE 'FORMAPAGORESERVA', R.BANCORESERVA, R.SERIECHEQUERESERVA, R.NROCHEQUERESERVA, R.ESTADOPAGORESERVA
				FROM RESERVA R
				LEFT JOIN RESERVAFORMA RF ON R.FORMAPAGORESERVA = RF.IDRESERVAFORMA
				WHERE R.IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
				AND R.NUMERO = '" . $numero . "'";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}

	}
	function consultaPagosPromesaContable($codigoProyecto, $numero){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT R.FECHARESERVA, R.VALORRESERVAUF, R.VALORPAGORESERVA, R.FECHAPAGORESERVA, RF.NOMBRE 'FORMAPAGORESERVA',
			 R.BANCORESERVA, R.SERIECHEQUERESERVA, R.NROCHEQUERESERVA, R.ESTADOPAGORESERVA, PR.FECHAPROMESA, PR.VALORPIEPROMESAUF,
			 PR.VALORPIESALDOUF, PR.VALORPAGOPROMESA, PR.VALORPIESALDO, PR.FECHAPAGOPROMESA, RFP.NOMBRE 'FORMAPAGOPROMESA', PR.BANCOPROMESA,
			 PR.SERIECHEQUEPROMESA, PR.NROCHEQUEPROMESA, PR.ESTADOPAGOPROMESA, PR.VALORSALDOTOTALUF
				FROM PROMESA PR
				LEFT JOIN RESERVAFORMA RFP ON PR.FORMAPAGOPROMESA = RFP.IDRESERVAFORMA
				LEFT JOIN RESERVA R ON PR.IDRESERVA = R.IDRESERVA
				LEFT JOIN RESERVAFORMA RF ON R.FORMAPAGORESERVA = RF.IDRESERVAFORMA
				WHERE PR.IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
				AND PR.NUMERO = '" . $numero . "'";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
	}
	function consultaCuotasPromesaContable($codigoProyecto,$numeroOperacion){
			$con = conectar();
			if($con != 'No conectado'){
				$sql = "SELECT P.NUMEROCUOTA, P.VALORUFCUOTA, P.VALORMONTOCUOTA, RF.NOMBRE 'FORMAPAGOCUOTA', P.BANCOCUOTA,
				P.SERIECHEQUECUOTA,	P.NROCHEQUECUOTA, P.FECHAPAGOCUOTA, P.ESTADOPAGOCUOTA
				FROM PROMESACUOTAS P
				LEFT JOIN RESERVAFORMA RF ON P.FORMAPAGOCUOTA = RF.IDRESERVAFORMA
				WHERE P.IDPROMESA = (SELECT IDPROMESA FROM PROMESA
				WHERE IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '".$codigoProyecto."')
				 AND NUMERO = '".$numeroOperacion."')
				 ORDER BY P.NUMEROCUOTA ASC";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
		}
	function consultaPagosEscrituraContable($codigoProyecto, $numero){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT R.FECHARESERVA, R.VALORRESERVAUF, R.VALORPAGORESERVA, R.FECHAPAGORESERVA, RFR.NOMBRE 'FORMAPAGORESERVA',
			 R.BANCORESERVA, R.SERIECHEQUERESERVA, R.NROCHEQUERESERVA, R.ESTADOPAGORESERVA, PR.FECHAPROMESA, PR.VALORPIEPROMESAUF,
			 PR.VALORPAGOPROMESA, PR.VALORPIESALDOUF, PR.VALORPIESALDO, PR.FECHAPAGOPROMESA, RFP.NOMBRE 'FORMAPAGOPROMESA', PR.BANCOPROMESA, PR.SERIECHEQUEPROMESA,
			 PR.NROCHEQUEPROMESA, PR.ESTADOPAGOPROMESA, ES.FECHAESCRITURA, ES.VALORSALDOTOTALUF, ES.VALORPAGOESCRITURA,
			 ES.FECHAPAGOESCRITURA, RFE.NOMBRE 'FORMAPAGOESCRITURA', ES.BANCOESCRITURA, ES.SERIECHEQUEESCRITURA,
			 ES.NROCHEQUEESCRITURA, ES.ESTADOPAGOESCRITURA, ESC.VALORMONTOCUOTA, RFC.NOMBRE 'FORMAPAGOCUOTA', ESC.BANCOCUOTA,
			 ESC.SERIECHEQUECUOTA, ESC.NROCHEQUECUOTA, ESC.FECHAPAGOCUOTA, ESC.ESTADOPAGOCUOTA, ESC.MONTO_UF
				FROM ESCRITURA ES
				LEFT JOIN RESERVAFORMA RFE ON ES.FORMAPAGOESCRITURA = RFE.IDRESERVAFORMA
				LEFT JOIN ESCRITURACUOTA ESC ON ES.IDESCRITURA = ESC.IDESCRITURA
				LEFT JOIN RESERVAFORMA RFC ON ESC.FORMAPAGOCUOTA = RFC.IDRESERVAFORMA
				LEFT JOIN PROMESA PR ON ES.IDPROMESA = PR.IDPROMESA
				LEFT JOIN RESERVAFORMA RFP ON PR.FORMAPAGOPROMESA = RFP.IDRESERVAFORMA
				LEFT JOIN RESERVA R ON PR.IDRESERVA = R.IDRESERVA
				LEFT JOIN RESERVAFORMA RFR ON R.FORMAPAGORESERVA = RFR.IDRESERVAFORMA
				WHERE ES.IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
				AND ES.NUMERO = '" . $numero . "'";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
	}

	function actualizarEstadoPagoReserva($codigoProyecto, $numeroOperacion, $estadoPagoReserva){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE RESERVA
			SET ESTADOPAGORESERVA = '" . $estadoPagoReserva . "'
			WHERE IDPROYECTO =
			(
				SELECT IDPROYECTO
				FROM PROYECTO
				WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			AND NUMERO = '" . $numeroOperacion . "'";
			if ($con->query($sql)) {
					return $con;
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function actualizarEstadoPagoPromesa($codigoProyecto, $numeroOperacion, $estadoPagoPromesa){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE PROMESA
			SET ESTADOPAGOPROMESA = '" . $estadoPagoPromesa . "'
			WHERE IDPROYECTO =
			(
				SELECT IDPROYECTO
				FROM PROYECTO
				WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			AND NUMERO = '" . $numeroOperacion . "'";
			if ($con->query($sql)) {
					return $con;
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function actualizarEstadoPagoPromesaCuota($codigoProyecto, $numeroOperacion, $estadoPagoPromesaCuota, $numeroPromesaCuota){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE PROMESACUOTAS
			SET ESTADOPAGOCUOTA = '" . $estadoPagoPromesaCuota . "'
			WHERE IDPROMESA = (
			SELECT IDPROMESA FROM PROMESA
			WHERE IDPROYECTO =
			(
				SELECT IDPROYECTO
				FROM PROYECTO
				WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			AND NUMERO = '" . $numeroOperacion . "')
			AND NUMEROCUOTA ='" . $numeroPromesaCuota . "'";
			if ($con->query($sql)) {
					return $con;
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function actualizarEstadoPagoEscritura($codigoProyecto, $numeroOperacion, $estadoPagoEscritura){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE ESCRITURA
			SET ESTADOPAGOESCRITURA = '" . $estadoPagoEscritura . "'
			WHERE IDPROYECTO =
			(
				SELECT IDPROYECTO
				FROM PROYECTO
				WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			AND NUMERO = '" . $numeroOperacion . "'";
			if ($con->query($sql)) {
					return $con;
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function actualizarEstadoPagoResidual($codigoProyecto, $numeroOperacion, $estadoPagoResidual){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE ESCRITURACUOTA
			SET ESTADOPAGOCUOTA = '" . $estadoPagoResidual . "'
			WHERE IDESCRITURA =(
			SELECT IDESCRITURA FROM ESCRITURA
			WHERE IDPROYECTO =
			(
				SELECT IDPROYECTO
				FROM PROYECTO
				WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			AND NUMERO = '" . $numeroOperacion . "')";
			if ($con->query($sql)) {
					return $con;
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	function consultaComentariosContable($codigoProyecto, $numeroOperacion, $codigoUnidad){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT * FROM COMENTARIO WHERE
				IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
				AND NUMERO = '" . $numeroOperacion . "'
				AND IDUNIDAD = (SELECT IDUNIDAD FROM UNIDAD WHERE CODIGO = '" . $codigoUnidad . "' AND IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'))";
				if ($row = $con->query($sql)) {
						while($array = $row->fetch_array(MYSQLI_BOTH)){
							$return[] = $array;
						}

						return $return;
				}
				else{
					return "Error";
				}
			}
			else{
				return "Error";
			}
	}

	function insertarComentariosContable($codigoProyecto, $codigoUnidad, $numeroOperacion, $comentarioReserva, $comentarioPromesa, $comentarioEscritura, $comentarioGeneral, $idUso, $idBanco){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO  COMENTARIO (IDPROYECTO, IDUNIDAD, NUMERO, COMENTARIORESERVA, COMENTARIOPROMESA, COMENTARIOESCRITURA, COMENTARIOGENERAL, IDTIPO, IDBANCO)
			VALUES((SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'),
    		(SELECT IDUNIDAD FROM UNIDAD WHERE CODIGO = '" . $codigoUnidad . "' AND IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')),	'" . $numeroOperacion . "', '" . $comentarioReserva ."', '" . $comentarioPromesa . "', '" . $comentarioEscritura . "', '" . $comentarioGeneral . "', " . $idUso . ", " . $idBanco . ")";
			if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
			}
			else{
				// return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			// return $con->error;
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function actualizarComentariosContable($codigoProyecto, $codigoUnidad, $numeroOperacion, $comentarioReserva, $comentarioPromesa, $comentarioEscritura, $comentarioGeneral, $idUso, $idBanco){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE COMENTARIO
			SET COMENTARIORESERVA = '" . $comentarioReserva . "',
			COMENTARIOPROMESA = '" . $comentarioPromesa . "',
			COMENTARIOESCRITURA = '" . $comentarioEscritura . "',
			COMENTARIOGENERAL = '" . $comentarioGeneral . "',
			IDTIPO = " . $idUso . ",
			IDBANCO = " . $idBanco . "
			WHERE IDPROYECTO =
			(
				SELECT IDPROYECTO
				FROM PROYECTO
				WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			AND NUMERO = '" . $numeroOperacion . "'
			AND IDUNIDAD =
			(
				SELECT IDUNIDAD
				FROM UNIDAD
				WHERE CODIGO = '" . $codigoUnidad . "'
				AND IDPROYECTO =
				(
					SELECT IDPROYECTO
					FROM PROYECTO
					WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
				)
			)";
			if ($con->query($sql)) {
					$con->query("COMMIT");
					return "Ok";
			}
			else{
				// return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function eliminaComentarioReserva($codigoProyecto, $numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE COMENTARIO
			SET COMENTARIORESERVA = ''
			WHERE IDPROYECTO =
			(
				SELECT IDPROYECTO
				FROM PROYECTO
				WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			AND NUMERO = '" . $numeroOperacion . "'";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function eliminaComentarioPromesa($codigoProyecto, $idUnidad, $numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE COMENTARIO
			SET COMENTARIOPROMESA = ''
			WHERE IDPROYECTO =
			(
				SELECT IDPROYECTO
				FROM PROYECTO
				WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			AND NUMERO = '" . $numeroOperacion . "'
			AND IDUNIDAD = '" . $idUnidad . "'";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function eliminaComentarioEscritura($codigoProyecto, $idUnidad, $numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE COMENTARIO
			SET COMENTARIOESCRITURA = ''
			WHERE IDPROYECTO =
			(
				SELECT IDPROYECTO
				FROM PROYECTO
				WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			)
			AND NUMERO = '" . $numeroOperacion . "'
			AND IDUNIDAD = '" . $idUnidad . "'";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function consultaProyectosPorVendedor($mes, $ano){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL COMISIONVENDEDORES(". $mes .",". $ano .")";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function consultaComisionCorTotal($mes, $ano){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT SUM(P.MONTOPAGOCOM) 'VALOR'
FROM COMISIONPROMESA C LEFT JOIN PAGOCOMISIONPROMESA P
ON C.IDCOMISIONPROMESA = P.IDCOMISIONPROMESA
WHERE MONTH(FECHACOMISIONPROMESA) = " . $mes . "
AND YEAR(FECHACOMISIONPROMESA) = " . $ano;
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function consultaProyectosPorEmpresa($mes, $ano){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL COMISIONEMPRESA(". $mes .",". $ano .")";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function consultaOperacionesPorVendedor($rutVendedor, $codigoProyecto, $mesComision, $anoComision){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT S.RUT, CONCAT(S.NOMBRES, ' ', S.APELLIDOS) 'NOMBRE',
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.NUMERO ELSE ES.NUMERO END 'NUMERO',
U.CODIGO 'DEPTO',
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN GROUP_CONCAT(DISTINCT UPE.CODIGO) ELSE GROUP_CONCAT(DISTINCT UEE.CODIGO) END 'EST',
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN GROUP_CONCAT(DISTINCT UPB.CODIGO) ELSE GROUP_CONCAT(DISTINCT UEB.CODIGO) END 'BOD',
ROUND((CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.VALORTOTALUF ELSE ES.VALORTOTALUF END)
/
(((CASE WHEN P.IVA IS NULL THEN 0  ELSE P.IVA END)/100)+1),2) 'VALORNETOUF',
CASE WHEN (MONTH(CP.FECHACOMISIONPROMESA) = '" . $mesComision . "'
AND YEAR(CP.FECHACOMISIONPROMESA) = '" . $anoComision . "') THEN CP.FECHACOMISIONPROMESA ELSE NULL END 'FECHACOMISIONPROMESA',
CASE WHEN (MONTH(CP.FECHACOMISIONPROMESA) = '" . $mesComision . "'
AND YEAR(CP.FECHACOMISIONPROMESA) = '" . $anoComision . "') THEN ROUND(CP.PORCENTAJECOMISIONVENDEDOR,3) ELSE NULL END 'COMISIONPROMESA',
CASE WHEN (MONTH(CP.FECHACOMISIONPROMESA) = '" . $mesComision . "'
AND YEAR(CP.FECHACOMISIONPROMESA) = '" . $anoComision . "') THEN ROUND(CP.MONTOUFVENDEDOR,2) ELSE NULL END 'UFPROMESA',
CP.MONTOPESOSVENDEDOR 'PESOSPROMESA',
CASE WHEN (MONTH(CP.FECHACOMISIONPROMESA) = '" . $mesComision . "'
AND YEAR(CP.FECHACOMISIONPROMESA) = '" . $anoComision . "') THEN CP.ESTADOPAGO ELSE NULL END 'ESTADOPROMESA',
CASE WHEN (MONTH(CE.FECHACOMISIONESCRITURA) = '" . $mesComision . "'
AND YEAR(CE.FECHACOMISIONESCRITURA) = '" . $anoComision . "') THEN CE.FECHACOMISIONESCRITURA ELSE NULL END 'FECHACOMISIONESCRITURA',
CASE WHEN (MONTH(CE.FECHACOMISIONESCRITURA) = '" . $mesComision . "'
AND YEAR(CE.FECHACOMISIONESCRITURA) = '" . $anoComision . "') THEN ROUND(CE.PORCENTAJECOMISIONVENDEDOR,3) ELSE NULL END 'COMISIONESCRITURA',
CASE WHEN (MONTH(CE.FECHACOMISIONESCRITURA) = '" . $mesComision . "'
AND YEAR(CE.FECHACOMISIONESCRITURA) = '" . $anoComision . "') THEN ROUND(CE.MONTOUFVENDEDOR,2) ELSE NULL END 'UFESCRITURA',
CE.MONTOPESOSVENDEDOR 'PESOSESCRITURA',
CASE WHEN (MONTH(CE.FECHACOMISIONESCRITURA) = '" . $mesComision . "'
AND YEAR(CE.FECHACOMISIONESCRITURA) = '" . $anoComision . "') THEN CE.ESTADOPAGO ELSE NULL END 'ESTADOESCRITURA'
FROM PROMESA PR
LEFT JOIN USUARIO S
ON PR.IDUSUARIO = S.IDUSUARIO
LEFT JOIN COMISIONPROMESA CP
ON PR.IDPROMESA = CP.IDPROMESA
LEFT JOIN COMISIONESCRITURA CE
ON PR.IDPROMESA = CE.IDPROMESA
LEFT JOIN PROYECTO P
ON PR.IDPROYECTO = P.IDPROYECTO
LEFT JOIN ESCRITURA ES
ON CE.IDESCRITURA = ES.IDESCRITURA
LEFT JOIN UNIDAD U
ON CASE WHEN PR.IDUNIDAD IS NULL THEN ES.IDUNIDAD ELSE PR.IDUNIDAD END = U.IDUNIDAD
LEFT JOIN PROMESAESTACIONAMIENTO PE
ON PR.IDPROMESA = PE.IDPROMESA
LEFT JOIN UNIDAD UPE
ON PE.IDUNIDAD = UPE.IDUNIDAD
LEFT JOIN PROMESABODEGA PB
ON PR.IDPROMESA = PB.IDPROMESA
LEFT JOIN UNIDAD UPB
ON PB.IDUNIDAD = UPB.IDUNIDAD
LEFT JOIN ESCRITURAESTACIONAMIENTO EE
ON ES.IDESCRITURA = EE.IDESCRITURA
LEFT JOIN UNIDAD UEE
ON EE.IDUNIDAD = UEE.IDUNIDAD
LEFT JOIN ESCRITURABODEGA EB
ON ES.IDESCRITURA = EB.IDESCRITURA
LEFT JOIN UNIDAD UEB
ON EB.IDUNIDAD = UEB.IDUNIDAD
WHERE S.IDUSUARIO = (
SELECT IDUSUARIO FROM USUARIO WHERE RUT = '" . $rutVendedor . "'
)
AND
(
(MONTH(CE.FECHACOMISIONESCRITURA) = '" . $mesComision . "'
AND YEAR(CE.FECHACOMISIONESCRITURA) = '" . $anoComision . "')
OR
(MONTH(CP.FECHACOMISIONPROMESA) = '" . $mesComision . "'
AND YEAR(CP.FECHACOMISIONPROMESA) = '" . $anoComision . "')
)
AND P.IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.NUMERO ELSE ES.NUMERO END IS NOT NULL
GROUP BY S.RUT, S.NOMBRES, S.APELLIDOS, PR.NUMERO, U.CODIGO, PR.VALORTOTALUF, CP.FECHACOMISIONPROMESA, CP.PORCENTAJECOMISIONVENDEDOR, CP.MONTOUFVENDEDOR,
CP.MONTOPESOSVENDEDOR, CP.ESTADOPAGO, CE.FECHACOMISIONESCRITURA, CE.PORCENTAJECOMISIONVENDEDOR, CE.MONTOUFVENDEDOR, CE.MONTOPESOSVENDEDOR,
CE.ESTADOPAGO, PR.IDUNIDAD, ES.NUMERO, ES.VALORTOTALUF, P.IVA";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function consultaOperacionesPorEmpresa($codigoProyecto, $mesComision, $anoComision){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT P.CODIGOPROYECTO, CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.NUMERO ELSE ES.NUMERO END 'NUMERO',
U.CODIGO 'DEPTO',
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN GROUP_CONCAT(DISTINCT UPE.CODIGO) ELSE GROUP_CONCAT(DISTINCT UEE.CODIGO) END 'EST',
CASE WHEN PR.IDUNIDAD IS NOT NULL THEN GROUP_CONCAT(DISTINCT UPB.CODIGO) ELSE GROUP_CONCAT(DISTINCT UEB.CODIGO) END 'BOD',
ROUND((CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.VALORTOTALUF ELSE ES.VALORTOTALUF END)
/
(((CASE WHEN P.IVA IS NULL THEN 0  ELSE P.IVA END)/100)+1),2) 'VALORNETOUF',
CASE WHEN (MONTH(CP.FECHACOMISIONPROMESA) = '" . $mesComision. "'
AND YEAR(CP.FECHACOMISIONPROMESA) = '" . $anoComision . "') THEN CP.FECHACOMISIONPROMESA ELSE NULL END 'FECHACOMISIONPROMESA',
CASE WHEN (MONTH(CP.FECHACOMISIONPROMESA) = '" . $mesComision. "'
AND YEAR(CP.FECHACOMISIONPROMESA) = '" . $anoComision . "') THEN ROUND(CP.PORCENTAJECOMISIONEMPRESA,3) ELSE NULL END 'COMISIONPROMESA',
CASE WHEN (MONTH(CP.FECHACOMISIONPROMESA) = '" . $mesComision. "'
AND YEAR(CP.FECHACOMISIONPROMESA) = '" . $anoComision . "') THEN ROUND(CP.MONTOUFEMPRESA,2) ELSE NULL END 'UFPROMESA',
CP.MONTOPESOSEMPRESA 'PESOSPROMESA',
CASE WHEN (MONTH(CP.FECHACOMISIONPROMESA) = '" . $mesComision. "'
AND YEAR(CP.FECHACOMISIONPROMESA) = '" . $anoComision . "') THEN CP.ESTADOPAGO ELSE NULL END 'ESTADOPROMESA',
CASE WHEN (MONTH(CE.FECHACOMISIONESCRITURA) = '" . $mesComision. "'
AND YEAR(CE.FECHACOMISIONESCRITURA) = '" . $anoComision . "') THEN CE.FECHACOMISIONESCRITURA ELSE NULL END 'FECHACOMISIONESCRITURA',
CASE WHEN (MONTH(CE.FECHACOMISIONESCRITURA) = '" . $mesComision. "'
AND YEAR(CE.FECHACOMISIONESCRITURA) = '" . $anoComision . "') THEN CE.PORCENTAJECOMISIONEMPRESA ELSE NULL END 'COMISIONESCRITURA',
CASE WHEN (MONTH(CE.FECHACOMISIONESCRITURA) = '" . $mesComision. "'
AND YEAR(CE.FECHACOMISIONESCRITURA) = '" . $anoComision . "') THEN CE.MONTOUFEMPRESA ELSE NULL END 'UFESCRITURA',
CE.MONTOPESOSEMPRESA 'PESOSESCRITURA',
CASE WHEN (MONTH(CE.FECHACOMISIONESCRITURA) = '" . $mesComision. "'
AND YEAR(CE.FECHACOMISIONESCRITURA) = '" . $anoComision . "') THEN CE.ESTADOPAGO ELSE NULL END 'ESTADOESCRITURA'
FROM USUARIO S
LEFT JOIN COMISIONPROMESA CP
ON S.IDUSUARIO = CP.IDUSUARIO
LEFT JOIN COMISIONESCRITURA CE
ON S.IDUSUARIO = CE.IDUSUARIO
AND CE.IDPROMESA = CP.IDPROMESA
LEFT JOIN PROYECTO P
ON CP.IDPROYECTO = P.IDPROYECTO
LEFT JOIN PROMESA PR
ON CP.IDPROMESA = PR.IDPROMESA
LEFT JOIN ESCRITURA ES
ON CE.IDESCRITURA = ES.IDESCRITURA
LEFT JOIN UNIDAD U
ON CASE WHEN PR.IDUNIDAD IS NULL THEN ES.IDUNIDAD ELSE PR.IDUNIDAD END = U.IDUNIDAD
LEFT JOIN PROMESAESTACIONAMIENTO PE
ON PR.IDPROMESA = PE.IDPROMESA
LEFT JOIN UNIDAD UPE
ON PE.IDUNIDAD = UPE.IDUNIDAD
LEFT JOIN PROMESABODEGA PB
ON PR.IDPROMESA = PB.IDPROMESA
LEFT JOIN UNIDAD UPB
ON PB.IDUNIDAD = UPB.IDUNIDAD
LEFT JOIN ESCRITURAESTACIONAMIENTO EE
ON ES.IDESCRITURA = EE.IDESCRITURA
LEFT JOIN UNIDAD UEE
ON EE.IDUNIDAD = UEE.IDUNIDAD
LEFT JOIN ESCRITURABODEGA EB
ON ES.IDESCRITURA = EB.IDESCRITURA
LEFT JOIN UNIDAD UEB
ON EB.IDUNIDAD = UEB.IDUNIDAD
WHERE
(MONTH(CE.FECHACOMISIONESCRITURA) = '" . $mesComision. "'
AND YEAR(CE.FECHACOMISIONESCRITURA) = '" . $anoComision . "')
OR
(MONTH(CP.FECHACOMISIONPROMESA) = '" . $mesComision. "'
AND YEAR(CP.FECHACOMISIONPROMESA) = '" . $anoComision . "')
AND P.IDPROYECTO =
(
SELECT IDPROYECTO
FROM PROYECTO
WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)

AND CASE WHEN PR.IDUNIDAD IS NOT NULL THEN PR.NUMERO ELSE ES.NUMERO END IS NOT NULL
GROUP BY S.RUT, S.NOMBRES, S.APELLIDOS, P.CODIGOPROYECTO, PR.NUMERO, U.CODIGO, PR.VALORTOTALUF,
CP.FECHACOMISIONPROMESA, CP.PORCENTAJECOMISIONEMPRESA, CP.MONTOUFEMPRESA,
CP.MONTOPESOSEMPRESA, CP.ESTADOPAGO, CE.FECHACOMISIONESCRITURA, CE.PORCENTAJECOMISIONEMPRESA, CE.MONTOUFEMPRESA, CE.MONTOPESOSEMPRESA,
CE.ESTADOPAGO, PR.IDUNIDAD, ES.NUMERO, ES.VALORTOTALUF, P.IVA";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function insertarComisionPromesa($idUsuario, $fechaComisionPromesa, $idProyecto, $idPromesa, $montoUFEmpresa, $montoUFVendedor, $porcentajeComisionEmpresa, $porcentajeComisionVendedor){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO  COMISIONPROMESA (IDUSUARIO, FECHACOMISIONPROMESA, IDPROYECTO, IDPROMESA, MONTOUFEMPRESA, MONTOUFVENDEDOR, PORCENTAJECOMISIONEMPRESA, PORCENTAJECOMISIONVENDEDOR, ESTADOPAGO, ACTOR)
			VALUES('" . $idUsuario . "','" . $fechaComisionPromesa . "','" . $idProyecto . "','" . $idPromesa . "','" . $montoUFEmpresa . "','" . $montoUFVendedor . "','" . $porcentajeComisionEmpresa . "','" . $porcentajeComisionVendedor . "','NO PAGADO', '')";
			if ($con->query($sql)) {
					return $con;
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function insertarComisionEscritura($idUsuario, $fechaComisionEscritura, $idProyecto, $idEscritura, $idPromesa, $montoUFEmpresa, $montoUFVendedor, $porcentajeComisionEmpresa, $porcentajeComisionVendedor){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO  COMISIONESCRITURA (IDUSUARIO, FECHACOMISIONESCRITURA, IDPROYECTO, IDESCRITURA, IDPROMESA, MONTOUFEMPRESA, MONTOUFVENDEDOR, PORCENTAJECOMISIONEMPRESA, PORCENTAJECOMISIONVENDEDOR, ESTADOPAGO)
			VALUES('" . $idUsuario . "','" . $fechaComisionEscritura . "','" . $idProyecto . "','" . $idEscritura . "','" . $idPromesa . "','" . $montoUFEmpresa . "','" . $montoUFVendedor . "','" . $porcentajeComisionEmpresa . "','" . $porcentajeComisionVendedor . "','NO PAGADO')";
			if ($con->query($sql)) {
					return $con;
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function datosPromesaParaComision($codigoProyecto, $numeroOperacion){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDPROMESA, IDUSUARIO, VALORTOTALUF, FECHAPROMESA
			 FROM PROMESA WHERE IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO
			 WHERE CODIGOPROYECTO = '" . $codigoProyecto . "') AND NUMERO = '" . $numeroOperacion . "'";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function checkComisionPromesa($codigoProyecto, $numeroOperacion, $idUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDPROMESA FROM COMISIONPROMESA WHERE IDPROMESA =
			(SELECT IDPROMESA FROM PROMESA WHERE IDPROYECTO =
			(SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
			 AND NUMERO = '" . $numeroOperacion . "') AND IDUSUARIO = '" . $idUsuario . "'";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function datosEscrituraParaComision($codigoProyecto, $numeroOperacion){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDESCRITURA, IDUSUARIO, VALORTOTALUF, FECHAESCRITURA
			 FROM ESCRITURA WHERE IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO
			 WHERE CODIGOPROYECTO = '" . $codigoProyecto . "') AND NUMERO = '" . $numeroOperacion . "'";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function checkComisionEscritura($codigoProyecto, $numeroOperacion, $idUsuario){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDESCRITURA FROM COMISIONESCRITURA WHERE IDESCRITURA =
			(SELECT IDESCRITURA FROM ESCRITURA WHERE IDPROYECTO =
			(SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
			 AND NUMERO = '" . $numeroOperacion . "') AND IDUSUARIO = '" . $idUsuario . "'";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function consultaEstadoComisionPromesa($rutVendedor, $codigoProyecto, $numeroOperacion){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT PORCENTAJECOMISIONVENDEDOR, ESTADOPAGO
					FROM COMISIONPROMESA
					WHERE IDUSUARIO = (SELECT IDUSUARIO FROM USUARIO WHERE RUT = '" . $rutVendedor . "')
					AND IDPROMESA = (SELECT IDPROMESA FROM PROMESA
					WHERE IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto ."')
					AND NUMERO = '" . $numeroOperacion . "')";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function consultaEstadoComisionEscritura($rutVendedor, $codigoProyecto, $numeroOperacion){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT PORCENTAJECOMISIONVENDEDOR, ESTADOPAGO
					FROM COMISIONESCRITURA
					WHERE IDUSUARIO = (SELECT IDUSUARIO FROM USUARIO WHERE RUT = '" . $rutVendedor . "')
					AND IDESCRITURA = (SELECT IDESCRITURA FROM ESCRITURA
					WHERE IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto ."')
					AND NUMERO = '" . $numeroOperacion . "')";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function actualizarComisionPromesa($rutVendedor, $codigoProyecto, $numeroOperacion, $estadoPagoPromesa){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE COMISIONPROMESA
			SET ESTADOPAGO = '" . $estadoPagoPromesa . "'
			WHERE IDPROMESA =
			(SELECT IDPROMESA FROM PROMESA WHERE
				IDPROYECTO =
					(
						SELECT IDPROYECTO
						FROM PROYECTO
						WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
					)
				AND NUMERO = '" . $numeroOperacion . "'
			)
			AND IDUSUARIO =
			(
				SELECT IDUSUARIO
				FROM USUARIO
				WHERE RUT = '" . $rutVendedor . "'
			)";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function actualizarComisionEscritura($rutVendedor, $codigoProyecto, $numeroOperacion, $estadoPagoEscritura){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE COMISIONESCRITURA
			SET ESTADOPAGO = '" . $estadoPagoEscritura . "'
			WHERE IDESCRITURA =
			(SELECT IDESCRITURA FROM ESCRITURA WHERE
				IDPROYECTO =
					(
						SELECT IDPROYECTO
						FROM PROYECTO
						WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
					)
				AND NUMERO = '" . $numeroOperacion . "'
			)
			AND IDUSUARIO =
			(
				SELECT IDUSUARIO
				FROM USUARIO
				WHERE RUT = '" . $rutVendedor . "'
			)";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function actualizarPagosComisionPromesa($rutVendedor, $codigoProyecto, $numeroOperacion, $montoUFVendedor){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE COMISIONPROMESA
			SET MONTOUFVENDEDOR = '" . $montoUFVendedor . "',
			WHERE IDPROMESA =
			(SELECT IDPROMESA FROM PROMESA WHERE
				IDPROYECTO =
					(
						SELECT IDPROYECTO
						FROM PROYECTO
						WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
					)
				AND NUMERO = '" . $numeroOperacion . "'
			)
			AND IDUSUARIO =
			(
				SELECT IDUSUARIO
				FROM USUARIO
				WHERE RUT = '" . $rutVendedor . "'
			)";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function actualizarPagosComisionEscritura($rutVendedor, $codigoProyecto, $numeroOperacion, $montoUFVendedor){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE COMISIONESCRITURA
			SET MONTOUFVENDEDOR = '" . $montoUFVendedor . "',
			WHERE IDESCRITURA =
			(SELECT ESCRITURA FROM ESCRITURA WHERE
				IDPROYECTO =
					(
						SELECT IDPROYECTO
						FROM PROYECTO
						WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
					)
				AND NUMERO = '" . $numeroOperacion . "'
			)
			AND IDUSUARIO =
			(
				SELECT IDUSUARIO
				FROM USUARIO
				WHERE RUT = '" . $rutVendedor . "'
			)";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function datosComisionPromesaMontos($rutVendedor, $codigoProyecto, $numeroOperacion){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT PR.IVA, P.VALORTOTALUF, CP.PORCENTAJECOMISIONVENDEDOR
			 FROM COMISIONPROMESA CP
			 LEFT JOIN PROMESA P ON CP.IDPROMESA = P.IDPROMESA
			 LEFT JOIN PROYECTO PR ON CP.IDPROYECTO = PR.IDPROYECTO
			 WHERE CP.IDPROMESA =
			(SELECT IDPROMESA FROM PROMESA WHERE
				IDPROYECTO =
					(
						SELECT IDPROYECTO
						FROM PROYECTO
						WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
					)
				AND NUMERO = '" . $numeroOperacion . "'
			)
			AND CP.IDUSUARIO =
			(
				SELECT IDUSUARIO
				FROM USUARIO
				WHERE RUT = '" . $rutVendedor . "'
			)";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function datosComisionEscrituraMontos($rutVendedor, $codigoProyecto, $numeroOperacion){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT PR.IVA, ES.VALORTOTALUF, CE.PORCENTAJECOMISIONVENDEDOR
			 FROM COMISIONESCRITURA CE
			 LEFT JOIN ESCRITURA ES ON CE.IDESCRITURA = ES.IDESCRITURA
			 LEFT JOIN PROYECTO PR ON CE.IDPROYECTO = PR.IDPROYECTO
			 WHERE CE.IDESCRITURA =
			(SELECT IDESCRITURA FROM ESCRITURA WHERE
				IDPROYECTO =
					(
						SELECT IDPROYECTO
						FROM PROYECTO
						WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
					)
				AND NUMERO = '" . $numeroOperacion . "'
			)
			AND CE.IDUSUARIO =
			(
				SELECT IDUSUARIO
				FROM USUARIO
				WHERE RUT = '" . $rutVendedor . "'
			)";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function consultaFiltroContableMesAno(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL FILTROMESANO()";
			if ($row = $con->query($sql)) {
					while($array = $row->fetch_array(MYSQLI_BOTH)){
						$return[] = $array;
					}
					return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function datosFiltroContableMesAnoProyecto($anoComision, $mesComision, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL COMISIONVENDEDORESPROYECTO(".$mesComision.",".$anoComision.",'".$codigoProyecto."')";
			if ($row = $con->query($sql)) {
					while($array = $row->fetch_array(MYSQLI_BOTH)){
						$return[] = $array;
					}
					return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function datosFiltroContableMesAnoProyectoEmpresa($anoComision, $mesComision, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "CALL COMISIONEMPRESAPROYECTO(".$mesComision.",".$anoComision.",'".$codigoProyecto."')";
			if ($row = $con->query($sql)) {
					while($array = $row->fetch_array(MYSQLI_BOTH)){
						$return[] = $array;
					}
					return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function checkComisionCantidadVentasPromesa($idUsuario, $fechaPromesa, $idProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT COUNT(PR.IDUSUARIO) 'VENTAS'
					FROM PROMESA PR LEFT JOIN UNIDAD U ON PR.IDUNIDAD = U.IDUNIDAD
					WHERE U.IDTIPOUNIDAD <> 3 AND U.IDTIPOUNIDAD <> 4 AND PR.IDUNIDAD <> '9999'
					AND PR.IDUSUARIO = '" . $idUsuario . "'
					AND PR.ESTADO IN (1,3,6)
					AND DATE_FORMAT(PR.FECHAPROMESA,'%y%m') = DATE_FORMAT('" . $fechaPromesa . "', '%y%m')
					AND PR.IDPROYECTO = '" . $idProyecto . "'";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function checkComisionCantidadVentasEscritura($idUsuario, $fechaEscritura, $idProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT COUNT(ES.IDUSUARIO) 'VENTAS'
					FROM ESCRITURA ES LEFT JOIN UNIDAD U ON ES.IDUNIDAD = U.IDUNIDAD
					WHERE U.IDTIPOUNIDAD <> 3 AND U.IDTIPOUNIDAD <> 4 AND ES.IDUNIDAD <> '9999'
					AND ES.IDUSUARIO = '" . $idUsuario . "'
					AND ES.ESTADO IN (1,3,7)
					AND DATE_FORMAT(ES.FECHAESCRITURA,'%y%m') = DATE_FORMAT('" . $fechaEscritura . "', '%y%m')
					AND ES.IDPROYECTO = '" . $idProyecto . "'";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function checkComisionPromesaParaSaltoDeUnidad($idUsuario, $fechaComisionPromesa){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDCOMISIONPROMESA, MONTOUFVENDEDOR, PORCENTAJECOMISIONVENDEDOR
			FROM COMISIONPROMESA WHERE IDUSUARIO = '" . $idUsuario . "'
			AND DATE_FORMAT(FECHACOMISIONPROMESA, '%y%m') = DATE_FORMAT('" . $fechaComisionPromesa . "','%y%m');";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function actualizarPagosComisionPromesaSalto($idComisionPromesa, $idUsuario, $montoUFVendedor, $porcentajeComision){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE COMISIONPROMESA
			SET MONTOUFVENDEDOR = '" . $montoUFVendedor . "',
			PORCENTAJECOMISIONVENDEDOR = '" . $porcentajeComision . "'
			WHERE IDCOMISIONPROMESA = '" . $idComisionPromesa . "'
			AND IDUSUARIO = '" . $idUsuario . "'
			";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function checkComisionEscrituraParaSaltoDeUnidad($idUsuario, $fechaComisionEscritura){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT IDCOMISIONESCRITURA, MONTOUFVENDEDOR, PORCENTAJECOMISIONVENDEDOR
			FROM COMISIONESCRITURA WHERE IDUSUARIO = '" . $idUsuario . "'
			AND DATE_FORMAT(FECHACOMISIONESCRITURA, '%y%m') = DATE_FORMAT('" . $fechaComisionEscritura . "','%y%m');";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function actualizarPagosComisionEscrituraSalto($idComisionEscritura, $idUsuario, $montoUFVendedor, $porcentajeComision){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE COMISIONESCRITURA
			SET MONTOUFVENDEDOR = '" . $montoUFVendedor . "',
			PORCENTAJECOMISIONVENDEDOR = '" . $porcentajeComision . "'
			WHERE IDCOMISIONESCRITURA = '" . $idComisionEscritura . "'
			AND IDUSUARIO = '" . $idUsuario . "'
			";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function eliminaComisionPromesa($codigoProyecto, $numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM COMISIONPROMESA WHERE
			IDPROMESA = (SELECT IDPROMESA FROM PROMESA WHERE
			IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
			AND NUMERO = '" . $numeroOperacion . "')";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function eliminaComisionEscritura($codigoProyecto, $numeroOperacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM COMISIONESCRITURA WHERE
			IDESCRITURA = (SELECT IDESCRITURA FROM ESCRITURA WHERE
			IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
			AND NUMERO = '" . $numeroOperacion . "')";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function checkUnidadComisionPromesa($unidadRevisar, $codigoProyecto){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT U.CODIGO FROM COMISIONPROMESA CP
			LEFT JOIN PROMESA P ON P.IDPROMESA = CP.IDPROMESA
			LEFT JOIN UNIDAD U ON P.IDUNIDAD  = U.IDUNIDAD
			WHERE U.CODIGO = '" . $unidadRevisar . "' AND
			P.IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function ingresoMonitoreoLog($usuario, $rut, $modulo, $accion, $detalle, $codigoProyecto, $operacion){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO MONITOREOLOG (USUARIO, RUT, MODULO, ACCION, DETALLE, CODIGOPROYECTO, OPERACION, DIA_HORA)
			VALUES('" . $usuario . "','" . $rut . "','" . $modulo . "','" . $accion . "','" . $detalle . "','" . $codigoProyecto ."','" . $operacion . "', NOW())";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				// return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function insertarComisionPromesaCorretaje($idUsuario, $fechaComisionPromesa, $idProyecto, $idPromesa, $montoUFEmpresa, $porcentajeComisionEmpresa, $actor, $con){
		// $con = conectar();
		// $con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO  COMISIONPROMESA(IDUSUARIO, FECHACOMISIONPROMESA, IDPROYECTO, IDPROMESA, MONTOUFEMPRESA, PORCENTAJECOMISIONEMPRESA, ESTADOPAGO, ACTOR)
			VALUES('" . $idUsuario . "','" . $fechaComisionPromesa . "','" . $idProyecto . "','" . $idPromesa . "','" . $montoUFEmpresa . "','" . $porcentajeComisionEmpresa . "','NO PAGADO','" . $actor . "')";
			if ($con->query($sql)) {
				return $con;
				 // "Ok";
			}
			else{
				return $con->error;
				// $con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			// $con->query("ROLLBACK");
			return "Error";
		}
	}
	function consultaDatosCalculoComision($codigoProyecto, $numeroOperacion, $codigoUnidad){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT FORMAT(CASE WHEN IDACCION = 1 THEN (COMISIONLVDUENO/100)*VALORUF ELSE (COMISIONVENDUENO/100)*VALORUF END,2) 'COMISIONDUENO',
				FORMAT(CASE WHEN IDACCION = 1 THEN (COMISIONLVCLIENTE/100)*VALORUF ELSE (COMISIONVENCLIENTE/100)*VALORUF END,2) 'COMISIONCLIENTE'
				FROM UNIDAD U
				LEFT JOIN PROMESA P ON U.IDUNIDAD = P.IDUNIDAD
				WHERE P.IDPROYECTO = (
				SELECT IDPROYECTO
				FROM PROYECTO
				WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
				AND P.NUMERO = '" . $numeroOperacion ."'
				AND U.CODIGO = '" . $codigoUnidad . "'";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function consultaComisionPromesaCorretaje($codigoProyecto, $numeroOperacion, $codigoUnidad){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT U.VALORUF, U.IDACCION, U.COMISIONLVDUENO, U.COMISIONLVCLIENTE, U.COMISIONVENDUENO, U.COMISIONVENCLIENTE
				FROM UNIDAD U
				LEFT JOIN PROMESA P ON U.IDUNIDAD = P.IDUNIDAD
				WHERE P.IDPROYECTO = (
				SELECT IDPROYECTO
				FROM PROYECTO
				WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
				AND P.NUMERO = '" . $numeroOperacion ."'
				AND U.CODIGO = '" . $codigoUnidad . "'";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
		function ingresoFormaPagoComision($idComisionPromesa, $formaPago, $bancoPromesa, $serieChequePromesa, $nroChequePromesa, $fechaPagoComision, $con, $valorUFCom, $montoPagoCom){
		// $con = conectar();
		// $con->query("START TRANSACTION");
		if($con != 'No conectado'){
			if(is_null($fechaPagoComision)){
				$sql = "INSERT INTO PAGOCOMISIONPROMESA(IDCOMISIONPROMESA, FORMAPAGO, BANCOPROMESA, SERIECHEQUEPROMESA, NROCHEQUEPROMESA, FECHAPAGOCOMISION, VALORUFCOM, MONTOPAGOCOM)
				VALUES('" . $idComisionPromesa . "','" . $formaPago . "','" . $bancoPromesa . "','" . $serieChequePromesa . "','" . $nroChequePromesa . "',NULL,'" . $valorUFCom . "','" . $montoPagoCom ."')";
			}
			else{
				$sql = "INSERT INTO PAGOCOMISIONPROMESA(IDCOMISIONPROMESA, FORMAPAGO, BANCOPROMESA, SERIECHEQUEPROMESA, NROCHEQUEPROMESA, FECHAPAGOCOMISION, VALORUFCOM, MONTOPAGOCOM)
				VALUES('" . $idComisionPromesa . "','" . $formaPago . "','" . $bancoPromesa . "','" . $serieChequePromesa . "','" . $nroChequePromesa . "','" . $fechaPagoComision ."','" . $valorUFCom . "','" . $montoPagoCom ."')";
			}
			if ($con->query($sql)) {
				return $con;
			    // return "Ok";
			}
			else{
				// return $con->error;
				// $con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			// $con->query("ROLLBACK");
			return "Error";
		}
	}
	function cambiarEstadoPromesaCorretaje($codigoProyecto, $numeroOperacion, $estadoPromesa){
			$con = conectar();
			$con->query("START TRANSACTION");
			if($con != 'No conectado'){
				$sql = "UPDATE PROMESA
SET ESTADO = '" . $estadoPromesa . "'
WHERE IDPROYECTO =
(
	SELECT IDPROYECTO
	FROM PROYECTO
	WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
)
AND NUMERO = '" . $numeroOperacion . "'";
				if ($con->query($sql)) {
				    return $con;
				    // return "Ok";
				}
				else{
					// $con->query("ROLLBACK");
					return "Error";
				}
			}
			else{
				// $con->query("ROLLBACK");
				return "Error";
			}
		}
	function eliminaComisionPromesaCorretaje($codigoProyecto, $numeroOperacion, $con){
		// $con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM COMISIONPROMESA WHERE
			IDPROMESA = (SELECT IDPROMESA FROM PROMESA WHERE
			IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO = '" . $codigoProyecto . "')
			AND NUMERO = '" . $numeroOperacion . "')";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function eliminaFormaPagoComision($codigoProyecto, $numeroOperacion, $con){
		// $con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "DELETE FROM PAGOCOMISIONPROMESA
			WHERE IDCOMISIONPROMESA IN(
			    SELECT IDCOMISIONPROMESA
			    FROM COMISIONPROMESA
			    WHERE IDPROMESA = (
			        SELECT IDPROMESA
			        FROM PROMESA
			        WHERE IDPROYECTO = (
			            SELECT IDPROYECTO
			            FROM PROYECTO
			            WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			        )
			        AND NUMERO = '" . $numeroOperacion . "'
			    )
			)";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			  return "Ok";
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function eliminarFormaPagoComisionSinCon($codigoProyecto, $numeroOperacion){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "DELETE FROM PAGOCOMISIONPROMESA
			WHERE IDCOMISIONPROMESA IN(
			    SELECT IDCOMISIONPROMESA
			    FROM COMISIONPROMESA
			    WHERE IDPROMESA = (
			        SELECT IDPROMESA
			        FROM PROMESA
			        WHERE IDPROYECTO = (
			            SELECT IDPROYECTO
			            FROM PROYECTO
			            WHERE CODIGOPROYECTO = '" . $codigoProyecto . "'
			        )
			        AND NUMERO = '" . $numeroOperacion . "'
			    )
			)";
			if ($row = $con->query($sql)) {
				return "Ok";
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function consultaEstadoComisionCorretaje($codigoProyecto, $numeroOperacion, $actor){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT ESTADOPAGO
					FROM COMISIONPROMESA
					WHERE IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO='" . $codigoProyecto . "')
					AND IDPROMESA = (
					SELECT IDPROMESA FROM PROMESA WHERE NUMERO = '" . $numeroOperacion . "'
					AND IDPROYECTO = (
					SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO='" . $codigoProyecto . "')
					) AND ACTOR = '" . $actor . "'";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function actualizarComisionPromesaEmpresa($codigoProyecto, $numeroOperacion, $actor, $estadoPagoPromesa){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE COMISIONPROMESA
			SET ESTADOPAGO = '" . $estadoPagoPromesa . "'
			WHERE IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO='" . $codigoProyecto . "')
			AND IDPROMESA = (
			SELECT IDPROMESA FROM PROMESA WHERE NUMERO = '" . $numeroOperacion . "'
			AND IDPROYECTO = (
			SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO='" . $codigoProyecto . "')
			) AND ACTOR = '" . $actor . "'";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function consultaDatosPagoComisionCorretaje($codigoProyecto, $numeroOperacion, $actor){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT * FROM PAGOCOMISIONPROMESA
					WHERE IDCOMISIONPROMESA= (SELECT IDCOMISIONPROMESA FROM COMISIONPROMESA
					WHERE IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO='" . $codigoProyecto . "')
					AND IDPROMESA = (
					SELECT IDPROMESA FROM PROMESA WHERE NUMERO = '" . $numeroOperacion . "'
					AND IDPROYECTO = (
					SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO='" . $codigoProyecto . "')
					) AND ACTOR = '" . $actor . "')";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
	function actualizarPagoComisionPromesaEmpresa($codigoProyecto, $numeroOperacion, $actor, $formaPagoComision, $bancoComision, $serieComision, $nroComision, $fechaComision, $valorUFCom, $montoPagoCom){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE PAGOCOMISIONPROMESA
			SET FORMAPAGO = '" . $formaPagoComision . "',
			BANCOPROMESA = '" . $bancoComision . "',
			SERIECHEQUEPROMESA = '" . $serieComision . "',
			NROCHEQUEPROMESA = '" . $nroComision . "',
			FECHAPAGOCOMISION ='" . $fechaComision . "',
			VALORUFCOM = '" . $valorUFCom . "',
			MONTOPAGOCOM = '" . $montoPagoCom . "'
			WHERE IDCOMISIONPROMESA = (SELECT IDCOMISIONPROMESA FROM COMISIONPROMESA WHERE IDPROYECTO =
			(SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO='" . $codigoProyecto . "')
			AND IDPROMESA = (
			SELECT IDPROMESA FROM PROMESA WHERE NUMERO = '" . $numeroOperacion . "'
			AND IDPROYECTO = (
			SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO='" . $codigoProyecto . "')
			) AND ACTOR = '" . $actor . "')";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				return $con->error;
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}
	function consultaDatosComisionCorretaje($codigoProyecto, $numeroOperacion, $actor){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT *
					FROM COMISIONPROMESA
					WHERE IDPROYECTO = (SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO='" . $codigoProyecto . "')
					AND IDPROMESA = (
					SELECT IDPROMESA FROM PROMESA WHERE NUMERO = '" . $numeroOperacion . "'
					AND IDPROYECTO = (
					SELECT IDPROYECTO FROM PROYECTO WHERE CODIGOPROYECTO='" . $codigoProyecto . "')
					) AND ACTOR = '" . $actor . "'";
			if ($row = $con->query($sql)) {

				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}
?>
