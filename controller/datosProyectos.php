<?php
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');
	session_start();

	if(count($_POST) == 0){
			$row = '';
			if($_SESSION['idperfil'] == 1 || $_SESSION['idperfil'] == 2){
    		$row = consultaProyectos();
			}
			else{
				$row = consultaProyectosUser($_SESSION['rutUser']);
			}

        if(is_array($row))
        {
            $return = "";

            for($i = 0; $i < count($row); $i++){
                if($row[$i] != null){
                    if($i == 0){
											if($row[$i]['FECHAINICIOVENTA'] == '1900-01-01'){
												$fechaIV = '';
											}
											else{
												$date2 = new Datetime($row[$i]['FECHAINICIOVENTA']);
                      	$fechaIV = $date2->format('d-m-Y');
											}

											if($row[$i]['FECHAFINOBRA'] == '1900-01-01'){
												$fechaFO = '';
											}
											else{
												$date2 = new Datetime($row[$i]['FECHAFINOBRA']);
                      	$fechaFO = $date2->format('d-m-Y');
											}

											if($row[$i]['FECHAENTREGA'] == '1900-01-01'){
												$fechaE = '';
											}
											else{
												$date3 = new Datetime($row[$i]['FECHAENTREGA']);
	                      $fechaE = $date3->format('d-m-Y');
											}

                      $return = $return . $row[$i]['CODIGOPROYECTO'] . ',' . $row[$i]['NOMBRE'] . ',' . $row[$i]['INMOBILIARIA']   . ',' . $fechaIV . ',' . $fechaFO . ',' . $fechaE . ',' . $row[$i]['ESTADO'];
                    }
                    else{
											if($row[$i]['FECHAINICIOVENTA'] == '1900-01-01'){
												$fechaIV = '';
											}
											else{
												$date2 = new Datetime($row[$i]['FECHAINICIOVENTA']);
												$fechaIV = $date2->format('d-m-Y');
											}

											if($row[$i]['FECHAFINOBRA'] == '1900-01-01'){
												$fechaFO = '';
											}
											else{
												$date2 = new Datetime($row[$i]['FECHAFINOBRA']);
                      	$fechaFO = $date2->format('d-m-Y');
											}

											if($row[$i]['FECHAENTREGA'] == '1900-01-01'){
												$fechaE = '';
											}
											else{
												$date3 = new Datetime($row[$i]['FECHAENTREGA']);
	                      $fechaE = $date3->format('d-m-Y');
											}

                      $return = $return . ',' . $row[$i]['CODIGOPROYECTO'] . ',' . $row[$i]['NOMBRE'] . ',' . $row[$i]['INMOBILIARIA']  . ',' . $fechaIV . ',' . $fechaFO . ',' . $fechaE . ',' . $row[$i]['ESTADO'];
                    }
                }
            }

            echo $return;
        }
        else{
            echo "Sin datos";
        }
	}
	else{
		echo "Sin datos";
	}
?>
