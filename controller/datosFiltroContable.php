<?php
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');
	session_start();
	if(count($_POST) >= 0){
		$codigoProyecto = $_POST['codigoProyecto'];
		$estado = $_POST['estado'];
    	$row = '';
    	if ($_SESSION['idperfil'] == 1 || $_SESSION['idperfil'] == 2) {
    		if ($codigoProyecto == "todos" && $estado == "todos") {
				$row = consultaOperacionesContables();
			}else if ($codigoProyecto == "todos" && $estado == "Todos") {
				$row = consultaOperacionesContables();
			}else if ($codigoProyecto == "Todos" && $estado == "todos") {
				$row = consultaOperacionesContables();
			}else if ($codigoProyecto == "Todos" && $estado == "Todos") {
				$row = consultaOperacionesContables();
			}else if($codigoProyecto == "todos" && $estado <> "todos"){
				$row = filtroContableEstado($estado);
			}else if($codigoProyecto == "todos" && $estado <> "Todos"){
				$row = filtroContableEstado($estado);
			}else if($codigoProyecto == "Todos" && $estado <> "todos"){
				$row = filtroContableEstado($estado);
			}else if($codigoProyecto == "Todos" && $estado <> "Todos"){
				$row = filtroContableEstado($estado);
			}else if($codigoProyecto <> "todos" && $estado == "todos"){
				$row = filtroContableProyecto($codigoProyecto);
			}else if($codigoProyecto <> "todos" && $estado == "Todos"){
				$row = filtroContableProyecto($codigoProyecto);
			}else if($codigoProyecto <> "Todos" && $estado == "todos"){
				$row = filtroContableProyecto($codigoProyecto);
			}else if($codigoProyecto <> "Todos" && $estado == "Todos"){
				$row = filtroContableProyecto($codigoProyecto);
			}
			else{
				$row = filtroContable($codigoProyecto, $estado);	
			}
    	}else{
    		if ($codigoProyecto == "todos" && $estado == "todos") {
				$row = consultaOperacionesContablesUsu($_SESSION['rutUser']);
			}else if ($codigoProyecto == "todos" && $estado == "Todos") {
				$row = consultaOperacionesContablesUsu($_SESSION['rutUser']);
			}else if ($codigoProyecto == "Todos" && $estado == "todos") {
				$row = consultaOperacionesContablesUsu($_SESSION['rutUser']);
			}else if ($codigoProyecto == "Todos" && $estado == "Todos") {
				$row = consultaOperacionesContablesUsu($_SESSION['rutUser']);
			}else if($codigoProyecto == "todos" && $estado <> "todos"){
				$row = filtroContableEstadoUsu($estado,$_SESSION['rutUser']);
			}else if($codigoProyecto == "todos" && $estado <> "Todos"){
				$row = filtroContableEstadoUsu($estado,$_SESSION['rutUser']);
			}else if($codigoProyecto == "Todos" && $estado <> "todos"){
				$row = filtroContableEstadoUsu($estado,$_SESSION['rutUser']);
			}else if($codigoProyecto == "Todos" && $estado <> "Todos"){
				$row = filtroContableEstadoUsu($estado,$_SESSION['rutUser']);
			}else if($codigoProyecto <> "todos" && $estado == "todos"){
				$row = filtroContableProyecto($codigoProyecto);
			}else if($codigoProyecto <> "todos" && $estado == "Todos"){
				$row = filtroContableProyecto($codigoProyecto);
			}else if($codigoProyecto <> "Todos" && $estado == "todos"){
				$row = filtroContableProyecto($codigoProyecto);
			}else if($codigoProyecto <> "Todos" && $estado == "Todos"){
				$row = filtroContableProyecto($codigoProyecto);
			}
			else{
				$row = filtroContable($codigoProyecto, $estado);	
			}
    	}				

				
	    		
				// }
				// else{
				// 	$row = consultaPromesasUsuAutorizadasParaEscritura($_SESSION['rutUser']);
				// }

        if(is_array($row))
        {
					$results = array(
							"sEcho" => 1,
							"iTotalRecords" => count($row),
							"iTotalDisplayRecords" => count($row),
							"aaData"=>$row
					);

					echo json_encode($results);
        }
        else{
					$results = array(
              "sEcho" => 1,
              "iTotalRecords" => 0,
              "iTotalDisplayRecords" => 0,
              "aaData"=>[]
          );
          echo json_encode($results);
        }
	}
	else{
		echo "Sin datos";
	}
?>
