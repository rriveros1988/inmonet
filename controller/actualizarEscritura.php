<?php
  header('Access-Control-Allow-Origin: *');
  //ini_set('display_errors', 'On');
  require('../model/consultas.php');
  session_start();

if(count($_SESSION) > 0 && count($_POST) > 0){
    $codigoProyecto = $_SESSION['codigoProyecto'];
    $numeroOperacion = $_SESSION['numeroOperacion'];

    $valorTotalUF = $_POST['escrituraTotalUF'];
    $valorSaldoTotalUF = $_POST['valorSaldoTotalUF'];

    $fechaPagoEscritura =  new DateTime($_POST['fechaPagoEscritura']);
    $fechaPagoEscritura = $fechaPagoEscritura->format("Y-m-d");

    $valorPagoEscritura = $_POST['valorPagoEscritura'];
    $formaPagoEscritura = $_POST['formaPagoEscritura'];
    $bancoEscritura = $_POST['bancoEscritura'];
    $serieChequeEscritura = $_POST['serieChequeEscritura'];
    $nroChequeEscritura = $_POST['nroChequeEscritura'];
    $usuario = chequeaUsuario($_SESSION['rutUser']);
    $vendedorID = $_POST['escrituraVendedor'];
    $_SESSION['escrituraIdVendedor'] = $_POST['escrituraVendedor'];
    $fechaEscritura = new DateTime($_POST['fechaEscritura']);
    $fechaEscritura = $fechaEscritura->format("Y-m-d");

    $valorMontoCuota = $_POST['valorMontoCuota'];
    $formaPagoCuota = $_POST['formaPagoCuota'];
    $bancoCuota = $_POST['bancoCuota'];
    $serieCuota = $_POST['serieCuota'];
    $nroCuota = $_POST['nroCuota'];
    $fechaPagoCuota = new DateTime($_POST['fechaPagoCuota']);
    $fechaPagoCuota = $fechaPagoCuota->format("Y-m-d");
    $montoUFCuota = $_POST['montoUFCuota'];

    $idUsuario = $usuario['IDUSUARIO'];

    if(array_key_exists('escrituraIdVendedor', $_SESSION)){
      $vendedor = chequeaUsuarioID($_SESSION['escrituraIdVendedor']);
      $_SESSION['escrituraVendedor'] = $vendedor['NOMBRES'] . ' ' . $vendedor['APELLIDOS'];
      $_SESSION['escrituraVendedorRut'] = $vendedor['RUT'];
    }
    else{
      $_SESSION['escrituraVendedor'] = $_SESSION['nombreUser'];
      $_SESSION['escrituraVendedorRut'] = $_SESSION['rutUser'];
    }
    // $unidades_liberar = $_POST['unidades_liberar'];
    // $bodegas_borrar = $_POST['bodegas_borrar'];
    // $estacionamientos_borrar = $_POST['estacionamientos_borrar'];
    /*
    if($_SESSION['codProyectoClienteCotizacion'] == "COR"){
      $descuento1UF = 0;
      $descuento2UF = 0;
      $valorReservaUF = 0;
      $valorPiePromesaUF = 0;
      $valorPieSaldoUF = 0;
      $cantidadCuotasPie = 0;
      $valorSaldoTotalUF = 0;
    }else{
      $descuento1UF = $_SESSION['descuento1ClienteCotizacion'];
      $descuento2UF = $_SESSION['descuento2ClienteCotizacion'];
      $valorReservaUF = $_SESSION['reservaClienteCotizacion'];
      $valorPiePromesaUF = $_SESSION['piePromesaClienteCotizacion'];
      $valorPieSaldoUF = $_SESSION['pieCuotasClienteCotizacion'];
      $cantidadCuotasPie = $_SESSION['cuotasClienteCotizacion'];
      $valorSaldoTotalUF = $_SESSION['saldoClienteCotizacion'];
      $bonoVenta = $_SESSION['bonoVentaCrearCotizacion'];
    }
    */

    liberaUnidadesEscrituraEstacionamiento($codigoProyecto,$numeroOperacion);
    liberaUnidadesEscrituraBodega($codigoProyecto,$numeroOperacion);
    liberaUnidadesPromesaEstacionamiento($codigoProyecto,$numeroOperacion);
    liberaUnidadesPromesaBodega($codigoProyecto,$numeroOperacion);
    liberaUnidadesReservaEstacionamiento($codigoProyecto,$numeroOperacion);
    liberaUnidadesReservaBodega($codigoProyecto,$numeroOperacion);
    //liberaUnidadesPromesadas($codigoProyecto,$numeroOperacion);

    eliminaReservaBodega($codigoProyecto, $numeroOperacion);
    eliminaReservaEstacionamiento($codigoProyecto, $numeroOperacion);
    eliminaPromesaBodega($codigoProyecto, $numeroOperacion);
    eliminaPromesaEstacionamiento($codigoProyecto, $numeroOperacion);
    eliminaEscrituraBodega($codigoProyecto, $numeroOperacion);
    eliminaEscrituraEstacionamiento($codigoProyecto, $numeroOperacion);
    eliminaEscrituraCuota($codigoProyecto,$numeroOperacion);

    $row = actualizarEscritura($codigoProyecto, $numeroOperacion, $valorTotalUF,  $valorSaldoTotalUF, $fechaPagoEscritura, $valorPagoEscritura, $formaPagoEscritura, $bancoEscritura, $serieChequeEscritura, $nroChequeEscritura, $vendedorID, $fechaEscritura);

    $es = consultaEscrituraEspecificaActualizar($codigoProyecto, $numeroOperacion);
    $idProyecto = $es[0]['IDPROYECTO'];
    $idReserva = $es[0]['IDRESERVA'];

    $idPromesa = $es[0]['IDPROMESA'];
    $idEscritura = $es[0]['IDESCRITURA'];
    $unidad = consultaUnidadEspecifica($es[0]['IDUNIDAD']);
    $cliente1 = consultaClienteEspecifico($es[0]['IDCLIENTE1']);

    if($row != "Error")
    {

      $in = 'Ok';
      $in2 = 'Ok';
      $in3 = 'Ok';
      if($_POST['bodegasClienteEscritura'] != ''){
        for($i = 0; $i < count($_POST['bodegasClienteEscritura']); $i++){
          ingresaReservaBodega($idReserva,$_POST['bodegasClienteEscritura'][$i], $row);
          ingresaPromesaBodega($idPromesa,$_POST['bodegasClienteEscritura'][$i], $row);
          $in = ingresaEscrituraBodega($idEscritura,$_POST['bodegasClienteEscritura'][$i], $row);
          if($in == "Error"){
            break;
          }
          else{
            actualizaEstadoUnidad($_POST['bodegasClienteEscritura'][$i], 5, $row);
          }
        }
      }

      if(is_array($_POST['estacionamientosClienteEscritura'])){
        for($i = 0; $i < count($_POST['estacionamientosClienteEscritura']); $i++){
          ingresaReservaEstacionamiento($idReserva,$_POST['estacionamientosClienteEscritura'][$i], $row);
          ingresaPromesaEstacionamiento($idPromesa,$_POST['estacionamientosClienteEscritura'][$i], $row);
          $in2 = ingresaEscrituraEstacionamiento($idEscritura,$_POST['estacionamientosClienteEscritura'][$i], $row);
          if($in2 == "Error"){
            break;
          }
          else{
            actualizaEstadoUnidad($_POST['estacionamientosClienteEscritura'][$i], 5, $row);
          }
        }
      }
      $in3 = ingresaEscrituraCuota($idEscritura, $valorMontoCuota, $formaPagoCuota, $bancoCuota, $serieCuota, $nroCuota, $fechaPagoCuota, $montoUFCuota, $row);




      if($in != "Error" && $in2 != "Error" && $in3 != "Error"){
        //$_SESSION['numeroOperacion'] = $numeroOperacion;
        //Datos a session
        $fechaEscritura = new DateTime($_POST['fechaEscritura']);
        $fechaPagoEscritura = new DateTime($_POST['fechaPagoEscritura']);
        $fechaPagoCuota = new DateTime($_POST['fechaPagoCuota']);

        $fechaEscritura = $fechaEscritura->format("d-m-Y");
        $fechaPagoEscritura = $fechaPagoEscritura->format("d-m-Y");
        $fechaPagoCuota = $fechaPagoCuota->format("d-m-Y");



        $_SESSION['escrituraFecha'] = $fechaEscritura;

        $_SESSION['escrituraReservaUF'] = $es[0]['VALORRESERVAUF'];
        $fechaPagoReserva = new DateTime($es[0]['FECHAPAGORESERVA']);
        $_SESSION['escrituraFechaPagoReserva'] = $fechaPagoReserva->format("d-m-Y");
        $_SESSION['escrituraValorPagoReserva'] = $es[0]['VALORPAGORESERVA'];
        $formaPagoReserva = consultaFormaPagoReservaEspecifica($es[0]['FORMAPAGORESERVA']);
        $_SESSION['escrituraFormaPagoNombreReserva'] = $formaPagoReserva[0]['NOMBRE'];
        $_SESSION['escrituraBancoReserva'] = $es[0]['BANCORESERVA'];
        $_SESSION['escrituraSerieNroReserva'] = $es[0]['SERIECHEQUERESERVA'];
        $_SESSION['escrituraNroTransChequeReserva'] = $es[0]['NROCHEQUERESERVA'];

        $_SESSION['escrituraPieSaldoUF'] = $es[0]['VALORPIESALDOUF'];
        $_SESSION['escrituraCuotasPie'] = $es[0]['CANTCUOTASPIE'];
        $_SESSION['escrituraSaldoTotalUF'] = $valorSaldoTotalUF;

        $_SESSION['escrituraPiePromesaUF'] = $es[0]['VALORPIEPROMESAUF'];
        $fechaPagoPromesa = new DateTime($es[0]['FECHAPAGOPROMESA']);
        $_SESSION['escrituraFechaPagoPromesa'] = $fechaPagoPromesa->format("d-m-Y");
        $_SESSION['valorPiePromesa'] = $es[0]['VALORPIEPROMESA'];
        $formaPagoPromesa = consultaFormaPagoReservaEspecifica($es[0]['FORMAPAGOPROMESA']);
        $_SESSION['escrituraFormaPagoNombrePromesa'] = $formaPagoPromesa[0]['NOMBRE'];
        $_SESSION['escrituraBancoPromesa'] = $es[0]['BANCOPROMESA'];
        $_SESSION['escrituraSerieNroPromesa'] = $es[0]['SERIECHEQUEPROMESA'];
        $_SESSION['escrituraNroTransChequePromesa'] = $es[0]['NROCHEQUEPROMESA'];

        $_SESSION['escrituraFechaPagoEscritura'] = $fechaPagoEscritura;
        $_SESSION['escrituraValorPagoEscritura'] = $valorPagoEscritura;
        $_SESSION['escrituraFormaPagoEscritura'] = consultaFormaPagoReservaEspecifica($formaPagoEscritura);
        $_SESSION['escrituraBancoEscritura'] = $bancoEscritura;
        $_SESSION['escrituraSerieChequeEscritura'] = $serieChequeEscritura;
        $_SESSION['escrituraNroChequeEscritura'] = $nroChequeEscritura;
        $_SESSION['escrituraValorPieSaldo'] = $es[0]['VALORPIESALDO'];
        $_SESSION['escrituraPieCuotas'] = $es[0]['VALORPIESALDOUF'];
        $_SESSION['valorPieSaldo'] = $es[0]['VALORPIESALDO'];
        $_SESSION['promesaTotalUF'] = $valorTotalUF;


        $fechaC1 = new DateTime($cliente1[0]['FECHANAC']);
        $fechaNacCliente1 = $fechaC1->format("d-m-Y");

        $_SESSION['escrituraFechaNacCliente1'] = $fechaNacCliente1;
        $_SESSION['escrituraEstadoCivilIDCliente1'] = $cliente1[0]['ESTADOCIVIL'];
        $_SESSION['escrituraProfesionCliente1'] = $cliente1[0]['PROFESION'];
        if ($cliente1[0]['TIPODOMICILIO'] != '') {
        $_SESSION['escrituraDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'] . ', ' . $cliente1[0]['TIPODOMICILIO'];
        }else{
          $_SESSION['escrituraDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'];
        }
        $_SESSION['escrituraComunaCliente1'] = $cliente1[0]['COMUNA'];
        $_SESSION['escrituraTelefonoCliente1'] = $cliente1[0]['CELULAR'];
        $_SESSION['escrituraMailCliente1'] = $cliente1[0]['EMAIL'];
        $_SESSION['escrituraRentaCliente1'] = $cliente1[0]['EM_RENTALIQUIDA'];
        $_SESSION['escrituraCiudadCliente1'] = $cliente1[0]['CIUDAD'];
        $_SESSION['escrituraInstitucionCliente1'] = $cliente1[0]['INSTITUCION'];
        $_SESSION['escrituraRegionCliente1'] = $cliente1[0]['REGION'];
        $_SESSION['escrituraActividadCliente1'] = $cliente1[0]['ACTIVIDAD'];
        $_SESSION['escrituraPaisCliente1'] = $cliente1[0]['PAIS'];
        $_SESSION['escrituraNacionalidadCliente1'] = $cliente1[0]['NACIONALIDAD'];
        $_SESSION['escrituraResidenciaCliente1'] = $cliente1[0]['RESIDENCIA'];
        $_SESSION['escrituraSexoCliente1'] = $cliente1[0]['SEXO'];
        $_SESSION['escrituraFechaNacCliente1'] = $cliente1[0]['FECHANAC'];
        $_SESSION['escrituraNivelEducCliente1'] = $cliente1[0]['NIVELEDUCACIONAL'];
        $_SESSION['escrituraCasaHabitaCliente1'] = $cliente1[0]['CASAHABITA'];
        $_SESSION['escrituraMotivoCompraCliente1'] = $cliente1[0]['MOTIVOCOMPRA'];

        $proyecto = consultaProyectoEspecifico($idProyecto);
        $_SESSION['escrituraCodigoProyecto'] = $codigoProyecto;
        $_SESSION['escrituraLogoProyecto'] = '../' . $proyecto[0]['LOGO'];
        $_SESSION['escrituraNombreProyecto'] = $proyecto[0]['NOMBRE'];
        $_SESSION['escrituraInmobiliaria'] = $proyecto[0]['INMOBILIARIA'];
        $_SESSION['escrituraInmobiliariaDireccion'] = $proyecto[0]['DIRECCION'];

        $_SESSION['escrituraNumeroDepto'] = $unidad[0]['CODIGO'];

        $_SESSION['estacionamientosClienteEscritura'] = $_POST['estacionamientosClienteEscritura'];
        $_SESSION['bodegasClienteEscritura'] = $_POST['bodegasClienteEscritura'];

        //Actualiza datos de cliente en session_start
        $_SESSION['escrituraNombreCliente'] = $cliente1[0]['NOMBRES'];
        $_SESSION['escrituraApellidoCliente'] = $cliente1[0]['APELLIDOS'];
        $_SESSION['escrituraRutCliente'] = $cliente1[0]['RUT'];

        //OBSERVACION

        //VENDEDOR
        $_SESSION['escrituraDatosVendedor'] = promesaDatosVendedor($idUsuario);
        $_SESSION['escrituraCuotas'] = consultaCuotasPromesadas($codigoProyecto, $numeroOperacion);

        $_SESSION['residualMonto'] = $valorMontoCuota;
        $_SESSION['residualFechaCheque'] = $fechaPagoCuota;
        $_SESSION['residualFormaPago'] = consultaFormaPagoReservaEspecifica($formaPagoCuota);
        $_SESSION['residualBanco'] = $bancoCuota;
        $_SESSION['residualSerie'] = $serieCuota;
        $_SESSION['residualNro'] = $nroCuota;
        

        $row->query("COMMIT");
        echo "Ok¬" . $codigoProyecto . "¬" . $numeroOperacion;
      }
      else{
        $row->query("ROLLBACK");
        echo "Sin datos";
      }
    }
  	else{
  		echo "Sin datos";
  	}
	}
	else{
		echo "Sin datos";
	}
?>
