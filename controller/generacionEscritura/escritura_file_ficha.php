<?php
// ini_set('display_errors', 'On');
require '../html2pdf/vendor/autoload.php';
date_default_timezone_set("America/Santiago");
require('../../model/consultas.php');
session_start();

function pago($tasa, $monto, $meses){
  $I = $tasa / 12 / 100 ;
  $I2 = $I + 1 ;
  $I2 = pow($I2,-$meses) ;

  $CuotaMensual = ($I * $monto) / (1 - $I2);

  return $CuotaMensual;
}
if (!array_key_exists('escrituraNumeroOperacionescrituraNumeroOperacion', $_SESSION)) {
  $_SESSION['escrituraNumeroOperacion'] = $_SESSION['numeroOperacion'];
}
if($_SESSION['escrituraCodigoProyecto'] == 'COR'){
  $formPagoCliente = consultaPagoComisionCliente($_SESSION['escrituraCodigoProyecto'],$_SESSION['escrituraNumeroOperacion']);
  $_SESSION['formaPagoCliente'] = $formPagoCliente;
  if (array_key_exists('idPromesa', $_SESSION)) {
    $datosUnidadVendedor = datosUnidadPromesaParaEscritura($_SESSION['idPromesa']);
  }
  else{
    $datosUnidadVendedor = datosUnidadEscrituraId($_SESSION['idEscritura']);
  }
  $_SESSION['datosUnidadVendedor'] = $datosUnidadVendedor;
}

use Spipu\Html2Pdf\Html2Pdf;

ob_start();
/*
if($_SESSION['codProyectoClienteCotizacion'] == "COR"){
  require_once 'cotizacion_plantilla_pdf_corretaje.php';
}
else{
  require_once 'cotizacion_plantilla_pdf.php';
}
*/
require_once 'escritura_plantilla_pdf_ficha.php';

$html = ob_get_clean();

// $document = 'D:/xampp/htdocs/inmonet';
// $document = '/var/www/html/Git/inmonet';
// $document = '/home/livingne/inmonet.cl/test';
$document = '/home/livingne/inmonet.cl';

if(!is_dir("../../repositorio/" . $_SESSION['escrituraCodigoProyecto'])){
  mkdir("../../repositorio/" . $_SESSION['escrituraCodigoProyecto'], 0777);
  mkdir("../../repositorio/" . $_SESSION['escrituraCodigoProyecto'] . "/escritura", 0777);
}
if(!is_dir("../../repositorio/" . $_SESSION['escrituraCodigoProyecto'] . "/escritura")){
  mkdir("../../repositorio/" . $_SESSION['escrituraCodigoProyecto'] . "/escritura", 0777);
}

$html2pdf = new Html2Pdf('P','LETTER','es','true','UTF-8');
$html2pdf->writeHTML($html);
$html2pdf->output($document . '/repositorio/' . $_SESSION['escrituraCodigoProyecto'] . '/escritura/' . $_SESSION['numeroOperacion'] . '_' . $_SESSION['escrituraCodigoProyecto'] . '_' . $_SESSION['escrituraNumeroDepto'] . '_' . str_replace(' ', '_',$_SESSION['escrituraNombreCliente']) . '_' . str_replace(' ', '_',$_SESSION['escrituraApellidoCliente']) . '_ES.pdf', 'F');

$_SESSION['FICHA_PDF_ACTUAL'] = '';

if(file_exists($document . '/repositorio/' . $_SESSION['escrituraCodigoProyecto'] . '/escritura/' . $_SESSION['numeroOperacion'] . '_' . $_SESSION['escrituraCodigoProyecto'] . '_' . $_SESSION['escrituraNumeroDepto'] . '_' . str_replace(' ', '_',$_SESSION['escrituraNombreCliente']) . '_' . str_replace(' ', '_',$_SESSION['escrituraApellidoCliente']) . '_ES.pdf')){
  echo "Ok";
  actualizaEscrituraFICHA_PDF($_SESSION['escrituraCodigoProyecto'] . '/escritura/' . $_SESSION['numeroOperacion'] . '_' . $_SESSION['escrituraCodigoProyecto'] . '_' . $_SESSION['escrituraNumeroDepto'] . '_' . str_replace(' ', '_',$_SESSION['escrituraNombreCliente']) . '_' . str_replace(' ', '_',$_SESSION['escrituraApellidoCliente']) . '_ES.pdf',$_SESSION['idEscritura']);
  $_SESSION['FICHA_PDF_ACTUAL'] = $_SESSION['escrituraCodigoProyecto'] . '/escritura/' . $_SESSION['numeroOperacion'] . '_' . $_SESSION['escrituraCodigoProyecto'] . '_' . $_SESSION['escrituraNumeroDepto'] . '_' . str_replace(' ', '_',$_SESSION['escrituraNombreCliente']) . '_' . str_replace(' ', '_',$_SESSION['escrituraApellidoCliente']) . '_ES.pdf';
}
else{
  echo "Sin datos";
}
?>
