<?php
  header('Access-Control-Allow-Origin: *');
  require('../model/consultas.php');
  session_start();

	if(count($_POST) > 0){
    //Código proyecto guardado en sesión
    $codigoProyecto = $_SESSION['codigoProyectoBack'];
    //Datos de post
    $tipoCrearUnidad = $_POST['tipoCrearUnidad'];
    $accionCrearUniad = $_POST['accionCrearUnidad'];
    $numeroCrearUnidad = $_POST['numeroCrearUnidad'];
    $estadoCrearUnidad = $_POST['estadoCrearUnidad'];
    $pisoCrearUnidad = $_POST['pisoCrearUnidad'];
    $orientacionCrearUnidad = $_POST['orientacionCrearUnidad'];
    $cantDormitoriosCrearUnidad = $_POST['cantDormitoriosCrearUnidad'];
    $cantBanosCrearUnidad = $_POST['cantBanosCrearUnidad'];
    $logiaCrearUnidad = $_POST['logiaCrearUnidad'];
    $m2UtilCrearUnidad = $_POST['m2UtilCrearUnidad'];
    $m2TerrazaCrearUnidad = $_POST['m2TerrazaCrearUnidad'];
    $m2OtroCrearUnidad = $_POST['m2OtroCrearUnidad'];
    $valorCrearUnidad = $_POST['valorCrearUnidad'];
    $comisionLVDuenoCrearUnidad = $_POST['comisionLVDuenoCrearUnidad'];
    $comisionLVClienteCrearUnidad = $_POST['comisionLVClienteCrearUnidad'];
    $comisionVenDuenoCrearUnidad = $_POST['comisionVenDuenoCrearUnidad'];
    $comisionVenClienteCrearUnidad = $_POST['comisionVenClienteCrearUnidad'];
    $direccionCrearUnidad = $_POST['direccionCrearUnidad'];
    $comunaCrearUnidad = $_POST['comunaCrearUnidad'];
    $codigoTipoCrearUnidad = $_POST['codigoTipoCrearUnidad'];
    $cantidadBodegasCrearUnidad = $_POST['cantidadBodegasCrearUnidad'];
    $cantidadEstacionamientosCrearUnidad = $_POST['cantidadEstacionamientosCrearUnidad'];
    $descripcion1CrearUnidad = $_POST['descripcion1CrearUniad'];
    $descripcion2CrearUnidad = $_POST['descripcion2CrearUniad'];
    $tipologiaCrearUnidad = $_POST['tipologiaCrearUnidad'];
    $m2TotalCrearUnidad = $_POST['m2TotalCrearUnidad'];

    $rutVendedorCrearUnidad = $_POST['rutVendedorCrearUnidad'];
    $rutVendedorCrearUnidad = str_replace(".","",$rutVendedorCrearUnidad);
    $nombresVendedorCrearUnidad = $_POST['nombresVendedorCrearUnidad'];
    $apellidosVendedorCrearUnidad = $_POST['apellidosVendedorCrearUnidad'];
    $celularVendedorCrearUnidad = $_POST['celularVendedorCrearUnidad'];
    $emailVendedorCrearUnidad = $_POST['emailVendedorCrearUnidad'];

    if($pisoCrearUnidad == ''){
      $pisoCrearUnidad = 0;
    };
    if($cantDormitoriosCrearUnidad == ''){
      $cantDormitoriosCrearUnidad = 0;
    };
    if($cantBanosCrearUnidad == ''){
      $cantBanosCrearUnidad = 0;
    };
    if($logiaCrearUnidad == ''){
      $logiaCrearUnidad = 0;
    }
    if($m2UtilCrearUnidad == ''){
      $m2UtilCrearUnidad = 0;
    }
    if($m2TerrazaCrearUnidad == ''){
      $m2TerrazaCrearUnidad = 0;
    }
    if($m2OtroCrearUnidad == ''){
      $m2OtroCrearUnidad = 0;
    }
    if($m2TotalCrearUnidad == ''){
      $m2TotalCrearUnidad = 0;
    }
    if($valorCrearUnidad == ''){
      $valorCrearUnidad = 0;
    }
    if($comisionLVDuenoCrearUnidad == ''){
      $comisionLVDuenoCrearUnidad = 0;
    }
    if($comisionLVClienteCrearUnidad == ''){
      $comisionLVClienteCrearUnidad = 0;
    }
    if($comisionVenDuenoCrearUnidad == ''){
      $comisionVenDuenoCrearUnidad = 0;
    }
    if($comisionVenClienteCrearUnidad == ''){
      $comisionVenClienteCrearUnidad = 0;
    }
    if($cantidadBodegasCrearUnidad == ''){
      $cantidadBodegasCrearUnidad = 0;
    }
    if($cantidadEstacionamientosCrearUnidad == ''){
      $cantidadEstacionamientosCrearUnidad = 0;
    }

    //Imágenes
    $imgPlano = '';
    $imgOtra = '';

    if($codigoProyecto != 'COR'){
      $pathPlano = $_FILES['imgPlanoCrearUnidad']['name'];
      $extPlano = pathinfo($pathPlano, PATHINFO_EXTENSION);
      if($extPlano != ''){
        $imgPlano =  "../view/img/imagenes_cliente/proyectos/" . $codigoProyecto . "/unidad/plano/" . $codigoProyecto  . "_" . $codigoTipoCrearUnidad . "_plano." . $extPlano;
      }
      else{
        $imgPlano = '';
      }

      $pathOtra = $_FILES['imgOtraCrearUnidad']['name'];
      $extOtra = pathinfo($pathOtra, PATHINFO_EXTENSION);
      if($extOtra != ''){
        $imgOtra =  "../view/img/imagenes_cliente/proyectos/" . $codigoProyecto . "/unidad/otra/" . $codigoProyecto . "_" . $codigoTipoCrearUnidad . "_otra." . $extOtra;
      }
      else{
        $imgOtra = '';
      }
    }
    else{
      $pathPlano = $_FILES['imgPlanoCrearUnidad']['name'];
      $extPlano = pathinfo($pathPlano, PATHINFO_EXTENSION);
      if($extPlano != ''){
        $imgPlano =  "../view/img/imagenes_cliente/proyectos/" . $codigoProyecto . "/unidad/plano/" . $codigoProyecto  . "_" . $numeroCrearUnidad . "_plano." . $extPlano;
      }
      else{
        $imgPlano = '';
      }

      $pathOtra = $_FILES['imgOtraCrearUnidad']['name'];
      $extOtra = pathinfo($pathOtra, PATHINFO_EXTENSION);
      if($extOtra != ''){
        $imgOtra =  "../view/img/imagenes_cliente/proyectos/" . $codigoProyecto . "/unidad/otra/" . $codigoProyecto . "_" . $numeroCrearUnidad . "_otra." . $extOtra;
      }
      else{
        $imgOtra = '';
      }
    }

    $row = ingresaUnidad($codigoProyecto,	$tipoCrearUnidad,	$accionCrearUniad,	$numeroCrearUnidad,	$estadoCrearUnidad,	$pisoCrearUnidad,	$orientacionCrearUnidad,	$cantDormitoriosCrearUnidad,	$cantBanosCrearUnidad,	$logiaCrearUnidad,	$m2UtilCrearUnidad,	$m2TerrazaCrearUnidad,	$m2OtroCrearUnidad,	$valorCrearUnidad,	$comisionLVDuenoCrearUnidad,	$comisionLVClienteCrearUnidad,	$comisionVenDuenoCrearUnidad,	$comisionVenClienteCrearUnidad,	$direccionCrearUnidad,	$comunaCrearUnidad,	$codigoTipoCrearUnidad,	$cantidadBodegasCrearUnidad,	$cantidadEstacionamientosCrearUnidad,	$descripcion1CrearUnidad,	$descripcion2CrearUnidad,
  $imgPlano,$imgOtra,$m2TotalCrearUnidad,$tipologiaCrearUnidad, $rutVendedorCrearUnidad, $nombresVendedorCrearUnidad, $apellidosVendedorCrearUnidad, $celularVendedorCrearUnidad, $emailVendedorCrearUnidad);

    if($row == "Ok")
    {
      move_uploaded_file($_FILES['imgPlanoCrearUnidad']['tmp_name'],$imgPlano);
      move_uploaded_file($_FILES['imgOtraCrearUnidad']['tmp_name'],$imgOtra);

      if($codigoProyecto != 'COR'){
        $act = actualizaIMG($codigoProyecto, $codigoTipoCrearUnidad, $imgPlano, $imgOtra);
      }

      echo "Ok";
    }
    else{
      //echo $row;
      echo "Sin datos";
    }
	}
	else{
		echo "Sin datos";
	}
?>
