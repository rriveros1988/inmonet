<?php
	header('Access-Control-Allow-Origin: *');
	// ini_set('display_errors', 'On');
	require('../model/consultas.php');

	if(count($_POST) > 0){
			$codigoProyecto = $_POST['codigoProyecto'];
			$numeroOperacion = $_POST['numeroOperacion'];
			$estadoEscritura = $_POST['estadoEscritura'];
			$proy = consultaProyectoEspecificoCodigo($codigoProyecto);
			// var_dump($proy);
			if($_POST['codigoProyecto'] == 'COR' || ($proy[0]["COMISIONLVESCRITURA"] != '0' && $proy[0]["COMISIONVENESCRITURA"] != '0' && $proy[0]["COMISIONLVESCRITURA"] != null && $proy[0]["COMISIONVENESCRITURA"] != null && $proy[0]["COMISIONLVESCRITURA2"] != '0' && $proy[0]["COMISIONVENESCRITURA2"] != '0' && $proy[0]["COMISIONLVESCRITURA2"] != null && $proy[0]["COMISIONVENESCRITURA2"] != null && $proy[0]['SALTO'] != '0' && $proy[0]['SALTO'] != null)){

	    	$row = cambiarEstadoEscritura($codigoProyecto, $numeroOperacion, $estadoEscritura);

	    	if($row == "Ok")
	    	{
					if($estadoEscritura == '8'){
						liberaUnidadesEscrituraEstacionamiento($codigoProyecto,$numeroOperacion);
				    liberaUnidadesEscrituraBodega($codigoProyecto,$numeroOperacion);
				    liberaUnidadesEscrituradas($codigoProyecto,$numeroOperacion);
						cambiarEstadoPromesa($codigoProyecto, $numeroOperacion, 7);
						liberaUnidadesPromesaEstacionamiento($codigoProyecto,$numeroOperacion);
				    liberaUnidadesPromesaBodega($codigoProyecto,$numeroOperacion);
				    liberaUnidadesPromesadas($codigoProyecto,$numeroOperacion);
						cambiarEstadoReserva($codigoProyecto, $numeroOperacion, 7);
						liberaUnidadesReservaBodega($codigoProyecto,$numeroOperacion);
				    liberaUnidadesReservaEstacionamiento($codigoProyecto,$numeroOperacion);
				    liberaUnidadesReserva($codigoProyecto,$numeroOperacion);
					}
					else if($estadoEscritura == '1'){
						liberaUnidadesEscrituraEstacionamiento($codigoProyecto,$numeroOperacion);
				    liberaUnidadesEscrituraBodega($codigoProyecto,$numeroOperacion);
				    liberaUnidadesEscrituradas($codigoProyecto,$numeroOperacion);
						cambiarEstadoPromesa($codigoProyecto, $numeroOperacion, 1);
						liberaUnidadesPromesaEstacionamiento($codigoProyecto,$numeroOperacion);
				    liberaUnidadesPromesaBodega($codigoProyecto,$numeroOperacion);
				    liberaUnidadesPromesadas($codigoProyecto,$numeroOperacion);
						cambiarEstadoReserva($codigoProyecto, $numeroOperacion, 9);
						liberaUnidadesReservaBodega($codigoProyecto,$numeroOperacion);
				    liberaUnidadesReservaEstacionamiento($codigoProyecto,$numeroOperacion);
				    liberaUnidadesReserva($codigoProyecto,$numeroOperacion);
					}
					If ($estadoEscritura == '2') {
						eliminaComisionEscritura($codigoProyecto, $numeroOperacion);
					}
					if($estadoEscritura == '1' || $estadoEscritura == '3' || $estadoEscritura == '7'){
						$escritura = datosEscrituraParaComision($codigoProyecto, $numeroOperacion);
						$promesa = datosPromesaParaComision($codigoProyecto, $numeroOperacion);
						$idUsuario = $escritura[0]["IDUSUARIO"];

						if(is_null(checkComisionEscritura($codigoProyecto, $numeroOperacion, $idUsuario))){
							$totalUF = $escritura[0]["VALORTOTALUF"];
							$fechaComisionEscritura = new DateTime($escritura[0]['FECHAESCRITURA']);
		      				$fechaComisionEscritura = $fechaComisionEscritura->format('Y-m-d');
							$idProyecto = $proy[0]["IDPROYECTO"];
							$idEscritura = $escritura[0]["IDESCRITURA"];
							$idPromesa = $promesa[0]["IDPROMESA"];
							$IVA = $proy[0]["IVA"]/100;
							$valorAgregado = 1 + $IVA;
							$valorNeto = $totalUF / $valorAgregado;
							$valorNeto = number_format($valorNeto, 2, '.', '');
							if (checkComisionCantidadVentasEscritura($idUsuario, $fechaComisionEscritura, $idProyecto)[0]['VENTAS'] > $proy[0]['SALTO']) {
								$porcentajeComisionEmpresa = $proy[0]["COMISIONLVESCRITURA2"];
								$comisionEmpresaCalculado = $porcentajeComisionEmpresa/100;
								$porcentajeComisionVendedor = $proy[0]["COMISIONVENESCRITURA2"];
								$comisionVendedorCalculado = $porcentajeComisionVendedor/100;

								$comisionesSalto = checkComisionEscrituraParaSaltoDeUnidad($idUsuario, $fechaComisionEscritura);
								for ($i=0; $i < count($comisionesSalto); $i++) {
									$idComisionEscritura = $comisionesSalto[$i]['IDCOMISIONESCRITURA'];
									$montoUFAntiguo = $comisionesSalto[$i]['MONTOUFVENDEDOR'];
									$porcentajeAntiguo = $comisionesSalto[$i]['PORCENTAJECOMISIONVENDEDOR'];
									$montoUFNuevo = ($montoUFAntiguo / $porcentajeAntiguo) * $porcentajeComisionVendedor;
									$montoUFNuevo = number_format($montoUFNuevo,2,'.','');

									actualizarPagosComisionEscrituraSalto($idComisionEscritura, $idUsuario, $montoUFNuevo, $porcentajeComisionVendedor);

								}
							}else{
								$porcentajeComisionEmpresa = $proy[0]["COMISIONLVESCRITURA"];
								$comisionEmpresaCalculado = $porcentajeComisionEmpresa/100;
								$porcentajeComisionVendedor = $proy[0]["COMISIONVENESCRITURA"];
								$comisionVendedorCalculado = $porcentajeComisionVendedor/100;
							}

							$montoUFEmpresa = $valorNeto * $comisionEmpresaCalculado;
							$montoUFEmpresa = number_format($montoUFEmpresa, 2,'.','');
							$montoUFVendedor = $valorNeto * $comisionVendedorCalculado;
							$montoUFVendedor = number_format($montoUFVendedor,2,'.','');
							if($_POST['codigoProyecto'] != 'COR'){
								insertarComisionEscritura($idUsuario, $fechaComisionEscritura, $idProyecto, $idEscritura, $idPromesa, $montoUFEmpresa, $montoUFVendedor, $porcentajeComisionEmpresa, $porcentajeComisionVendedor)->query("COMMIT");
							}
						}else{
							echo "Existente";
						}
					}
					echo "Ok";
				}
				else{
					echo "Sin datos";
				}
			}
			else{
				echo "sincomisiones";
			}
		}
		else{
    		echo "Sin datos";
  	}
?>
