<?php
//ini_set('display_errors', 'On');
require '../html2pdf/vendor/autoload.php';
date_default_timezone_set("America/Santiago");
session_start();

function pago($tasa, $monto, $meses){
  $I = $tasa / 12 / 100 ;
  $I2 = $I + 1 ;
  $I2 = pow($I2,-$meses) ;

  $CuotaMensual = ($I * $monto) / (1 - $I2);

  return $CuotaMensual;
}

use Spipu\Html2Pdf\Html2Pdf;

ob_start();
if($_SESSION['codProyectoClienteCotizacion'] == "COR"){
  require_once 'cotizacion_plantilla_pdf_corretaje.php';
}
else{
  require_once 'cotizacion_plantilla_pdf.php';
}
$html = ob_get_clean();

//$document = '/var/www/html/Git/inmonet';
$document = '/home/livingne/inmonet.cl';

$html2pdf = new Html2Pdf('P','LETTER','es','true','UTF-8');
$html2pdf->writeHTML($html);
//$html2pdf->output($document . '/tmp/cotiza.pdf', 'F');
$html2pdf->output();

/*
$desc = fopen("../../tmp/cotiza.pdf", "w+");
fwrite($desc, $documento);
fclose($desc);
*/
?>
