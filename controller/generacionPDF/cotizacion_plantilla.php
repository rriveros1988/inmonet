<style type="text/css">
.headTabla{
  background-color: #e6f2ff;
  border: 1px solid black;
  padding: 2px;
}
.bodyTabla{
  border: 1px solid black;
  padding: 1px;
}
label{
  font-size: 12px;
}
input{
  font-size: 12px;
}
</style>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
  <label>Fecha:</label>
  <input disabled value=<?php session_start(); echo '"' . $_SESSION['fechaCotizacion'] . '"' ?> class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
  <label>Cotización válida por:</label>
  <input disabled  value=<?php echo '"' . $_SESSION['diasCotProyectoCotizacion']  . ' días"'; ?> class="form-control"></input><!-- Pendiente -->
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <label>Nombre cliente:</label>
  <input disabled value=<?php echo '"' . $_SESSION['nombreClienteCotizacion'] . '"'; ?> class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <label>Apellido cliente:</label>
  <input disabled value=<?php echo '"' . $_SESSION['apellidoClienteCotizacion'] . '"'; ?> class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
  <label>Rut:</label>
  <input disabled value=<?php echo '"' . $_SESSION['rutClienteCotizacion'] . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
  <label>Celular:</label>
  <input disabled value=<?php echo '"' . $_SESSION['celularClienteCotizacion'] . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
  <label>Email:</label>
  <input disabled value=<?php echo '"' . $_SESSION['mailClienteCotizacion'] . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label>Departamento:</label>
  <input disabled value=<?php echo '"' . $_SESSION['departaentoClienteCotizacion'] . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label>Tipología:</label>
  <input disabled value=<?php echo '"' . $_SESSION['tipologiaClienteCotizacion'] . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label>Modelo:</label>
  <input disabled value=<?php echo '"' . $_SESSION['modeloClienteCotizacion'] . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label>Orientación:</label>
  <input disabled value=<?php echo '"' . $_SESSION['orientacionClienteCotizacion'] . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label>Utiles:</label>
  <input disabled value=<?php echo '"' . number_format($_SESSION['m2UtilesClienteCotizacion'],2,',','.')  . ' Mt2"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label>Terraza:</label>
  <input disabled value=<?php echo '"' . number_format($_SESSION['m2TerrazaClienteCotizacion'],2,',','.')  . ' Mt2"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label>Total:</label>
  <input disabled value=<?php echo '"' . number_format($_SESSION['m2TotalClienteCotizacion'],2,',','.')  . ' Mt2"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label>Unidad de estacionamiento:</label>
  <input disabled value=<?php echo '"' . $_SESSION['estacionamientosClienteCotizacion']  . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label>Unidad de bodega:</label>
  <input disabled value=<?php echo '"' . $_SESSION['bodegasClienteCotizacion']  . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <hr style="background-color: #c1c1c1; height: 1px;" />
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label>Departamento:</label>
  <input disabled value=<?php echo '"UF ' . number_format($_SESSION['departamentoUfClienteCotizacion'], 2, ',', '.')  . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php echo '"$ ' . number_format(($_SESSION['departamentoUfClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label>Bodega:</label>
  <input disabled value=<?php
    if($_SESSION['accionClienteCotizacion'] != "Leasing"){
      echo '"UF ' . number_format($_SESSION['bodegaUfClienteCotizacion'], 2, ',', '.')  . '"';
    }
    else{
      echo '"UF ' . number_format(0, 2, ',', '.')  . '"';
    }
  ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=

  <?php
  if($_SESSION['accionClienteCotizacion'] != "Leasing"){
    echo '"$ ' . number_format(($_SESSION['bodegaUfClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.')  . '"';
  }
  else{
    echo '"$ ' . number_format(0, 0, '.', '.')  . '"';
  }
  ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label>Estacionamiento:</label>
  <input disabled value=
  <?php
  if($_SESSION['accionClienteCotizacion'] != "Leasing"){
    echo '"UF ' . number_format($_SESSION['estacionamientoUfClienteCotizacion'], 2, ',', '.')  . '"';
  }
  else{
    echo '"UF ' . number_format(0, 2, ',', '.')  . '"';
  }
  ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=
  <?php
  if($_SESSION['accionClienteCotizacion'] != "Leasing"){
    echo '"$ ' . number_format(($_SESSION['estacionamientoUfClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.')  . '"';
  }
  else{
    echo '"$ ' . number_format(0, 0, '.', '.')  . '"';
  }
  ?>  class="form-control"></input>
</div>
<!-- Espacio -->
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 hidden-sm hidden-xs">
  &nbsp;
  <br/>
  <br/>
  &nbsp;
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 hidden-sm hidden-xs">
  &nbsp;
  <br/>
  <br/>
  &nbsp;
</div>
<!-- fin espacio -->
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label>Desc. sala:</label>
  <input disabled value=<?php
  if($_SESSION['accionClienteCotizacion'] != "Leasing"){
    echo '"UF ' . number_format(($_SESSION['descuento1ClienteCotizacion']*$_SESSION['departamentoUfClienteCotizacion']/100), 2, ',', '.')  . '"';
  }
  else{
    echo '"UF ' . number_format(0, 2, ',', '.')  . '"';
  }
  ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php
  if($_SESSION['accionClienteCotizacion'] != "Leasing"){
    echo '"$ ' . number_format(number_format(($_SESSION['descuento1ClienteCotizacion']*$_SESSION['departamentoUfClienteCotizacion']/100), 2, ',', '')*$_SESSION['ufClienteCotizacion'], 0, '.', '.')  . '"';
  }
  else{
    echo '"$ ' . number_format(0, 0, ',', '.')  . '"';
  }
  ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php
  if($_SESSION['accionClienteCotizacion'] != "Leasing"){
    echo '"' . number_format($_SESSION['descuento1ClienteCotizacion'], 2, '.', '')  . ' %"';
  }
  else{
    echo '"' . number_format(0, 2, '.', '')  . ' %"';
  }
  ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label>Desc. especial:</label>
  <input disabled value=<?php
   if($_SESSION['accionClienteCotizacion'] != "Leasing"){
     echo '"UF ' .  number_format(($_SESSION['descuento2ClienteCotizacion']*$_SESSION['total2UFClienteCotizacion']/100), 2, ',', '.') . '"';
   }
   else{
     echo '"UF ' .  number_format(0, 2, ',', '.') . '"';
   }
   ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php
   if($_SESSION['accionClienteCotizacion'] != "Leasing"){
     echo '"$ ' . number_format(number_format(($_SESSION['descuento2ClienteCotizacion']*$_SESSION['total2UFClienteCotizacion']/100), 2, ',', '')*$_SESSION['ufClienteCotizacion'], 0, '.', '.')  . '"';
   }
   else{
     echo '"$ ' . number_format(0, 0, '.', '.')  . '"';
   }
   ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php
  if($_SESSION['accionClienteCotizacion'] != "Leasing"){
    echo '"' . number_format($_SESSION['descuento2ClienteCotizacion'], 2, '.', '')  . ' %"';
  }
  else{
    echo '"' . number_format(0, 2, '.', '')  . ' %"';
  }
  ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label>Total:</label>
  <input disabled value=<?php
      if($_SESSION['accionClienteCotizacion'] != "Venta"){
        echo '"UF ' . number_format($_SESSION['departamentoUfClienteCotizacion'], 2, ',', '.')  . '"';
      }
      else{
        echo '"UF ' . number_format($_SESSION['totalFUFClienteCotizacion'], 2, ',', '.')  . '"';
      }
   ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php
  if($_SESSION['accionClienteCotizacion'] != "Venta"){
    echo '"$ ' . number_format(($_SESSION['departamentoUfClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.')  . '"';
  }
  else{
    echo '"$ ' . number_format(($_SESSION['totalFUFClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.')  . '"';
  }
  ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 20pt;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <font style="font-weight: bold;">Formas de pago</font>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label>Reserva:</label>
  <input disabled value=<?php echo '"UF ' . number_format($_SESSION['reservaClienteCotizacion'], 2, ',', '.')  . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php echo '"$ ' . number_format(($_SESSION['reservaClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php echo '"' . number_format((($_SESSION['reservaClienteCotizacion']/$_SESSION['totalFUFClienteCotizacion'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label>Pie promesa:</label>
  <input disabled value=<?php echo '"UF ' .  number_format($_SESSION['piePromesaClienteCotizacion'], 2, ',', '.') . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php echo '"$ ' . number_format(($_SESSION['piePromesaClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php echo '"' . number_format((($_SESSION['piePromesaClienteCotizacion']/$_SESSION['totalFUFClienteCotizacion'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label>Pie cuotas:</label>
  <input disabled value=<?php echo '"UF ' . number_format($_SESSION['pieCuotasClienteCotizacion'], 2, ',', '.') . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php echo '"$ ' . number_format(($_SESSION['pieCuotasClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php echo '"' . number_format((($_SESSION['pieCuotasClienteCotizacion']/$_SESSION['totalFUFClienteCotizacion'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label>Cuotas:</label>
  <input disabled value=<?php echo '"' . $_SESSION['cuotasClienteCotizacion']  . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php
  if($_SESSION['cuotasClienteCotizacion'] == '0'){
    echo '"UF ' . number_format(0, 2, ',', '.')   . '"';
  }
  else{
    echo '"UF ' . number_format($_SESSION['pieCuotasClienteCotizacion']/$_SESSION['cuotasClienteCotizacion'], 2, ',', '.')   . '"';
  }
  ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label>Saldo:</label>
  <input disabled value=<?php echo '"UF ' . number_format($_SESSION['saldoClienteCotizacion'], 2, ',', '.') . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php echo '"$ ' . number_format(($_SESSION['saldoClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php echo '"' . number_format((($_SESSION['saldoClienteCotizacion']/$_SESSION['totalFUFClienteCotizacion'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label>Total:</label>
  <input disabled value=<?php echo '"UF ' . number_format($_SESSION['totalFUFClienteCotizacion'], 2, ',', '.') . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php echo '"$ ' . number_format(($_SESSION['totalFUFClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label class="hidden-sm hidden-xs">&nbsp;</label>
  <input disabled value=<?php echo '"' . number_format((($_SESSION['totalFUFClienteCotizacion']/$_SESSION['totalFUFClienteCotizacion'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 20pt;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <?php
   echo 'Precios Montos en pesos $CH a UF ' . number_format($_SESSION['ufClienteCotizacion'], 2, ',', '.') . ' corresponden como referencia al valor UF al ' . $_SESSION['fechaCotizacion'];

  ?>
</div>
