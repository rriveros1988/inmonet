<?php
  header('Access-Control-Allow-Origin: *');
  //ini_set('display_errors', 'On');
  require('../model/consultas.php');

  session_start();

if(count($_SESSION) > 0 && count($_POST) > 0){
    $idReserva = $_SESSION['idReserva'];
    $idProyecto = $_SESSION['idProyecto'];
    $idUnidad = $_SESSION['idUnidad'];
    $idCliente1 = $_SESSION['idCliente1'];
    $idCliente2 = 0; //Pendiente
    $_SESSION['idUsuario'] = $_POST['idUsuario'];
    $idUsuario = $_SESSION['idUsuario'];
    $numeroOperacion = $_SESSION['promesaNumeroReserva'];
    $fecha = new DateTime($_POST['fechaPromesa']);
    $fechaPromesa = $fecha->format("Y-m-d");
    $valorBrutoUF = $_SESSION['promesaValorBrutoUF'];
    $descuento1 = $_SESSION['promesaDescuentoSala'];
    $descuento2 = $_SESSION['promesaDescuentoEspecial'];
    $bonoVentaUF = $_SESSION['promesaBono'];
    $valorTotalUF = $_SESSION['promesaTotalUF'];
    $valorReservaUF = $_POST['valorReservaUF'];
    $valorReserva = $_POST['valorPagoReserva'];
    $valorPiePromesaUF = $_POST['valorPiePromesaUF'];
    $valorPieSaldoUF = $_POST['valorPieSaldoUF'];
    $cantidadCuotasPie = $_POST['cantidadCuotasPie'];
    $valorSaldoTotalUF = $_POST['valorSaldoTotalUF'];
    $fecha2 =  new DateTime($_POST['fechaPagoPromesa']);
    $fechaPagoPromesa = $fecha2->format("Y-m-d");
    $valorPagoPromesa = $_POST['valorPagoPromesa'];
    $formaPagoPromesa = $_POST['formaPagoPromesa'];
    $bancoPromesa = $_POST['bancoPromesa'];
    $serieChequePromesa = $_POST['serieChequePromesa'];
    $nroChequePromesa = $_POST['nroChequePromesa'];
    $valorPiePromesa = $_POST['valorPiePromesa'];
    $valorPieSaldo = $_POST['valorPieSaldo'];
    $codigoProyecto = $_SESSION['promesaCodigoProyecto'];

    if(array_key_exists('promesaIdVendedor', $_SESSION)){
      $vendedor = chequeaUsuarioID($_SESSION['promesaIdVendedor']);
      $_SESSION['promesaVendedor'] = $vendedor[0]['NOMBRES'] + ' ' + $vendedor[0]['APELLIDOS'];
      $_SESSION['promesaVendedorRut'] = $vendedor[0]['RUT'];
    }
    else{
      $_SESSION['promesaVendedor'] = $_SESSION['nombreUser'];
      $_SESSION['promesaVendedorRut'] = $_SESSION['rutUser'];
    }

    /*
    if($_SESSION['codProyectoClienteCotizacion'] == "COR"){
      $descuento1UF = 0;
      $descuento2UF = 0;
      $valorPromesaUF = 0;
      $valorPiePromesaUF = 0;
      $valorPieSaldoUF = 0;
      $cantidadCuotasPie = 0;
      $valorSaldoTotalUF = 0;
    }else{
      $descuento1UF = $_SESSION['descuento1ClienteCotizacion'];
      $descuento2UF = $_SESSION['descuento2ClienteCotizacion'];
      $valorPromesaUF = $_SESSION['promesaClienteCotizacion'];
      $valorPiePromesaUF = $_SESSION['piePromesaClienteCotizacion'];
      $valorPieSaldoUF = $_SESSION['pieCuotasClienteCotizacion'];
      $cantidadCuotasPie = $_SESSION['cuotasClienteCotizacion'];
      $valorSaldoTotalUF = $_SESSION['saldoClienteCotizacion'];
      $bonoVenta = $_SESSION['bonoVentaCrearCotizacion'];
    }
    */

    liberaUnidadesReservaEstacionamiento($codigoProyecto,$numeroOperacion);
    liberaUnidadesReservaBodega($codigoProyecto,$numeroOperacion);
    //liberaUnidadesReserva($codigoProyecto,$numeroOperacion);
    eliminaReservaBodega($codigoProyecto, $numeroOperacion);
    eliminaReservaEstacionamiento($codigoProyecto, $numeroOperacion);

    $row = ingresaPromesa($idReserva, $idProyecto, $idUnidad, $idCliente1, $idCliente2, $idUsuario,$numeroOperacion, $fechaPromesa, $valorBrutoUF, $descuento1, $descuento2, $bonoVentaUF, $valorTotalUF, $valorReservaUF, $valorPiePromesaUF, $valorPieSaldoUF, $cantidadCuotasPie, $valorSaldoTotalUF, $fechaPagoPromesa, $valorPagoPromesa, $formaPagoPromesa, $bancoPromesa, $serieChequePromesa, $nroChequePromesa, $valorPiePromesa, $valorPieSaldo, $valorReserva);

    if($row != "Error")
    {
      $_SESSION['idPromesa'] = $row->insert_id;

      actualizaEstadoUnidad($idUnidad, 4, $row);
      //actualizaInteresCotizacion($idCotizacion, $row);

      $in = 'Ok';
      $in2 = 'Ok';

      if($_POST['bodegasClientePromesa'] != ''){

        for($i = 0; $i < count($_POST['bodegasClientePromesa']); $i++){
          ingresaReservaBodega($_SESSION['idReserva'],$_POST['bodegasClientePromesa'][$i], $row);
          $in = ingresaPromesaBodega($_SESSION['idPromesa'],$_POST['bodegasClientePromesa'][$i], $row);
          if($in == "Error"){
            break;
          }
          else{
            actualizaEstadoUnidad($_POST['bodegasClientePromesa'][$i], 4, $row);
          }
        }
      }

      if(is_array($_POST['estacionamientosClientePromesa'])){

        for($i = 0; $i < count($_POST['estacionamientosClientePromesa']); $i++){
          ingresaReservaEstacionamiento($_SESSION['idReserva'],$_POST['estacionamientosClientePromesa'][$i], $row);
          $in2 = ingresaPromesaEstacionamiento($_SESSION['idPromesa'],$_POST['estacionamientosClientePromesa'][$i], $row);
          if($in2 == "Error"){
            break;
          }
          else{
            actualizaEstadoUnidad($_POST['estacionamientosClientePromesa'][$i], 4, $row);
          }
        }
      }

      $ingreso_cuotas = "Ok";

      if(array_key_exists('cuotas_promesa', $_POST) && count($_POST["cuotas_promesa"]) > 0){
        $cuotasPromesa = $_POST["cuotas_promesa"];

        $_SESSION['promesaFormaPagoCuota'] = array();

        $_SESSION['cuotasPromesa'] = $cuotasPromesa;

        foreach ($cuotasPromesa as $cuota) {
            $numero_cuota = $cuota['numero_cuota'];
            $uf_cuota = $cuota['uf_cuota'];
            $monto_cuota = $cuota['monto_cuota'];
            $forma_cuota = $cuota['forma_cuota'];
            array_push($_SESSION['promesaFormaPagoCuota'], consultaFormaPagoReservaEspecifica($forma_cuota));
            $banco_cuota = $cuota['banco_cuota'];
            $serie_cuota = $cuota['serie_cuota'];
            $nro_cuota = $cuota['nro_cuota'];
            $fecha_cuota = new DateTime($cuota['fecha_cuota']);
            $fecha_cuota = $fecha_cuota->format("Y-m-d");
            $ingreso_cuotas = ingresaPromesaCuotas($_SESSION['idPromesa'], $numero_cuota, $uf_cuota, $monto_cuota, $forma_cuota, $banco_cuota, $serie_cuota, $nro_cuota, $fecha_cuota,$row);
            if($ingreso_cuotas == "Error")
            {
              $ingreso_cuotas->query("ROLLBACK");
              break;
              echo "Sin datos";
            }
        }
      }

      if($in != "Error" && $in2 != "Error" && $ingreso_cuotas != "Error"){
        $row->query("COMMIT");
        $numCot = numeroOperacion($_SESSION['idReserva']);
        $_SESSION['numeroOperacion'] = $numCot['NUMERO'];

        //Datos a session
        $fechaPromesa = $fecha->format("d-m-Y");
        $fechaPagoPromesa = $fecha2->format("d-m-Y");
        $_SESSION['promesaFecha'] = $fechaPromesa;
        $_SESSION['promesaReservaUF'] = $valorReservaUF;
        $_SESSION['promesaPiePromesaUF'] = $valorPiePromesaUF;
        $_SESSION['promesaPieSaldoUF'] = $valorPieSaldoUF;
        $_SESSION['promesaCuotasPie'] = $cantidadCuotasPie;
        $_SESSION['promesaSaldoTotalUF'] = $valorSaldoTotalUF;
        $_SESSION['promesaFechaPagoPromesa'] = $fechaPagoPromesa;
        $_SESSION['promesaValorPagoPromesa'] = $valorPagoPromesa;
        $_SESSION['promesaFormaPagoPromesa'] = consultaFormaPagoReservaEspecifica($formaPagoPromesa);
        $_SESSION['promesaBancoPromesa'] = $bancoPromesa;
        $_SESSION['promesaSerieChequePromesa'] = $serieChequePromesa;
        $_SESSION['promesaNroChequePromesa'] = $nroChequePromesa;
        $_SESSION['promesaValorPiePromesa'] = $valorPiePromesa;
        $_SESSION['promesaValorPieSaldo'] = $valorPieSaldo;
        $_SESSION['promesaPieCuotas'] = $valorPieSaldoUF;
        $_SESSION['valorPieSaldo'] = $valorPieSaldo;
        $_SESSION['valorPiePromesa'] = $valorPiePromesa;

        $cliente1 = consultaClienteEspecifico($_SESSION['idCliente1']);

        $fechaC1 = new DateTime($cliente1[0]['FECHANAC']);
        $fechaNacCliente1 = $fechaC1->format("d-m-Y");

        $_SESSION['promesaFechaNacCliente1'] = $fechaNacCliente1;
        $_SESSION['promesaEstadoCivilIDCliente1'] = $cliente1[0]['ESTADOCIVIL'];
        $_SESSION['promesaProfesionCliente1'] = $cliente1[0]['PROFESION'];
        if ($cliente1[0]['TIPODOMICILIO'] != '') {
        $_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'] . ', ' . $cliente1[0]['TIPODOMICILIO'];
        }else{
          $_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'];
        }

        $_SESSION['promesaComunaCliente1'] = $cliente1[0]['COMUNA'];
        $_SESSION['promesaTelefonoCliente1'] = $cliente1[0]['CELULAR'];
        $_SESSION['promesaMailCliente1'] = $cliente1[0]['EMAIL'];
        $_SESSION['promesaRentaCliente1'] = $cliente1[0]['EM_RENTALIQUIDA'];
        $_SESSION['promesaCiudadCliente1'] = $cliente1[0]['CIUDAD'];
        $_SESSION['promesaInstitucionCliente1'] = $cliente1[0]['INSTITUCION'];
        $_SESSION['promesaRegionCliente1'] = $cliente1[0]['REGION'];
        $_SESSION['promesaActividadCliente1'] = $cliente1[0]['ACTIVIDAD'];
        $_SESSION['promesaPaisCliente1'] = $cliente1[0]['PAIS'];
        $_SESSION['promesaNacionalidadCliente1'] = $cliente1[0]['NACIONALIDAD'];
        $_SESSION['promesaResidenciaCliente1'] = $cliente1[0]['RESIDENCIA'];
        $_SESSION['promesaSexoCliente1'] = $cliente1[0]['SEXO'];
        $_SESSION['promesaFechaNacCliente1'] = $cliente1[0]['FECHANAC'];
        $_SESSION['promesaNivelEducCliente1'] = $cliente1[0]['NIVELEDUCACIONAL'];
        $_SESSION['promesaCasaHabitaCliente1'] = $cliente1[0]['CASAHABITA'];
        $_SESSION['promesaMotivoCompraCliente1'] = $cliente1[0]['MOTIVOCOMPRA'];

        $proyecto = consultaProyectoEspecifico($_SESSION['idProyecto']);

        $_SESSION['promesaInmobiliaria'] = $proyecto[0]['INMOBILIARIA'];
        $_SESSION['promesaInmobiliariaDireccion'] = $proyecto[0]['DIRECCION'];

        $_SESSION['estacionamientosClientePromesa'] = $_POST['estacionamientosClientePromesa'];
        $_SESSION['bodegasClientePromesa'] = $_POST['bodegasClientePromesa'];

        //Actualiza datos de cliente en session_start
        $_SESSION['promesaNombreCliente'] = $cliente1[0]['NOMBRES'];
        $_SESSION['promesaApellidoCliente'] = $cliente1[0]['APELLIDOS'];
        if ($codigoProyecto == "COR") {
          $_SESSION['MONTOUFCOMISION'] = '&nbsp;&nbsp;';
          $_SESSION['MONTOPESOCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
          $_SESSION['FORMAPAGOCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
          $_SESSION['BANCOCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
          $_SESSION['SERIECOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
          $_SESSION['NROCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
          $_SESSION['FECHAPAGOCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';

          if($_SESSION["promesaAccion"] == "Arriendo"){
            $montoUF = $_POST['garantia_uf'];
            $montoPesos = $_POST['pesos_uf'];
            $banco = $_POST['garantia_banco'];
            $serie = $_POST['garantia_serie'];
            $nro = $_POST['garantia_numero'];
            $formaPago = $_POST['garantia_formaPago'];
            if($_POST['garantia_fechaCheque'] == ''){
              $fechaPago2 = '';
            }
            else{
              $fechaPago = new DateTime($_POST['garantia_fechaCheque']);
              $fechaPago2 = $fechaPago->format("Y-m-d");
            }

            ingresaGarantia($_SESSION['idPromesa'],$montoUF,$montoPesos,$formaPago,$banco,$serie,$nro,$fechaPago2, $_SESSION["promesaAccion"], "Cliente");
          }else if($_SESSION["promesaAccion"] == "Venta"){
            $montoUF = $_POST['garantia_ufDueno'];
            $montoPesos = $_POST['pesos_ufDueno'];
            $banco = $_POST['garantia_bancoDueno'];
            $serie = $_POST['garantia_serieDueno'];
            $nro = $_POST['garantia_numeroDueno'];
            $formaPago = $_POST['garantia_formaPagoDueno'];
            if($_POST['garantia_fechaChequeDueno'] == ''){
              $fechaPago2 = '';
            }
            else{
              $fechaPago = new DateTime($_POST['garantia_fechaChequeDueno']);
              $fechaPago2 = $fechaPago->format("Y-m-d");
            }

            ingresaGarantia($_SESSION['idPromesa'],$montoUF,$montoPesos,$formaPago,$banco,$serie,$nro,$fechaPago2, $_SESSION["promesaAccion"], "Dueno");

            $montoUF = $_POST['garantia_ufCliente'];
            $montoPesos = $_POST['pesos_ufCliente'];
            $banco = $_POST['garantia_bancoCliente'];
            $serie = $_POST['garantia_serieCliente'];
            $nro = $_POST['garantia_numeroCliente'];
            $formaPago = $_POST['garantia_formaPagoCliente'];
            if($_POST['garantia_fechaChequeCliente'] == ''){
              $fechaPago2 = '';
            }
            else{
              $fechaPago = new DateTime($_POST['garantia_fechaChequeCliente']);
              $fechaPago2 = $fechaPago->format("Y-m-d");
            }

            ingresaGarantia($_SESSION['idPromesa'],$montoUF,$montoPesos,$formaPago,$banco,$serie,$nro,$fechaPago2, $_SESSION["promesaAccion"], "Cliente");
          }
        }
        //OBSERVACION

        //VENDEDOR
        $_SESSION['promesaDatosVendedor'] = promesaDatosVendedor($idUsuario);

        //Cambia estado de reserva a promesada
        cambiarEstadoReserva($codigoProyecto, $numeroOperacion, 6);

        function generateRandomString($length = 8) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        $usuario = checkUsuarioSinPass($cliente1[0]['RUT']);

        // if(is_null($usuario)){
          // $passUsuarioCliente = generateRandomString();
          //
          // ingresaUsuario($cliente1[0]['RUT'],$cliente1[0]['APELLIDOS'],$cliente1[0]['NOMBRES'],$cliente1[0]['EMAIL'],$cliente1[0]['CELULAR'],'4',md5($passUsuarioCliente));

          //instancio un objeto de la clase PHPMailer
          // $mail = new PHPMailer(); // defaults to using php "mail()"
          //
          // //Codificacion
          // $mail->CharSet = 'UTF-8';
          //
          // //indico a la clase que use SMTP
          // $mail->IsSMTP();
          // //Debo de hacer autenticación SMTP
          // $mail->SMTPAuth = true;
          // //indico el servidor SMTP
          // $mail->Host = "mail.inmonet.cl";
          // //indico el puerto que usa Gmail
          // $mail->Port = 465;
          // //indico un usuario / clave de un usuario
          // $mail->Username = "no-reply@inmonet.cl";
          // $mail->Password = "inmonet2018";
          //
          // $mail->SMTPSecure = 'ssl';
          //
          // $mail->SMTPOptions = array(
    	    //     'ssl' => array(
    	    //         'verify_peer' => false,
    	    //         'verify_peer_name' => false,
    	    //         'allow_self_signed' => true
    	    //     )
    	    // );
          //
          // $firma = "LivingNet
          //           <br />
          //           Gestión inmobiliaria
          //           <br />
          //           ..........................................................................................................................................................................
          //           <br>
          //           <br>
          //           AVISO LEGAL.
          //           <br>
          //           <font style='margin-top: 0; line-height: 15px;font-family: Arial;font-size:7.5pt; text-align: justify; width: 100%'>
          //           Este mensaje y sus documentos anexos pueden contener información confidencial o legalmente protegida. Está dirigido única y exclusivamente a la persona o entidad reseñada como destinatarios del mensaje. Si este mensaje le hubiera llegado por error, por favor elimínelo sin revisarlo ni reenviarlo y notifíquelo lo antes posible al remitente. Cualquier divulgación, copia o utilización de dicha información es contraria a la ley. Le agradecemos su colaboración.
          //           </font>
          //           <br>";
          //
          // //$mail->AddEmbeddedImage('../view/img/logos/living_logo_mail.png', 'firmaPng', 'firmaPng.png');
          //
          // $body = "<div style='width: 100%; text-align: justify; margin: 0 auto;'>
  		    // <font style='font-size: 14px;'>
  		    // Estimado " . $cliente1[0]['NOMBRES'] . " " . $cliente1[0]['APELLIDOS'] . ",
  		    // <br />
  		    // <br />
  		    // Le informamos que se ha creado una cuenta a su nombre en el sistema InmoNet Gestión Inmobiliaria.<br /><br />
          // A continuación le indicamos sus credenciales de acceso:<br /><br />
          // URL: <a href='https://www.inmonet.cl'>www.inmonet.cl</a>
  		    // <br />
          // Usuario: " . $cliente1[0]['RUT'] . "
          // <br />
          // Contraseña: " . $passUsuarioCliente . "
  		    // <br />
          // <br />
  		    // </font>
  		    // <div'>
  		    //     <font style='font-size: 14px;'>
  		    //         Saludos cordiales.
  		    //     </font>
  		    //     <br />
  		    //     <br />
  		    //     " . $firma . "
  		    // </div>
  		    // ";
          //
          // $mail->SetFrom('no-reply@inmonet.cl', "Alertas InmoNet");
          //
  		    // //defino la dirección de email de "reply", a la que responder los mensajes
  		    // //Obs: es bueno dejar la misma dirección que el From, para no caer en spam
  		    // $mail->AddReplyTo('no-reply@inmonet.cl', "Alertas InmoNet");
  		    // //Defino la dirección de correo a la que se envía el mensaje
          //
  		    // $listaMails = array($cliente1[0]['EMAIL']);
          //
          // //Agregamos destinatarios
  		    // for($i = 0; $i < count($listaMails); $i++){
  		    //     $mail->AddAddress($listaMails[$i], $listaMails[$i]);
  		    // }
          //
          // $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
      		// $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
          //
  		    // $fecha = strtotime('+0 day');
      		// $fecha = $dias[date('w', $fecha)]." ".date('d', $fecha)." de ".$meses[date('n', $fecha)-1]. " ".date('Y', $fecha) . " a las " . date('h:i:s A', $fecha);
          //
          // $mail->Subject = "Creación de cuenta " . $fecha . "";
          //
  		    // //Puedo definir un cuerpo alternativo del mensaje, que contenga solo texto
  		    // $mail->AltBody = "Creación de cuenta " . $fecha . "";
          //
  		    // //inserto el texto del mensaje en formato HTML
  		    // $mail->MsgHTML($body);
          //
          // $mail->Send();
        // }


        echo "Ok¬".$_SESSION['promesaCodigoProyecto']."¬".$_SESSION['numeroOperacion'];
      }
      else{
        $row->query("ROLLBACK");
        echo "Sin datos";
      }
    }
  	else{
  		echo "Sin datos";
  	}
	}
	else{
		echo "Sin datos";
	}
?>
