<?php
  header('Access-Control-Allow-Origin: *');
  ini_set('display_errors', 'On');
  require('../model/consultas.php');
  session_start();

if(count($_SESSION) > 0 && count($_POST) > 0){
    $codigoProyecto = $_SESSION['codigoProyecto'];
    $numeroOperacion = $_SESSION['numeroOperacion'];
    $valorPiePromesaUF = $_POST['valorPiePromesaUF'];
    $valorPieSaldoUF = $_POST['valorPieSaldoUF'];
    $cantidadCuotasPie = $_POST['cantidadCuotasPie'];
    $valorSaldoTotalUF = $_POST['valorSaldoTotalUF'];
    $fecha2 =  new DateTime($_POST['fechaPagoPromesa']);
    $fechaPagoPromesa = $fecha2->format("Y-m-d");
    $valorPagoPromesa = $_POST['valorPagoPromesa'];
    $formaPagoPromesa = $_POST['formaPagoPromesa'];
    $bancoPromesa = $_POST['bancoPromesa'];
    $serieChequePromesa = $_POST['serieChequePromesa'];
    $nroChequePromesa = $_POST['nroChequePromesa'];
    $valorPiePromesa = $_POST['valorPiePromesa'];
    $valorPieSaldo = $_POST['valorPieSaldo'];
    $valorTotalUF = $_POST['promesaTotalUF'];
    $valorReservaUF = $_POST['valorReservaUF'];
    $usuario = chequeaUsuario($_SESSION['rutUser']);

    $idUsuario = $usuario['IDUSUARIO'];
    // $unidades_liberar = $_POST['unidades_liberar'];
    // $bodegas_borrar = $_POST['bodegas_borrar'];
    // $estacionamientos_borrar = $_POST['estacionamientos_borrar'];
    /*
    if($_SESSION['codProyectoClienteCotizacion'] == "COR"){
      $descuento1UF = 0;
      $descuento2UF = 0;
      $valorReservaUF = 0;
      $valorPiePromesaUF = 0;
      $valorPieSaldoUF = 0;
      $cantidadCuotasPie = 0;
      $valorSaldoTotalUF = 0;
    }else{
      $descuento1UF = $_SESSION['descuento1ClienteCotizacion'];
      $descuento2UF = $_SESSION['descuento2ClienteCotizacion'];
      $valorReservaUF = $_SESSION['reservaClienteCotizacion'];
      $valorPiePromesaUF = $_SESSION['piePromesaClienteCotizacion'];
      $valorPieSaldoUF = $_SESSION['pieCuotasClienteCotizacion'];
      $cantidadCuotasPie = $_SESSION['cuotasClienteCotizacion'];
      $valorSaldoTotalUF = $_SESSION['saldoClienteCotizacion'];
      $bonoVenta = $_SESSION['bonoVentaCrearCotizacion'];
    }
    */

    liberaUnidadesPromesaEstacionamiento($codigoProyecto,$numeroOperacion);
    liberaUnidadesPromesadas($codigoProyecto,$numeroOperacion);
    eliminaPromesaBodega($codigoProyecto, $numeroOperacion);
    eliminaPromesaEstacionamiento($codigoProyecto, $numeroOperacion);
    eliminaPromesaCuotas($codigoProyecto,$numeroOperacion);

    $row = actualizarPromesa($codigoProyecto,$numeroOperacion, $valorTotalUF, $valorPiePromesaUF, $valorPieSaldoUF, $cantidadCuotasPie, $valorSaldoTotalUF, $fechaPagoPromesa, $valorPagoPromesa, $formaPagoPromesa, $bancoPromesa, $serieChequePromesa, $nroChequePromesa, $valorPiePromesa, $valorPieSaldo);

    $consultaPromesa = consultaPromesaEspecifica($codigoProyecto, $numeroOperacion);
    $idProyecto = $consultaPromesa[0]['IDPROYECTO'];
    $idPromesa = $consultaPromesa[0]['IDPROMESA'];
    $unidad = consultaUnidadEspecifica($consultaPromesa[0]['IDUNIDAD']);
    $cliente1 = consultaClienteEspecifico($consultaPromesa[0]['IDCLIENTE1']);

    if($row != "Error")
    {

      $in = 'Ok';
      $in2 = 'Ok';

      if($_POST['bodegasClientePromesa'] != ''){
        for($i = 0; $i < count($_POST['bodegasClientePromesa']); $i++){
          $in = ingresaPromesaBodega($idPromesa,$_POST['bodegasClientePromesa'][$i], $row);
          if($in == "Error"){
            break;
          }
          else{
            actualizaEstadoUnidad($_POST['bodegasClientePromesa'][$i], 3, $row);
          }
        }
      }

      if(is_array($_POST['estacionamientosClientePromesa'])){
        for($i = 0; $i < count($_POST['estacionamientosClientePromesa']); $i++){
          $in2 = ingresaPromesaEstacionamiento($idPromesa,$_POST['estacionamientosClientePromesa'][$i], $row);
          if($in2 == "Error"){
            break;
          }
          else{
            actualizaEstadoUnidad($_POST['estacionamientosClientePromesa'][$i], 3, $row);
          }
        }
      }

      $ingreso_cuotas = "Ok";

      if(array_key_exists('cuotas_promesa', $_POST) && count($_POST["cuotas_promesa"]) > 0){
        $cuotasPromesa = $_POST["cuotas_promesa"];

        $_SESSION['promesaFormaPagoCuota'] = array();

        $_SESSION['cuotasPromesa'] = $cuotasPromesa;

        foreach ($cuotasPromesa as $cuota) {
            $numero_cuota = $cuota['numero_cuota'];
            $uf_cuota = $cuota['uf_cuota'];
            $monto_cuota = $cuota['monto_cuota'];
            $forma_cuota = $cuota['forma_cuota'];
            array_push($_SESSION['promesaFormaPagoCuota'], consultaFormaPagoReservaEspecifica($forma_cuota));
            $banco_cuota = $cuota['banco_cuota'];
            $serie_cuota = $cuota['serie_cuota'];
            $nro_cuota = $cuota['nro_cuota'];
            $fecha_cuota = new DateTime($cuota['fecha_cuota']);
            $fecha_cuota = $fecha_cuota->format("Y-m-d");
            $ingreso_cuotas = ingresaPromesaCuotas($_SESSION['idPromesa'], $numero_cuota, $uf_cuota, $monto_cuota, $forma_cuota, $banco_cuota, $serie_cuota, $nro_cuota, $fecha_cuota,$row);
            if($ingreso_cuotas == "Error")
            {
              $ingreso_cuotas->query("ROLLBACK");
              break;
              echo "Sin datos";
            }
        }
      }
      else{
        unset($_SESSION['cuotasPromesa']);
      }

      if($in != "Error" && $in2 != "Error"){
        $_SESSION['numeroOperacion'] = $numeroOperacion;
        //Datos a session
        $fecha = new DateTime($_POST['fechaPromesa']);
        $fechaPromesa = $fecha->format("d-m-Y");
        $fechaPagoPromesa = $fecha2->format("d-m-Y");

        $_SESSION['promesaFecha'] = $fechaPromesa;
        $_SESSION['promesaReservaUF'] = $valorReservaUF;
        $_SESSION['promesaPiePromesaUF'] = $valorPiePromesaUF;
        $_SESSION['promesaPieSaldoUF'] = $valorPieSaldoUF;
        $_SESSION['promesaCuotasPie'] = $cantidadCuotasPie;
        $_SESSION['promesaSaldoTotalUF'] = $valorSaldoTotalUF;
        $_SESSION['promesaFechaPagoPromesa'] = $fechaPagoPromesa;
        $_SESSION['promesaValorPagoPromesa'] = $valorPagoPromesa;
        $_SESSION['promesaFormaPagoPromesa'] = consultaFormaPagoReservaEspecifica($formaPagoPromesa);
        $_SESSION['promesaBancoPromesa'] = $bancoPromesa;
        $_SESSION['promesaSerieChequePromesa'] = $serieChequePromesa;
        $_SESSION['promesaNroChequePromesa'] = $nroChequePromesa;
        $_SESSION['promesaValorPieSaldo'] = $valorPieSaldo;
        $_SESSION['promesaPieCuotas'] = $valorPieSaldoUF;
        $_SESSION['valorPieSaldo'] = $valorPieSaldo;
        $_SESSION['valorPiePromesa'] = $valorPiePromesa;
        $_SESSION['promesaTotalUF'] = $valorTotalUF;


        $fechaC1 = new DateTime($cliente1[0]['FECHANAC']);
        $fechaNacCliente1 = $fechaC1->format("d-m-Y");

        $_SESSION['promesaFechaNacCliente1'] = $fechaNacCliente1;
        $_SESSION['promesaEstadoCivilIDCliente1'] = $cliente1[0]['ESTADOCIVIL'];
        $_SESSION['promesaProfesionCliente1'] = $cliente1[0]['PROFESION'];
        if ($cliente1[0]['TIPODOMICILIO'] != '') {
        $_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'] . ', ' . $cliente1[0]['TIPODOMICILIO'];
        }else{
          $_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'];
        }
        $_SESSION['promesaComunaCliente1'] = $cliente1[0]['COMUNA'];
        $_SESSION['promesaTelefonoCliente1'] = $cliente1[0]['CELULAR'];
        $_SESSION['promesaMailCliente1'] = $cliente1[0]['EMAIL'];
        $_SESSION['promesaRentaCliente1'] = $cliente1[0]['EM_RENTALIQUIDA'];
        $_SESSION['promesaCiudadCliente1'] = $cliente1[0]['CIUDAD'];
        $_SESSION['promesaInstitucionCliente1'] = $cliente1[0]['INSTITUCION'];
        $_SESSION['promesaRegionCliente1'] = $cliente1[0]['REGION'];
        $_SESSION['promesaActividadCliente1'] = $cliente1[0]['ACTIVIDAD'];
        $_SESSION['promesaPaisCliente1'] = $cliente1[0]['PAIS'];
        $_SESSION['promesaNacionalidadCliente1'] = $cliente1[0]['NACIONALIDAD'];
        $_SESSION['promesaResidenciaCliente1'] = $cliente1[0]['RESIDENCIA'];
        $_SESSION['promesaSexoCliente1'] = $cliente1[0]['SEXO'];
        $_SESSION['promesaFechaNacCliente1'] = $cliente1[0]['FECHANAC'];
        $_SESSION['promesaNivelEducCliente1'] = $cliente1[0]['NIVELEDUCACIONAL'];
        $_SESSION['promesaCasaHabitaCliente1'] = $cliente1[0]['CASAHABITA'];
        $_SESSION['promesaMotivoCompraCliente1'] = $cliente1[0]['MOTIVOCOMPRA'];

        $proyecto = consultaProyectoEspecifico($idProyecto);
        $_SESSION['promesaCodigoProyecto'] = $codigoProyecto;
        $_SESSION['promesaLogoProyecto'] = '../' . $proyecto[0]['LOGO'];
        $_SESSION['promesaNombreProyecto'] = $proyecto[0]['NOMBRE'];
        $_SESSION['promesaInmobiliaria'] = $proyecto[0]['INMOBILIARIA'];
        $_SESSION['promesaInmobiliariaDireccion'] = $proyecto[0]['DIRECCION'];

        $_SESSION['promesaNumeroDepto'] = $unidad[0]['CODIGO'];

        $_SESSION['estacionamientosClientePromesa'] = $_POST['estacionamientosClientePromesa'];
        $_SESSION['bodegasClientePromesa'] = $_POST['bodegasClientePromesa'];

        //Actualiza datos de cliente en session_start
        $_SESSION['promesaNombreCliente'] = $cliente1[0]['NOMBRES'];
        $_SESSION['promesaApellidoCliente'] = $cliente1[0]['APELLIDOS'];
        $_SESSION['promesaRutCliente'] = $cliente1[0]['RUT'];

        //OBSERVACION

        //VENDEDOR
        $_SESSION['promesaDatosVendedor'] = promesaDatosVendedor($idUsuario);

        $row->query("COMMIT");
        echo "Ok";
      }
      else{
        $row->query("ROLLBACK");
        echo "Sin datos";
      }
    }
  	else{
  		echo "Sin datos";
  	}
	}
	else{
		echo "Sin datos";
	}
?>
