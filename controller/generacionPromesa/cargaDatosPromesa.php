<?php
//ini_set('display_errors', 'On');
date_default_timezone_set("America/Santiago");
require('../../model/consultas.php');
session_start();
$ch = curl_init();

$fecha = new Datetime();
$dia = $fecha->format('d');
$mes = $fecha->format('m');
$ano = $fecha->format('Y');

$fecha = $fecha->format('d-m-Y');

// set url
$valor = $_POST['valorUFJqueryHoy'];

$_SESSION['promesaUFActual'] = $valor;

$reserva = consultaReservaEspecifica($_POST['codigoProyecto'], $_POST['numeroReserva']);
$cotizacion = consultaCodigoCotizacionPromesa($reserva[0]['IDRESERVA']);
$proyecto = consultaProyectoEspecifico($reserva[0]['IDPROYECTO']);
$cliente = consultaClienteEspecifico($reserva[0]['IDCLIENTE1']);
$unidad = consultaUnidadEspecifica($reserva[0]['IDUNIDAD']);
$reservaBodega = consultaReservaBodega($reserva[0]['IDRESERVA']);
$reservaEstacionamiento = consultaReservaEstacionamiento($reserva[0]['IDRESERVA']);
$reservaBodegaCantidad = consultaReservaBodegaCantidad($reserva[0]['IDRESERVA']);
$reservaEstacionamientoCantidad = consultaReservaEstacionamientoCantidad($reserva[0]['IDRESERVA']);
$usuario = chequeaUsuario($_SESSION['rutUser']);

$_SESSION['promesaCodigoProyecto'] = $_POST['codigoProyecto'];
//datos extra
$_SESSION['idReserva'] = $reserva[0]['IDRESERVA'];
$_SESSION['idCliente1'] = $cliente[0]['IDCLIENTE'];
$_SESSION['idUnidad'] = $unidad[0]['IDUNIDAD'];
$_SESSION['idProyecto'] = $proyecto[0]['IDPROYECTO'];
$_SESSION['tipoUnidad'] = $unidad[0]['TIPOUNIDAD'];

$_SESSION['promesaNombreProyecto'] = $proyecto[0]['NOMBRE'];
$_SESSION['promesaLogoProyecto'] = '../' . $proyecto[0]['LOGO'];
if($_SESSION['promesaLogoProyecto'] == '../'){
  $_SESSION['promesaLogoProyecto'] = '../../view/img/imagenes_cliente/proyectos/NO_LOGO/no_logo.png';
}

$_SESSION['idUsuario'] = $usuario['IDUSUARIO'];

//Generamos datos en session para la IMG promesa
if($unidad[0]['IMGPLANO'] == NULL){
  $_SESSION['promesaImgPlano'] = "../../view/img/imagenes_cliente/proyectos/NO_LOGO/no_logo.png";
}
else{
  $_SESSION['promesaImgPlano'] = $unidad[0]['IMGPLANO'];
}

//Campos de cliente deben ser chequeados para ingreso de promesa
$_SESSION['promesaNombreCliente'] = $cliente[0]['NOMBRES'];
$_SESSION['promesaApellidoCliente'] = $cliente[0]['APELLIDOS'];
$_SESSION['promesaRutCliente'] = $cliente[0]['RUT'];
$_SESSION['promesaCelularCliente'] = $cliente[0]['CELULAR'];
$_SESSION['promesaEmailCliente'] = $cliente[0]['EMAIL'];
$_SESSION['promesaDomicilioCliente'] = $cliente[0]['DOMICILIO'];
$_SESSION['promesaNumeroDomicilioCliente'] = $cliente[0]['NUMERODOMICILIO'];
$_SESSION['promesaComunaCliente'] = $cliente[0]['COMUNA'];
$_SESSION['promesaCiudadCliente'] = $cliente[0]['CIUDAD'];
$_SESSION['promesaRegionCliente'] = $cliente[0]['REGION'];
$_SESSION['promesaPaisCliente'] = $cliente[0]['PAIS'];
$_SESSION['promesaProfesionCliente'] = $cliente[0]['PROFESION'];
$_SESSION['promesaInstitucionCliente'] = $cliente[0]['INSTITUCION'];
$_SESSION['promesaNacionalidadCliente'] = $cliente[0]['NACIONALIDAD'];
$_SESSION['promesaSexoCliente'] = $cliente[0]['SEXO'];

//Generamos datos en session para la promesa
$fechaReserva = new Datetime($reserva[0]['FECHARESERVA']);
$fechaPagoReserva = new Datetime($reserva[0]['FECHAPAGORESERVA']);
$_SESSION['promesaFechaReserva'] = $fechaReserva->format('d-m-Y');
$_SESSION['promesaFechaPagoReserva'] = $fechaPagoReserva->format('d-m-Y');

$_SESSION['promesaNumeroReserva'] = $reserva[0]['NUMERO'];
$_SESSION['promesaNumeroCotizacion'] = $cotizacion[0]['CODIGO'];


//Demas datos
$_SESSION['promesaNumeroDepto'] = $unidad[0]['CODIGO'];
$_SESSION['promesaTipologiaDepto'] = $unidad[0]['TIPOLOGIADET'];
$_SESSION['promesaModeloDepto'] = $unidad[0]['CODIGOTIPO'];
$_SESSION['promesaOrientacionDepto'] = $unidad[0]['ORIENTACION'];
$_SESSION['promesaMT2UtilesDepto'] = $unidad[0]['M2UTIL'];
$_SESSION['promesaMT2TerrazaDepto'] = $unidad[0]['M2TERRAZA'];
$_SESSION['promesaMT2TotalDepto'] = $unidad[0]['M2TOTAL'];
$_SESSION['promesaEst'] = $reservaEstacionamientoCantidad[0]['EST'];
$_SESSION['promesaBod'] = $reservaBodegaCantidad[0]['BOD'];
$_SESSION['promesaValorDepto'] = $unidad[0]['VALORUF'];

//UF de bodega y estacionamiento
$_SESSION['promesaValorEst'] = 0;
$_SESSION['promesaValorBod'] = 0;

for($i = 0; $i < count($reservaEstacionamiento); $i++){
  $_SESSION['promesaValorEst'] = $_SESSION['promesaValorEst'] + $reservaEstacionamiento[0]['VALORUF'];
}

for($i = 0; $i < count($reservaBodega); $i++){
  $_SESSION['promesaValorBod'] = $_SESSION['promesaValorBod'] + $reservaBodega[0]['VALORUF'];
}


$_SESSION['promesaValorBrutoUF'] = round(($_SESSION['promesaValorDepto']+$_SESSION['promesaValorEst']+$_SESSION['promesaValorBod']),2);

//Seguimos con datos necesarios
$_SESSION['promesaBono'] = $reserva[0]['BONOVENTAUF'];

$_SESSION['promesaDescuentoSala'] = number_format(($reserva[0]['DESCUENTO1']),2, '.', '');
$_SESSION['promesaDescuentoEspecial'] = number_format(($reserva[0]['DESCUENTO2']), 2, '.', '');

$_SESSION['promesaTotal2'] = number_format(($_SESSION['promesaValorDepto']-$_SESSION['promesaValorDepto']*$_SESSION['promesaDescuentoSala']/100),2,'.','');

$_SESSION['promesaTotalUF'] = $reserva[0]['VALORTOTALUF'];
$_SESSION['promesaReserva'] = $reserva[0]['VALORRESERVAUF'];
$_SESSION['promesaPieContadoMonto'] = $reserva[0]['VALORPIEPROMESA'];
$_SESSION['promesaPieContado'] = $reserva[0]['VALORPIEPROMESA']/$valor;
$_SESSION['promesaPieCuotas'] = $reserva[0]['VALORPIESALDO'];
$_SESSION['promesaPieCuotasUF'] = $reserva[0]['VALORPIESALDO']/$valor;
$_SESSION['promesaPieSaldo'] = $reserva[0]['VALORTOTALUF'] - $reserva[0]['VALORRESERVAUF'] - $_SESSION['promesaPieContado'] - $_SESSION['promesaPieCuotasUF'];

$_SESSION['promesaPieCantCuotas'] = $reserva[0]['CANTCUOTASPIE'];
$_SESSION['promesaAccion'] = $unidad[0]['ACCIONDET'];

$_SESSION['promesaValorPagoReserva'] = $reserva[0]['VALORPAGORESERVA'];
$_SESSION['promesaFormaPagoValor'] = $reserva[0]['FORMAPAGORESERVA'];
$formaPagoPromesa = consultaFormaPagoReservaEspecifica($_SESSION['promesaFormaPagoValor']);
$_SESSION['promesaFormaPagoNombre'] = $formaPagoPromesa[0]['NOMBRE'];
$_SESSION['promesaBanco'] = $reserva[0]['BANCORESERVA'];
$_SESSION['promesaSerieNro'] = $reserva[0]['SERIECHEQUERESERVA'];
$_SESSION['promesaNroTransCheque'] = $reserva[0]['NROCHEQUERESERVA'];

$obj->nombreproyecto = $proyecto[0]['NOMBRE'];
$obj->idreserva = $reserva[0]['IDRESERVA'];
echo json_encode($obj);
?>
