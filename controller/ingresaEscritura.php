<?php
  header('Access-Control-Allow-Origin: *');
  ini_set('display_errors', 'On');
  require('../model/consultas.php');

  session_start();

if(count($_SESSION) > 0 && count($_POST) > 0){
    $idPromesa = $_SESSION['idPromesa'];
    $idProyecto = $_SESSION['idProyecto'];
    $idUnidad = $_SESSION['idUnidad'];
    $idCliente1 = $_SESSION['idCliente1'];
    $idCliente2 = 0; //Pendiente
    $_SESSION['idUsuario'] = $_POST['idUsuario'];
    $idUsuario = $_SESSION['idUsuario'];
    $codigoProyecto = $_SESSION['escrituraCodigoProyecto'];
    $numeroOperacion = $_SESSION['escrituraNumeroOperacion'];

    $fechape =  new DateTime($_POST['fechaPagoEscritura']);
    $fechaPagoEscritura = $fechape->format("Y-m-d");

    $fechae = new DateTime($_POST['fechaEscritura']);
    $fechaEscritura = $fechae->format("Y-m-d");

    $valorBrutoUF = $_SESSION['escrituraValorBrutoUF'];
    $descuento1 = $_SESSION['escrituraDescuentoSala'];
    $descuento2 = $_SESSION['escrituraDescuentoEspecial'];
    $bonoVentaUF = $_SESSION['escrituraBono'];
    $valorTotalUF = $_SESSION['escrituraTotalUF'];
    $valorReservaUF = $_SESSION['escrituraReserva'];
    $valorReserva = $_SESSION['escrituraValorPagoReserva'];
    $valorPiePromesaUF = $_SESSION['escrituraPieContado'];
    $valorPiePromesa = $_SESSION['escrituraPieContadoMonto'];
    if($_SESSION['escrituraCodigoProyecto'] == "COR"){
      $valorSaldoTotalUF = $_POST['UFSaldoEscrituraCorretaje'];
      $_SESSION['escrituraPieSaldo'] =   $_POST['UFSaldoEscrituraCorretaje'];
    }
    else{
      $valorSaldoTotalUF = $_SESSION['escrituraPieSaldo'];
    }
    $valorPieSaldoUF = $_SESSION['escrituraPieCuotasUF'];
    $valorPieSaldo = $_SESSION['escrituraPieCuotas'];
    $cantidadCuotasPie = $_SESSION['escrituraPieCantCuotas'];

    $valorPagoEscritura = $_POST['valorPagoEscritura'];
    $formaPagoEscritura = $_POST['formaPagoEscritura'];
    $bancoEscritura = $_POST['bancoEscritura'];
    $serieChequeEscritura = $_POST['serieChequeEscritura'];
    $nroChequeEscritura = $_POST['nroChequeEscritura'];

    //valores cuota residual
    $residualMonto = $_POST['residualMonto'];

    $residualFormaPago = $_POST['residualFormaPago'];
    $residualBanco = $_POST['residualBanco'];
    $residualSerie = $_POST['residualSerie'];
    $residualNro = $_POST['residualNro'];
    $residualFechaCheque = new DateTime($_POST['residualFechaCheque']);
    $residualFechaCheque = $residualFechaCheque->format("Y-m-d");
    $residualMontoUF = $_POST['residualMontoUF'];

    if(array_key_exists('escrituraIdVendedor', $_SESSION)){
      $vendedor = chequeaUsuarioID($_SESSION['escrituraIdVendedor']);
      $_SESSION['escrituraVendedor'] = $vendedor[0]['NOMBRES'] + ' ' + $vendedor[0]['APELLIDOS'];
      $_SESSION['escrituraVendedorRut'] = $vendedor[0]['RUT'];
    }
    else{
      $_SESSION['escrituraVendedor'] = $_SESSION['nombreUser'];
      $_SESSION['escrituraVendedorRut'] = $_SESSION['rutUser'];
    }

    /*
    if($_SESSION['codProyectoClienteCotizacion'] == "COR"){
      $descuento1UF = 0;
      $descuento2UF = 0;
      $valorPromesaUF = 0;
      $valorPiePromesaUF = 0;
      $valorPieSaldoUF = 0;
      $cantidadCuotasPie = 0;
      $valorSaldoTotalUF = 0;
    }else{
      $descuento1UF = $_SESSION['descuento1ClienteCotizacion'];
      $descuento2UF = $_SESSION['descuento2ClienteCotizacion'];
      $valorPromesaUF = $_SESSION['promesaClienteCotizacion'];
      $valorPiePromesaUF = $_SESSION['piePromesaClienteCotizacion'];
      $valorPieSaldoUF = $_SESSION['pieCuotasClienteCotizacion'];
      $cantidadCuotasPie = $_SESSION['cuotasClienteCotizacion'];
      $valorSaldoTotalUF = $_SESSION['saldoClienteCotizacion'];
      $bonoVenta = $_SESSION['bonoVentaCrearCotizacion'];
    }
    */
    liberaUnidadesPromesaBodega_escritura($codigoProyecto,$numeroOperacion);
    liberaUnidadesPromesaEstacionamiento_escritura($codigoProyecto,$numeroOperacion);
    liberaUnidadesReservaEstacionamiento($codigoProyecto,$numeroOperacion);
    liberaUnidadesReservaBodega($codigoProyecto,$numeroOperacion);
    //liberaUnidadesReserva($codigoProyecto,$numeroOperacion);
    eliminaPromesaBodega($codigoProyecto, $numeroOperacion);
    eliminaPromesaEstacionamiento($codigoProyecto, $numeroOperacion);
    eliminaReservaBodega($codigoProyecto, $numeroOperacion);
    eliminaReservaEstacionamiento($codigoProyecto, $numeroOperacion);

    $row = ingresaEscritura($idPromesa, $idProyecto, $idUnidad, $idCliente1, $idCliente2, $idUsuario,$numeroOperacion, $fechaEscritura, $valorBrutoUF, $descuento1, $descuento2, $bonoVentaUF, $valorTotalUF, $valorReservaUF, $valorPiePromesaUF, $valorPieSaldoUF, $cantidadCuotasPie, $valorSaldoTotalUF, $fechaPagoEscritura, $valorPagoEscritura, $formaPagoEscritura, $bancoEscritura, $serieChequeEscritura, $nroChequeEscritura, $valorPiePromesa, $valorPieSaldo, $valorReserva);
    //var_dump($row);
    if($row != "Error")
    {
      $_SESSION['idEscritura'] = $row->insert_id;

      //Ingreso Cuota Residual
      ingresaEscrituraCuota($_SESSION['idEscritura'], $residualMonto, $residualFormaPago, $residualBanco, $residualSerie, $residualNro, $residualFechaCheque, $residualMontoUF, $row);
      actualizaEstadoUnidad($idUnidad, 5, $row);
      //actualizaInteresCotizacion($idCotizacion, $row);

      $in = 'Ok';
      $in2 = 'Ok';

      if($_POST['bodegasClienteEscritura'] != ''){

        for($i = 0; $i < count($_POST['bodegasClienteEscritura']); $i++){
          ingresaReservaBodega($_SESSION['idReserva'],$_POST['bodegasClienteEscritura'][$i], $row);
          ingresaPromesaBodega($_SESSION['idPromesa'],$_POST['bodegasClienteEscritura'][$i], $row);
          $in = ingresaEscrituraBodega($_SESSION['idEscritura'],$_POST['bodegasClienteEscritura'][$i], $row);
          if($in == "Error"){
            break;
          }
          else{
            actualizaEstadoUnidad($_POST['bodegasClienteEscritura'][$i], 5, $row);
          }
        }
      }

      if(is_array($_POST['estacionamientosClienteEscritura'])){

        for($i = 0; $i < count($_POST['estacionamientosClienteEscritura']); $i++){
          ingresaReservaEstacionamiento($_SESSION['idReserva'],$_POST['estacionamientosClienteEscritura'][$i], $row);
          ingresaPromesaEstacionamiento($_SESSION['idPromesa'],$_POST['estacionamientosClienteEscritura'][$i], $row);
          $in2 = ingresaEscrituraEstacionamiento($_SESSION['idEscritura'],$_POST['estacionamientosClienteEscritura'][$i], $row);
          if($in2 == "Error"){
            break;
          }
          else{
            actualizaEstadoUnidad($_POST['estacionamientosClienteEscritura'][$i], 5, $row);
          }
        }
      }


      if($in != "Error" && $in2 != "Error"){
        $row->query("COMMIT");
        $_SESSION['numeroOperacion'] = $numeroOperacion;

        //Datos a session
        // $fechaPromesa = $fecha->format("d-m-Y");

        $fechaEscritura = $fechae->format("d-m-Y");
        $fechaPagoEscritura = $fechape->format("d-m-Y");
        $_SESSION['escrituraFecha'] = $fechaEscritura;


        $_SESSION['escrituraReservaUF'] = $valorReservaUF;
        $_SESSION['escrituraPiePromesaUF'] = $valorPiePromesaUF;
        $_SESSION['escrituraPieSaldoUF'] = $valorPieSaldoUF;
        $_SESSION['escrituraCuotasPie'] = $cantidadCuotasPie;
        $_SESSION['escrituraSaldoTotalUF'] = $valorSaldoTotalUF;
        $_SESSION['escrituraFechaPagoEscritura'] = $fechaPagoEscritura;
        $_SESSION['escrituraValorPagoEscritura'] = $valorPagoEscritura;
        $_SESSION['escrituraFormaPagoEscritura'] = consultaFormaPagoReservaEspecifica($formaPagoEscritura);
        $_SESSION['escrituraBancoEscritura'] = $bancoEscritura;
        $_SESSION['escrituraSerieChequeEscritura'] = $serieChequeEscritura;
        $_SESSION['escrituraNroChequeEscritura'] = $nroChequeEscritura;
        $_SESSION['escrituraValorPiePromesa'] = $valorPiePromesa;
        $_SESSION['escrituraValorPieSaldo'] = $valorPieSaldo;
        $_SESSION['escrituraPieCuotas'] = $valorPieSaldoUF;
        $_SESSION['valorPieSaldo'] = $valorPieSaldo;
        $_SESSION['valorPiePromesa'] = $valorPiePromesa;

        $cliente1 = consultaClienteEspecifico($_SESSION['idCliente1']);

        $fechaC1 = new DateTime($cliente1[0]['FECHANAC']);
        $fechaNacCliente1 = $fechaC1->format("d-m-Y");

        $_SESSION['escrituraFechaNacCliente1'] = $fechaNacCliente1;
        $_SESSION['escrituraEstadoCivilIDCliente1'] = $cliente1[0]['ESTADOCIVIL'];
        $_SESSION['escrituraProfesionCliente1'] = $cliente1[0]['PROFESION'];
        if ($cliente1[0]['TIPODOMICILIO'] != '') {
        $_SESSION['escrituraDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'] . ', ' . $cliente1[0]['TIPODOMICILIO'];
        }else{
          $_SESSION['escrituraDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'];
        }

        $_SESSION['escrituraComunaCliente1'] = $cliente1[0]['COMUNA'];
        $_SESSION['escrituraTelefonoCliente1'] = $cliente1[0]['CELULAR'];
        $_SESSION['escrituraMailCliente1'] = $cliente1[0]['EMAIL'];
        $_SESSION['escrituraRentaCliente1'] = $cliente1[0]['EM_RENTALIQUIDA'];
        $_SESSION['escrituraCiudadCliente1'] = $cliente1[0]['CIUDAD'];
        $_SESSION['escrituraInstitucionCliente1'] = $cliente1[0]['INSTITUCION'];
        $_SESSION['escrituraRegionCliente1'] = $cliente1[0]['REGION'];
        $_SESSION['escrituraActividadCliente1'] = $cliente1[0]['ACTIVIDAD'];
        $_SESSION['escrituraPaisCliente1'] = $cliente1[0]['PAIS'];
        $_SESSION['escrituraNacionalidadCliente1'] = $cliente1[0]['NACIONALIDAD'];
        $_SESSION['escrituraResidenciaCliente1'] = $cliente1[0]['RESIDENCIA'];
        $_SESSION['escrituraSexoCliente1'] = $cliente1[0]['SEXO'];
        $_SESSION['escrituraFechaNacCliente1'] = $cliente1[0]['FECHANAC'];
        $_SESSION['escrituraNivelEducCliente1'] = $cliente1[0]['NIVELEDUCACIONAL'];
        $_SESSION['escrituraCasaHabitaCliente1'] = $cliente1[0]['CASAHABITA'];
        $_SESSION['escrituraMotivoCompraCliente1'] = $cliente1[0]['MOTIVOCOMPRA'];

        $proyecto = consultaProyectoEspecifico($_SESSION['idProyecto']);

        $_SESSION['escrituraInmobiliaria'] = $proyecto[0]['INMOBILIARIA'];
        $_SESSION['escrituraInmobiliariaDireccion'] = $proyecto[0]['DIRECCION'];

        $_SESSION['estacionamientosClienteEscritura'] = $_POST['estacionamientosClienteEscritura'];
        $_SESSION['bodegasClienteEscritura'] = $_POST['bodegasClienteEscritura'];

        //Actualiza datos de cliente en session_start
        $_SESSION['escrituraNombreCliente'] = $cliente1[0]['NOMBRES'];
        $_SESSION['escrituraApellidoCliente'] = $cliente1[0]['APELLIDOS'];

        //OBSERVACION

        //VENDEDOR
        $_SESSION['escrituraDatosVendedor'] = promesaDatosVendedor($idUsuario);
        $_SESSION['escrituraCuotas'] = consultaCuotasPromesadas($codigoProyecto, $numeroOperacion);

        //CUOTA RESIDUAL
        $_SESSION['residualMonto'] = $residualMonto;
        $_SESSION['residualFormaPago'] = consultaFormaPagoReservaEspecifica($residualFormaPago);
        $_SESSION['residualBanco'] = $residualBanco;
        $_SESSION['residualSerie'] = $residualSerie;
        $_SESSION['residualNro'] = $residualNro;
        $residualFechaCheque = new DateTime($_POST['residualFechaCheque']);
        $_SESSION['residualFechaCheque'] = $residualFechaCheque->format("d-m-Y");

        //Cambia estado de promesa a escriturada
        cambiarEstadoPromesa($codigoProyecto, $numeroOperacion, 6);
        //Cambia estado de reserva a escriturada
        cambiarEstadoReserva($codigoProyecto, $numeroOperacion, 8);
        echo "Ok¬" . $_SESSION['escrituraCodigoProyecto'] . "¬" . $_SESSION['numeroOperacion'];
      }
      else{
        $row->query("ROLLBACK");
        echo "Sin datos";
      }
    }
  	else{
  		echo "Sin datos";
  	}
	}
	else{
		echo "Sin datos";
	}
?>
