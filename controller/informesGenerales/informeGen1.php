<?php
// ini_set('display_errors', 'On');
set_time_limit(1200);
require('../../model/consultas.php');
require '../html2pdf/vendor/autoload.php';
date_default_timezone_set('America/Santiago');
session_start();

// $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

$mesesCortos = array("ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic");


// $document = '/var/www/html/Git/inmonet/';
// $url = 'http://127.0.0.1/Git/inmonet/';
$document = '/home/livingne/inmonet.cl/';
$url = 'https://inmonet.cl/';
// $document = '/home/housene1/public_html/inmonet/';
// $url = 'https://house-net.cl/inmonet/';

$mes = $_POST['mes'];
$mesTexto = $meses[$mes-1];
$ano = $_POST['ano'];
$codigoProyecto = $_POST['codigoProyecto'];
$accion = $_POST['accion'];
$datosInforme1 = datosInforme1($accion, $mes, $ano,$codigoProyecto);

$fechasRango = array();
$fechasRangoIndex = array();
$fechaCompara = array();

if($mes == 12){
	$fechaMax = new Datetime(($ano + 1) . '-' . '01-01');
}
else{
	$fechaMax = new Datetime($ano . '-' . ($mes + 1) . '-01');
}
for($i = 0; $i < 12; $i++){
	$f = strtotime(($i - 12) . 'month', strtotime($fechaMax->format('y-m-d')));
	$m =  date('m',$f);
	$y =  date('y',$f);
	$mText = $mesesCortos[$m-1];
	$fFinal = $mText . "-" . $y;
	$fechasRango[] = $fFinal;
	$fechasRangoIndex[] = $y . '_' . $m;
	$fechaCompara[] = $y . '-' . $m;
}

$datosProyecto = consultaDatosProyecto($codigoProyecto);
$datosInforme1NivelInteres = datosInforme1NivelInteres($accion, $ano, $mes, $codigoProyecto);
$datosInforme2 = datosInforme2($accion, $mes,$ano,$codigoProyecto);
$datosInforme3 = datosInforme3($accion, $mes, $ano,$codigoProyecto);
$datosPorVendedor = datosCantidadPorVendedor($accion, $mes, $ano, $codigoProyecto);
$datosInforme1Reservas = datosInforme1Reservas($accion, $mes,$ano,$codigoProyecto);
$datosInforme1Promesas = datosInforme1Promesas($accion, $mes, $ano, $codigoProyecto);
$datosInforme1Escrituras = datosInforme1Escrituras($accion, $mes, $ano, $codigoProyecto);
$datosInforme1ReservasDetalle = datosInforme1ReservasDetalle($accion, $mes, $ano, $codigoProyecto);
$datosInforme1PromesasDetalle = datosInforme1PromesasDetalle($accion, $mes, $ano, $codigoProyecto);
$datosInforme1EscriturasDetalle = datosInforme1EscriturasDetalle($accion, $mes, $ano, $codigoProyecto);
$nombreProyecto = $datosProyecto[0]['NOMBRE'];

// var_dump($datosInforme1ReservasDetalle);

$mi_curl = curl_init ($url . "controller/informesGenerales/gra1.php?accion=" . $accion . "&mes=" . $mes . "&ano=" . $ano . "&codigoProyecto=" . $codigoProyecto);
$fs_archivo = fopen ($document . "controller/informesGenerales/gra1.png", "w");
curl_setopt ($mi_curl, CURLOPT_FILE, $fs_archivo);
curl_setopt ($mi_curl, CURLOPT_HEADER, 0);
curl_exec ($mi_curl);
curl_close ($mi_curl);
fclose ($fs_archivo);

$mi_curl = curl_init ($url . "controller/informesGenerales/gra2.php?accion=" . $accion . "&ano=" . $ano . "&mes=" . $mes . "&codigoProyecto=" . $codigoProyecto);
$fs_archivo = fopen ($document . "controller/informesGenerales/gra2.png", "w");
curl_setopt ($mi_curl, CURLOPT_FILE, $fs_archivo);
curl_setopt ($mi_curl, CURLOPT_HEADER, 0);
curl_exec ($mi_curl);
curl_close ($mi_curl);
fclose ($fs_archivo);

$mi_curl = curl_init ($url . "controller/informesGenerales/gra3.php?accion=" . $accion . "&mes=" . $mes .  "&ano=" . $ano . "&codigoProyecto=" . $codigoProyecto);
$fs_archivo = fopen ($document . "controller/informesGenerales/gra3.png", "w");
curl_setopt ($mi_curl, CURLOPT_FILE, $fs_archivo);
curl_setopt ($mi_curl, CURLOPT_HEADER, 0);
curl_exec ($mi_curl);
curl_close ($mi_curl);
fclose ($fs_archivo);

$mi_curl = curl_init ($url . "controller/informesGenerales/gra5.php?accion=" . $accion . "&mes=" . $mes . "&ano=" . $ano . "&codigoProyecto=" . $codigoProyecto);
$fs_archivo = fopen ($document . "controller/informesGenerales/gra5.png", "w");
curl_setopt ($mi_curl, CURLOPT_FILE, $fs_archivo);
curl_setopt ($mi_curl, CURLOPT_HEADER, 0);
curl_exec ($mi_curl);
curl_close ($mi_curl);
fclose ($fs_archivo);

$mi_curl = curl_init ($url . "controller/informesGenerales/gra6.php?accion=" . $accion . "&mes=" . $mes . "&ano=" . $ano . "&codigoProyecto=" . $codigoProyecto);
$fs_archivo = fopen ($document . "controller/informesGenerales/gra6.png", "w");
curl_setopt ($mi_curl, CURLOPT_FILE, $fs_archivo);
curl_setopt ($mi_curl, CURLOPT_HEADER, 0);
curl_exec ($mi_curl);
curl_close ($mi_curl);
fclose ($fs_archivo);

$mi_curl = curl_init ($url . "controller/informesGenerales/gra7.php?accion=" . $accion . "&mes=" . $mes . "&ano=" . $ano . "&codigoProyecto=" . $codigoProyecto);
$fs_archivo = fopen ($document . "controller/informesGenerales/gra7.png", "w");
curl_setopt ($mi_curl, CURLOPT_FILE, $fs_archivo);
curl_setopt ($mi_curl, CURLOPT_HEADER, 0);
curl_exec ($mi_curl);
curl_close ($mi_curl);
fclose ($fs_archivo);

use Spipu\Html2Pdf\Html2Pdf;

ob_start();
?>
<!-- Contenido -->
<page>
<table style="margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr>
					<td style="padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">LIVING NET - Informe de Gestión - Proyecto <?php echo $nombreProyecto ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr style="border-spacing: 0 0; border-collapse : collapse;">
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
				<tr>
					<td style="padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;">Correspondiente a <?php echo $mesTexto; ?> de <?php echo $ano ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr style="border-spacing: 0 0; border-collapse : collapse;">
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
				<tr>
					<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">COTIZACIONES POR MES</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr style="background-color: #d8d8d8;font-weight: bold;">
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 91px; min-width: 91px; width: 91px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[0]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[1]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[2]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[3]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[4]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[5]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[6]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[7]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[8]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[9]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[10]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[11]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Totales</td>
				</tr>
			</table>
		</td>
	</tr>
	<?php
	$suma = array(0,0,0,0,0,0,0,0,0,0,0,0,0);
	for($i = 0; $i < count($datosInforme1); $i++){
		$total = 0;
		for($k = 0; $k < count($fechasRangoIndex); $k++){
			$total = $total + $datosInforme1[$i][$fechasRangoIndex[$k]];
			if(array_key_exists($fechasRangoIndex[$k],$datosInforme1[$i])){
				$suma[$k] = $suma[$k] + $datosInforme1[$i][$fechasRangoIndex[$k]];
			}
			else{
				$suma[$j] = $suma[$j] + 0;
			}
		}
		$suma[12] = $suma[12] + $total;
		echo '<tr>
			<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
				<table style="margin-top: -3px; border-spacing: 0 0; border-collapse : collapse;">
					<tr>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 91px; min-width: 91px; width: 91px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><b>' . $datosInforme1[$i]['COTIZAEN'] .  '</b></td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[0],$datosInforme1[$i])){
								echo $datosInforme1[$i][$fechasRangoIndex[0]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[1],$datosInforme1[$i])){
								echo $datosInforme1[$i][$fechasRangoIndex[1]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[2],$datosInforme1[$i])){
								echo $datosInforme1[$i][$fechasRangoIndex[2]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[3],$datosInforme1[$i])){
								echo $datosInforme1[$i][$fechasRangoIndex[3]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[4],$datosInforme1[$i])){
								echo $datosInforme1[$i][$fechasRangoIndex[4]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[5],$datosInforme1[$i])){
								echo $datosInforme1[$i][$fechasRangoIndex[5]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[6],$datosInforme1[$i])){
								echo $datosInforme1[$i][$fechasRangoIndex[6]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[7],$datosInforme1[$i])){
								echo $datosInforme1[$i][$fechasRangoIndex[7]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[8],$datosInforme1[$i])){
								echo $datosInforme1[$i][$fechasRangoIndex[8]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[9],$datosInforme1[$i])){
								echo $datosInforme1[$i][$fechasRangoIndex[9]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[10],$datosInforme1[$i])){
								echo $datosInforme1[$i][$fechasRangoIndex[10]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[11],$datosInforme1[$i])){
								echo $datosInforme1[$i][$fechasRangoIndex[11]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><b>' . $total . '</b></td>
					</tr>
				</table>
			</td>
		</tr>';
	}
	echo '<tr>
		<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
			<table style="margin-top: -3px; border-spacing: 0 0; border-collapse : collapse;">
				<tr style="background-color: #d8d8d8;font-weight: bold;">
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 91px; min-width: 91px; width: 91px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Totales</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma[0] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma[1] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'. $suma[2] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma[3] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma[4] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma[5] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma[6] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma[7] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma[8] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma[9] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma[10] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma[11] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma[12] . '</td>
				</tr>
			</table>
		</td>
	</tr>';
	?>
</table>
<table style="margin-top: 2px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr>
					<td style="padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">
						<img src="gra1.png" style="width: 100%;">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table style="margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr style="border-spacing: 0 0; border-collapse : collapse;">
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
				<tr>
					<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">POSIBILIDAD DE CIERRE PROXIMAS 4 SEMANAS Y CIERRES ULTIMOS 30 DIAS</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 250px; min-width: 250px; width: 250px;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 250px; min-width: 250px; width: 250px; height: 200px; min-height: 200px; max-height: 200px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">
						<table style="border-spacing: 0 0; border-collapse : collapse;">
							<tr>
								<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 150px; min-width: 150px; width: 150px; text-align: center; border-spacing: 0 0; border-collapse : collapse;"></td>
								<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 100px; min-width: 100px; width: 100px; text-align: center; border-spacing: 0 0; border-collapse : collapse;">Cantidad</td>
							</tr>
							<?php
								$nivel = array(5,4,3,2,1);
								$valor = array(0,0,0,0,0);
								for($j = 0; $j < count($datosInforme1NivelInteres); $j++){
									for($k = 0; $k < count($nivel); $k++){
										if($nivel[$k] == $datosInforme1NivelInteres[$j][0]){
											$valor[$k] = $datosInforme1NivelInteres[$j][1];
										}
									}
								}

								for($i = 0; $i < count($nivel); $i++){
									$tipo = '';
									switch ($nivel[$i]) {
										case 5:
											$tipo = 'P 5 - Cierre';
											break;
										case 4:
											$tipo = 'P 4 - 75% Prob Cierre';
											break;
										case 3:
											$tipo = 'P 3 - 50% Prob Cierre';
											break;
										case 2:
											$tipo = 'P 2 - 25% Prob Cierre';
											break;
										case 1:
											$tipo = 'P 1 - 5% Prob Cierre';
											break;
									}
									echo '<tr>
										<td style="padding-left: 20px; padding-right: 6px; padding-top: 10px; padding-bottom: 8px;font-size: 12px;  max-width: 150px; min-width: 150px; width: 150px; text-align: left; border-spacing: 0 0; border-collapse : collapse;">' . $tipo . '</td>
										<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 100px; min-width: 100px; width: 100px; text-align: center; border-spacing: 0 0; border-collapse : collapse;">' . $valor[$i] . '</td>
									</tr>';
								}
							?>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td style="max-width: 594px; min-width: 594px; width: 594px;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 594px; min-width: 594px; width: 594px; height: 200px; min-height: 200px; max-height: 200px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse: collapse;">
						<img src="gra2.png" style="width: 100%;">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<page_footer>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
  </page_footer></page>
<page>
<table style="margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr>
					<td style="padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">LIVING NET - Informe de Gestión - Proyecto <?php echo $nombreProyecto ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr style="border-spacing: 0 0; border-collapse : collapse;">
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
				<tr>
					<td style="padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;">Correspondiente a <?php echo $mesTexto; ?> de <?php echo $ano ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr style="border-spacing: 0 0; border-collapse : collapse;">
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
				<tr>
					<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">INFORME DE MEDIOS</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr style="background-color: #d8d8d8;font-weight: bold;">
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 91px; min-width: 91px; width: 91px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[0]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[1]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[2]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[3]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[4]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[5]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[6]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[7]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[8]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[9]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[10]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[11]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Totales</td>
				</tr>
			</table>
		</td>
	</tr>
	<?php
	$suma2 = array(0,0,0,0,0,0,0,0,0,0,0,0,0);
	for($i = 0; $i < count($datosInforme2); $i++){
		$total2 = 0;
		for($k = 0; $k < count($fechasRangoIndex); $k++){
			$total2 = $total2 + $datosInforme2[$i][$fechasRangoIndex[$k]];
			if(array_key_exists($fechasRangoIndex[$k],$datosInforme2[$i])){
				$suma2[$k] = $suma2[$k] + $datosInforme2[$i][$fechasRangoIndex[$k]];
			}
			else{
				$suma2[$j] = $suma2[$j] + 0;
			}
		}
		$suma2[12] = $suma2[12] + $total2;
		echo '<tr>
			<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
				<table style="margin-top: -3px; border-spacing: 0 0; border-collapse : collapse;">
					<tr>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 91px; min-width: 91px; width: 91px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><b>' . $datosInforme2[$i]['MEDIO'] .  '</b></td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[0],$datosInforme2[$i])){
								echo $datosInforme2[$i][$fechasRangoIndex[0]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[1],$datosInforme2[$i])){
								echo $datosInforme2[$i][$fechasRangoIndex[1]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[2],$datosInforme2[$i])){
								echo $datosInforme2[$i][$fechasRangoIndex[2]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[3],$datosInforme2[$i])){
								echo $datosInforme2[$i][$fechasRangoIndex[3]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[4],$datosInforme2[$i])){
								echo $datosInforme2[$i][$fechasRangoIndex[4]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[5],$datosInforme2[$i])){
								echo $datosInforme2[$i][$fechasRangoIndex[5]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[6],$datosInforme2[$i])){
								echo $datosInforme2[$i][$fechasRangoIndex[6]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[7],$datosInforme2[$i])){
								echo $datosInforme2[$i][$fechasRangoIndex[7]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[8],$datosInforme2[$i])){
								echo $datosInforme2[$i][$fechasRangoIndex[8]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[9],$datosInforme2[$i])){
								echo $datosInforme2[$i][$fechasRangoIndex[9]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[10],$datosInforme2[$i])){
								echo $datosInforme2[$i][$fechasRangoIndex[10]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[11],$datosInforme2[$i])){
								echo $datosInforme2[$i][$fechasRangoIndex[11]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><b>' . $total2 . '</b></td>
					</tr>
				</table>
			</td>
		</tr>';
	}
	echo '<tr>
		<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
			<table style="margin-top: -3px; border-spacing: 0 0; border-collapse : collapse;">
				<tr style="background-color: #d8d8d8;font-weight: bold;">
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 91px; min-width: 91px; width: 91px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Totales</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma2[0] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma2[1] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'. $suma2[2] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma2[3] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma2[4] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma2[5] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma2[6] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma2[7] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma2[8] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma2[9] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma2[10] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma2[11] .  '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' . $suma2[12] . '</td>
				</tr>
			</table>
		</td>
	</tr>';
	?>
</table>
<table style="margin-top: 2px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr>
					<td style="padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">
						<img src="gra3.png" style="width: 100%;">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<page_footer>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
  </page_footer></page>
<page>
<table style="margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr>
					<td style="padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">LIVING NET - Informe de Gestión - Proyecto <?php echo $nombreProyecto ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr style="border-spacing: 0 0; border-collapse : collapse;">
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
				<tr>
					<td style="padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;">Correspondiente a <?php echo $mesTexto; ?> de <?php echo $ano ?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table style="margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 450px; min-width: 450px; width: 450px;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse; text-align: center;">
				<tr>
					<td style="padding-top: 0px; padding-bottom: 2px;font-size: 14px; font-weight: bold; height: 20px; min-height: 20px; max-height: 20px; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;" colspan=5>COTIZACIONES POR DEPARTAMENTO</td>
				</tr>
				<tr>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;"></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;">SALA</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;">MAIL</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;">OTRO</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;">TOTAL</td>
				</tr>
				<?php
					$z1 = 29;
					if($z1 > count($datosInforme3)){
						$z1 = count($datosInforme3);
					}
					for($i = 0; $i < $z1; $i++){
						$d = array(0,0,0);
						if(array_key_exists('Sala',$datosInforme3[$i]))
						{
							$d[0] = $datosInforme3[$i]['Sala'];
						}
						if(array_key_exists('Mail',$datosInforme3[$i]))
						{
							$d[1] = $datosInforme3[$i]['Mail'];
						}
						if(array_key_exists('Otro',$datosInforme3[$i]))
						{
							$d[2] = $datosInforme3[$i]['Otro'];
						}
						if($datosInforme3[$i]['ESTADO'] == 4){
							echo '<tr style="background-color: #e0e0e0;">';
						}
						else{
							echo '<tr>';
						}
						echo '<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;">' . $datosInforme3[$i]['CODIGO'] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;">' . $d[0] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;">' . $d[1] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;">' . $d[2] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;">' . ($d[0] + $d[1] + $d[2]) . '</td>
						</tr>';
					}
				?>
			</table>
		</td>
		<td style="max-width: 470px; min-width: 470px; width: 470px; border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; text-align: center; padding-bottom: 0;">
			<?php
				$mi_curl = curl_init ($url . "controller/informesGenerales/gra4.php?accion=" . $accion . "&mes=" . $mes . "&ano=" . $ano . "&codigoProyecto=" . $codigoProyecto . "&ciclo=1");
				$fs_archivo = fopen ($document . "controller/informesGenerales/gra4a.png", "w");
				curl_setopt ($mi_curl, CURLOPT_FILE, $fs_archivo);
				curl_setopt ($mi_curl, CURLOPT_HEADER, 0);
				curl_exec ($mi_curl);
				curl_close ($mi_curl);
				fclose ($fs_archivo);
				echo '<img src="gra4a.png" style="width: 100%;">';
			?>
		</td>
	</tr>
</table>
<page_footer>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
  </page_footer></page>
<?php
$z2 = $z1 + 29;
if($z2 > count($datosInforme3)){
	$z2 = count($datosInforme3);
}
if($z1 < count($datosInforme3)){
	echo "<page>
	<table style='margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse;'>
	        <tr>
	          <td style='padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 948px; min-width: 948px; width: 948px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;'>LIVING NET - Informe de Gestión - Proyecto $nombreProyecto </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	  <tr style='border-spacing: 0 0; border-collapse : collapse;'>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;'>
	        <tr>
	          <td style='padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;'>Correspondiente a $mesTexto de $ano </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>
	<table style='margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 450px; min-width: 450px; width: 450px;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; text-align: center;'>
	        <tr>
	          <td style='padding-top: 0px; padding-bottom: 2px;font-size: 14px; font-weight: bold; height: 20px; min-height: 20px; max-height: 20px; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;' colspan=5>COTIZACIONES POR DEPARTAMENTO</td>
	        </tr>
	        <tr>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'></td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>SALA</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>MAIL</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>OTRO</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>TOTAL</td>
	        </tr>";
	          for($i = $z1; $i < $z2; $i++){
							$d = array(0,0,0);
							if(array_key_exists('Sala',$datosInforme3[$i]))
							{
								$d[0] = $datosInforme3[$i]['Sala'];
							}
							if(array_key_exists('Mail',$datosInforme3[$i]))
							{
								$d[1] = $datosInforme3[$i]['Mail'];
							}
							if(array_key_exists('Otro',$datosInforme3[$i]))
							{
								$d[2] = $datosInforme3[$i]['Otro'];
							}
							if($datosInforme3[$i]['ESTADO'] == 4){
								echo '<tr style="background-color: #e0e0e0;">';
							}
							else{
								echo '<tr>';
							}
							echo "<td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $datosInforme3[$i]['CODIGO'] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[0] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[1] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[2] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . ($d[0] + $d[1] + $d[2]) . "</td>
	            </tr>";
	          }
	      echo "</table>
	    </td>
	    <td style='max-width: 480px; min-width: 480px; width: 480px; border-spacing: 0 0; border-collapse : collapse; border: 1px solid black;'>";?>
	      <?php
	        $mi_curl = curl_init ($url . 'controller/informesGenerales/gra4.php?accion=' . $accion . '&mes=' . $mes . '&ano=' . $ano . '&codigoProyecto=' . $codigoProyecto . '&ciclo=2');
	        $fs_archivo = fopen ($document . 'controller/informesGenerales/gra4b.png', 'w');
	        curl_setopt ($mi_curl, CURLOPT_FILE, $fs_archivo);
	        curl_setopt ($mi_curl, CURLOPT_HEADER, 0);
	        curl_exec ($mi_curl);
	        curl_close ($mi_curl);
	        fclose ($fs_archivo);
	        echo "<img src='gra4b.png' style='width: 100%;'>";
	      ?>
	    <?php echo "</td>
	  </tr>
	</table>
	<page_footer>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
  </page_footer></page>";
}
?>
<?php
$z3 = $z2 + 29;
if($z3 > count($datosInforme3)){
	$z3 = count($datosInforme3);
}
if($z2 < count($datosInforme3)){
	echo "<page>
	<table style='margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse;'>
	        <tr>
	          <td style='padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 948px; min-width: 948px; width: 948px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;'>LIVING NET - Informe de Gestión - Proyecto $nombreProyecto </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	  <tr style='border-spacing: 0 0; border-collapse : collapse;'>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;'>
	        <tr>
	          <td style='padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;'>Correspondiente a $mesTexto de $ano </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>
	<table style='margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 450px; min-width: 450px; width: 450px;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; text-align: center;'>
	        <tr>
	          <td style='padding-top: 0px; padding-bottom: 2px;font-size: 14px; font-weight: bold; height: 20px; min-height: 20px; max-height: 20px; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;' colspan=5>COTIZACIONES POR DEPARTAMENTO</td>
	        </tr>
	        <tr>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'></td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>SALA</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>MAIL</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>OTRO</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>TOTAL</td>
	        </tr>";
	          for($i = $z2; $i < $z3; $i++){
							$d = array(0,0,0);
							if(array_key_exists('Sala',$datosInforme3[$i]))
							{
								$d[0] = $datosInforme3[$i]['Sala'];
							}
							if(array_key_exists('Mail',$datosInforme3[$i]))
							{
								$d[1] = $datosInforme3[$i]['Mail'];
							}
							if(array_key_exists('Otro',$datosInforme3[$i]))
							{
								$d[2] = $datosInforme3[$i]['Otro'];
							}
							if($datosInforme3[$i]['ESTADO'] == 4){
								echo '<tr style="background-color: #e0e0e0;">';
							}
							else{
								echo '<tr>';
							}
							echo "<td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $datosInforme3[$i]['CODIGO'] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[0]. "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[1] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[2] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . ($d[0] + $d[1] + $d[2]) . "</td>
	            </tr>";
	          }
	      echo "</table>
	    </td>
	    <td style='max-width: 480px; min-width: 480px; width: 480px; border-spacing: 0 0; border-collapse : collapse; border: 1px solid black;'>";?>
	      <?php
	        $mi_curl = curl_init ($url . 'controller/informesGenerales/gra4.php?accion=' . $accion . '&mes=' . $mes . '&ano=' . $ano . '&codigoProyecto=' . $codigoProyecto . '&ciclo=3');
	        $fs_archivo = fopen ($document . 'controller/informesGenerales/gra4c.png', 'w');
	        curl_setopt ($mi_curl, CURLOPT_FILE, $fs_archivo);
	        curl_setopt ($mi_curl, CURLOPT_HEADER, 0);
	        curl_exec ($mi_curl);
	        curl_close ($mi_curl);
	        fclose ($fs_archivo);
	        echo "<img src='gra4c.png' style='width: 100%;'>";
	      ?>
	    <?php echo "</td>
	  </tr>
	</table>
	<page_footer>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
  </page_footer></page>";
}
?>
<?php
$z4 = $z3 + 29;
if($z4 > count($datosInforme3)){
	$z4 = count($datosInforme3);
}
if($z3 < count($datosInforme3)){
	echo "<page>
	<table style='margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse;'>
	        <tr>
	          <td style='padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 948px; min-width: 948px; width: 948px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;'>LIVING NET - Informe de Gestión - Proyecto $nombreProyecto </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	  <tr style='border-spacing: 0 0; border-collapse : collapse;'>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;'>
	        <tr>
	          <td style='padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;'>Correspondiente a $mesTexto de $ano </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>
	<table style='margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 450px; min-width: 450px; width: 450px;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; text-align: center;'>
	        <tr>
	          <td style='padding-top: 0px; padding-bottom: 2px;font-size: 14px; font-weight: bold; height: 20px; min-height: 20px; max-height: 20px; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;' colspan=5>COTIZACIONES POR DEPARTAMENTO</td>
	        </tr>
	        <tr>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'></td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>SALA</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>MAIL</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>OTRO</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>TOTAL</td>
	        </tr>";
	          for($i = $z3; $i < $z4; $i++){
							$d = array(0,0,0);
							if(array_key_exists('Sala',$datosInforme3[$i]))
							{
								$d[0] = $datosInforme3[$i]['Sala'];
							}
							if(array_key_exists('Mail',$datosInforme3[$i]))
							{
								$d[1] = $datosInforme3[$i]['Mail'];
							}
							if(array_key_exists('Otro',$datosInforme3[$i]))
							{
								$d[2] = $datosInforme3[$i]['Otro'];
							}
							if($datosInforme3[$i]['ESTADO'] == 4){
								echo '<tr style="background-color: #e0e0e0;">';
							}
							else{
								echo '<tr>';
							}
							echo "<td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $datosInforme3[$i]['CODIGO'] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[0] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[1] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[2] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . ($d[0] + $d[1] + $d[2]) . "</td>
	            </tr>";
	          }
	      echo "</table>
	    </td>
	    <td style='max-width: 480px; min-width: 480px; width: 480px; border-spacing: 0 0; border-collapse : collapse; border: 1px solid black;'>";?>
	      <?php
	        $mi_curl = curl_init ($url . 'controller/informesGenerales/gra4.php?accion=' . $accion . '&mes=' . $mes . '&ano=' . $ano . '&codigoProyecto=' . $codigoProyecto . '&ciclo=4');
	        $fs_archivo = fopen ($document . 'controller/informesGenerales/gra4d.png', 'w');
	        curl_setopt ($mi_curl, CURLOPT_FILE, $fs_archivo);
	        curl_setopt ($mi_curl, CURLOPT_HEADER, 0);
	        curl_exec ($mi_curl);
	        curl_close ($mi_curl);
	        fclose ($fs_archivo);
	        echo "<img src='gra4d.png' style='width: 100%;'>";
	      ?>
	    <?php echo "</td>
	  </tr>
	</table>
	<page_footer>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
  </page_footer></page>";
}
?>
<?php
$z5 = $z4 + 29;
if($z5 > count($datosInforme3)){
	$z5 = count($datosInforme3);
}
if($z4 < count($datosInforme3)){
	echo "<page>
	<table style='margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse;'>
	        <tr>
	          <td style='padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 948px; min-width: 948px; width: 948px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;'>LIVING NET - Informe de Gestión - Proyecto $nombreProyecto </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	  <tr style='border-spacing: 0 0; border-collapse : collapse;'>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;'>
	        <tr>
	          <td style='padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;'>Correspondiente a $mesTexto de $ano </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>
	<table style='margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 450px; min-width: 450px; width: 450px;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; text-align: center;'>
	        <tr>
	          <td style='padding-top: 0px; padding-bottom: 2px;font-size: 14px; font-weight: bold; height: 20px; min-height: 20px; max-height: 20px; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;' colspan=5>COTIZACIONES POR DEPARTAMENTO</td>
	        </tr>
	        <tr>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'></td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>SALA</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>MAIL</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>OTRO</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>TOTAL</td>
	        </tr>";
	          for($i = $z4; $i < $z5; $i++){
							$d = array(0,0,0);
							if(array_key_exists('Sala',$datosInforme3[$i]))
							{
								$d[0] = $datosInforme3[$i]['Sala'];
							}
							if(array_key_exists('Mail',$datosInforme3[$i]))
							{
								$d[1] = $datosInforme3[$i]['Mail'];
							}
							if(array_key_exists('Otro',$datosInforme3[$i]))
							{
								$d[2] = $datosInforme3[$i]['Otro'];
							}
							if($datosInforme3[$i]['ESTADO'] == 4){
								echo '<tr style="background-color: #e0e0e0;">';
							}
							else{
								echo '<tr>';
							}
							echo "<td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $datosInforme3[$i]['CODIGO'] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[0] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[1] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[2] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . ($d[0] + $d[1] + $d[2]) . "</td>
	            </tr>";
	          }
	      echo "</table>
	    </td>
	    <td style='max-width: 480px; min-width: 480px; width: 480px; border-spacing: 0 0; border-collapse : collapse; border: 1px solid black;'>";?>
	      <?php
	        $mi_curl = curl_init ($url . 'controller/informesGenerales/gra4.php?accion=' . $accion . '&mes=' . $mes . '&ano=' . $ano . '&codigoProyecto=' . $codigoProyecto . '&ciclo=5');
	        $fs_archivo = fopen ($document . 'controller/informesGenerales/gra4e.png', 'w');
	        curl_setopt ($mi_curl, CURLOPT_FILE, $fs_archivo);
	        curl_setopt ($mi_curl, CURLOPT_HEADER, 0);
	        curl_exec ($mi_curl);
	        curl_close ($mi_curl);
	        fclose ($fs_archivo);
	        echo "<img src='gra4e.png' style='width: 100%;'>";
	      ?>
	    <?php echo "</td>
	  </tr>
	</table>
	<page_footer>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
  </page_footer></page>";
}
?>
<?php
$z6 = $z5 + 29;
if($z6 > count($datosInforme3)){
	$z6 = count($datosInforme3);
}
if($z5 < count($datosInforme3)){
	echo "<page>
	<table style='margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse;'>
	        <tr>
	          <td style='padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 948px; min-width: 948px; width: 948px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;'>LIVING NET - Informe de Gestión - Proyecto $nombreProyecto </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	  <tr style='border-spacing: 0 0; border-collapse : collapse;'>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;'>
	        <tr>
	          <td style='padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;'>Correspondiente a $mesTexto de $ano </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>
	<table style='margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 450px; min-width: 450px; width: 450px;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; text-align: center;'>
	        <tr>
	          <td style='padding-top: 0px; padding-bottom: 2px;font-size: 14px; font-weight: bold; height: 20px; min-height: 20px; max-height: 20px; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;' colspan=5>COTIZACIONES POR DEPARTAMENTO</td>
	        </tr>
	        <tr>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'></td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>SALA</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>MAIL</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>OTRO</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>TOTAL</td>
	        </tr>";
	          for($i = $z5; $i < $z6; $i++){
							$d = array(0,0,0);
							if(array_key_exists('Sala',$datosInforme3[$i]))
							{
								$d[0] = $datosInforme3[$i]['Sala'];
							}
							if(array_key_exists('Mail',$datosInforme3[$i]))
							{
								$d[1] = $datosInforme3[$i]['Mail'];
							}
							if(array_key_exists('Otro',$datosInforme3[$i]))
							{
								$d[2] = $datosInforme3[$i]['Otro'];
							}
							if($datosInforme3[$i]['ESTADO'] == 4){
								echo '<tr style="background-color: #e0e0e0;">';
							}
							else{
								echo '<tr>';
							}
							echo "<td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $datosInforme3[$i]['CODIGO'] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[0] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[1] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[2] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . ($d[0] + $d[1] + $d[2]) . "</td>
	            </tr>";
	          }
	      echo "</table>
	    </td>
	    <td style='max-width: 480px; min-width: 480px; width: 480px; border-spacing: 0 0; border-collapse : collapse; border: 1px solid black;'>";?>
	      <?php
	        $mi_curl = curl_init ($url . 'controller/informesGenerales/gra4.php?accion=' . $accion . '&mes=' . $mes . '&ano=' . $ano . '&codigoProyecto=' . $codigoProyecto . '&ciclo=6');
	        $fs_archivo = fopen ($document . 'controller/informesGenerales/gra4f.png', 'w');
	        curl_setopt ($mi_curl, CURLOPT_FILE, $fs_archivo);
	        curl_setopt ($mi_curl, CURLOPT_HEADER, 0);
	        curl_exec ($mi_curl);
	        curl_close ($mi_curl);
	        fclose ($fs_archivo);
	        echo "<img src='gra4f.png' style='width: 100%;'>";
	      ?>
	    <?php echo "</td>
	  </tr>
	</table>
	<page_footer>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
  </page_footer></page>";
}
?>
<?php
$z7 = $z6 + 29;
if($z7 > count($datosInforme3)){
	$z7 = count($datosInforme3);
}
if($z6 < count($datosInforme3)){
	echo "<page>
	<table style='margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse;'>
	        <tr>
	          <td style='padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 948px; min-width: 948px; width: 948px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;'>LIVING NET - Informe de Gestión - Proyecto $nombreProyecto </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	  <tr style='border-spacing: 0 0; border-collapse : collapse;'>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;'>
	        <tr>
	          <td style='padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;'>Correspondiente a $mesTexto de $ano </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>
	<table style='margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 450px; min-width: 450px; width: 450px;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; text-align: center;'>
	        <tr>
	          <td style='padding-top: 0px; padding-bottom: 2px;font-size: 14px; font-weight: bold; height: 20px; min-height: 20px; max-height: 20px; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;' colspan=5>COTIZACIONES POR DEPARTAMENTO</td>
	        </tr>
	        <tr>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'></td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>SALA</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>MAIL</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>OTRO</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>TOTAL</td>
	        </tr>";
	          for($i = $z6; $i < $z7; $i++){
							$d = array(0,0,0);
							if(array_key_exists('Sala',$datosInforme3[$i]))
							{
								$d[0] = $datosInforme3[$i]['Sala'];
							}
							if(array_key_exists('Mail',$datosInforme3[$i]))
							{
								$d[1] = $datosInforme3[$i]['Mail'];
							}
							if(array_key_exists('Otro',$datosInforme3[$i]))
							{
								$d[2] = $datosInforme3[$i]['Otro'];
							}
							if($datosInforme3[$i]['ESTADO'] == 4){
								echo '<tr style="background-color: #e0e0e0;">';
							}
							else{
								echo '<tr>';
							}
							echo "<td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $datosInforme3[$i]['CODIGO'] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[0] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[1] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[2] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . ($d[0] + $d[1] + $d[2]) . "</td>
	            </tr>";
	          }
	      echo "</table>
	    </td>
	    <td style='max-width: 480px; min-width: 480px; width: 480px; border-spacing: 0 0; border-collapse : collapse; border: 1px solid black;'>";?>
	      <?php
	        $mi_curl = curl_init ($url . 'controller/informesGenerales/gra4.php?accion=' . $accion . '&mes=' . $mes . '&ano=' . $ano . '&codigoProyecto=' . $codigoProyecto . '&ciclo=7');
	        $fs_archivo = fopen ($document . 'controller/informesGenerales/gra4g.png', 'w');
	        curl_setopt ($mi_curl, CURLOPT_FILE, $fs_archivo);
	        curl_setopt ($mi_curl, CURLOPT_HEADER, 0);
	        curl_exec ($mi_curl);
	        curl_close ($mi_curl);
	        fclose ($fs_archivo);
	        echo "<img src='gra4g.png' style='width: 100%;'>";
	      ?>
	    <?php echo "</td>
	  </tr>
	</table>
	<page_footer>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
  </page_footer></page>";
}
?>
<?php
$z8 = $z7 + 29;
if($z8 > count($datosInforme3)){
	$z8 = count($datosInforme3);
}
if($z7 < count($datosInforme3)){
	echo "<page>
	<table style='margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse;'>
	        <tr>
	          <td style='padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 948px; min-width: 948px; width: 948px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;'>LIVING NET - Informe de Gestión - Proyecto $nombreProyecto </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	  <tr style='border-spacing: 0 0; border-collapse : collapse;'>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;'>
	        <tr>
	          <td style='padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;'>Correspondiente a $mesTexto de $ano </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>
	<table style='margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 450px; min-width: 450px; width: 450px;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; text-align: center;'>
	        <tr>
	          <td style='padding-top: 0px; padding-bottom: 2px;font-size: 14px; font-weight: bold; height: 20px; min-height: 20px; max-height: 20px; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;' colspan=5>COTIZACIONES POR DEPARTAMENTO</td>
	        </tr>
	        <tr>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'></td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>SALA</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>MAIL</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>OTRO</td>
	          <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;'>TOTAL</td>
	        </tr>";
	          for($i = $z7; $i < $z8; $i++){
							$d = array(0,0,0);
							if(array_key_exists('Sala',$datosInforme3[$i]))
							{
								$d[0] = $datosInforme3[$i]['Sala'];
							}
							if(array_key_exists('Mail',$datosInforme3[$i]))
							{
								$d[1] = $datosInforme3[$i]['Mail'];
							}
							if(array_key_exists('Otro',$datosInforme3[$i]))
							{
								$d[2] = $datosInforme3[$i]['Otro'];
							}
							if($datosInforme3[$i]['ESTADO'] == 4){
								echo '<tr style="background-color: #e0e0e0;">';
							}
							else{
								echo '<tr>';
							}
							echo "<td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $datosInforme3[$i]['CODIGO'] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[0] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[1]. "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[2] . "</td>
	              <td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . ($d[0] + $d[1] + $d[2]) . "</td>
	            </tr>";
	          }
	      echo "</table>
	    </td>
	    <td style='max-width: 480px; min-width: 480px; width: 480px; border-spacing: 0 0; border-collapse : collapse; border: 1px solid black;'>";?>
	      <?php
	        $mi_curl = curl_init ($url . 'controller/informesGenerales/gra4.php?accion=' . $accion . '&mes=' . $mes . '&ano=' . $ano . '&codigoProyecto=' . $codigoProyecto . '&ciclo=8');
	        $fs_archivo = fopen ($document . 'controller/informesGenerales/gra4h.png', 'w');
	        curl_setopt ($mi_curl, CURLOPT_FILE, $fs_archivo);
	        curl_setopt ($mi_curl, CURLOPT_HEADER, 0);
	        curl_exec ($mi_curl);
	        curl_close ($mi_curl);
	        fclose ($fs_archivo);
	        echo "<img src='gra4h.png' style='width: 100%;'>";
	      ?>
	    <?php echo "</td>
	  </tr>
	</table>
	";
}
?>
<?php
$d = array(0,0,0);
for($i = 0; $i < count($datosInforme3); $i++){
	if(array_key_exists('Sala',$datosInforme3[$i]))
	{
		$d[0] = $d[0] + $datosInforme3[$i]['Sala'];
	}
	if(array_key_exists('Mail',$datosInforme3[$i]))
	{
		$d[1] = $d[1] + $datosInforme3[$i]['Mail'];
	}
	if(array_key_exists('Otro',$datosInforme3[$i]))
	{
		$d[2] = $d[2] + $datosInforme3[$i]['Otro'];
	}
}
echo
"<table style='margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	<tr>
		<td style='max-width: 450px; min-width: 450px; width: 450px;border-spacing: 0 0; border-collapse : collapse;'>
			<table style='border-spacing: 0 0; border-collapse : collapse; text-align: center;'>
			<tr>
				<td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black; background-color: #d8d8d8; font-weight: bold;'>Total</td>
				<td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[0] . "</td>
				<td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[1] . "</td>
				<td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . $d[2] . "</td>
				<td style='padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;'>" . ($d[0] + $d[1] + $d[2]) . "</td>
			</tr>
			</table>
		</td>
	</tr>
</table>";
?>
<table style="margin-top: 3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 431px; min-width: 431px; width: 431px;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse; text-align: center;">
				<tr>
					<td style="padding-top: 0px; padding-bottom: 2px;font-size: 14px; font-weight: bold; height: 20px; min-height: 20px; max-height: 20px; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;" colspan=2>COTIZACIONES POR VENDEDOR</td>
				</tr>
				<tr>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 331px; min-width: 331px; width: 331px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;">POR VENDEDOR</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 100px; min-width: 100px; width: 100px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;background-color: #d8d8d8; font-weight: bold;">CANTIDAD</td>
				</tr>
				<?php
					$totalPorVendedor = 0;
					for($i = 0; $i < count($datosPorVendedor); $i++){
						$totalPorVendedor = $totalPorVendedor + $datosPorVendedor[$i][1];
						echo '<tr>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 331px; min-width: 331px; width: 331px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;font-weight: bold;">' . $datosPorVendedor[$i][0]. '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 100px; min-width: 100px; width: 100px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;">' . $datosPorVendedor[$i][1]. '</td>
						</tr>';
					}
					echo '<tr>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 331px; min-width: 331px; width: 331px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;font-weight: bold; background-color: #d8d8d8; font-weight: bold;">Total</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 2px;font-size: 12px;  max-width: 100px; min-width: 100px; width: 100px; text-align: center; border-spacing: 0 0; border-collapse : collapse;border: 1px solid black;">' . $totalPorVendedor . '</td>
					</tr>';
				?>
			</table>
		</td>
	</tr>
</table>
<!-- Hoja Nueva -->
<page>
<table style="margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr>
					<td style="padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">LIVING NET - Informe de Gestión - Proyecto <?php echo $nombreProyecto ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr style="border-spacing: 0 0; border-collapse : collapse;">
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
				<tr>
					<td style="padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;">Correspondiente a <?php echo $mesTexto; ?> de <?php echo $ano ?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table style="margin-top: 0px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr style="border-spacing: 0 0; border-collapse : collapse;">
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
				<tr>
					<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">RESERVAS POR MES</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr style="background-color: #d8d8d8;font-weight: bold;">
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 91px; min-width: 91px; width: 91px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[0]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[1]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[2]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[3]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[4]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[5]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[6]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[7]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[8]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[9]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[10]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[11]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Totales</td>
				</tr>
			</table>
		</td>
	</tr>
	<?php
	$suma = array(0,0,0,0,0,0,0,0,0,0,0,0,0);
	$tope = 0;
	if(count($datosInforme1Reservas) < 1){
		$tope = 1;
	}
	else{
		$tope = count($datosInforme1Reservas);
	}
	for($i = 0; $i < $tope; $i++){
		$total = 0;
		$a = array(0,0,0,0,0,0,0,0,0,0,0,0,0);
		for($j = 1; $j < count($datosInforme1Reservas[$i]); $j++){
			$total = $total + $datosInforme1Reservas[$i][$j];
			if(array_key_exists($fechasRangoIndex[$j-1],$datosInforme1Reservas[$i])){
				$suma[$j] = $suma[$j] + $datosInforme1Reservas[$i][$fechasRangoIndex[$j-1]];
			}
			else{
				$suma[$j] = $suma[$j] + 0;
			}
		}
		$suma[13] = $suma[13] + $total;
		echo '<tr>
			<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
				<table style="margin-top: -3px; border-spacing: 0 0; border-collapse : collapse;">
					<tr>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 91px; min-width: 91px; width: 91px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><b>Totales</b></td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[0],$datosInforme1Reservas[$i])){
								echo $datosInforme1Reservas[$i][$fechasRangoIndex[0]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[1],$datosInforme1Reservas[$i])){
								echo $datosInforme1Reservas[$i][$fechasRangoIndex[1]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[2],$datosInforme1Reservas[$i])){
								echo $datosInforme1Reservas[$i][$fechasRangoIndex[2]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[3],$datosInforme1Reservas[$i])){
								echo $datosInforme1Reservas[$i][$fechasRangoIndex[3]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[4],$datosInforme1Reservas[$i])){
								echo $datosInforme1Reservas[$i][$fechasRangoIndex[4]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[5],$datosInforme1Reservas[$i])){
								echo $datosInforme1Reservas[$i][$fechasRangoIndex[5]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[6],$datosInforme1Reservas[$i])){
								echo $datosInforme1Reservas[$i][$fechasRangoIndex[6]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[7],$datosInforme1Reservas[$i])){
								echo $datosInforme1Reservas[$i][$fechasRangoIndex[7]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[8],$datosInforme1Reservas[$i])){
								echo $datosInforme1Reservas[$i][$fechasRangoIndex[8]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[9],$datosInforme1Reservas[$i])){
								echo $datosInforme1Reservas[$i][$fechasRangoIndex[9]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[10],$datosInforme1Reservas[$i])){
								echo $datosInforme1Reservas[$i][$fechasRangoIndex[10]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[11],$datosInforme1Reservas[$i])){
								echo $datosInforme1Reservas[$i][$fechasRangoIndex[11]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><b>' . $total . '</b></td>
					</tr>
				</table>
			</td>
		</tr>';
	}
	?>
</table>

<table style="margin-top: 2px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr>
					<td style="padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">
						<img src="gra5.png" style="width: 100%;">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<page_footer>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
  </page_footer></page>
<!-- Hoja nueva detalle reservas -->
<page>
	<table style="margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr>
			<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse;">
					<tr>
						<td style="padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">LIVING NET - Informe de Gestión - Proyecto <?php echo $nombreProyecto ?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style="border-spacing: 0 0; border-collapse : collapse;">
			<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
					<tr>
						<td style="padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;">Correspondiente a <?php echo $mesTexto; ?> de <?php echo $ano ?></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table style="margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr style="border-spacing: 0 0; border-collapse : collapse;">
			<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
					<tr>
						<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">RESERVAS</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<?php
	echo '<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr>
			<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse;">
					<tr style="background-color: #d8d8d8;font-weight: bold;">
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nro</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 50px; min-width: 50px; width: 50px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Fecha</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 30px; min-width: 30px; width: 30px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Dto</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 80px; min-width: 80px; width: 80px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Estacionamiento</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 80px; min-width: 80px; width: 80px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Bodega</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 30px; min-width: 30px; width: 30px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">UF</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Apellido</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nombre</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Celular</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 213px; min-width: 213px; width: 213px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Mail</td>
					</tr>';
					$y1 = 0 + 27;
					if($y1 > count($datosInforme1ReservasDetalle)){
						$y1 = count($datosInforme1ReservasDetalle);
					}
					for($i = 0; $i < $y1; $i++){
						if(strpos($datosInforme1ReservasDetalle[$i][1], $fechaCompara[11])){
							echo '<tr style="background-color: #e0e0e0;">';
						}
						else{
							echo '<tr>';
						}
						echo '<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][0] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 50px; min-width: 50px; width: 50px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][1] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 30px; min-width: 30px; width: 30px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][2] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 80px; min-width: 80px; width: 80px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][3] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 80px; min-width: 80px; width: 80px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][4] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 30px; min-width: 30px; width: 30px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][5] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][6] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][7] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][8] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 213px; min-width: 213px; width: 213px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse; word-wrap: break-word;">' .$datosInforme1ReservasDetalle[$i][9] . '</td>
						</tr>';
					}
				echo '</table>
			</td>
		</tr>
	</table>
	<page_footer>
	    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
	  </page_footer></page>';
?>
<?php
$y2 = $y1 + 27;
if($y2 > count($datosInforme1ReservasDetalle)){
	$y2 = count($datosInforme1ReservasDetalle);
}
if($y1 < count($datosInforme1ReservasDetalle)){
	echo "<page><table style='margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse;'>
	        <tr>
	          <td style='padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;'>LIVING NET - Informe de Gestión - Proyecto $nombreProyecto </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	  <tr style='border-spacing: 0 0; border-collapse : collapse;'>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;'>
	        <tr>
	          <td style='padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;'>Correspondiente a $mesTexto de $ano </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>";
	echo '
		<table style="margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr style="border-spacing: 0 0; border-collapse : collapse;">
			<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
					<tr>
						<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">RESERVAS</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr>
			<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse;">
					<tr style="background-color: #d8d8d8;font-weight: bold;">
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nro</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 50px; min-width: 50px; width: 50px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Fecha</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 30px; min-width: 30px; width: 30px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Dto</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 80px; min-width: 80px; width: 80px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Estacionamiento</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 80px; min-width: 80px; width: 80px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Bodega</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 30px; min-width: 30px; width: 30px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">UF</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Apellido</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nombre</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Celular</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 213px; min-width: 213px; width: 213px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Mail</td>
					</tr>';
					for($i = $y1; $i < $y2; $i++){
						if(strpos($datosInforme1ReservasDetalle[$i][1], $fechaCompara[11])){
							echo '<tr style="background-color: #e0e0e0;">';
						}
						else{
							echo '<tr>';
						}
						echo '<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][0] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 50px; min-width: 50px; width: 50px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][1] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 30px; min-width: 30px; width: 30px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][2] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 80px; min-width: 80px; width: 80px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][3] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 80px; min-width: 80px; width: 80px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][4] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 30px; min-width: 30px; width: 30px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][5] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][6] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][7] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][8] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 213px; min-width: 213px; width: 213px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse; word-wrap: break-word;">' .$datosInforme1ReservasDetalle[$i][9] . '</td>
						</tr>';
					}
					echo '</table>
				</td>
			</tr>
		</table>
		<page_footer>
		    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
		  </page_footer></page>';
}
?>
<?php
$y3 = $y2 + 27;
if($y3 > count($datosInforme1ReservasDetalle)){
	$y3 = count($datosInforme1ReservasDetalle);
}
if($y2 < count($datosInforme1ReservasDetalle)){
	echo "<page><table style='margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse;'>
	        <tr>
	          <td style='padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;'>LIVING NET - Informe de Gestión - Proyecto $nombreProyecto </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	  <tr style='border-spacing: 0 0; border-collapse : collapse;'>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;'>
	        <tr>
	          <td style='padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;'>Correspondiente a $mesTexto de $ano </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>";
	echo '
		<table style="margin-top: -3x; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr style="border-spacing: 0 0; border-collapse : collapse;">
			<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
					<tr>
						<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">RESERVAS</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr>
			<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse;">
					<tr style="background-color: #d8d8d8;font-weight: bold;">
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nro</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 50px; min-width: 50px; width: 50px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Fecha</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 30px; min-width: 30px; width: 30px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Dto</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 80px; min-width: 80px; width: 80px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Estacionamiento</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 80px; min-width: 80px; width: 80px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Bodega</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 30px; min-width: 30px; width: 30px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">UF</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Apellido</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nombre</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Celular</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 213px; min-width: 213px; width: 213px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Mail</td>
					</tr>';
					for($i = $y2; $i < $y3; $i++){
						if(strpos($datosInforme1ReservasDetalle[$i][1], $fechaCompara[11])){
							echo '<tr style="background-color: #e0e0e0;">';
						}
						else{
							echo '<tr>';
						}
						echo '<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][0] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 50px; min-width: 50px; width: 50px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][1] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 30px; min-width: 30px; width: 30px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][2] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 80px; min-width: 80px; width: 80px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][3] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 80px; min-width: 80px; width: 80px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][4] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 30px; min-width: 30px; width: 30px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][5] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][6] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][7] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][8] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 213px; min-width: 213px; width: 213px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse; word-wrap: break-word;">' .$datosInforme1ReservasDetalle[$i][9] . '</td>
						</tr>';
					}
					echo '</table>
				</td>
			</tr>
		</table>
		<page_footer>
		    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
		  </page_footer></page>';
}
?>
<?php
$y4 = $y3 + 27;
if($y4 > count($datosInforme1ReservasDetalle)){
	$y4 = count($datosInforme1ReservasDetalle);
}
if($y3 < count($datosInforme1ReservasDetalle)){
	echo "<page><table style='margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse;'>
	        <tr>
	          <td style='padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;'>LIVING NET - Informe de Gestión - Proyecto $nombreProyecto </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	  <tr style='border-spacing: 0 0; border-collapse : collapse;'>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;'>
	        <tr>
	          <td style='padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;'>Correspondiente a $mesTexto de $ano </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>";
	echo '
		<table style="margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr style="border-spacing: 0 0; border-collapse : collapse;">
			<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
					<tr>
						<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">RESERVAS</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr>
			<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse;">
					<tr style="background-color: #d8d8d8;font-weight: bold;">
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nro</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 50px; min-width: 50px; width: 50px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Fecha</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 30px; min-width: 30px; width: 30px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Dto</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 80px; min-width: 80px; width: 80px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Estacionamiento</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 80px; min-width: 80px; width: 80px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Bodega</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 30px; min-width: 30px; width: 30px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">UF</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Apellido</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nombre</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Celular</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 213px; min-width: 213px; width: 213px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Mail</td>
					</tr>';
					for($i = $y3; $i < $y4; $i++){
						if(strpos($datosInforme1ReservasDetalle[$i][1], $fechaCompara[11])){
							echo '<tr style="background-color: #e0e0e0;">';
						}
						else{
							echo '<tr>';
						}
						echo '<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][0] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 50px; min-width: 50px; width: 50px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][1] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 30px; min-width: 30px; width: 30px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][2] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 80px; min-width: 80px; width: 80px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][3] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 80px; min-width: 80px; width: 80px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][4] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 30px; min-width: 30px; width: 30px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][5] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][6] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][7] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1ReservasDetalle[$i][8] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 213px; min-width: 213px; width: 213px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse; word-wrap: break-word;">' .$datosInforme1ReservasDetalle[$i][9] . '</td>
						</tr>';
					}
					echo '</table>
				</td>
			</tr>
		</table>
		<page_footer>
		    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
		  </page_footer></page>';
}
?>
<!-- Promesas -->
<page>
<table style="margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr>
					<td style="padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">LIVING NET - Informe de Gestión - Proyecto <?php echo $nombreProyecto ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr style="border-spacing: 0 0; border-collapse : collapse;">
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
				<tr>
					<td style="padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;">Correspondiente a <?php echo $mesTexto; ?> de <?php echo $ano ?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table style="margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr style="border-spacing: 0 0; border-collapse : collapse;">
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
				<tr>
					<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">PROMESAS POR MES</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr style="background-color: #d8d8d8;font-weight: bold;">
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 91px; min-width: 91px; width: 91px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[0]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[1]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[2]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[3]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[4]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[5]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[6]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[7]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[8]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[9]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[10]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[11]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Totales</td>
				</tr>
			</table>
		</td>
	</tr>
	<?php
	$suma = array(0,0,0,0,0,0,0,0,0,0,0,0,0);
	$tope = 0;
	if(count($datosInforme1Promesas) < 1){
		$tope = 1;
	}
	else{
		$tope = count($datosInforme1Promesas);
	}
	for($i = 0; $i < $tope; $i++){
		$total = 0;
		$a = array(0,0,0,0,0,0,0,0,0,0,0,0,0);
		for($j = 1; $j < count($datosInforme1Promesas[$i]); $j++){
			$total = $total + $datosInforme1Promesas[$i][$j];
			$a[$j] = 0 + $datosInforme1Promesas[$i][$j];
			$suma[$j] = $suma[$j] + $datosInforme1Promesas[$i][$j];
		}
		$suma[13] = $suma[13] + $total;
		echo '<tr>
			<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
				<table style="margin-top: -3px; border-spacing: 0 0; border-collapse : collapse;">
					<tr>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 91px; min-width: 91px; width: 91px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><b>Totales</b></td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[0],$datosInforme1Promesas[$i])){
								echo $datosInforme1Promesas[$i][$fechasRangoIndex[0]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[1],$datosInforme1Promesas[$i])){
								echo $datosInforme1Promesas[$i][$fechasRangoIndex[1]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[2],$datosInforme1Promesas[$i])){
								echo $datosInforme1Promesas[$i][$fechasRangoIndex[2]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[3],$datosInforme1Promesas[$i])){
								echo $datosInforme1Promesas[$i][$fechasRangoIndex[3]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[4],$datosInforme1Promesas[$i])){
								echo $datosInforme1Promesas[$i][$fechasRangoIndex[4]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[5],$datosInforme1Promesas[$i])){
								echo $datosInforme1Promesas[$i][$fechasRangoIndex[5]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[6],$datosInforme1Promesas[$i])){
								echo $datosInforme1Promesas[$i][$fechasRangoIndex[6]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[7],$datosInforme1Promesas[$i])){
								echo $datosInforme1Promesas[$i][$fechasRangoIndex[7]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[8],$datosInforme1Promesas[$i])){
								echo $datosInforme1Promesas[$i][$fechasRangoIndex[8]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[9],$datosInforme1Promesas[$i])){
								echo $datosInforme1Promesas[$i][$fechasRangoIndex[9]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[10],$datosInforme1Promesas[$i])){
								echo $datosInforme1Promesas[$i][$fechasRangoIndex[10]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[11],$datosInforme1Promesas[$i])){
								echo $datosInforme1Promesas[$i][$fechasRangoIndex[11]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><b>' . $total . '</b></td>
					</tr>
				</table>
			</td>
		</tr>';
	}
	?>
</table>
<table style="margin-top: 2px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr>
					<td style="padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">
						<img src="gra6.png" style="width: 100%;">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<page_footer>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
	</page_footer></page>
<page>
	<table style="margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr>
			<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse;">
					<tr>
						<td style="padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">LIVING NET - Informe de Gestión - Proyecto <?php echo $nombreProyecto ?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style="border-spacing: 0 0; border-collapse : collapse;">
			<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
					<tr>
						<td style="padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;">Correspondiente a <?php echo $mesTexto; ?> de <?php echo $ano ?></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table style="margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr style="border-spacing: 0 0; border-collapse : collapse;">
			<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
					<tr>
						<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">PROMESAS</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<?php
echo '<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr style="font-size: 10px;">
		<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr style="background-color: #d8d8d8;font-weight: bold;">
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nro</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 40px; min-width: 40px; width: 40px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Fecha</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Dto</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Estac</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Bodega</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">UF</td>
					<td style="padding-left: 2px; padding-right: 2px; padding-top: 4px; padding-bottom: 4px;  max-width: 36px; min-width: 36px; width: 36px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Res+Pie</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Saldo</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Apellido</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nombre</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 45px; min-width: 45px; width: 45px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Celular</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 204px; min-width: 204px; width: 204px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Mail</td>
				</tr>';
				$y1 = 0 + 27;
				if($y1 > count($datosInforme1PromesasDetalle)){
					$y1 = count($datosInforme1PromesasDetalle);
				}
				for($i = 0; $i < $y1; $i++){
					if(strpos($datosInforme1PromesasDetalle[$i][1], $fechaCompara[11])){
						echo '<tr style="background-color: #e0e0e0;">';
					}
					else{
						echo '<tr>';
					}
					echo '<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][0] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 40px; min-width: 40px; width: 40px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][1] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][2] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][3] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][4] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][5] . '</td>
						<td style="padding-left: 2px; padding-right: 2px; padding-top: 4px; padding-bottom: 4px;  max-width: 36px; min-width: 36px; width: 36px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][6] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][7] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][8] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][9] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 45px; min-width: 45px; width: 45px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][10] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 204px; min-width: 204px; width: 204px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse; word-wrap: break-word;">' .$datosInforme1PromesasDetalle[$i][11] . '</td>
					</tr>';
				}
			echo '</table>
		</td>
	</tr>
</table>
<page_footer>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
	</page_footer></page>';
?>
<?php
$y2 = $y1 + 27;
if($y2 > count($datosInforme1PromesasDetalle)){
$y2 = count($datosInforme1PromesasDetalle);
}
if($y1 < count($datosInforme1PromesasDetalle)){
echo "<page><table style='margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	<tr>
		<td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
			<table style='border-spacing: 0 0; border-collapse : collapse;'>
				<tr>
					<td style='padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;'>LIVING NET - Informe de Gestión - Proyecto $nombreProyecto </td>
				</tr>
			</table>
		</td>
	</tr>
	<tr style='border-spacing: 0 0; border-collapse : collapse;'>
		<td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
			<table style='border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;'>
				<tr>
					<td style='padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;'>Correspondiente a $mesTexto de $ano </td>
				</tr>
			</table>
		</td>
	</tr>
</table>";
echo '
	<table style="margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr style="border-spacing: 0 0; border-collapse : collapse;">
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
				<tr>
					<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">PROMESAS</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
<tr style="font-size: 10px;">
	<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
		<table style="border-spacing: 0 0; border-collapse : collapse;">
			<tr style="background-color: #d8d8d8;font-weight: bold;">
				<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nro</td>
				<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 40px; min-width: 40px; width: 40px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Fecha</td>
				<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Dto</td>
				<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Estac</td>
				<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Bodega</td>
				<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">UF</td>
				<td style="padding-left: 2px; padding-right: 2px; padding-top: 4px; padding-bottom: 4px;  max-width: 36px; min-width: 36px; width: 36px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Res+Pie</td>
				<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Saldo</td>
				<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Apellido</td>
				<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nombre</td>
				<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 45px; min-width: 45px; width: 45px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Celular</td>
				<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 204px; min-width: 204px; width: 204px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Mail</td>
			</tr>';
			for($i = $y1; $i < $y2; $i++){
				if(strpos($datosInforme1PromesasDetalle[$i][1], $fechaCompara[11])){
					echo '<tr style="background-color: #e0e0e0;">';
				}
				else{
					echo '<tr>';
				}
				echo '<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][0] . '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 40px; min-width: 40px; width: 40px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][1] . '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][2] . '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][3] . '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][4] . '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][5] . '</td>
					<td style="padding-left: 2px; padding-right: 2px; padding-top: 4px; padding-bottom: 4px;  max-width: 36px; min-width: 36px; width: 36px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][6] . '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][7] . '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][8] . '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][9] . '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 45px; min-width: 45px; width: 45px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][10] . '</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 204px; min-width: 204px; width: 204px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse; word-wrap: break-word;">' .$datosInforme1PromesasDetalle[$i][11] . '</td>
				</tr>';
			}
		echo '</table>
	</td>
</tr>
	</table>
	<page_footer>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
		</page_footer></page>';
}
?>
<?php
$y3 = $y2 + 27;
if($y3 > count($datosInforme1PromesasDetalle)){
	$y3 = count($datosInforme1PromesasDetalle);
}
if($y2 < count($datosInforme1PromesasDetalle)){
	echo "<page><table style='margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse;'>
	        <tr>
	          <td style='padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;'>LIVING NET - Informe de Gestión - Proyecto $nombreProyecto </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	  <tr style='border-spacing: 0 0; border-collapse : collapse;'>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;'>
	        <tr>
	          <td style='padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;'>Correspondiente a $mesTexto de $ano </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>";
	echo '
		<table style="margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr style="border-spacing: 0 0; border-collapse : collapse;">
			<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
					<tr>
						<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">PROMESAS</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr style="font-size: 10px;">
		<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr style="background-color: #d8d8d8;font-weight: bold;">
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nro</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 40px; min-width: 40px; width: 40px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Fecha</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Dto</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Estac</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Bodega</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">UF</td>
					<td style="padding-left: 2px; padding-right: 2px; padding-top: 4px; padding-bottom: 4px;  max-width: 36px; min-width: 36px; width: 36px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Res+Pie</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Saldo</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Apellido</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nombre</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 45px; min-width: 45px; width: 45px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Celular</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 204px; min-width: 204px; width: 204px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Mail</td>
				</tr>';
				for($i = $y2; $i < $y3; $i++){
					if(strpos($datosInforme1PromesasDetalle[$i][1], $fechaCompara[11])){
						echo '<tr style="background-color: #e0e0e0;">';
					}
					else{
						echo '<tr>';
					}
					echo '<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][0] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 40px; min-width: 40px; width: 40px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][1] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][2] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][3] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][4] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][5] . '</td>
						<td style="padding-left: 2px; padding-right: 2px; padding-top: 4px; padding-bottom: 4px;  max-width: 36px; min-width: 36px; width: 36px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][6] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][7] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][8] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][9] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 45px; min-width: 45px; width: 45px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][10] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 204px; min-width: 204px; width: 204px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse; word-wrap: break-word;">' .$datosInforme1PromesasDetalle[$i][11] . '</td>
					</tr>';
				}
			echo '</table>
		</td>
	</tr>
		</table>
		<page_footer>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
			</page_footer></page>';
}
?>
<?php
$y4 = $y3 + 27;
if($y4 > count($datosInforme1PromesasDetalle)){
	$y4 = count($datosInforme1PromesasDetalle);
}
if($y3 < count($datosInforme1PromesasDetalle)){
	echo "<page><table style='margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
	  <tr>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse;'>
	        <tr>
	          <td style='padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;'>LIVING NET - Informe de Gestión - Proyecto $nombreProyecto </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	  <tr style='border-spacing: 0 0; border-collapse : collapse;'>
	    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
	      <table style='border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;'>
	        <tr>
	          <td style='padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;'>Correspondiente a $mesTexto de $ano </td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	</table>";
	echo '
		<table style="margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr style="border-spacing: 0 0; border-collapse : collapse;">
			<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
					<tr>
						<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">PROMESAS</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr style="font-size: 10px;">
		<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr style="background-color: #d8d8d8;font-weight: bold;">
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nro</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 40px; min-width: 40px; width: 40px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Fecha</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Dto</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Estac</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Bodega</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">UF</td>
					<td style="padding-left: 2px; padding-right: 2px; padding-top: 4px; padding-bottom: 4px;  max-width: 36px; min-width: 36px; width: 36px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Res+Pie</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Saldo</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Apellido</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nombre</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 45px; min-width: 45px; width: 45px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Celular</td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 204px; min-width: 204px; width: 204px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Mail</td>
				</tr>';
				for($i = $y3; $i < $y4; $i++){
					if(strpos($datosInforme1PromesasDetalle[$i][1], $fechaCompara[11])){
						echo '<tr style="background-color: #e0e0e0;">';
					}
					else{
						echo '<tr>';
					}
					echo '<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][0] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 40px; min-width: 40px; width: 40px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][1] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][2] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][3] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][4] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][5] . '</td>
						<td style="padding-left: 2px; padding-right: 2px; padding-top: 4px; padding-bottom: 4px;  max-width: 36px; min-width: 36px; width: 36px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][6] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][7] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][8] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][9] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 45px; min-width: 45px; width: 45px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1PromesasDetalle[$i][10] . '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 204px; min-width: 204px; width: 204px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse; word-wrap: break-word;">' .$datosInforme1PromesasDetalle[$i][11] . '</td>
					</tr>';
				}
			echo '</table>
		</td>
	</tr>
		</table>
		<page_footer>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
			</page_footer></page>';
}
?>
<!-- Escrituras -->
<page>
<table style="margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr>
					<td style="padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">LIVING NET - Informe de Gestión - Proyecto <?php echo $nombreProyecto ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr style="border-spacing: 0 0; border-collapse : collapse;">
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
				<tr>
					<td style="padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;">Correspondiente a <?php echo $mesTexto; ?> de <?php echo $ano ?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table style="margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr style="border-spacing: 0 0; border-collapse : collapse;">
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
				<tr>
					<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">ESCRITURAS POR MES</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr style="background-color: #d8d8d8;font-weight: bold;">
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 91px; min-width: 91px; width: 91px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[0]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[1]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[2]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[3]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[4]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[5]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[6]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[7]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[8]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[9]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[10]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><?php echo $fechasRango[11]; ?></td>
					<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Totales</td>
				</tr>
			</table>
		</td>
	</tr>
	<?php
	$suma = array(0,0,0,0,0,0,0,0,0,0,0,0,0);
	$tope = 0;
	if(count($datosInforme1Escrituras) < 1){
		$tope = 1;
	}
	else{
		$tope = count($datosInforme1Escrituras);
	}
	for($i = 0; $i < $tope; $i++){
		$total = 0;
		$a = array(0,0,0,0,0,0,0,0,0,0,0,0,0);
		for($j = 1; $j < count($datosInforme1Escrituras[$i]); $j++){
			$total = $total + $datosInforme1Escrituras[$i][$j];
			$a[$j] = 0 + $datosInforme1Escrituras[$i][$j];
			$suma[$j] = $suma[$j] + $datosInforme1Escrituras[$i][$j];
		}
		$suma[13] = $suma[13] + $total;
		echo '<tr>
			<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
				<table style="margin-top: -3px; border-spacing: 0 0; border-collapse : collapse;">
					<tr>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 91px; min-width: 91px; width: 91px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><b>Totales</b></td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[0],$datosInforme1Escrituras[$i])){
								echo $datosInforme1Escrituras[$i][$fechasRangoIndex[0]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[1],$datosInforme1Escrituras[$i])){
								echo $datosInforme1Escrituras[$i][$fechasRangoIndex[1]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[2],$datosInforme1Escrituras[$i])){
								echo $datosInforme1Escrituras[$i][$fechasRangoIndex[2]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[3],$datosInforme1Escrituras[$i])){
								echo $datosInforme1Escrituras[$i][$fechasRangoIndex[3]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[4],$datosInforme1Escrituras[$i])){
								echo $datosInforme1Escrituras[$i][$fechasRangoIndex[4]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[5],$datosInforme1Escrituras[$i])){
								echo $datosInforme1Escrituras[$i][$fechasRangoIndex[5]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[6],$datosInforme1Escrituras[$i])){
								echo $datosInforme1Escrituras[$i][$fechasRangoIndex[6]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[7],$datosInforme1Escrituras[$i])){
								echo $datosInforme1Escrituras[$i][$fechasRangoIndex[7]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[8],$datosInforme1Escrituras[$i])){
								echo $datosInforme1Escrituras[$i][$fechasRangoIndex[8]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[9],$datosInforme1Escrituras[$i])){
								echo $datosInforme1Escrituras[$i][$fechasRangoIndex[9]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[10],$datosInforme1Escrituras[$i])){
								echo $datosInforme1Escrituras[$i][$fechasRangoIndex[10]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">'?>
						<?php
							if(array_key_exists($fechasRangoIndex[11],$datosInforme1Escrituras[$i])){
								echo $datosInforme1Escrituras[$i][$fechasRangoIndex[11]];
							}
							else{
								echo 0;
							}
						?>
						<?php echo '</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;font-size: 12px;  max-width: 38px; min-width: 38px; width: 38px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;"><b>' . $total . '</b></td>
					</tr>
				</table>
			</td>
		</tr>';
	}
	?>
</table>
<table style="margin-top: 2px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
	<tr>
		<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
			<table style="border-spacing: 0 0; border-collapse : collapse;">
				<tr>
					<td style="padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">
						<img src="gra7.png" style="width: 100%;">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<page_footer>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
  </page_footer></page>
	<page>
		<table style="margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
			<tr>
				<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
					<table style="border-spacing: 0 0; border-collapse : collapse;">
						<tr>
							<td style="padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">LIVING NET - Informe de Gestión - Proyecto <?php echo $nombreProyecto ?></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr style="border-spacing: 0 0; border-collapse : collapse;">
				<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
					<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
						<tr>
							<td style="padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;">Correspondiente a <?php echo $mesTexto; ?> de <?php echo $ano ?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table style="margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
			<tr style="border-spacing: 0 0; border-collapse : collapse;">
				<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
					<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
						<tr>
							<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">ESCRITURAS</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<?php
		echo '<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr style="font-size: 10px;">
			<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse;">
					<tr style="background-color: #d8d8d8;font-weight: bold;">
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nro</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 40px; min-width: 40px; width: 40px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Fecha</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Dto</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Estac</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Bodega</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">UF</td>
						<td style="padding-left: 2px; padding-right: 2px; padding-top: 4px; padding-bottom: 4px;  max-width: 36px; min-width: 36px; width: 36px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Res+Pie</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Saldo</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Apellido</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nombre</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 45px; min-width: 45px; width: 45px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Celular</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 204px; min-width: 204px; width: 204px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Mail</td>
					</tr>';
					$y1 = 0 + 28;
					if($y1 > count($datosInforme1EscriturasDetalle)){
						$y1 = count($datosInforme1EscriturasDetalle);
					}
					for($i = 0; $i < $y1; $i++){
						if(strpos($datosInforme1EscriturasDetalle[$i][1], $fechaCompara[11])){
							echo '<tr style="background-color: #e0e0e0;">';
						}
						else{
							echo '<tr>';
						}
						echo '<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][0] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 40px; min-width: 40px; width: 40px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][1] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][2] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][3] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][4] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][5] . '</td>
							<td style="padding-left: 2px; padding-right: 2px; padding-top: 4px; padding-bottom: 4px;  max-width: 36px; min-width: 36px; width: 36px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][6] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][7] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][8] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][9] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 45px; min-width: 45px; width: 45px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][10] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 204px; min-width: 204px; width: 204px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse; word-wrap: break-word;">' .$datosInforme1EscriturasDetalle[$i][11] . '</td>
						</tr>';
					}
				echo '</table>
			</td>
		</tr>
		</table>
		<page_footer>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
			</page_footer></page>';
	?>
	<?php
	$y2 = $y1 + 28;
	if($y2 > count($datosInforme1EscriturasDetalle)){
		$y2 = count($datosInforme1EscriturasDetalle);
	}
	if($y1 < count($datosInforme1EscriturasDetalle)){
		echo "<page><table style='margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
		  <tr>
		    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
		      <table style='border-spacing: 0 0; border-collapse : collapse;'>
		        <tr>
		          <td style='padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;'>LIVING NET - Informe de Gestión - Proyecto $nombreProyecto </td>
		        </tr>
		      </table>
		    </td>
		  </tr>
		  <tr style='border-spacing: 0 0; border-collapse : collapse;'>
		    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
		      <table style='border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;'>
		        <tr>
		          <td style='padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;'>Correspondiente a $mesTexto de $ano </td>
		        </tr>
		      </table>
		    </td>
		  </tr>
		</table>";
		echo '
			<table style="margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
			<tr style="border-spacing: 0 0; border-collapse : collapse;">
				<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
					<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
						<tr>
							<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">PROMESAS</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr style="font-size: 10px;">
			<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse;">
					<tr style="background-color: #d8d8d8;font-weight: bold;">
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nro</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 40px; min-width: 40px; width: 40px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Fecha</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Dto</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Estac</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Bodega</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">UF</td>
						<td style="padding-left: 2px; padding-right: 2px; padding-top: 4px; padding-bottom: 4px;  max-width: 36px; min-width: 36px; width: 36px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Res+Pie</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Saldo</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Apellido</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nombre</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 45px; min-width: 45px; width: 45px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Celular</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 204px; min-width: 204px; width: 204px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Mail</td>
					</tr>';
					for($i = $y1; $i < $y2; $i++){
						if(strpos($datosInforme1EscriturasDetalle[$i][1], $fechaCompara[11])){
							echo '<tr style="background-color: #e0e0e0;">';
						}
						else{
							echo '<tr>';
						}
						echo '<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][0] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 40px; min-width: 40px; width: 40px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][1] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][2] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][3] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][4] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][5] . '</td>
							<td style="padding-left: 2px; padding-right: 2px; padding-top: 4px; padding-bottom: 4px;  max-width: 36px; min-width: 36px; width: 36px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][6] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][7] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][8] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][9] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 45px; min-width: 45px; width: 45px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][10] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 204px; min-width: 204px; width: 204px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse; word-wrap: break-word;">' .$datosInforme1EscriturasDetalle[$i][11] . '</td>
						</tr>';
					}
				echo '</table>
			</td>
		</tr>
			</table>
			<page_footer>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
				</page_footer></page>';
	}
	?>
	<?php
	$y3 = $y2 + 28;
	if($y3 > count($datosInforme1EscriturasDetalle)){
		$y3 = count($datosInforme1EscriturasDetalle);
	}
	if($y2 < count($datosInforme1EscriturasDetalle)){
		echo "<page><table style='margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
		  <tr>
		    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
		      <table style='border-spacing: 0 0; border-collapse : collapse;'>
		        <tr>
		          <td style='padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;'>LIVING NET - Informe de Gestión - Proyecto $nombreProyecto </td>
		        </tr>
		      </table>
		    </td>
		  </tr>
		  <tr style='border-spacing: 0 0; border-collapse : collapse;'>
		    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
		      <table style='border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;'>
		        <tr>
		          <td style='padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;'>Correspondiente a $mesTexto de $ano </td>
		        </tr>
		      </table>
		    </td>
		  </tr>
		</table>";
		echo '
			<table style="margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
			<tr style="border-spacing: 0 0; border-collapse : collapse;">
				<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
					<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
						<tr>
							<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">PROMESAS</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr style="font-size: 10px;">
			<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse;">
					<tr style="background-color: #d8d8d8;font-weight: bold;">
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nro</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 40px; min-width: 40px; width: 40px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Fecha</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Dto</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Estac</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Bodega</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">UF</td>
						<td style="padding-left: 2px; padding-right: 2px; padding-top: 4px; padding-bottom: 4px;  max-width: 36px; min-width: 36px; width: 36px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Res+Pie</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Saldo</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Apellido</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nombre</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 45px; min-width: 45px; width: 45px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Celular</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 204px; min-width: 204px; width: 204px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Mail</td>
					</tr>';
					for($i = $y2; $i < $y3; $i++){
						if(strpos($datosInforme1EscriturasDetalle[$i][1], $fechaCompara[11])){
							echo '<tr style="background-color: #e0e0e0;">';
						}
						else{
							echo '<tr>';
						}
						echo '<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][0] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 40px; min-width: 40px; width: 40px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][1] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][2] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][3] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][4] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][5] . '</td>
							<td style="padding-left: 2px; padding-right: 2px; padding-top: 4px; padding-bottom: 4px;  max-width: 36px; min-width: 36px; width: 36px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][6] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][7] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][8] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][9] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 45px; min-width: 45px; width: 45px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][10] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 204px; min-width: 204px; width: 204px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse; word-wrap: break-word;">' .$datosInforme1EscriturasDetalle[$i][11] . '</td>
						</tr>';
					}
				echo '</table>
			</td>
		</tr>
			</table>
			<page_footer>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
				</page_footer></page>';
	}
	?>
	<?php
	$y4 = $y3 + 28;
	if($y4 > count($datosInforme1EscriturasDetalle)){
		$y4 = count($datosInforme1EscriturasDetalle);
	}
	if($y3 < count($datosInforme1EscriturasDetalle)){
		echo "<page><table style='margin-top: 20px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;'>
		  <tr>
		    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
		      <table style='border-spacing: 0 0; border-collapse : collapse;'>
		        <tr>
		          <td style='padding-top: 3px; padding-left: 10px; font-size: 15px; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;'>LIVING NET - Informe de Gestión - Proyecto $nombreProyecto </td>
		        </tr>
		      </table>
		    </td>
		  </tr>
		  <tr style='border-spacing: 0 0; border-collapse : collapse;'>
		    <td style='max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;'>
		      <table style='border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;'>
		        <tr>
		          <td style='padding-left: 10px; font-size: 10px; font-weight: normal; max-height: 20px; min-height: 20px;  height: 20px; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse;'>Correspondiente a $mesTexto de $ano </td>
		        </tr>
		      </table>
		    </td>
		  </tr>
		</table>";
		echo '
			<table style="margin-top: -3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
			<tr style="border-spacing: 0 0; border-collapse : collapse;">
				<td style="max-width: 100%; min-width: 100%; width: 100%;border-spacing: 0 0; border-collapse : collapse;">
					<table style="border-spacing: 0 0; border-collapse : collapse; margin-top: -3px;">
						<tr>
							<td style="padding-top: 3px; padding-left: 10px; font-size: 12x; font-weight: normal; max-width: 938px; min-width: 938px; width: 938px; text-align: center;border-spacing: 0 0; border-collapse : collapse; border: 1px solid black; background-color: #d8d8d8; font-weight: bold;">PROMESAS</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table style="margin-top:-3px; margin-left: 20px; width: 100%; max-width: 100%; min-width: 100%; border-spacing: 0 0; border-collapse : collapse;">
		<tr style="font-size: 10px;">
			<td style="max-width: 100px; min-width: 100px; width: 100px;border-spacing: 0 0; border-collapse : collapse;">
				<table style="border-spacing: 0 0; border-collapse : collapse;">
					<tr style="background-color: #d8d8d8;font-weight: bold;">
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nro</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 40px; min-width: 40px; width: 40px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Fecha</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Dto</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Estac</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Bodega</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">UF</td>
						<td style="padding-left: 2px; padding-right: 2px; padding-top: 4px; padding-bottom: 4px;  max-width: 36px; min-width: 36px; width: 36px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Res+Pie</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Saldo</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Apellido</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Nombre</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 45px; min-width: 45px; width: 45px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Celular</td>
						<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 204px; min-width: 204px; width: 204px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">Mail</td>
					</tr>';
					for($i = $y3; $i < $y4; $i++){
						if(strpos($datosInforme1EscriturasDetalle[$i][1], $fechaCompara[11])){
							echo '<tr style="background-color: #e0e0e0;">';
						}
						else{
							echo '<tr>';
						}
						echo '<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 10px; min-width: 10px; width: 10px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][0] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 40px; min-width: 40px; width: 40px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][1] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][2] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][3] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 70px; min-width: 70px; width: 70px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][4] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][5] . '</td>
							<td style="padding-left: 2px; padding-right: 2px; padding-top: 4px; padding-bottom: 4px;  max-width: 36px; min-width: 36px; width: 36px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][6] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 20px; min-width: 20px; width: 20px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][7] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][8] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 60px; min-width: 60px; width: 60px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][9] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px; max-width: 45px; min-width: 45px; width: 45px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse;">' .$datosInforme1EscriturasDetalle[$i][10] . '</td>
							<td style="padding-left: 6px; padding-right: 6px; padding-top: 4px; padding-bottom: 4px;  max-width: 204px; min-width: 204px; width: 204px; text-align: center;  border: 1px solid black; border-spacing: 0 0; border-collapse : collapse; word-wrap: break-word;">' .$datosInforme1EscriturasDetalle[$i][11] . '</td>
						</tr>';
					}
				echo '</table>
			</td>
		</tr>
			</table>
			<page_footer>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[page_cu]]
				</page_footer></page>';
	}
	?>
<?php
// unlink("gra1.png");

$html = ob_get_clean();

$html2pdf = new Html2Pdf('L','LETTER','es','true','UTF-8');
$html2pdf->writeHTML($html);
$html2pdf->output($document . 'repositorio/INFORMES/Informe_Gestion_' . $codigoProyecto . '_' . $mes . '_' . $ano . '.pdf', 'F');

if(file_exists($document . 'repositorio/INFORMES/Informe_Gestion_' . $codigoProyecto . '_' . $mes . '_' . $ano . '.pdf')){
	// updatePDFormularioEPP($id, 'Formulario_EPP_' . $id . '.pdf');
  	echo 'repositorio/INFORMES/Informe_Gestion_' . $codigoProyecto . '_' . $mes . '_' . $ano . '.pdf';
}
else{
  	echo "Sin datos";
}


?>
