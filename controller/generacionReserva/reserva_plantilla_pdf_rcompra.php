<?php
  //ini_set('display_errors', 'On');
  require('../../model/consultas.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
  <head>
    <style type="text/css">
    #tablaOC{
      width: 100%;
      margin-left: 35px;
      margin-right:35px;
      margin-top: 20px;
    }
    #cabecera1Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height:70px;
    }
    #cabecera1Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height:70px;
    }
    #cabecera2Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height: 45px;
    }
    #cabecera2Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height: 45px;
    }
    #cabecera3Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height: 400px;
    }
    #cabecera3Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height: 400px;
    }
    .headTabla{
      background-color: #e6f2ff;
      border: 1px solid black;
      padding: 2px;
    }
    .bodyTabla{
      border: 1px solid black;
      padding: 1px;
    }

    td {
      padding: 0;
      margin: 0;
    }

    tr {
      padding: 0;
      padding: 0;
    }

    </style>
    <title>Orden de Compra</title>
  </head>
  <body style="font-size: 9px; font-family: Arial">
    <?php
      session_start();
    ?>
    <table id="tablaOC">
      <tr>
        <td id="cabecera1Izquierda">
          <img src="../../view/img/logos/living_logo.png" style='height: 60px;'>
        </td>
        <td id="cabecera1Derecha">
          <?php
            echo "<img src='" . $_SESSION['reservaLogoProyecto'] . "' style='height: 60px;'>";
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-top: 5px;">
      <tr>
        <td style="text-align: center; width: 750px;">
          <b>RESERVA DE <?php if ($_SESSION['reservaAccion'] == "Venta") {
            echo "COMPRA";
          }else{
            echo "ARRIENDO";
          } ?> (Carta Oferta)<br/>
          <?php
            if ($_SESSION['reservaCodigoProyecto'] == "COR") {
              echo "";
            }else{
              echo $_SESSION['reservaNombreProyecto'];
            }
          ?></b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 20px;">
      <tr>
        <td style="text-align: right; width: 720px;">
          <?php
            echo $_SESSION['reservaFecha'];
          ?>
          <br/>
          Número Operación:&nbsp;&nbsp;
          <b>
          <?php
            echo $_SESSION['numeroOperacion'];
          ?>
          </b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 720; height: 10px;">
          <b>PRIMERO: Individualización de las Unidades</b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 100px; height: 10px;">
          <?php echo strtoupper($_SESSION['tipoUnidad']); ?> Nro:
        </td>
        <td style="text-align: center; width: 65px; border: 1; height: 10px;">
          <?php
            echo $_SESSION['reservaNumeroDepto'];
          ?>
        </td>
        <td style="text-align: center; width: 80px; height: 10px;">
          Valor Lista UF:
        </td>
        <td style="text-align: center; width: 65px; border: 1; height: 10px;">
          <?php
            echo number_format($_SESSION['reservaValorDepto'],2,',','.');
          ?>
        </td>
        <td style="text-align: center; width: 55px; height: 10px;">
          Desc 1 %
        </td>
        <td style="text-align: center; width: 45px; border: 1; height: 10px;">
          <?php
            echo number_format($_SESSION['reservaDescuentoSala'],2,',','.');
          ?>
        </td>
        <td style="text-align: center; width: 55px; height: 10px;">
          Desc 2 %
        </td>
        <td style="text-align: center; width: 45px; border: 1; height: 10px;">
          <?php
            echo number_format($_SESSION['reservaDescuentoEspecial'],2,',','.');
          ?>
        </td>
        <td style="text-align: center; width: 80px; height: 10px;">
          Valor Venta UF:
        </td>
        <td style="text-align: center; width: 65px; border: 1; height: 10px;">
          <?php
            echo number_format($_SESSION['reservaTotal3'],2,',','.');
          ?>
        </td>
      </tr>
    </table>
    <?php
    if(consultaUnidadEspecificaReserva($_SESSION['estacionamientosClienteReserva'][0])[0]['CODIGO'] != ''){
      for($i = 0; $i < count($_SESSION['estacionamientosClienteReserva']); $i++){
      echo
        '<table style="margin-left: 40px; margin-top: -3px;">
          <tr>
            <td style="text-align: left; width: 100px; height: 10px;">
              ESTACIONAMIENTO ' . ($i + 1) . ':
            </td>
            <td style="text-align: center; width: 65px; border: 1; height: 10px;">
              ' . consultaUnidadEspecificaReserva($_SESSION['estacionamientosClienteReserva'][$i])[0]['CODIGO'] . '
            </td>
            <td style="text-align: center; width: 80px; height: 10px;">
              Valor Lista UF:
            </td>
            <td style="text-align: center; width: 65px; border: 1; height: 10px;">
              ' . number_format(valorUnidadReservaEstacionamiento($_SESSION['estacionamientosClienteReserva'][$i],$_SESSION['idReserva'])['VALORUF'],2,',','.') . '
            </td>
            <td style="text-align: center; width: 55px; height: 10px;">
              Desc 1 %
            </td>
            <td style="text-align: center; width: 45px; border: 1; height: 10px;">
              0,00
            </td>
            <td style="text-align: center; width: 55px; height: 10px;">
              Desc 2 %
            </td>
            <td style="text-align: center; width: 45px; border: 1; height: 10px;">
              0,00
            </td>
            <td style="text-align: center; width: 80px; height: 10px;">
              Valor Venta UF:
            </td>
            <td style="text-align: center; width: 65px; border: 1; height: 10px;">
              ' . number_format(valorUnidadReservaEstacionamiento($_SESSION['estacionamientosClienteReserva'][$i],$_SESSION['idReserva'])['VALORUF'],2,',','.') . '
            </td>
          </tr>
        </table>';
      }
    }
    if(consultaUnidadEspecificaReserva($_SESSION['bodegasClienteReserva'][0])[0]['CODIGO'] != ''){
      for($i = 0; $i < count($_SESSION['bodegasClienteReserva']); $i++){
      echo
        '<table style="margin-left: 40px; margin-top: -3px;">
          <tr>
            <td style="text-align: left; width: 100px; height: 10px;">
              BODEGA ' . ($i + 1) . ':
            </td>
            <td style="text-align: center; width: 65px; border: 1; height: 10px;">
              ' . consultaUnidadEspecificaReserva($_SESSION['bodegasClienteReserva'][$i])[0]['CODIGO'] . '
            </td>
            <td style="text-align: center; width: 80px; height: 10px;">
              Valor Lista UF:
            </td>
            <td style="text-align: center; width: 65px; border: 1; height: 10px;">
              ' . number_format(valorUnidadReservaBodega($_SESSION['bodegasClienteReserva'][$i],$_SESSION['idReserva'])['VALORUF'],2,',','.') . '
            </td>
            <td style="text-align: center; width: 55px; height: 10px;">
              Desc 1 %
            </td>
            <td style="text-align: center; width: 45px; border: 1; height: 10px;">
              0,00
            </td>
            <td style="text-align: center; width: 55px; height: 10px;">
              Desc 2 %
            </td>
            <td style="text-align: center; width: 45px; border: 1; height: 10px;">
              0,00
            </td>
            <td style="text-align: center; width: 80px; height: 10px;">
              Valor Venta UF:
            </td>
            <td style="text-align: center; width: 65px; border: 1; height: 10px;">
              ' . number_format(valorUnidadReservaBodega($_SESSION['bodegasClienteReserva'][$i],$_SESSION['idReserva'])['VALORUF'],2,',','.') . '
            </td>
          </tr>
        </table>';
      }
    }
    if($_SESSION['reservaBono'] != '' && number_format($_SESSION['reservaBono'],0,',','.') != '0'){
      echo
        '<table style="margin-left: 40px; margin-top: -3px;">
          <tr>
            <td style="text-align: left; width: 100px; height: 10px;">
              BONO:
            </td>
            <td style="text-align: center; width: 65px; height: 10px;">

            </td>
            <td style="text-align: center; width: 80px; height: 10px;">

            </td>
            <td style="text-align: center; width: 81px; height: 10px;">

            </td>
            <td style="text-align: center; width: 55px; height: 10px;">

            </td>
            <td style="text-align: center; width: 45px; height: 10px;">

            </td>
            <td style="text-align: center; width: 55px; height: 10px;">

            </td>
            <td style="text-align: center; width: 45px; height: 10px;">

            </td>
            <td style="text-align: center; width: 80px; height: 10px;">
              Valor Venta UF:
            </td>
            <td style="text-align: center; width: 65px; border: 1; height: 10px;">
              ' . number_format($_SESSION['reservaBono'],2,',','.') . '
            </td>
          </tr>
        </table>';
      }
      echo
      '<table style="margin-left: 40px; margin-top: 5px;">
        <tr>
          <td style="text-align: left; width: 100px; height: 10px;">

          </td>
          <td style="text-align: center; width: 80px; height: 10px;">

          </td>
          <td style="text-align: center; width: 80px; height: 10px;">

          </td>
          <td style="text-align: center; width: 80px; height: 10px;">

          </td>
          <td style="text-align: center; width: 70px; height: 10px;">

          </td>
          <td style="text-align: center; width: 30px; height: 10px;">

          </td>
          <td style="text-align: center; width: 170px; height: 10px;">
            <b>Precio Total de la Compraventa UF:</b>
          </td>
          <td style="text-align: center; width: 65px; border: 1; height: 10px;">
            <b>' . number_format($_SESSION['reservaTotalF'],2,',','.') . '</b>
          </td>
        </tr>
      </table>';
    ?>
    <table style="margin-top: 15px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 720; height: 10px;">
          <b>SEGUNDO: Individualización del Oferente Comprador que entrega la siguiente carta oferta a <?php echo $_SESSION['reservaInmobiliaria']; ?>.</b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 15px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 100px; height: 10px;">
          Nombre
        </td>
        <td style="text-align: left; width: 590px; height: 10px;">
          <b style="margin-left: 10px;"><?php
            echo strtoupper($_SESSION['reservaNombreCliente']) . ' ' . strtoupper($_SESSION['reservaApellidoCliente']);
          ?></b>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 100px; height: 10px;">
          Estado Civil
        </td>
        <td style="text-align: left; width: 590px; height: 10px;">
          <b style="margin-left: 40px;"><?php
            echo $_SESSION['reservaEstadoCivilIDCliente1'];
          ?></b><font style="font-size: 7px; margin-left: 80px;">1. Soltero | 2. Separado | 3.Viudo | 4. Cas. Soc. Conyugal | 5. Cas. Sep. Bienes | 6. Cas. Part. Ganancias | 7. Divorciado</font>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 100px; height: 10px;">
          Cedula de identidad
        </td>
        <td style="text-align: left; width: 590px; height: 10px;">
          <b style="margin-left: 10px;"><?php
            echo strtoupper($_SESSION['reservaRutCliente']);
          ?></b>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 100px; height: 10px;">
          Fecha de nacimiento
        </td>
        <td style="text-align: left; width: 590px; height: 10px;">
          <b style="margin-left: 10px;"><?php
            echo strtoupper($_SESSION['reservaFechaNacCliente1']);
          ?></b>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 100px; height: 10px;">
          Profesión
        </td>
        <td style="text-align: left; width: 590px; height: 10px;">
          <b style="margin-left: 10px;"><?php
            echo strtoupper($_SESSION['reservaProfesionCliente1']);
          ?></b>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 100px; height: 10px;">
          Domicilio
        </td>
        <td style="text-align: left; width: 590px; height: 10px;">
          <b style="margin-left: 10px;"><?php
            echo strtoupper($_SESSION['reservaDomicilioCliente1']);
          ?></b>
        </td>
      </tr>

      <tr>
        <td style="text-align: left; width: 100px; height: 10px;">
          Comuna
        </td>
        <td style="text-align: left; width: 590px; height: 10px;">
          <b style="margin-left: 10px;"><?php
            echo strtoupper($_SESSION['reservaComunaCliente1']);
          ?></b>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 100px; height: 10px;">
          Teléfono
        </td>
        <td style="text-align: left; width: 590px; height: 10px;">
          <b style="margin-left: 10px;"><?php
            echo strtoupper($_SESSION['reservaTelefonoCliente1']);
          ?></b>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 100px; height: 10px;">
          Dirección e-mail
        </td>
        <td style="text-align: left; width: 590px; height: 10px;">
          <b style="margin-left: 10px;"><?php
            echo strtoupper($_SESSION['reservaMailCliente1']);
          ?></b>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 100px; height: 10px;">
          Renta
        </td>
        <td style="text-align: left; width: 590px; height: 10px;">
          <b style="margin-left: 10px;"><?php
            echo strtoupper($_SESSION['reservaRentaCliente1']);
          ?></b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 15px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 100px; height: 10px;">
          Ejecutivo de ventas
        </td>
        <td style="text-align: left; width: 590px; height: 10px;">
          <b style="margin-left: 10px;"><?php
            echo strtoupper($_SESSION['reservaVendedor']);
          ?></b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 15px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 705px; line-height: 15px;">
          <b>TERCERO: Forma de pago del precio: El oferente comprador se obliga a pagar el precio de la siguiente forma:</b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 100px; height: 10px; padding-left: 10px;">
          <b>a)</b>   Con la suma de <b>UF</b>
        </td>
        <td style="text-align: center; width: 80px; border: 1; height: 10px;">
          <b><?php
            echo number_format($_SESSION['reservaReservaUF'],2,',','.');
          ?></b>
        </td>
        <td style="text-align: left; width: 400px; height: 10px; padding-left: 10px;">
          que paga en el acto de la firma en concepto de Reserva, con recursos propios y se abonarán al pie,
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px; margin-top: -3px;">
      <tr>
        <td style="text-align: left; width: 100px; height: 10px;  padding-left: 10px;">
          &nbsp;&nbsp;&nbsp;&nbsp;correspondiente al
        </td>
        <td style="text-align: center; width: 80px; border: 1; height: 10px;">
          <b><?php
            echo number_format(($_SESSION['reservaReservaUF'])*100/$_SESSION['reservaTotalF'],2,',','.');
          ?> %</b>
        </td>
        <td style="text-align: left; width: 400px; height: 10px; padding-left: 10px;">
          del precio de la Compraventa.
        </td>
      </tr>
    </table>
    <table style="margin-top: 5px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 100px; height: 10px; padding-left: 10px;">
          <b>b)</b>   Con la suma de <b>UF</b>
        </td>
        <td style="text-align: center; width: 80px; border: 1; height: 10px;">
          <b><?php
            echo number_format($_SESSION['reservaPiePromesaUF'],2,',','.');
          ?></b>
        </td>
        <td style="text-align: left; width: 400px; height: 10px; padding-left: 10px;">
          que pagará en el acto de la firma de la Promesa, con recursos propios y se abonarán al pie,
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px; margin-top: -3px;">
      <tr>
        <td style="text-align: left; width: 100px; height: 10px;  padding-left: 10px;">
          &nbsp;&nbsp;&nbsp;&nbsp;correspondiente al
        </td>
        <td style="text-align: center; width: 80px; border: 1; height: 10px;">
          <b><?php
            echo number_format(($_SESSION['reservaPiePromesaUF'])*100/$_SESSION['reservaTotalF'],2,',','.');
          ?> %</b>
        </td>
        <td style="text-align: left; width: 400px; height: 10px; padding-left: 10px;">
          del precio de la Compraventa.
        </td>
      </tr>
    </table>
    <table style="margin-top: 5px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 100px; height: 10px; padding-left: 10px;">
          <b>c)</b>   Con la suma de <b>UF</b>
        </td>
        <td style="text-align: center; width: 80px; border: 1; height: 10px;">
          <b><?php
            echo number_format($_SESSION['reservaPieSaldoUF'],2,',','.');
          ?></b>
        </td>
        <td style="text-align: left; width: 400px; height: 10px; padding-left: 10px;">
          que pagará en el acto de la firma de la Promesa, con recursos propios y se abonarán al pie,
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px; margin-top: -3px;">
      <tr>
        <td style="text-align: left; width: 100px; height: 10px;  padding-left: 10px;">
          &nbsp;&nbsp;&nbsp;&nbsp;correspondiente al
        </td>
        <td style="text-align: center; width: 80px; border: 1; height: 10px;">
          <b><?php
            echo number_format(($_SESSION['reservaPieSaldoUF'])*100/$_SESSION['reservaTotalF'],2,',','.');
          ?> %</b>
        </td>
        <td style="text-align: left; width: 400px; height: 10px; padding-left: 10px;">
          del precio de la Compraventa.
        </td>
      </tr>
    </table>
    <table style="margin-top: 5px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 100px; height: 10px; padding-left: 10px;">
          <b>d)</b>   Con la suma de <b>UF</b>
        </td>
        <td style="text-align: center; width: 80px; border: 1; height: 10px;">
          <b><?php
            echo number_format($_SESSION['reservaSaldoTotalUF'],2,',','.');
          ?></b>
        </td>
        <td style="text-align: left; width: 400px; height: 10px; padding-left: 10px;">
          que se obliga a pagar con recursos propios, o con el dinero que obtenga en préstamo de una
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px; margin-top: 0px;">
      <tr>
        <td style="text-align: left; width: 580px; height: 10px; padding-left: 10px;">
          Institución Financiera, contra la celebración del contrato de compraventa definitivo.
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px; margin-top: 10px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;  padding-left: 10px;">
          Detalle del pago de Reserva:
        </td>
        <td style="text-align: left; width: 50px; height: 10px;  padding-left: 10px;">
          Forma
        </td>
        <td style="text-align: center; width: 110px; border: 1; height: 10px;">
          <b><?php
            echo consultaFormaPagoReservaId($_SESSION['reservaFormaPagoReserva'])[0]['NOMBRE'];
          ?></b>
        </td>
        <td style="text-align: left; width: 33px; height: 10px;  padding-left: 10px;">
          Banco
        </td>
        <td style="text-align: center; width: 110px; border: 1; height: 10px;">
          <b><?php
            echo $_SESSION['reservaBancoReserva'];
          ?></b>
        </td>
        <td style="text-align: left; width: 56px; height: 10px;  padding-left: 10px;">
          Cheque Serie
        </td>
        <td style="text-align: center; width: 110px; border: 1; height: 10px;">
          <b><?php
            echo $_SESSION['reservaSerieChequeReserva'];
          ?></b>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px; margin-top: 0px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;  padding-left: 10px;">

        </td>
        <td style="text-align: left; width: 50px; height: 10px;  padding-left: 10px;">
          Cheque Nro
        </td>
        <td style="text-align: center; width: 110px; border: 1; height: 10px;">
          <b><?php
            echo $_SESSION['reservaNroChequeReserva'];
          ?></b>
        </td>
        <td style="text-align: left; width: 33px; height: 10px;  padding-left: 10px;">
          Monto $
        </td>
        <td style="text-align: center; width: 110px; border: 1; height: 10px;">
          <b><?php
            echo number_format($_SESSION['reservaValorPagoReserva'],0,'.','.');
          ?></b>
        </td>
        <td style="text-align: left; width: 55px; height: 10px;  padding-left: 10px;">
          A nombre de
        </td>
        <td style="text-align: left; width: 110px; height: 10px;">
          <b><?php
          if ($_SESSION['reservaCodigoProyecto'] == "COR") {
            echo "<td style='border:0; border-bottom:1px; margin-bottom:-10px;'></td>";
          }else{
            echo $_SESSION['reservaInmobiliaria'];
          }
          ?></b>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px; margin-top: 15px;">
      <tr>
        <td style="text-align: justify; width: 695px; height: 10px;">
          <b>CUARTO:</b> Por el presente instrumento, el oferente comprador se obliga a suscribir el correspondiente contrato de promesa de compraventa
          en un plazo  máximo  de 7  días corridos, contados desde esta fecha, en las oficinas del vendedor, ubicadas en <?php echo $_SESSION['reservaInmobiliariaDireccion']; ?>
          siempre que la presente oferta hubiere sido aceptada por el referido vendedor.
      		<br/><br/><br/>
          <b>QUINTO:</b> En el evento que el oferente comprador no concurriere a formalizar el negocio mediante su firma de la correspondiente promesa de
          compraventa en el plazo indicado anteriormente, esta Reserva quedará sin efecto, perderá  la  suma  indicada  en la  cláusula  precedente y autoriza a la
          Inmobiliaria para ofrecer libremente dicho inmueble a quien estime conveniente.
          Tratándose de cheques, como medio de pago de la reserva referida, el prominente comprador, autoriza desde ya a la inmobiliaria, para fechar y cobrar
          el documento respectivo.
        </td>
      </tr>
    </table>
    <table style="margin-top: 35px; margin-left: 90px;">
      <tr>
        <td style="width: 150px; text-align: center;">
          <hr style="height: 1px;"/>
          <b>Comprador</b>
          <br/>
          <?php
            echo ucwords($_SESSION['reservaNombreCliente']) . ' ' . ucwords($_SESSION['reservaApellidoCliente'])  . '<br/>' . $_SESSION['reservaRutCliente'];
          ?>
        </td>
        <td style="width: 290px;">

        </td>
        <td style="width: 150px; text-align: center;">
          <hr style="height: 1px;"/>
          <b>P/Vendedor</b>
          <br/>
          <?php
            $user = consultaUsuarioId($_SESSION['idUsuario']);
            echo $user[0]['NOMBRE'] . '<br/>' . $user[0]['RUT'];
          ?>
        </td>
      </tr>
    </table>
  </body>
</html>
