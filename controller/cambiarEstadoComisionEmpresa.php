<?php
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');
	session_start();
	if(count($_POST) > 0){
		$codigoProyecto = $_POST['codigoProyecto'];
		$numeroOperacion = $_POST['numeroOperacion'];
		$actor = $_POST['actor'];
		$estadoPagoPromesa = $_POST['estadoPagoPromesa'];
		$consultaFormaPago = consultaDatosPagoComisionCorretaje($codigoProyecto, $numeroOperacion, $actor)[0];
		$bancoPromesa = $consultaFormaPago['BANCOPROMESA'];
		$serieCheque = $consultaFormaPago['SERIECHEQUEPROMESA'];
		$nroCheque = $consultaFormaPago['NROCHEQUEPROMESA'];

		if ($estadoPagoPromesa == "PAGADO") {
			if ($bancoPromesa != '' && $serieCheque != '' && $nroCheque != '') {
				$cambioPromesa = actualizarComisionPromesaEmpresa($codigoProyecto, $numeroOperacion, $actor, $estadoPagoPromesa);
				if ($cambioPromesa == 'Ok') {
					echo 'Ok';
					ingresoMonitoreoLog($_SESSION['nombreUser'], $_SESSION['rutUser'], "Comisiones", "Actualizacion pago comision", "Actualizacion de pago comision promesa a " . $estadoPagoPromesa, $codigoProyecto, $numeroOperacion);
				}else{
					echo 'Sin datos';
				}
			}else{
				echo 'faltapago';
			}
		}
		else{
			$cambioPromesa = actualizarComisionPromesaEmpresa($codigoProyecto, $numeroOperacion, $actor, $estadoPagoPromesa);
			if ($cambioPromesa == 'Ok') {
				echo 'Ok';
				ingresoMonitoreoLog($_SESSION['nombreUser'], $_SESSION['rutUser'], "Comisiones", "Actualizacion pago comision", "Actualizacion de pago comision promesa a " . $estadoPagoPromesa, $codigoProyecto, $numeroOperacion);
			}else{
				echo 'Sin datos';
			}
		}
	}else{
   		echo 'Sin datos';
  	}
?>
