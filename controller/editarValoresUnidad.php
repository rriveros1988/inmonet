<?php
  header('Access-Control-Allow-Origin: *');
  require('../model/consultas.php');
  session_start();

	if(count($_POST) > 0){
    $rutUsuario = $_SESSION['rutUsuarioAsigProyecto'];
    $codigoProyecto = $_SESSION['codigoProyectoBack'];

    $row = '';

    $datos = $_POST['array'];
    $valor = $_POST['valor'];

    $con = conectar();

    $con->query("START TRANSACTION");

    for($i = 0; $i < count($datos); $i++){
      $row = editarValorUnidad($codigoProyecto, $datos[$i], $valor);
      if($row == 'Error'){
        $con->query("ROLLBACK");
        break;
      }
    }
    $con->query("COMMIT");

    if($row == "Ok")
    {
      echo "Ok";
    }
    else{
      echo "Sin datos";
    }
	}
	else{
		echo "Sin datos";
	}
?>
