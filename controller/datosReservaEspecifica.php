<?php
	ini_set('display_errors', 'On');
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');
	session_start();

	if(count($_POST) >= 0){
			$codigoProyecto = $_POST['codigoProyecto'];
			$numeroOperacion = $_POST['numeroOperacion'];
			$_SESSION['codigoProyecto'] = $codigoProyecto;
			$_SESSION['numeroOperacion'] = $numeroOperacion;
    	$row = datosReservaEspecifica($codigoProyecto, $numeroOperacion);

      if(is_array($row))
      {
				$_SESSION['idCliente1'] = $row['IDCLIENTE1'];
				$_SESSION['RCOMPRA_PDF_ACTUAL'] = $row['RCOMPRA_PDF'];
				$_SESSION['FICHA_PDF_ACTUAL'] = $row['FICHA_PDF'];
				$_SESSION['idReserva'] = $row['IDRESERVA'];
				$_SESSION['reservaBod'] = $row['BOD'];
				$_SESSION['reservaEst'] = $row['EST'];
				$results = array(
						"sEcho" => 1,
						"iTotalRecords" => count($row),
						"iTotalDisplayRecords" => count($row),
						"aaData"=>$row
				);

				echo json_encode($results);
      }
      else{
				$results = array(
            "sEcho" => 1,
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "aaData"=>[]
        );
        echo json_encode($results);
      }
	}
	else{
		echo "Sin datos";
	}
?>
